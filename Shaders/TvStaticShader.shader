﻿Shader "CustomShaders/TvStatic"
{

	Properties
	{

		_ColorA("Color A", Color) = (1,1,1,1)
		_ColorB("Color B", Color) = (0,0,0,0)

		_ResX("X Resolution", Float) = 100
		_ResY("Y Resolution", Float) = 100

		_ScaleWithZoom("Scale With Cam Distance", Range(0,1)) = 1.0

	}

	SubShader
		
	{
		//Tags { "RenderType"="Opaque" }

		Pass
		{
			CGPROGRAM

			#pragma fragment frag
			#pragma vertex vert
			//#pragma target 3.0
			#include "UnityCG.cginc"

			float rand(float2 co)
			{
				return frac((sin(dot(co.xy , float2(12.345 * _Time.w, 67.890 * _Time.w))) * 12345.67890 + _Time.w));
			}

			fixed4 _ColorA;
			fixed4 _ColorB;

			float _ResX;
			float _ResY;
			float _ScaleWithZoom;

			struct vertexInput
			{
				float4 vertex : POSITION;
				float4 texcoord0 : TEXCOORD0;
				float4 texcoord1 : TEXCOORD1; 
			};

			struct vertexOutput
			{
				float4 position : SV_POSITION;
				float4 texcoord0 : TEXCOORD0;
				float4 camDist : TEXCOORD1;
			};

			vertexOutput vert(vertexInput i)
			{
				vertexOutput o;
				UNITY_INITIALIZE_OUTPUT(vertexOutput, o);

				o.position = UnityObjectToClipPos(i.vertex);
				o.texcoord0 = i.texcoord0;

				float4 modelOrigin = mul(unity_ObjectToWorld, float4(0.0, 0.0, 0.0, 1.0));

				o.camDist.x = distance(_WorldSpaceCameraPos.xyz, modelOrigin.xyz);
				o.camDist.y = lerp(1.0, o.camDist.x, _ScaleWithZoom);

				return o;
			}


			fixed4 frag(float4 screenPos : SV_POSITION, vertexOutput o) : SV_target 
			{
				fixed4 sc = fixed4((screenPos.xy), 0.0, 1.0);
				sc *= 0.001;
				sc.xy -= 0.5;
				sc.xy *= o.camDist.xx;
				sc.xy += 0.5;

				float noise = rand(sc.xy);
				float4 stat = lerp(_ColorA, _ColorB, noise.x);

				return fixed4(stat.xyz, 1.0);
			}

			ENDCG
		}
	}
}
