This repository is used to show the source code of a school project, Resistance Squad. This project was made in Unity.

The project is a real time strategy (RTS) game where players control a small group of survivors and fight against an alien machine race.

DEMO download: https://mega.nz/#!4bBRTQZa!EmlzcIQ5cIP2fruNlDTBnYqIr4toEjKiANbLMyy5L80

Video: https://www.youtube.com/watch?v=nVArgN432Ew&t=178s