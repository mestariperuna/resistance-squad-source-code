﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsUI : MonoBehaviour
{
    private MainMenuUI _menu;

    private GameObject _optionsPanel;
    private GameObject _cameraPanel;
    private GameObject _generalPanel;
    private GameObject _commandsPanel;
    private GameObject _currentPanel;

    private Button _optionsButton;
    private Button _cameraButton;
    private Button _generalButton;
    private Button _commandsButton;
    private Button _backButton;

    private Slider[] volumeSliders;
    private Dropdown resolutionsDropdown;
    private Toggle fullScreenToggle;
    private Toggle cameraPanningToggle;
    private Toggle cameraRotateToggle;
    private Toggle enableTutorialsToggle;

    private Resolution[] allResolutions;
    private List<string> resolutions = new List<string>();
    private int activeScreenResIndex;

    private void Start()
    {
        _menu = GetComponentInParent<MainMenuUI>();
        _optionsPanel = transform.Find("OptionsControls").gameObject;
        _cameraPanel = transform.Find("CameraControls").gameObject;
        _generalPanel = transform.Find("GeneralControls").gameObject;
        _commandsPanel = transform.Find("CommandsControls").gameObject;

        _optionsButton = transform.Find("OptionsButton").GetComponent<Button>();
        _cameraButton = transform.Find("CameraButton").GetComponent<Button>();
        _generalButton = transform.Find("GeneralButton").GetComponent<Button>();
        _commandsButton = transform.Find("CommandsButton").GetComponent<Button>();
        _backButton = transform.Find("BackButton").GetComponent<Button>();

        _optionsButton.onClick.AddListener(() => ChangePanel(_optionsPanel));
        _cameraButton.onClick.AddListener(() => ChangePanel(_cameraPanel));
        _generalButton.onClick.AddListener(() => ChangePanel(_generalPanel));
        _commandsButton.onClick.AddListener(() => ChangePanel(_commandsPanel));
        _backButton.onClick.AddListener(() => _menu.GoBackButton());

        InitializeOptions();
        InitializeResolutionOptions();
        InitializeVolumeOptions();
        InitializeCameraOptions();
        InitializeTutorialOptions();
    }

    private void InitializeOptions()
    {
        InitializeSliders();

        resolutionsDropdown = _optionsPanel.transform.Find("Dropdown").GetComponent<Dropdown>();
        resolutionsDropdown.onValueChanged.AddListener((x) => SetScreenResolution(x));

        fullScreenToggle = _optionsPanel.transform.Find("FullScreen").GetComponent<Toggle>();
        fullScreenToggle.onValueChanged.AddListener((x) => SetFullScreen(x));

        enableTutorialsToggle = _optionsPanel.transform.Find("EnableTutorials").GetComponent<Toggle>();
        enableTutorialsToggle.onValueChanged.AddListener((x) => EnableTutorials(x));

        cameraPanningToggle = _cameraPanel.transform.Find("CameraZoom/CameraPanningToggle").GetComponent<Toggle>();
        cameraPanningToggle.onValueChanged.AddListener((x) => EnableCameraPanning(x));

        cameraRotateToggle = _cameraPanel.transform.Find("CameraRotate/CameraRotationToggle").GetComponent<Toggle>();
        cameraRotateToggle.onValueChanged.AddListener((x) => EnableCameraRotation(x));
    }

    private void InitializeSliders()
    {
        volumeSliders = _optionsPanel.GetComponentsInChildren<Slider>();

        volumeSliders[0].onValueChanged.AddListener((x) => SetMasterVolume(x));
        volumeSliders[1].onValueChanged.AddListener((x) => SetMusicVolume(x));
        volumeSliders[2].onValueChanged.AddListener((x) => SetSfxVolume(x));
    }

    private void OnEnable()
    {
        ChangePanel(_optionsPanel);
    }

    private void ChangePanel(GameObject newPanel)
    {
        if (_currentPanel != null)
            _currentPanel.SetActive(false);

        if (newPanel != null)
            newPanel.SetActive(true);

        _currentPanel = newPanel;
    }

    #region Resolutions

    private void InitializeResolutionOptions()
    {
        activeScreenResIndex = PlayerPrefs.GetInt("screenResIndex");
        bool isFullScreen = (PlayerPrefs.GetInt("fullscreen") == 1) ? true : false;

        resolutionsDropdown.ClearOptions();
        allResolutions = Screen.resolutions;
        for (int i = 0; i < allResolutions.Length; i++)
        {
            resolutions.Add(allResolutions[i].ToString());
        }
        resolutionsDropdown.AddOptions(resolutions);
        resolutionsDropdown.value = activeScreenResIndex;

        fullScreenToggle.isOn = isFullScreen;
    }

    // set screen resolution to the index of available resolutions from the dropdown menu in options and save it to playerprefs
    private void SetScreenResolution(int i)
    {
        activeScreenResIndex = i;
        Screen.SetResolution(allResolutions[i].width, allResolutions[i].height, false);
        PlayerPrefs.SetInt("screenResIndex", activeScreenResIndex);
        PlayerPrefs.Save();
    }

    // set fullscreen resolution and save it to playerprefs
    private void SetFullScreen(bool isFullScreen)
    {
        resolutionsDropdown.interactable = !isFullScreen;

        if (isFullScreen)
        {
            Resolution maxResolution = allResolutions[allResolutions.Length - 1];
            Screen.SetResolution(maxResolution.width, maxResolution.height, true);
        }
        else
        {
            SetScreenResolution(activeScreenResIndex);
        }
        PlayerPrefs.SetInt("fullscreen", ((isFullScreen) ? 1 : 0));
        PlayerPrefs.Save();
    }

    #endregion

    #region Volumes

    private void InitializeVolumeOptions()
    {
        if (AudioManager.Instance != null)
        {
            volumeSliders[0].value = AudioManager.Instance.masterVolumePercent;
            volumeSliders[1].value = AudioManager.Instance.musicVolumePercent;
            volumeSliders[2].value = AudioManager.Instance.sfxVolumePercent;
        }
        else
        {
            Debug.LogWarning("Missing AudioManager instance!");
        }
    }

    // set master volume in options
    private void SetMasterVolume(float value)
    {
        AudioManager.Instance.SetVolume(value, AudioManager.AudioChannel.Master);
    }

    // set music volume in options
    private void SetMusicVolume(float value)
    {
        AudioManager.Instance.SetVolume(value, AudioManager.AudioChannel.Music);
    }

    // set sound effects volume in options
    private void SetSfxVolume(float value)
    {
        AudioManager.Instance.SetVolume(value, AudioManager.AudioChannel.Sfx);
    }

    #endregion

    #region Camera

    private void InitializeCameraOptions()
    {
        bool enablePanning = (PlayerPrefs.GetInt("camerapanning", 1) == 1) ? true : false;
        bool enableRotating = (PlayerPrefs.GetInt("camerarotate", 1) == 1) ? true : false;
        cameraPanningToggle.isOn = enablePanning;
        cameraRotateToggle.isOn = enableRotating;
    }

    private void EnableCameraPanning(bool enable)
    {
        PlayerPrefs.SetInt("camerapanning", ((enable) ? 1 : 0));
        PlayerPrefs.Save();
    }

    private void EnableCameraRotation(bool enable)
    {
        PlayerPrefs.SetInt("camerarotate", ((enable) ? 1 : 0));
        PlayerPrefs.Save();
    }

    #endregion

    #region tutorials

    private void InitializeTutorialOptions()
    {
        bool enableTutorials = (PlayerPrefs.GetInt("enabletutorials", 1) == 1) ? true : false;
        enableTutorialsToggle.isOn = enableTutorials;
    }

    private void EnableTutorials(bool enable)
    {
        PlayerPrefs.SetInt("enabletutorials", ((enable) ? 1 : 0));
        PlayerPrefs.Save();
    }

    #endregion

}
