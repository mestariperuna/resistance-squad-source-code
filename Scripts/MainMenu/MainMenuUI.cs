﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuUI : MonoBehaviour
{
    private GameObject _mainPanel;
    private GameObject _optionsPanel;
    private GameObject _creditsPanel;
    private GameObject _currentPanel;
    private GameObject _loadingScreen;

    private Button _spButton;
    private Button _mpButton;
    private Button _optionsButton;
    private Button _creditsButton;
    private Button _quitButton;
    private Button _backButton;

    private Slider _loadingBar;
    private Text _loadingText;

    private void Start()
    {
        SetMainPanelElements();
        SetCreditsPanelElements();
        _optionsPanel = transform.Find("OptionsPanel").gameObject;

        SetUpLoadingScreen();

        _optionsPanel.SetActive(false);
        ChangePanel(_mainPanel);
    }

    private void SetMainPanelElements()
    {
        _mainPanel = transform.Find("MainPanel").gameObject;
        _spButton = _mainPanel.transform.Find("SingleplayerButton").GetComponent<Button>();
        _mpButton = _mainPanel.transform.Find("MultiplayerButton").GetComponent<Button>();
        _optionsButton = _mainPanel.transform.Find("OptionsButton").GetComponent<Button>();
        _creditsButton = _mainPanel.transform.Find("CreditsButton").GetComponent<Button>();
        _quitButton = _mainPanel.transform.Find("QuitButton").GetComponent<Button>();

        _spButton.onClick.AddListener(() => LoadByIndex(1));
        _mpButton.onClick.AddListener(() => LoadByIndex(2));
        _optionsButton.onClick.AddListener(() => ToggleOptionsPanel());
        _creditsButton.onClick.AddListener(() => ToggleCreditsPanel());
        _quitButton.onClick.AddListener(() => Quit());
    }

    private void SetCreditsPanelElements()
    {
        _creditsPanel = transform.Find("CreditsPanel").gameObject;
        _backButton = _creditsPanel.transform.Find("BackButton").GetComponent<Button>();
        _backButton.onClick.AddListener(() => GoBackButton());
    }

    private void SetUpLoadingScreen()
    {
        _loadingScreen = transform.Find("LoadingScreen").gameObject;    
        _loadingBar = _loadingScreen.transform.Find("LoadingBar").GetComponent<Slider>();
        _loadingText = _loadingBar.transform.Find("LoadingText").GetComponent<Text>();
    }

    public void ToggleOptionsPanel()
    {
        ChangePanel(_optionsPanel);
    }

    public void ToggleCreditsPanel()
    {
        ChangePanel(_creditsPanel);
    }

    public void GoBackButton()
    {
        ChangePanel(_mainPanel);
    }

    private void ChangePanel(GameObject newPanel)
    {
        if(_currentPanel != null)
            _currentPanel.SetActive(false);

        if(newPanel != null)
            newPanel.SetActive(true);

        _currentPanel = newPanel;
    }

    #region SceneLoading

    // reload scene
    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    // load a wanted scene
    public void LoadByIndex(int sceneIndex)
    {
        StartCoroutine(LoadAsynchronously(sceneIndex));
    }

    IEnumerator LoadAsynchronously(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        ChangePanel(_loadingScreen);

        while(!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            _loadingBar.value = progress;
            _loadingText.text = "LOADING...\t" + Mathf.RoundToInt(progress * 100) + "%";

            yield return null;
        }
    }

    // quit the game
    public void Quit()
    {
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #else
        Application.Quit();
        #endif
    }

    #endregion

}
