﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FogCoverable : MonoBehaviour
{
    new Renderer renderer;
    private CanvasRenderer[] healthBars;
    //bool triggered = false;
    
    void Start()
    {
       // triggered = false;
        renderer = GetComponentInChildren<Renderer>();
        healthBars = GetComponentsInChildren<CanvasRenderer>();
        FieldOfView.OnTargetsVisibilityChange += FieldOfViewOnTargetsVisibilityChange;
    }
    
    
    void OnDestroy()
    {
        FieldOfView.OnTargetsVisibilityChange -= FieldOfViewOnTargetsVisibilityChange;
    }
    
    void FieldOfViewOnTargetsVisibilityChange(List<Transform> newTargets)
    {
        renderer.enabled = newTargets.Contains(transform);
        foreach (CanvasRenderer i in healthBars)
        {
            if (i.GetAlpha() == 1.0f && renderer.enabled == false)
            {
                i.SetAlpha(0);
                //triggered = false;
            }
            else if (i.GetAlpha() == 0.0f && renderer.enabled == true)
            {
                i.SetAlpha(1);
                //triggered = true;
            }
        }
    }
}