﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
//public delegate void TargetsVisibilityChange(List<Transform> newTargets);
public class FogProjector : MonoBehaviour
{
    
    public RenderTexture fogTexture;
    RenderTexture projecTexture;

    RenderTexture oldTexture;
    public List<FogCoverable> fogCoverables = new List<FogCoverable>();
    public Shader blurShader;

    
    public List<Transform> visibleTargets = new List<Transform>();
    public List<Transform> notVisibleTargets = new List<Transform>();
    [Range(1, 4)]
    public int upsample = 2;

    Material blurMaterial;
    public float blur=1;

    Projector projector;

    public float blendSpeed = 1;
    float blend;
    int blendNameId;

    //public static event TargetsVisibilityChange OnTargetsVisibilityChange;

    
    void OnEnable()
    {
        

        projector = GetComponent<Projector>();
        blurMaterial = new Material(blurShader);
        blurMaterial.SetVector("_Parameter", new Vector4(blur, -blur, 0, 0));

        projecTexture = new RenderTexture(
                            fogTexture.width * upsample,
                            fogTexture.height * upsample,
                            0,
                            fogTexture.format) {filterMode = FilterMode.Bilinear};

        oldTexture = new RenderTexture(
                         fogTexture.width * upsample,
                         fogTexture.height * upsample,
                         0,
                         fogTexture.format) {filterMode = FilterMode.Bilinear};

        projector.material.SetTexture("_FogTex", projecTexture);
        projector.material.SetTexture("_OldFogTex", oldTexture);
        blendNameId = Shader.PropertyToID("_Blend");
        blend = 1;
        projector.material.SetFloat(blendNameId, blend);
        Graphics.Blit(fogTexture, projecTexture);
        UpdateFog();
        //hideTargets();

    }
    void Update()
    {
        
        //foreach (Transform i in visibleTargets)
        //{
        //    triggered = true;
        //}
        //hideTargets();
        //revealTargets();
        //if (OnTargetsVisibilityChange != null) OnTargetsVisibilityChange(visibleTargets);
    }
    public void hideTargets()
    {
        Renderer renderer;
        CanvasRenderer[] healthBars;
        foreach (Transform i in notVisibleTargets)
        {
            renderer = i.GetComponentInChildren<Renderer>();
            healthBars = i.GetComponentsInChildren<CanvasRenderer>();

            renderer.enabled = false;
            foreach (CanvasRenderer c in healthBars)
            {
                if (c.GetAlpha() == 0.0f && renderer.enabled == true)
                {
                    c.SetAlpha(1);
                }
                else if (c.GetAlpha() == 1.0f && renderer.enabled == false)
                {
                    c.SetAlpha(0);
                }
            }
        }

    }
    public void revealTargets()
    {
        Renderer renderer;
        CanvasRenderer[] healthBars;
        foreach (Transform i in visibleTargets)
        {
            renderer = i.GetComponentInChildren<Renderer>();
            healthBars = i.GetComponentsInChildren<CanvasRenderer>();

            renderer.enabled = true;
            foreach (CanvasRenderer c in healthBars)
            {
                if (c.GetAlpha() == 1.0f && renderer.enabled == false)
                {
                    c.SetAlpha(0);
                }
                else if (c.GetAlpha() == 0.0f && renderer.enabled == true)
                {
                    c.SetAlpha(1);
                }
            }
        }
       

    }
    /*public void FindVisibleTargets()
    {
        foreach (FieldOfView fov in fovs) {
            visibleTargets.Clear();
            Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, fov.viewRadius, fov.targetMask);

            for (int i = 0; i < targetsInViewRadius.Length; i++)
            {
                Transform target = targetsInViewRadius[i].transform;
                Vector3 dirToTarget = (target.position - transform.position).normalized;
                Canvas targetCanvas = targetsInViewRadius[i].GetComponentInChildren<Canvas>();

                if (Vector3.Angle(transform.forward, dirToTarget) < fov.viewAngle / 2)
                {
                    float dstToTarget = Vector3.Distance(transform.position, target.position);
                    if (!Physics.Raycast(fov.eyesTransform.position, dirToTarget, dstToTarget, fov.obstacleMask))
                    {
                        visibleTargets.Add(target);
                    }
                }
            }
            if (OnTargetsVisibilityChange != null) OnTargetsVisibilityChange(visibleTargets);
        }
    }*/
    
    public void UpdateFog()
    {
        //fovs = GameObject.FindObjectsOfType<FieldOfView>();
        Graphics.Blit(projecTexture, oldTexture);
        Graphics.Blit(fogTexture, projecTexture);

        RenderTexture temp = RenderTexture.GetTemporary(
            projecTexture.width,
            projecTexture.height,
            0,
            projecTexture.format);

        temp.filterMode = FilterMode.Bilinear;

        Graphics.Blit(projecTexture, temp, blurMaterial, 1);
        Graphics.Blit(temp, projecTexture, blurMaterial, 2);

        StartCoroutine(Blend());

        RenderTexture.ReleaseTemporary(temp);
    }

    
    IEnumerator Blend()
    {
        blend = 0;
        projector.material.SetFloat(blendNameId, blend);
        while (blend < 1)
        {
            blend = Mathf.MoveTowards(blend, 1, blendSpeed * Time.deltaTime);
            projector.material.SetFloat(blendNameId, blend);
            yield return null;
        }
    }
}