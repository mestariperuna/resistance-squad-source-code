﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkPlayerManager : NetworkBehaviour
{
    public event System.Action<NetworkIdentity> OnDestroyed;

    public float unitDistance = 1f;
    public GameObject networkCanvas;

    private NetworkInventoryUI _inventory;
    private NetworkTalentUI _talentTree;
    private NetworkSkillsUI _skills;
    private NetworkSelectionUI _availableUnits;
    private NetworkMouseSelection _selection;
    private GameObject _clickPos;

    private int selectionIndex = 0;
    private bool attackCommand = false;

    #region Initialization

    public override void OnStartAuthority()
    {
        InitializeCanvas();
        InitializeSelection();
        InitializeUI();
    }

    private void InitializeCanvas()
    {
        if (networkCanvas != null)
        {
            GameObject canvas = Instantiate(networkCanvas, transform) as GameObject;
            canvas.name = "NetworkUICanvas";
        }
    }

    private void InitializeUI()
    {
        _inventory = GetComponent<NetworkInventoryUI>();
        _talentTree = GetComponent<NetworkTalentUI>();
        _skills = GetComponent<NetworkSkillsUI>();
        _availableUnits = GetComponent<NetworkSelectionUI>();
        _availableUnits.OnSelectionSlotClicked += SelectOnClick;

        UpdateAvailableUnits();
    }

    private void InitializeSelection()
    {
        _selection = GetComponent<NetworkMouseSelection>();
        _selection.OnAvailableUnitsChanged += UpdateAvailableUnits;
        _selection.OnSelectedUnitsChanged += UpdateUI;

        _clickPos = Resources.Load<GameObject>("GFX/Arrows");
    }

    #endregion

    private void Update()
    {
        if (!hasAuthority)
            return;

        if (InputFunctions.GetKeyDown("Switch"))
            SelectNextOrPreviousFromAvailableUnits(true);

        if (InputFunctions.GetKeyUp("Inventory"))
            ActivateInventory();

        if (InputFunctions.GetKeyUp("Talents"))
            ActivateTalentTree();

        Command();
    }

    public void SelectNextOrPreviousFromAvailableUnits(bool next)
    {
        if (_selection.SelectFromAvailableUnits(next))
            selectionIndex = 0;
        else
            selectionIndex++;

        HandleSelectionIndex();
        UpdateUI();
    }

    private void HandleSelectionIndex()
    {
        if (selectionIndex >= _selection.SelectedUnits.Count)
            selectionIndex = 0;
    }

    #region UI

    public void UpdateUI()
    {
        NetworkSelectable selectable = null;
        if (_selection.SelectedUnits.Count > 0)
        {
            HandleSelectionIndex();
            selectable = _selection.SelectedUnits[selectionIndex];
        }
        _inventory.UpdateCharacterPanel(selectable);
        _skills.UpdateSkills(selectable);
        _talentTree.UpdateTalentTree(selectable);
        _availableUnits.HighlightSelection(selectable);
    }

    private void UpdateAvailableUnits()
    {
        _availableUnits.UpdateAvailableUnits(_selection.AvailableUnits);
    }

    public void ActivateInventory()
    {
        NetworkSelectable selectable = null;
        if (_selection.SelectedUnits.Count > 0)
        {
            HandleSelectionIndex();
            selectable = _selection.SelectedUnits[selectionIndex];
        }
        _inventory.ActivateInventory(selectable);
    }

    public void ActivateTalentTree()
    {
        NetworkSelectable selectable = null;
        if (_selection.SelectedUnits.Count > 0)
        {
            HandleSelectionIndex();
            selectable = _selection.SelectedUnits[selectionIndex];
        }     
        _talentTree.ActivateTalentTree(selectable);
    }

    #endregion

    #region Commands

    public void Command()
    {
        if (InputFunctions.GetKeyDown("Command"))
            GiveCommands(Input.mousePosition);

        if (InputFunctions.GetKey("Centralize"))
            MoveCameraToSelectedUnits();

        if (InputFunctions.GetKeyUp("Ability1"))
            UseSkill(0);

        if (InputFunctions.GetKeyUp("Ability2"))
            UseSkill(1);

        if (InputFunctions.GetKeyUp("Ability3"))
            UseSkill(2);

        if (InputFunctions.GetKeyUp("Ability4"))
            UseSkill(3);

        if (InputFunctions.GetKeyUp("Ability5"))
            UseSkill(4);

        if (InputFunctions.GetKeyUp("Attack"))
            AttackMousePosition();

        if (InputFunctions.GetKeyUp("Stop"))
            Stop();
    }

    private void GiveCommands(Vector2 mousePos)
    {
        Ray ray = Camera.main.ScreenPointToRay(mousePos);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100f))
        {
            NetworkSelectable selectable = hit.transform.GetComponent<NetworkSelectable>();
            NetworkInteractable interactable = hit.transform.GetComponent<NetworkInteractable>();

            _selection.DeselectOtherTeamsUnits();
            RemoveFocus();

            if (selectable != null)
                SetTarget(selectable);
            else if (interactable != null)
                SetFocus(interactable);
            else
                MoveToFormation(hit.point);

            Instantiate(_clickPos, hit.point, Quaternion.identity);
        }
    }

    private void SetTarget(NetworkSelectable newTarget)
    {
        List<NetworkSelectable> currentUnits = _selection.SelectedUnits;
        NetworkIdentity targetIdentity = newTarget.GetComponent<NetworkIdentity>();
        for (int i = 0; i < currentUnits.Count; i++)
        {
            if (!_selection.IsAlly(newTarget))
                currentUnits[i].CmdSetTarget(targetIdentity);
        }
    }

    private void SetFocus(NetworkInteractable newFocus)
    {
        NetworkIdentity focusIdentity = newFocus.GetComponent<NetworkIdentity>();
        if(_selection.SelectedUnits.Count > 0)
        {
            HandleSelectionIndex();
            _selection.SelectedUnits[selectionIndex].CmdSetFocus(focusIdentity);
        }
    }

    private void RemoveFocus()
    {
        List<NetworkSelectable> currentUnits = _selection.SelectedUnits;
        for (int i = 0; i < currentUnits.Count; i++)
            currentUnits[i].CmdRemoveFocus();
    }

    public void MoveToFormation(Vector3 clickPoint)
    {
        List<NetworkSelectable> currentUnits = _selection.SelectedUnits;
        if (currentUnits.Count > 0)
        {
            if (currentUnits.Count == 1)
                currentUnits[0].CmdMoveUnit(clickPoint);
            else
            {
                HandleSelectionIndex();
                Vector3 firstPos = currentUnits[selectionIndex].transform.position;
                Vector3 direction = new Vector3(clickPoint.x - firstPos.x, 0, clickPoint.z - firstPos.z).normalized;
                List<Vector3> formationPoints = Formations.CalculateFormation(currentUnits.Count, unitDistance, direction);

                for (int i = 0; i < formationPoints.Count; i++)
                {
                    Vector3 destination = clickPoint + formationPoints[i];
                    currentUnits[i].CmdMoveUnit(destination);
                }
            }
        }
    }

    public void MoveCameraToSelectedUnits()
    {
        List<NetworkSelectable> currentUnits = _selection.SelectedUnits;
        if (currentUnits.Count > 0)
        {
            Vector3 minPoint = currentUnits[0].transform.position;
            Vector3 maxPoint = currentUnits[0].transform.position;

            for (int i = 0; i < currentUnits.Count; i++)
            {
                Vector3 pos = currentUnits[i].transform.position;
                if (pos.x < minPoint.x)
                    minPoint.x = pos.x;
                if (pos.x > maxPoint.x)
                    maxPoint.x = pos.x;
                if (pos.y < minPoint.y)
                    minPoint.y = pos.y;
                if (pos.y > maxPoint.y)
                    maxPoint.y = pos.y;
                if (pos.z < minPoint.z)
                    minPoint.z = pos.z;
                if (pos.z > maxPoint.z)
                    maxPoint.z = pos.z;
            }
            _selection.transform.position = minPoint + 0.5f * (maxPoint - minPoint);
        }
    }
    
    public void UseSkill(int index)
    {
        if (_selection.SelectedUnits.Count > 0)
        {
            HandleSelectionIndex();
            NetworkHumanoid currentUnit = _selection.SelectedUnits[selectionIndex].GetComponent<NetworkHumanoid>();
            if (currentUnit != null)
                CmdCheckIfOnCooldown(currentUnit.GetComponent<NetworkIdentity>(), index);
        }
    }

    [Command]
    private void CmdCheckIfOnCooldown(NetworkIdentity currentIdentity, int index)
    {
        NetworkHumanoid currentHumanoid = NetworkServer.FindLocalObject(currentIdentity.netId).GetComponent<NetworkHumanoid>();
        if (currentHumanoid != null)
        {
            GameObject[] skillsObjs = currentHumanoid.SkillManager.ActiveSkills;
            if (skillsObjs.Length > index)
            {
                if(!skillsObjs[index].GetComponent<Skill>().onCooldown)
                    TargetUseSkill(currentIdentity.clientAuthorityOwner, index);
            }
        }
    }

    [TargetRpc]
    private void TargetUseSkill(NetworkConnection target, int index)
    {
        if(_selection.SelectedUnits.Count > 0)
        {
            NetworkHumanoid currentUnit = _selection.SelectedUnits[selectionIndex].GetComponent<NetworkHumanoid>();
            if (currentUnit != null)
            {
                currentUnit.SkillManager.CmdUseSkillAtIndex(index);
                _selection.SetCommand(true);
                StartCoroutine(WaitForClick());
            }
        }
    }
    
    public void AttackMousePosition()
    {
        _selection.SetCommand(true);
        StartCoroutine(WaitForClick());
    }

    private IEnumerator WaitForClick()
    {
        yield return new WaitForEndOfFrame();

        while (!Input.anyKeyDown)
        {
            yield return null;
        }

        if (InputFunctions.GetKeyDown("Select") && attackCommand)
            GiveCommands(Input.mousePosition);

        attackCommand = false;
        _selection.SetCommand(false);

    }

    public void Stop()
    {
        List<NetworkSelectable> currentUnits = _selection.SelectedUnits;
        for (int i = 0; i < currentUnits.Count; i++)
        {
            currentUnits[i].CmdStopUnit();
        }
    }

    private void SelectOnClick(int index)
    {
        _selection.DeselectUnits();
        HandleSelectionIndex();
        _selection.SelectUnitFromNetworkIdentity(_selection.AvailableUnits[index]);
    }

    #endregion

    private void OnDestroy()
    {
        if (isServer)
        {
            if (OnDestroyed != null)
                OnDestroyed(GetComponent<NetworkIdentity>());
        }
    }

}
