﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkEquipmentManager : NetworkBehaviour
{
    public event System.Action<Equipment, Equipment> OnEquipmentChanged;

    public bool female;
    public string defaultOutfitNum;
    public Weapon defaultWeapon;
    public SkinnedMeshRenderer targetMesh;
    public Transform weaponHold;
    public Equipment[] CurrentEquipment { get; private set; }
    private SkinnedMeshRenderer[] _currentMeshes;
    private NetworkInventoryManager _inventory;
    private NetworkSkillManager _skillManager;

    public override void OnStartServer()
    {
        int numSlots = System.Enum.GetNames(typeof(GearSlot)).Length;
        CurrentEquipment = new Equipment[numSlots];       
        _inventory = GetComponent<NetworkInventoryManager>();
        _skillManager = GetComponent<NetworkSkillManager>();

        if (defaultWeapon != null)
            ServerEquipItem(defaultWeapon);
    }

    public override void OnStartClient()
    {
        int numSlots = System.Enum.GetNames(typeof(GearSlot)).Length;
        _currentMeshes = new SkinnedMeshRenderer[numSlots];
        EquipDefaultItems();
    }

    [Server]
    public void ServerEquipItem(Equipment newItem)
    {
        int slotIndex = (int)newItem.equipSlot;
        _inventory.ServerRemoveItem(newItem);

        Equipment oldItem = null;

        // unequip item if there is an item in a slot
        if(CurrentEquipment[slotIndex] != null)
        {
            oldItem = CurrentEquipment[slotIndex];
            // assume that inventory cannot be full when equipping and unequipping items
            _inventory.ServerAddItem(oldItem);
        }

        // check if the item is twohanded weapon, if it is, try equipping it
        if (ItemIsTwohandedWeapon(newItem))
        {
            // revert item swap if we failed to equip
            _inventory.ServerRemoveItem(oldItem);
            _inventory.ServerAddItem(newItem);
        }
        else
        {
            if (OnEquipmentChanged != null)
                OnEquipmentChanged(newItem, oldItem);

            if(female)
                RpcSetNewMesh(slotIndex, "FemaleMeshes/" + newItem.meshName, newItem is Weapon);
            else
                RpcSetNewMesh(slotIndex, "MaleMeshes/" + newItem.meshName, newItem is Weapon);

            CurrentEquipment[slotIndex] = newItem;
        }
    }

    [ClientRpc]
    private void RpcSetNewMesh(int slotIndex, string meshPath, bool isWeapon)
    {
        if (_currentMeshes[slotIndex] != null)
            Destroy(_currentMeshes[slotIndex].gameObject);

        SkinnedMeshRenderer newMesh = Instantiate(Resources.Load<SkinnedMeshRenderer>("GameItems/" + meshPath));

        if (isWeapon)
        {
            newMesh.transform.parent = weaponHold.parent;
            newMesh.transform.position = weaponHold.position;
            newMesh.transform.rotation = weaponHold.rotation;
        }
        else
        {
            newMesh.transform.parent = targetMesh.transform;
            newMesh.bones = targetMesh.bones;
            newMesh.rootBone = targetMesh.rootBone;
        }
        _currentMeshes[slotIndex] = newMesh;
    }

    // this method is called only in EquipItem
    private bool ItemIsTwohandedWeapon(Equipment item)
    {
        if(item is Weapon)
        {
            Weapon weapon = item as Weapon;
            if (!_skillManager.HasJuggernautSkill())
            {
                if (weapon.isTwoHander)
                    return TryEquippingTwoHandedWeapon();
            }
        }
        return false;
    }

    // this method is called only if the item is twohanded weapon
    private bool TryEquippingTwoHandedWeapon()
    {
        int offhandIndex = (int)GearSlot.OffHand;
        Equipment offhandItem = null;

        // when swapping a twohanded weapon remove offhand item aswell
        if (CurrentEquipment[offhandIndex] != null)
        {
            offhandItem = CurrentEquipment[offhandIndex];
            // if inventory is full then revert item swap
            if(_inventory.ServerAddItem(offhandItem))
            {
                CurrentEquipment[offhandIndex] = null;
                if (OnEquipmentChanged != null)
                    OnEquipmentChanged(null, offhandItem);
            }
            else
                return true;
        }
        return false;
    }

    [Server]
    public void ServerUnequipItem(int slotIndex, int? inventoryIndex)
    {
        if(CurrentEquipment[slotIndex] != null)
        {
            Equipment oldItem = CurrentEquipment[slotIndex];
            bool successful = false;

            if (inventoryIndex != null)
                successful = _inventory.ServerAddItemByIndex(oldItem, (int)inventoryIndex);

            if(!successful)
                successful = _inventory.ServerAddItem(oldItem);

            if(successful)
            {
                
                RpcRemoveEquipmentMesh(slotIndex);

                CurrentEquipment[slotIndex] = null;

                if (OnEquipmentChanged != null)
                    OnEquipmentChanged(null, oldItem);
            }
        }
    }

    [Server]
    public bool ServerDropEquipment(int slotIndex)
    {
        if(CurrentEquipment[slotIndex] != null)
        {
            Equipment oldItem = CurrentEquipment[slotIndex];
            CurrentEquipment[slotIndex] = null;
            RpcRemoveEquipmentMesh(slotIndex);
            if (OnEquipmentChanged != null)
                OnEquipmentChanged(null, oldItem);

            return true;
        }
        return false;
    }

    [ClientRpc]
    private void RpcRemoveEquipmentMesh(int slotIndex)
    {
        if (_currentMeshes[slotIndex] != null)
            Destroy(_currentMeshes[slotIndex].gameObject);
    }

    [Client]
    private void EquipDefaultItems()
    {
        SkinnedMeshRenderer[] defaultOutfitMeshes = Resources.LoadAll<SkinnedMeshRenderer>("GameItems/DefaultOutfit" + defaultOutfitNum);

        for (int i = 0; i < defaultOutfitMeshes.Length; ++i)
        {
            EquipDefaultItem(defaultOutfitMeshes[i]);
        }
    }

    [Client]
    private void EquipDefaultItem(SkinnedMeshRenderer mesh)
    {
        SkinnedMeshRenderer newMesh = Instantiate(mesh);

        newMesh.transform.parent = targetMesh.transform;
        newMesh.bones = targetMesh.bones;
        newMesh.rootBone = targetMesh.rootBone;
    }
    
}
