﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;

public class NetworkMouseSelection : NetworkBehaviour
{

    #region variables

    public event System.Action OnSelectedUnitsChanged;
    public event System.Action OnAvailableUnitsChanged;

    public GameObject camObj;
    private Camera _cam;

    [SyncVar]
    public Team currentPlayer;
    public Team[] allies;
    public int maxUnits = 4;
    public float unitDistance = 1f;
    public GUIStyle mouseDragSkin;

    private float rayCastLength = 100f;
    private bool usingCommand = false;

    public List<NetworkSelectable> SelectedUnits { get; private set; }
    public List<NetworkIdentity> AvailableUnits { get; private set; }

    // drag start and end points and the box between the points
    private Vector2? dragStart;
    private Vector2 dragEnd;
    private Rect dragRect
    {
        get
        {
            return Rect.MinMaxRect(
                Mathf.Min(dragStart.Value.x, dragEnd.x),
                Mathf.Min(dragStart.Value.y, dragEnd.y),
                Mathf.Max(dragStart.Value.x, dragEnd.x),
                Mathf.Max(dragStart.Value.y, dragEnd.y)
            );
        }
    }
    // to determine if user is dragging
    private bool isDragging
    {
        get { return dragStart != null && dragRect.width >= 4 && dragRect.height >= 4; }
    }

    #endregion

    public override void OnStartAuthority()
    {
        // add new camera
        GameObject go = Instantiate(camObj, transform) as GameObject;
        _cam = go.GetComponent<Camera>();
        // add camera movement script
        if (!GetComponent<CameraController>())
            gameObject.AddComponent<CameraController>();

        InitializeLists();
    }

    private void InitializeLists()
    {
        SelectedUnits = new List<NetworkSelectable>();
        AvailableUnits = new List<NetworkIdentity>(maxUnits);
    }

    void Update()
    {
        if (!hasAuthority)
            return;

        if (dragStart == null && InputFunctions.GetKeyDown("Select") && !usingCommand)
            dragStart = Input.mousePosition;

        if (dragStart != null)
        {
            if (InputFunctions.GetKey("Select"))
                dragEnd = Input.mousePosition;

            UpdateSelection(Input.mousePosition);

            if (InputFunctions.GetKeyUp("Select"))
                EndSelection(Input.mousePosition);
        }
    }

    #region GUI

    void OnGUI()
    {
        if (hasAuthority)
        {
            if (isDragging)
                if (!EventSystem.current.IsPointerOverGameObject())
                    GUI.Box(Transform(dragRect), "", mouseDragSkin);
        }
    }

    private Rect Transform(Rect rect)
    {
        return new Rect(rect.x, Screen.height - rect.yMax - 1, rect.width, rect.height);
    }

    #endregion

    #region SelectionFunctions

    public void AddGroupKeyDown()
    {
        if (!InputFunctions.GetKey("Group"))
            DeselectUnits();
    }

    public void UpdateSelection(Vector2 position)
    {
        if (dragStart != null)
            dragEnd = position;

        if (isDragging)
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                AddGroupKeyDown();
                SelectInRange(dragRect);
            }
        }
    }

    public void EndSelection(Vector2 position)
    {
        if (!isDragging)
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                AddGroupKeyDown();
                SelectAtRay(Input.mousePosition);
            }
        }
        dragStart = null;
    }

    public void SelectInRange(Rect rect)
    {
        foreach (NetworkSelectable unit in FindObjectsOfType(typeof(NetworkSelectable)))
        {
            if (unit.currentTeam == currentPlayer)
            {
                Vector2 screenPos = _cam.WorldToScreenPoint(unit.transform.position);
                if (rect.Contains(screenPos))
                    AddSelected(unit);
            }
        }
    }

    public void SelectAtRay(Vector2 mousePos)
    {
        NetworkSelectable unit = GetAtRay(mousePos);
        if (unit != null)
            AddSelected(unit);
    }

    public NetworkSelectable GetAtRay(Vector2 mousePos)
    {
        Ray ray = _cam.ScreenPointToRay(mousePos);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, rayCastLength))
            return hit.collider.GetComponent<NetworkSelectable>();
        return null;
    }

    public void AddSelected(NetworkSelectable unit)
    {
        if (!SelectedUnits.Contains(unit))
            SelectUnit(unit);
        else if (InputFunctions.GetKey("AddGroup") && !isDragging)
            DeselectUnit(unit);
    }

    public void SelectUnit(NetworkSelectable unit)
    {
        unit.transform.Find("Selected").gameObject.SetActive(true);
        SelectedUnits.Add(unit);

        if (SelectedUnits.Count == 1)
        {
            if (OnSelectedUnitsChanged != null)
                OnSelectedUnitsChanged();
        }
    }

    public void SelectUnitFromNetworkIdentity(NetworkIdentity id)
    {
        DeselectUnits();
        NetworkSelectable selectable = ClientScene.FindLocalObject(id.netId).GetComponent<NetworkSelectable>();
        SelectUnit(selectable);
    }

    public bool SelectFromAvailableUnits(bool next)
    {
        if (SelectedUnits.Count <= 1 && AvailableUnits.Count > 1)
        {
            int index = 0;

            if (SelectedUnits.Count == 0)
                index = SelectNextOrPrevious(-1, next);
            else
            {
                for (int i = 0; i < AvailableUnits.Count; i++)
                {

                    if (SelectedUnits[index] == AvailableUnits[i])
                    {
                        index = SelectNextOrPrevious(i, next);
                        break;
                    }
                }
            }

            SelectUnitFromNetworkIdentity(AvailableUnits[index]);
            return true;
        }
        return false;
    }

    private int SelectNextOrPrevious(int index, bool next)
    {
        if (next)
        {
            index++;
            if (index >= AvailableUnits.Count)
                index = 0;
        }
        else
        {
            index--;
            if (index < 0)
                index = AvailableUnits.Count - 1;
        }
        return index;
    }

    public void DeselectUnit(NetworkSelectable unit)
    {
        if(unit != null)
        {
            unit.transform.Find("Selected").gameObject.SetActive(false);
            SelectedUnits.Remove(unit);
        }

        if (SelectedUnits.Count == 0)
        {
            if (OnSelectedUnitsChanged != null)
                OnSelectedUnitsChanged();
        }
    }

    public void DeselectUnits()
    {
        if (SelectedUnits.Count > 0)
        {
            for (int i = SelectedUnits.Count - 1; i >= 0; i--)
                DeselectUnit(SelectedUnits[i]);

            SelectedUnits.Clear();
        }
    }

    public void DeselectOtherTeamsUnits()
    {
        for (int i = SelectedUnits.Count - 1; i >= 0; i--)
        {
            if (SelectedUnits[i].currentTeam != currentPlayer)
                DeselectUnit(SelectedUnits[i]);
        }
    }


    #endregion

    [TargetRpc]
    public void TargetOnUnitFound(NetworkConnection target, NetworkIdentity unit)
    {
            if (AvailableUnits.Count < maxUnits)
            {
                if (!AvailableUnits.Contains(unit))
                {
                    AvailableUnits.Add(unit);

                    if (OnAvailableUnitsChanged != null)
                        OnAvailableUnitsChanged();
                }
            }
    }

    [TargetRpc]
    public void TargetOnUnitDeath(NetworkConnection target, NetworkIdentity id)
    {
        if (!hasAuthority)
            return;

        RemoveDisabledUnits(id);

        if (OnAvailableUnitsChanged != null)
            OnAvailableUnitsChanged();
    }

    private void RemoveDisabledUnits(NetworkIdentity id)
    {
        for (int i = AvailableUnits.Count - 1; i >= 0; i--)
        {
            GameObject go = ClientScene.FindLocalObject(AvailableUnits[i].netId);
            if (go == null || AvailableUnits[i] == id)
                AvailableUnits.RemoveAt(i);
        }

        for (int i = SelectedUnits.Count - 1; i >= 0; i--)
        {
            if (SelectedUnits[i].gameObject == null || SelectedUnits[i] == null)
                SelectedUnits.RemoveAt(i);                
        }
    }

    public bool IsAlly(NetworkSelectable target)
    {
        for (int i = 0; i < allies.Length; i++)
        {
            if (target.currentTeam == allies[i])
                return true;
        }
        return false;
    }

    public void SetCommand(bool isUsingCommand)
    {
        usingCommand = isUsingCommand;
    }

}
