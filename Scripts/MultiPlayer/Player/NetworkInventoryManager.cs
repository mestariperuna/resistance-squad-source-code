﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkInventoryManager : NetworkBehaviour
{

    public int backBagSize = 10;
    public Item[] Inventory { get; private set; }

    public override void OnStartServer()
    {
        Inventory = new Item[backBagSize];
    }

    [Server]
    public bool ServerAddItem(Item item)
    {
        for (int i = 0; i < Inventory.Length; i++)
        {
            if (Inventory[i] == null)
            {
                Inventory[i] = item;
                return true;
            }
        }
        TargetInventoryFullMsg(connectionToClient);
        return false;
    }

    [TargetRpc]
    public void TargetInventoryFullMsg(NetworkConnection target)
    {
        Debug.Log("Inventory full");
    }

    [Server]
    public bool ServerAddItemByIndex(Item item, int index)
    {
        if(Inventory[index] == null)
        {
            Inventory[index] = item;
            return true;
        }
        return false;
    }

    [Server]
    public bool ServerRemoveItemFromIndex(int index)
    {
        if (Inventory[index] != null)
        {
            Inventory[index] = null;
            return true;
        }
        return false;
    }

    [Server]
    public bool ServerRemoveItem(Item item)
    {
        for (int i = 0; i < Inventory.Length; i++)
        {
            if (Inventory[i] == item)
            {
                Inventory[i] = null;
                return true;
            }
        }
        return false;
    }

    [Server]
    public void ServerSwapItems(int toIndex, int fromIndex)
    {
        Item tempItem = Inventory[toIndex];
        Inventory[toIndex] = Inventory[fromIndex];
        Inventory[fromIndex] = tempItem;

    }

}
