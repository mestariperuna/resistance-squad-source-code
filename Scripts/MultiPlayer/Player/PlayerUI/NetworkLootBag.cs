﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class NetworkLootBag : NetworkBehaviour
{
    public int inventorySize = 16;
    public GameObject uiLootSlot;
    public GameObject uiItem;
    public NetworkLoot CurrentLoot { get; set; }

    private List<GameObject> _lootSlots = new List<GameObject>();
    private Transform _slotPanel;

    #region Initialization

    public override void OnStartAuthority()
    {
        InitializeLootSlots();
    }

    private void InitializeLootSlots()
    {
        _slotPanel = transform.Find("NetworkUICanvas/LootBag").GetChild(0);

        // initialize loot image slots
        for (int i = 0; i < inventorySize; i++)
        {
            GameObject slotItem = Instantiate(uiLootSlot, _slotPanel.transform.position, Quaternion.identity, _slotPanel.transform);
            slotItem.transform.localScale = Vector3.one;

            NetworkLootSlot itemSlot = slotItem.GetComponent<NetworkLootSlot>();
            itemSlot.SlotId = i;
            itemSlot.OnItemSwap += SwapItems;

            _lootSlots.Add(Instantiate(uiItem, slotItem.transform.position, slotItem.transform.rotation, slotItem.transform));
            _lootSlots[i].transform.localScale = Vector3.one;
            _lootSlots[i].GetComponent<NetworkItemData>().Initialize();
        }
    }

    #endregion

    [TargetRpc]
    public void TargetUpdateLootBag(NetworkConnection target)
    {
        UpdateLootBag();
    }

    [Client]
    public void UpdateLootBag()
    {
        ClearLootBag();
        CmdRequestLootData();
    }

    [Command]
    private void CmdRequestLootData()
    {
        if (CurrentLoot != null)
        {
            Item[] loot = CurrentLoot.Inventory;
            for (int i = 0; i < loot.Length; i++)
            {
                if (loot[i] != null)
                {
                    ItemDataToSend data = NetworkGenerateLoot.Instance.ServerRequestItemData(loot[i]);
                    NetworkConnection conn = GetComponent<NetworkIdentity>().clientAuthorityOwner;
                    TargetUpdateLootSlot(conn, data, i);
                }
            }
        }
    }

    [TargetRpc]
    private void TargetUpdateLootSlot(NetworkConnection target, ItemDataToSend data, int index)
    {
        _lootSlots[index].GetComponent<NetworkItemData>().UpdateItemData(data);
    }

    [Client]
    private void ClearLootBag()
    {
        for (int i = 0; i < _lootSlots.Count; i++)
        {
            _lootSlots[i].GetComponent<NetworkItemData>().ClearItemData();
        }
    }

    [Server]
    public Item RemoveFromIndex(int index)
    {
        Item item = null;
        if (CurrentLoot.Inventory.Length > index)
        {
            if (CurrentLoot.Inventory[index] != null)
            {
                item = CurrentLoot.Inventory[index];
                CurrentLoot.Inventory[index] = null;
                UpdateLootBag();
            }
        }
        return item;
    }

    [Client]
    private void SwapItems(NetworkLootSlot toSlot, NetworkLootSlot fromSlot)
    {
        if(toSlot != null && fromSlot != null)
        {
            CmdSwapItems(toSlot.SlotId, fromSlot.SlotId);
        }
    }

    [Command]
    private void CmdSwapItems(int toSlot, int fromSlot)
    {
        Item tempItem = CurrentLoot.Inventory[toSlot];
        CurrentLoot.Inventory[toSlot] = CurrentLoot.Inventory[fromSlot];
        CurrentLoot.Inventory[fromSlot] = tempItem;
    }

}
