﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class NetworkSkillsUI : NetworkBehaviour
{
    private GameObject _skillPanel;
    private List<GameObject> _skillSlots = new List<GameObject>();
    private NetworkHumanoid currentHumanoid;

    #region Initialization

    public override void OnStartAuthority()
    {
        _skillPanel = transform.Find("NetworkUICanvas/UI_MidPanel/SkillPanel").gameObject;
        InitializeSkillSlots();
        UpdateSkills(null);
    }

    private void InitializeSkillSlots()
    {
        for (int i = 0; i < _skillPanel.transform.childCount; i++)
        {
            _skillSlots.Add(_skillPanel.transform.GetChild(i).gameObject);
            NetworkSkillSlot skillSlot = _skillSlots[i].GetComponent<NetworkSkillSlot>();
            skillSlot.OnSkillClicked += OnSkillClicked;
            skillSlot.SlotIndex = i;
        }
    }

    #endregion

    [Client]
    public void UpdateSkills(NetworkSelectable selectable)
    {
        UpdateCurrentHumanoid(selectable);
        ClearSkillSlots();
        UpdateSkillSlots();
    }

    [Client]
    private void UpdateCurrentHumanoid(NetworkSelectable selectable)
    {
        if (selectable is NetworkHumanoid)
            currentHumanoid = selectable as NetworkHumanoid;
        else
            currentHumanoid = null;
    }

    [Client]
    private void ClearSkillSlots()
    {
        for (int i = 0; i < _skillSlots.Count; i++)
            _skillSlots[i].transform.GetChild(0).GetComponent<Image>().enabled = false;
    }

    [Client]
    private void UpdateSkillSlots()
    {
        if (currentHumanoid != null)
        {
            NetworkIdentity humanoidId = currentHumanoid.GetComponent<NetworkIdentity>();
            CmdUpdateSkillSlots(humanoidId);
        }
    }

    [Command]
    private void CmdUpdateSkillSlots(NetworkIdentity humanoidId)
    {
        NetworkHumanoid humanoid = NetworkServer.FindLocalObject(humanoidId.netId).GetComponent<NetworkHumanoid>();
        if(humanoid != null)
        {
            GameObject[] skills = humanoid.SkillManager.ActiveSkills;
            for (int i = 0; i < skills.Length; i++)
            {
                if (skills[i] != null)
                {
                    SkillDataToSend data = ServerRequestSkillData(skills[i].GetComponent<Skill>());
                    TargetReceiveSkillData(humanoidId.clientAuthorityOwner, data, i);
                }
            }
        }
    }

    [Server]
    private SkillDataToSend ServerRequestSkillData(Skill skill)
    {
        SkillDataToSend data = new SkillDataToSend()
        {
            name = skill.name,
            iconName = skill.iconName,
            skillType = skill.effect.ToString(),
            description = skill.description,
            skillRange = skill.skillRange,
            cooldown = skill.cooldown
        };

        return data;
    }

    [TargetRpc]
    private void TargetReceiveSkillData(NetworkConnection target, SkillDataToSend data, int index)
    {
        Image image = _skillSlots[index].transform.GetChild(0).GetComponent<Image>();
        image.enabled = true;
        image.sprite = Resources.Load<Sprite>("Sprites/SkillSprites/" + data.iconName);
    }

    [Client]
    private void OnSkillClicked(int index)
    {
        if (currentHumanoid != null)
        {
            currentHumanoid.SkillManager.CmdUseSkillAtIndex(index);
        }
    }

}

public struct SkillDataToSend
{
    public string name;
    public string iconName;
    public string skillType;
    public string description;
    public float skillRange;
    public float cooldown;
}
