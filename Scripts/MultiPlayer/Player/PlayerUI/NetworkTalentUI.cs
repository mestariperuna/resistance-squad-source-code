﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class NetworkTalentUI : NetworkBehaviour
{
    private GameObject _characterTalentPanel;
    private Text _talentPointsText;
    private NetworkHumanoid currentHumanoid;
    private NetworkSkillsUI _skills;

    private List<GameObject> _marauderSlots = new List<GameObject>();
    private List<GameObject> _jaegerSlots = new List<GameObject>();
    private List<GameObject> _combatMedicSlots = new List<GameObject>();

    #region Initialization

    public override void OnStartAuthority()
    {
        _characterTalentPanel = transform.Find("NetworkUICanvas/CharacterTalentPanel").gameObject;
        _talentPointsText = _characterTalentPanel.transform.Find("TalentPointsPanel").GetComponentInChildren<Text>();
        _skills = GetComponent<NetworkSkillsUI>();

        InitializeTalentSlots();
        _characterTalentPanel.SetActive(false);

        UpdateTalentTree(null);
    }

    private void InitializeTalentSlots()
    {
        NetworkTalentSlot[] marauderSlots = _characterTalentPanel.transform.GetChild(1).GetComponentsInChildren<NetworkTalentSlot>();
        NetworkTalentSlot[] jaegerSlots = _characterTalentPanel.transform.GetChild(2).GetComponentsInChildren<NetworkTalentSlot>();
        NetworkTalentSlot[] combatMedicSlots = _characterTalentPanel.transform.GetChild(3).GetComponentsInChildren<NetworkTalentSlot>();

        AddTalentsToListFromArray(_marauderSlots, marauderSlots);
        AddTalentsToListFromArray(_jaegerSlots, jaegerSlots);
        AddTalentsToListFromArray(_combatMedicSlots, combatMedicSlots);
    }

    private void AddTalentsToListFromArray(List<GameObject> list, NetworkTalentSlot[] array)
    {
        for (int i = 0; i < array.Length; i++)
        {
            list.Add(array[i].gameObject);
            array[i].OnTalentClicked += OnTalentClicked;
        }
    }

    #endregion

    [Client]
    public void ActivateTalentTree(NetworkSelectable selectable)
    {
        _characterTalentPanel.SetActive(!_characterTalentPanel.activeSelf);
        UpdateTalentTree(selectable);
    }

    [Client]
    public void UpdateTalentTree(NetworkSelectable selectable)
    {
        UpdateCurrentHumanoid(selectable);
        ClearTalentTrees();
        UpdateTalentTrees();
    }

    [Client]
    private void UpdateCurrentHumanoid(NetworkSelectable selectable)
    {
        if (selectable is NetworkHumanoid)
            currentHumanoid = selectable as NetworkHumanoid;
        else
            currentHumanoid = null;
    }

    [Client]
    private void ClearTalentTrees()
    {
        ClearTree(_marauderSlots);
        ClearTree(_jaegerSlots);
        ClearTree(_combatMedicSlots);
    }

    [Client]
    private void ClearTree(List<GameObject> list)
    {
        for (int i = 0; i < list.Count; i++)
            list[i].GetComponent<NetworkTalentSlot>().ClearTalentSlot();
    }

    [Client]
    private void UpdateTalentTrees()
    {
        if (currentHumanoid != null)
        {
            NetworkIdentity humanoidId = currentHumanoid.GetComponent<NetworkIdentity>();
            CmdUpdateTalentTrees(humanoidId);      
        }
    }

    [Command]
    private void CmdUpdateTalentTrees(NetworkIdentity humanoidId)
    {
        NetworkHumanoid humanoid = NetworkServer.FindLocalObject(humanoidId.netId).GetComponent<NetworkHumanoid>();
        if(humanoid != null)
        {
            ServerUpdateTree(humanoidId.clientAuthorityOwner, humanoid.TalentManager.MarauderTalents, humanoid, humanoid.TalentManager.marauderPoints);
            ServerUpdateTree(humanoidId.clientAuthorityOwner, humanoid.TalentManager.JaegerTalents, humanoid, humanoid.TalentManager.jaegerPoints);
            ServerUpdateTree(humanoidId.clientAuthorityOwner, humanoid.TalentManager.CombatMedicTalents, humanoid, humanoid.TalentManager.combatMedicPoints);
            TargetReceiveTalentPoints(humanoidId.clientAuthorityOwner, humanoid.TalentManager.TalentPoints);
        }
    }

    [Command]
    private void CmdUpdateTree(NetworkIdentity humanoidId, int tree)
    {
        NetworkHumanoid humanoid = NetworkServer.FindLocalObject(humanoidId.netId).GetComponent<NetworkHumanoid>();
        if(humanoid != null)
        {
            switch (tree)
            {
                case 0:
                    ServerUpdateTree(humanoidId.clientAuthorityOwner, humanoid.TalentManager.MarauderTalents, humanoid, humanoid.TalentManager.marauderPoints);
                    break;
                case 1:
                    ServerUpdateTree(humanoidId.clientAuthorityOwner, humanoid.TalentManager.JaegerTalents, humanoid, humanoid.TalentManager.jaegerPoints);
                    break;
                case 2:
                    ServerUpdateTree(humanoidId.clientAuthorityOwner, humanoid.TalentManager.CombatMedicTalents, humanoid, humanoid.TalentManager.combatMedicPoints);
                    break;
            }
            TargetReceiveTalentPoints(humanoidId.clientAuthorityOwner, humanoid.TalentManager.TalentPoints);
        }
    }

    [Server]
    private void ServerUpdateTree(NetworkConnection target, Talent[] array, NetworkHumanoid humanoid, int points)
    {
        for (int i = 0; i < array.Length; i++)
        {
            TalentDataToSend data = ServerRequestTalentData(array[i]);
            bool availability = humanoid.TalentManager.ServerCheckAvailability(array, array[i], points);
            TargetReceiveTalentData(target, data, availability);
        }
    }

    [Server]
    public TalentDataToSend ServerRequestTalentData(Talent talent)
    {
        TalentDataToSend data = new TalentDataToSend()
        {
            name = talent.Name,
            iconName = talent.IconName,
            tree = talent.currentTree.ToString(),
            description = talent.Description,
            active = talent.Active,
            tier = (int)talent.talentTier,
            rank = talent.CurrentRank,
            maxRank = talent.maxRank,
            index = talent.talentIndex,
            pointsToActivate = talent.pointsToActivate
        };

        return data;
    }

    [TargetRpc]
    private void TargetReceiveTalentData(NetworkConnection target, TalentDataToSend data, bool availability)
    {
        if(currentHumanoid != null)
        {
            if (data.tree.Equals("Marauder"))
                UpdateTalentSlot(_marauderSlots, data, availability);
            else if (data.tree.Equals("Jaeger"))
                UpdateTalentSlot(_jaegerSlots, data, availability);
            else if (data.tree.Equals("CombatMedic"))
                UpdateTalentSlot(_combatMedicSlots, data, availability);
            else
                Debug.LogWarning("error: wrong data");
        }
    }

    [TargetRpc]
    private void TargetReceiveTalentPoints(NetworkConnection target, int talentPoints)
    {
        if(currentHumanoid != null)
            _talentPointsText.text = "Available talent points: " + talentPoints;
        else
            _talentPointsText.text = " ";
    }

    [Client]
    private void UpdateTalentSlot(List<GameObject> tree, TalentDataToSend data, bool availability)
    {
        tree[data.index].GetComponent<NetworkTalentSlot>().UpdateTalentSlot(data, availability);
    }

    [Client]
    public void InvestPoints(int tree)
    {
        if (currentHumanoid != null)
        {
            currentHumanoid.TalentManager.CmdInvestPoint(tree);
            NetworkIdentity humanoidId = currentHumanoid.GetComponent<NetworkIdentity>();
            CmdUpdateTree(humanoidId, tree);
        }
    }

    [Client]
    private void OnTalentClicked(TalentDataToSend data)
    {
        if (currentHumanoid != null)
        {
            currentHumanoid.TalentManager.CmdActivateTalent(data);
            NetworkIdentity humanoidId = currentHumanoid.GetComponent<NetworkIdentity>();

            int tree = 0;
            if (data.tree.Equals("Marauder"))
                tree = 0;
            else if (data.tree.Equals("Jaeger"))
                tree = 1;
            else if (data.tree.Equals("CombatMedic"))
                tree = 2;

            CmdUpdateTree(humanoidId, tree);
            _skills.UpdateSkills(currentHumanoid);
        }
    }

}

public struct TalentDataToSend
{
    public string name;
    public string iconName;
    public string tree;
    public string description;
    public bool active;
    public int tier;
    public int rank;
    public int maxRank;
    public int index;
    public int pointsToActivate;
}