﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkSelectionUI : NetworkBehaviour
{
    public event System.Action<int> OnSelectionSlotClicked;
    private GameObject _selectionPanel;
    private List<GameObject> _selectionSlots = new List<GameObject>();

    #region Initialization

    public override void OnStartAuthority()
    {
        InitializeSelectionSlots();
    }

    private void InitializeSelectionSlots()
    {
        _selectionPanel = GameObject.Find("CharacterPanel");

        for (int j = 0; j < _selectionPanel.transform.childCount; j++)
        {
            _selectionSlots.Add(_selectionPanel.transform.GetChild(j).gameObject);
            NetworkSelectionSlot selectSlot = _selectionSlots[j].GetComponent<NetworkSelectionSlot>();

            selectSlot.SlotId = j;
            selectSlot.SelectOnClick += SelectOnClick;
        }
    }

    #endregion

    public void UpdateAvailableUnits(List<NetworkIdentity> availableUnits)
    {
        ClearAvailableUnits();
        ShowAvailableUnits(availableUnits);
    }

    [Client]
    private void ClearAvailableUnits()
    {
        for (int i = 0; i < _selectionSlots.Count; i++)
            _selectionSlots[i].SetActive(false);
    }

    [Client]
    private void ShowAvailableUnits(List<NetworkIdentity> availableUnits)
    {
        for (int i = 0; i < availableUnits.Count; i++)
        {
            NetworkSelectable selectable = ClientScene.FindLocalObject(availableUnits[i].netId).GetComponent<NetworkSelectable>();                
            if (selectable != null)
            {
                _selectionSlots[i].SetActive(true);
                _selectionSlots[i].GetComponent<NetworkSelectionSlot>().UpdateSelectionSlot(selectable);
            }
        }
    }

    [Client]
    public void HighlightSelection(NetworkSelectable toHighlight)
    {
        for (int i = 0; i < _selectionSlots.Count; ++i)
        {
            NetworkSelectionSlot selectionSlot = _selectionSlots[i].GetComponent<NetworkSelectionSlot>();
            NetworkSelectable selectable = selectionSlot.CurrentSelectable;
            if (selectable != null)
            {
                if (toHighlight == selectable)
                    selectionSlot.HighlightSelectionSlot();
                else
                    selectionSlot.ClearHighlight();
            }
        }
    }

    private void SelectOnClick(NetworkSelectionSlot pressedObject)
    {
        if (OnSelectionSlotClicked != null)
            OnSelectionSlotClicked(pressedObject.SlotId);
    }

}
