﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class NetworkInventoryUI : NetworkBehaviour
{
    public int inventorySize = 18;
    public GameObject uiSlot;
    public GameObject uiItem;
    public GameObject LootBag { get; private set; }

    private GameObject _collectable;
    private GameObject _characterDetailsPanel;
    private GameObject _namePanel;
    private GameObject _unitStatsPanel;
    private GameObject _weaponStatsPanel;
    private GameObject _equipmentPanel;
    private GameObject _inventoryPanel;
    private GameObject _dropZonePanel;

    private TooltipData[] _unitStatsData;
    private TooltipData[] _weaponStatsData;

    private List<GameObject> _inventorySlots;
    private List<GameObject> _equipmentSlots;

    private NetworkHumanoid currentHumanoid;

    #region Initialization

    public override void OnStartAuthority()
    {
        _collectable = Resources.Load<GameObject>("GameItems/NetworkCollectable");

        FindCharacterPanel();
        FindStatisticsPanels();

        InitializeInventorySlots();
        InitializeEquipmentSlots();

        _characterDetailsPanel.SetActive(false);

        FindLootBag();
        FindDropZonePanel();

        UpdateCharacterPanel(null);
    }

    private void FindCharacterPanel()
    {
        _characterDetailsPanel = transform.Find("NetworkUICanvas/CharacterDetailsPanel").gameObject;
        _namePanel = _characterDetailsPanel.transform.Find("NamePanel").gameObject;
    }

    private void FindStatisticsPanels()
    {
        _unitStatsPanel = _characterDetailsPanel.transform.Find("UnitStatisticsPanel").gameObject;
        _weaponStatsPanel = _characterDetailsPanel.transform.Find("WeaponStatisticsPanel").gameObject;

        _unitStatsData = _unitStatsPanel.GetComponentsInChildren<TooltipData>();
        _weaponStatsData = _weaponStatsPanel.GetComponentsInChildren<TooltipData>();
    }

    private void InitializeInventorySlots()
    {
        _inventorySlots = new List<GameObject>();
        _inventoryPanel = _characterDetailsPanel.transform.Find("InventoryPanel").gameObject;

        for (int i = 0; i < inventorySize; i++)
        {
            GameObject slotItem = Instantiate(uiSlot, _inventoryPanel.transform.position, Quaternion.identity, _inventoryPanel.transform);
            slotItem.transform.localScale = Vector3.one;

            NetworkItemSlot itemSlot = slotItem.GetComponent<NetworkItemSlot>();
            itemSlot.SlotId = i;
            itemSlot.OnItemSwap += SwapItem;
            itemSlot.OnItemPickUp += AddItem;
            itemSlot.OnItemUnequip += UnequipItem;

            _inventorySlots.Add(Instantiate(uiItem, slotItem.transform.position, slotItem.transform.rotation, slotItem.transform));
            _inventorySlots[i].transform.localScale = Vector3.one;
            _inventorySlots[i].GetComponent<NetworkItemData>().Initialize();
        }
    }

    private void InitializeEquipmentSlots()
    {
        _equipmentSlots = new List<GameObject>();
        _equipmentPanel = _characterDetailsPanel.transform.Find("EquipmentHolderPanel/EquipmentPanel").gameObject;

        for (int k = 0; k < _equipmentPanel.transform.childCount; k++)
        {
            _equipmentSlots.Add(_equipmentPanel.transform.GetChild(k).gameObject);
            NetworkEquipmentSlot equipmentSlot = _equipmentSlots[k].GetComponent<NetworkEquipmentSlot>();
            equipmentSlot.OnItemEquipFromInventory += EquipItemFromInventory;
            equipmentSlot.OnItemEquipFromLoot += EquipItemFromLoot;

            GameObject slotItem = Instantiate(uiItem, _equipmentSlots[k].transform.position, _equipmentSlots[k].transform.rotation, _equipmentSlots[k].transform);
            slotItem.transform.localScale = Vector3.one;
            slotItem.GetComponent<NetworkItemData>().Initialize();
        }
    }

    private void FindDropZonePanel()
    {
        _dropZonePanel = transform.Find("NetworkUICanvas/DropZone").gameObject;
        _dropZonePanel.GetComponent<NetworkDropSlot>().OnGroundDrop += DropItem;
        _dropZonePanel.SetActive(false);
    }

    private void FindLootBag()
    {
        LootBag = transform.Find("NetworkUICanvas/LootBag").gameObject;
        LootBag.SetActive(false);
    }

    #endregion

    #region ItemFunctions

    [Client]
    private void AddItem(NetworkItemSlot toSlot, NetworkLootSlot fromSlot)
    {
        // add item to inventory when looting from other UI element
        if (toSlot != null && fromSlot != null)
        {
            if(currentHumanoid != null)
            {
                NetworkIdentity humanoidId = currentHumanoid.GetComponent<NetworkIdentity>();
                NetworkIdentity lootbagId = GetComponent<NetworkIdentity>();
                CmdAddItem(toSlot.SlotId, humanoidId, fromSlot.SlotId, lootbagId);
            }
        }
    }

    [Command]
    private void CmdAddItem(int inventoryIndex, NetworkIdentity unitId, int lootIndex, NetworkIdentity lootId)
    {
        NetworkHumanoid humanoid = NetworkServer.FindLocalObject(unitId.netId).GetComponent<NetworkHumanoid>();
        NetworkLootBag lootbag = NetworkServer.FindLocalObject(lootId.netId).GetComponent<NetworkLootBag>();
        if(humanoid != null && lootbag != null)
        {
            Item item = lootbag.RemoveFromIndex(lootIndex);
            if (item != null)
            {
                humanoid.InventoryManager.ServerAddItemByIndex(item, inventoryIndex);
                lootbag.TargetUpdateLootBag(unitId.clientAuthorityOwner);
                TargetUpdateInventory(unitId.clientAuthorityOwner);
            }
        }
    }

    [Client]
    private void SwapItem(NetworkItemSlot toSlot, NetworkItemSlot fromSlot)
    {
        // move item in inventory from original index to destination index
        if (toSlot != null && fromSlot != null && currentHumanoid != null)
        {
            NetworkIdentity humanoidId = currentHumanoid.GetComponent<NetworkIdentity>();
            CmdSwapItems(toSlot.SlotId, fromSlot.SlotId, humanoidId);
        }
    }

    [Command]
    private void CmdSwapItems(int toSlot, int fromSlot, NetworkIdentity id)
    {
        NetworkHumanoid humanoid = NetworkServer.FindLocalObject(id.netId).GetComponent<NetworkHumanoid>();
        if(humanoid != null)
        {
            humanoid.InventoryManager.ServerSwapItems(toSlot, fromSlot);
            TargetUpdateInventory(id.clientAuthorityOwner);
        }

    }

    [Client]
    private void EquipItemFromInventory(NetworkItemSlot itemSlot)
    {
        if (currentHumanoid != null && itemSlot != null)
        {
            NetworkIdentity humanoidId = currentHumanoid.GetComponent<NetworkIdentity>();
            CmdEquipItemFromInventory(itemSlot.SlotId, humanoidId);
        }
    }

    [Command]
    private void CmdEquipItemFromInventory(int inventorySlot, NetworkIdentity id)
    {
        NetworkHumanoid humanoid = NetworkServer.FindLocalObject(id.netId).GetComponent<NetworkHumanoid>();

        if(humanoid != null)
        {
            Item item = humanoid.InventoryManager.Inventory[inventorySlot];
            if(item != null)
            {
                if (item is Equipment)
                {
                    Equipment equipment = item as Equipment;
                    humanoid.EquipmentManager.ServerEquipItem(equipment);
                    TargetUpdateCharacterPanel(id.clientAuthorityOwner);
                }
            }
        }
    }

    [Client]
    private void EquipItemFromLoot(NetworkLootSlot slot)
    {
        if (currentHumanoid != null && slot != null)
        {
            NetworkIdentity lootId = slot.GetComponentInParent<NetworkIdentity>();
            NetworkIdentity humanoidId = currentHumanoid.GetComponent<NetworkIdentity>();
            CmdEquipItemFromLoot(slot.SlotId, humanoidId, lootId);
        }
    }

    [Command]
    private void CmdEquipItemFromLoot(int lootIndex, NetworkIdentity humanoidId, NetworkIdentity lootId)
    {
        NetworkHumanoid humanoid = NetworkServer.FindLocalObject(humanoidId.netId).GetComponent<NetworkHumanoid>();
        LootBag lootbag = NetworkServer.FindLocalObject(lootId.netId).GetComponent<LootBag>();
        if (humanoid != null && lootbag != null)
        {
            Item item = lootbag.RemoveFromIndex(lootIndex);
            if (item != null)
            {
                if(item is Equipment)
                {
                    Equipment equipment = item as Equipment;
                    humanoid.EquipmentManager.ServerEquipItem(equipment);
                    TargetUpdateCharacterPanel(humanoidId.clientAuthorityOwner);
                }
            }
        }
    }

    [Client]
    private void UnequipItem(NetworkItemSlot toSlot, NetworkEquipmentSlot fromSlot)
    {
        if (toSlot != null && fromSlot != null && currentHumanoid != null)
        {
            NetworkIdentity humanoidId = currentHumanoid.GetComponent<NetworkIdentity>();
            CmdUnequipItem(toSlot.SlotId, (int)fromSlot.slot, humanoidId);
        }
    }

    [Command]
    private void CmdUnequipItem(int toIndex, int fromIndex, NetworkIdentity id)
    {
        NetworkHumanoid humanoid = NetworkServer.FindLocalObject(id.netId).GetComponent<NetworkHumanoid>();

        if(humanoid != null)
        {
            humanoid.EquipmentManager.ServerUnequipItem(fromIndex, toIndex);
            TargetUpdateCharacterPanel(id.clientAuthorityOwner);
        }
    }

    [Client]
    private void DropItem(NetworkItemSlot itemSlot, NetworkEquipmentSlot equipmentSlot)
    {
        if(currentHumanoid != null)
        {
            NetworkIdentity humanoidId = currentHumanoid.GetComponent<NetworkIdentity>();
            if (itemSlot != null)
                CmdDropFromInventory(itemSlot.SlotId, humanoidId);
            else if (equipmentSlot != null)
                CmdDropFromEquipment((int)equipmentSlot.slot, humanoidId);
        }
    }

    [Command]
    private void CmdDropFromInventory(int index, NetworkIdentity id)
    {
        NetworkHumanoid humanoid = NetworkServer.FindLocalObject(id.netId).GetComponent<NetworkHumanoid>();
        if(humanoid != null)
        {
            Item item = humanoid.InventoryManager.Inventory[index];
            if(humanoid.InventoryManager.ServerRemoveItemFromIndex(index))
            {
                InstantiateCollectable(item);
                TargetUpdateInventory(id.clientAuthorityOwner);
            }
        }
    }

    [Command]
    private void CmdDropFromEquipment(int index, NetworkIdentity id)
    {
        NetworkHumanoid humanoid = NetworkServer.FindLocalObject(id.netId).GetComponent<NetworkHumanoid>();
        if(humanoid != null)
        {
            Item item = humanoid.EquipmentManager.CurrentEquipment[index];
            if(humanoid.EquipmentManager.ServerDropEquipment(index))
            {
                InstantiateCollectable(item);
                TargetUpdateCharacterPanel(id.clientAuthorityOwner);
            }
        }
    }

    [Server]
    private void InstantiateCollectable(Item item)
    {
        GameObject reusedObject = Instantiate(_collectable, DropNearby(currentHumanoid.transform), Quaternion.identity);
        NetworkCollectable collectable = reusedObject.GetComponent<NetworkCollectable>();
        collectable.CurrentItem = item;
        collectable.ServerOnCurrentItemChanged();
        //collectable.TransformItem();
        NetworkServer.Spawn(reusedObject);
    }

    private Vector3 DropNearby(Transform transform)
    {
        return new Vector3(transform.position.x + Random.Range(0.5f, 1), transform.position.y, transform.position.z + Random.Range(-0.5f, 0.5f));
    }

    #endregion

    #region UIUpdating

    [TargetRpc]
    private void TargetUpdateInventory(NetworkConnection target)
    {
        UpdateInventory();
    }

    [TargetRpc]
    private void TargetUpdateCharacterPanel(NetworkConnection target)
    {
        if(currentHumanoid != null)
            UpdateCharacterPanel(currentHumanoid);
    }

    [Client]
    public void ActivateInventory(NetworkSelectable selectable)
    {
        _characterDetailsPanel.SetActive(!_characterDetailsPanel.activeSelf);
        UpdateCharacterPanel(selectable);
    }

    [Client]
    public void UpdateCharacterPanel(NetworkSelectable selectable)
    {
        UpdateStats(selectable);
        UpdateHumanoid(selectable);

        UpdateInventory();
        UpdateEquippedItems();
    }

    [Client]
    private void UpdateHumanoid(NetworkSelectable selectable)
    {
        if (selectable is NetworkHumanoid)
            currentHumanoid = selectable as NetworkHumanoid;
        else
            currentHumanoid = null;
    }

    #endregion

    #region Inventory

    [Client]
    public void UpdateInventory()
    {
        ClearInventory();

        if(currentHumanoid != null)
        {
            NetworkIdentity id = currentHumanoid.GetComponent<NetworkIdentity>();
            CmdFetchInventoryData(id);
        }
    }

    [Command]
    private void CmdFetchInventoryData(NetworkIdentity id)
    {
        NetworkHumanoid humanoid = NetworkServer.FindLocalObject(id.netId).GetComponent<NetworkHumanoid>();
        if (humanoid != null)
        {
            Item[] inventory = humanoid.InventoryManager.Inventory;
            for (int i = 0; i < inventory.Length; i++)
            {
                if(inventory[i] != null)
                {
                    ItemDataToSend data = NetworkGenerateLoot.Instance.ServerRequestItemData(inventory[i]);
                    NetworkConnection conn = GetComponent<NetworkIdentity>().clientAuthorityOwner;
                    TargetReceiveInventoryData(conn, data, i);
                }
            }
        }
    }

    [TargetRpc]
    private void TargetReceiveInventoryData(NetworkConnection target, ItemDataToSend data, int index)
    {
        _inventorySlots[index].GetComponent<NetworkItemData>().UpdateItemData(data);
    }

    [Client]
    private void ClearInventory()
    {
        for (int i = 0; i < _inventorySlots.Count; i++)
            _inventorySlots[i].GetComponent<NetworkItemData>().ClearItemData();
    }

    #endregion

    #region Stats

    [Client]
    public void UpdateStats(NetworkSelectable selectable)
    {
        ClearStats();

        if (selectable != null)
        {
            NetworkIdentity id = selectable.GetComponent<NetworkIdentity>();
            CmdFetchSelectableData(id);
        }
    }

    [Command]
    private void CmdFetchSelectableData(NetworkIdentity id)
    {
        GameObject go = NetworkServer.FindLocalObject(id.netId);
        if (go != null)
        {
            NetworkSelectable selectable = go.GetComponent<NetworkSelectable>();
            if (selectable != null)
            {
                SelectableDataToSend data = selectable.ServerRequestSelectableData();
                NetworkConnection conn = GetComponent<NetworkIdentity>().clientAuthorityOwner;
                TargetReceiveSelectableData(conn, data);
            }
        }
    }

    [TargetRpc]
    private void TargetReceiveSelectableData(NetworkConnection target, SelectableDataToSend data)
    {
        UpdateStatTexts(data);
    }

    [Client]
    private void UpdateStatTexts(SelectableDataToSend data)
    {
        _namePanel.GetComponentInChildren<Text>().text = data.name + " lvl " + data.level;
        _unitStatsData[0].UpdateData("Health: ", data.health.ToString());
        _unitStatsData[3].UpdateData("Defence: ", data.defence.ToString());

        if (data.selectableType.Equals("Humanoid"))
            SetHumanoidStats(data);
        else if(data.selectableType.Equals("Machine"))
            SetMachineStats(data);
    }

    [Client]
    private void SetHumanoidStats(SelectableDataToSend data)
    {
        _unitStatsData[1].UpdateData("Regeneration: ", string.Format("{0:0.0}hp/s", data.regenRate));
        _unitStatsData[2].UpdateData("Accuracy: ", data.accuracy * 100 + "%");
        _unitStatsData[4].UpdateData("Experience: ", data.experience + " / " + data.experienceCap);

        if (data.hasWeapon)
        {
            _weaponStatsData[0].UpdateData("Damage: ", data.damage.ToString());
            _weaponStatsData[1].UpdateData("Fire Rate: ", data.fireRate.ToString());
            _weaponStatsData[2].UpdateData("Critical Chance: ", data.critChance * 100 + "%");
            _weaponStatsData[3].UpdateData("Critical Damage: ", data.critDamage + "%");
            _weaponStatsData[4].UpdateData("Effective Range: ", data.effectiveRange + "m");
        }
    }

    [Client]
    private void SetMachineStats(SelectableDataToSend data)
    {
        _weaponStatsData[0].UpdateData("Damage: ", data.damage.ToString());
        _weaponStatsData[1].UpdateData("Fire Rate: ", data.fireRate.ToString());
        _weaponStatsData[4].UpdateData("Attack Range: ", data.effectiveRange + "m");
    }

    [Client]
    private void ClearStats()
    {
        _namePanel.GetComponentInChildren<Text>().text = "";
        ClearStatsPanel(_unitStatsData);
        ClearStatsPanel(_weaponStatsData);
    }

    [Client]
    private void ClearStatsPanel(TooltipData[] array)
    {
        for (int i = 0; i < array.Length; i++)
            array[i].ClearData();
    }

    #endregion

    #region Equipment

    [Client]
    public void UpdateEquippedItems()
    {
        ClearEquippedItems();

        if (currentHumanoid != null)
        {
            NetworkIdentity id = currentHumanoid.GetComponent<NetworkIdentity>();
            CmdFetchEquipmentData(id);
        }
    }

    [Command]
    private void CmdFetchEquipmentData(NetworkIdentity id)
    {
        NetworkHumanoid humanoid = NetworkServer.FindLocalObject(id.netId).GetComponent<NetworkHumanoid>();
        if (humanoid != null)
        {
            Equipment[] equipment = humanoid.EquipmentManager.CurrentEquipment;
            for (int i = 0; i < equipment.Length; i++)
            {
                if(equipment[i] != null)
                {
                    ItemDataToSend data = NetworkGenerateLoot.Instance.ServerRequestItemData(equipment[i]);
                    NetworkConnection conn = GetComponent<NetworkIdentity>().clientAuthorityOwner;
                    TargetReceiveEquipmentData(conn, data, i);
                }
            }
        }
    }

    [TargetRpc]
    private void TargetReceiveEquipmentData(NetworkConnection target, ItemDataToSend data, int index)
    {
        if (_equipmentSlots[index].transform.childCount > 0)
            _equipmentSlots[index].transform.GetChild(0).GetComponent<NetworkItemData>().UpdateItemData(data);
    }

    [Client]
    private void ClearEquippedItems()
    {
        for (int i = 0; i < _equipmentSlots.Count; i++)
        {
            if(_equipmentSlots[i].transform.childCount > 0)
                _equipmentSlots[i].transform.GetChild(0).GetComponent<NetworkItemData>().ClearItemData();
        }
    }

    #endregion

}
