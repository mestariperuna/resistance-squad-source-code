﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;

public class NetworkTargetedTrigger : NetworkBehaviour
{
    private NetworkSelectable _target;
    private NetworkHumanoid _skillHolder;
    private bool _activated;
    private bool _targeted;
    private float _range;
    private Skill _currentSkill;

    private Vector3 _targetPos;

    public override void OnStartAuthority()
    {
        _skillHolder = GetComponent<NetworkHumanoid>();
    }

    [Server]
    public void ServerInitializeTrigger(bool targeted,Skill skill)
    {
        NetworkConnection conn = GetComponent<NetworkIdentity>().clientAuthorityOwner;
        _currentSkill = skill;
        TargetActivateTrigger(conn, skill.skillRange, targeted);
    }

    [TargetRpc]
    private void TargetActivateTrigger(NetworkConnection target, float range, bool targeted)
    {
        _activated = true;
        _targeted = targeted;
        _range = range;
    }

    private void Update()
    {
        if (hasAuthority)
        {
            if (_activated)
                ListenInput();
        }
    }

    [Client]
    private void ListenInput()
    {
       
        if (InputFunctions.GetKeyUp("Select"))
        {
            if(_targeted)
            {
                RayCastTarget();
            }
            else
            {
                CmdTriggerCastedSkill(GetComponent<NetworkIdentity>(), _targetPos);
                _activated = false;
            }
            
        }
            
        if (InputFunctions.GetKeyUp("Command"))
            _activated = false;

        if (_targeted)
        {
            if (_skillHolder != null && _target != null)
                MoveToTarget();
        }
        else
        {
            DrawPath();
        }
    }

    [Client]
    private void RayCastTarget()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100))
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                if (_targeted)
                {
                    NetworkSelectable selectable = hit.transform.GetComponent<NetworkSelectable>();
                    if (selectable != null)
                        _target = selectable;
                    else
                        _activated = false;
                }
            }                 
        }
    }

    [Client]
    private void RayCastPosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100))
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                _targetPos = CalculateTargetPosition(hit.point, transform.position);
            }
        }
    }

    private void DrawPath()
    {
        RayCastPosition();
        Debug.DrawLine(transform.position, _targetPos, Color.green);
    }

    private Vector3 CalculateTargetPosition(Vector3 hitPoint, Vector3 pos)
    {
        Vector3 skillVector = hitPoint - pos;
        if (skillVector.magnitude > _range)
            return pos + skillVector.normalized * _range;

        return pos + skillVector;
    }

    [Client]
    protected void MoveToTarget()
    {
        if (Vector3.Distance(transform.position, _target.transform.position) > _range)
            _skillHolder.CmdMoveUnit(_target.transform.position);
        else
        {
            _skillHolder.CmdStopUnit();
            CmdTriggerTargetedSkill(_target.GetComponent<NetworkIdentity>());
            _target = null;
            _activated = false;
        }
    }

    [Command]
    private void CmdTriggerTargetedSkill(NetworkIdentity target)
    {
        NetworkSelectable selectable = NetworkServer.FindLocalObject(target.netId).GetComponent<NetworkSelectable>();
        if(selectable != null)
            _currentSkill.TriggerTargetedSkill(selectable);
    }

    [Command]
    private void CmdTriggerCastedSkill(NetworkIdentity caster, Vector3 targetPos)
    {
        NetworkHumanoid humanoid = NetworkServer.FindLocalObject(caster.netId).GetComponent<NetworkHumanoid>();
        if(humanoid != null)
            _currentSkill.TriggerCastedSkill(humanoid, targetPos);
    }
}
