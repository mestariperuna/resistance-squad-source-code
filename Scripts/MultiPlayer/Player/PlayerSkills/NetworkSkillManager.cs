﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

// SkillManager holds the information of Unlocked skills and Active skills for each game character.
// Skills are unlocked via TalentSystem. Each talent knows which skill they refer to,
// and that skill is added to unlocked/active skills when sent from TalentSystem.

public class NetworkSkillManager : NetworkBehaviour
{
    public event Action<Skill> OnSkillUsed;
    public event Action<PassiveSkill> OnSkillUnlocked;
    public event Action<PassiveSkill> OnSkillRemoved;
    public event Action<PassiveSkill> OnSkillUpgraded;

    public int maxSkillCount = 5;
    public GameObject[] ActiveSkills { get; private set; }
    public List<GameObject> UnlockedSkills { get; private set; }

    public override void OnStartServer()
    {
        ActiveSkills = new GameObject[maxSkillCount];
        UnlockedSkills = new List<GameObject>();
    }

    [Server]
    public void ServerAddToUnlockedSkills(GameObject newSkillObject)
    {
        if (newSkillObject != null)
        {
            // ensure that the object has a skill attached to it
            Skill newSkill = newSkillObject.GetComponent<Skill>();
            if (newSkill != null)
            {
                // clone the object so we don't have one specific skill in our hands (multiple units can have same skill)
                GameObject addedSkillObject = Instantiate(newSkillObject, transform.position, Quaternion.identity, transform);
                UnlockedSkills.Add(addedSkillObject);

                Skill addedSkill = addedSkillObject.GetComponent<Skill>();

                if (addedSkill is PassiveSkill)
                {
                    PassiveSkill passiveSkill = addedSkill as PassiveSkill;
                    passiveSkill.InitializeStats();

                    if (OnSkillUnlocked != null)
                        OnSkillUnlocked(passiveSkill);
                }
                else
                    ServerAddToActiveSkills(addedSkillObject, null);
            }
        }
    }

    [Server]
    public void ServerUnlockTalent(Talent talent)
    {
        GameObject unlockedSkill = ServerUnlockedSkillsContains(talent.TalentSkill);
        talent.Activate();
        if (unlockedSkill != null)
        {
            Skill skill = unlockedSkill.GetComponent<Skill>();
            if (skill is PassiveSkill)
            {
                PassiveSkill passiveSkill = skill as PassiveSkill;

                if (OnSkillUpgraded != null)
                    OnSkillUpgraded(passiveSkill);
            }
            else
                skill.UpgradeSkill();
            
        }
        else if(unlockedSkill == null)
            ServerAddToUnlockedSkills(talent.TalentSkill);
    }

    [Server]
    public GameObject ServerUnlockedSkillsContains(GameObject objectToFind)
    {
        Skill skillToFind = objectToFind.GetComponent<Skill>();
        for (int i = 0; i < UnlockedSkills.Count; i++)
        {
            if (UnlockedSkills[i].GetComponent<Skill>().name.Equals(skillToFind.name))
                return UnlockedSkills[i];
        }
        return null;
    }

    [Server]
    public bool ServerRemoveFromUnlockedSkills(GameObject toRemoveObject)
    {
        GameObject toFindObject = ServerUnlockedSkillsContains(toRemoveObject);
        if (toFindObject != null)
        {
            UnlockedSkills.Remove(toFindObject);
            PassiveSkill skill = toFindObject.GetComponent<PassiveSkill>();
            if (skill != null)
            {
                if (OnSkillRemoved != null)
                    OnSkillRemoved(skill);
            }  

            Destroy(toFindObject);
            return true;
        }
        return false;
    }

    // returns true if succesfully added a skill otherwise return false
    [Server]
    public bool ServerAddToActiveSkills(GameObject newSkillObject, int? index)
    {
        if (newSkillObject != null)
        {
            // ensure that the object has a skill attached to it
            Skill newSkill = newSkillObject.GetComponent<Skill>();
            if (newSkill != null)
            {
                // ensure that we have only one clone of added skill
                ServerRemoveDuplicateSkills(newSkill);
                if (index == null)
                    index = FindEmptyIndex();

                if (index != null)
                {
                    ServerRemoveFromActiveSkills((int)index);
                    ActiveSkills[(int)index] = newSkillObject;
                }
                return true;
            }
        }
        return false;
    }

    private int? FindEmptyIndex()
    {
        for (int i = 0; i < ActiveSkills.Length; i++)
        {
            if (ActiveSkills[i] == null)
            {
                return i;
            }
        }
        return null;
    }

    // returns true if succesfully removed a skill otherwise return false
    [Server]
    public bool ServerRemoveFromActiveSkills(int index)
    {
        if (ActiveSkills[index] != null)
        {
            ActiveSkills[index] = null;
            return true;
        }
        return false;
    }

    // remove duplicate skills
    [Server]
    private void ServerRemoveDuplicateSkills(Skill skill)
    {
        for (int i = 0; i < ActiveSkills.Length; i++)
        {
            if (ActiveSkills[i] != null)
            {
                if (ActiveSkills[i].GetComponent<Skill>().name.Equals(skill.name))
                {
                    ServerRemoveFromActiveSkills(i);
                    break;
                }
            }
        }
    }

    [Command]
    public void CmdUseSkillAtIndex(int index)
    {
        if (index < ActiveSkills.Length)
        {
            if (ActiveSkills[index] != null)
            {
                Skill skill = ActiveSkills[index].GetComponent<Skill>();
                skill.UseSkill();

                if (OnSkillUsed != null)
                    OnSkillUsed(skill);
            }
        }
    }

    #region HasSkills

    public bool HasMadDocSkill()
    {
        for (int i = 0; i < UnlockedSkills.Count; i++)
        {
            MadDoc skill = UnlockedSkills[i].GetComponent<MadDoc>();
            if(skill != null)
                return skill.CastSkill();
        }
        return false;
    }

    public Rage HasRageSkill()
    {
        for (int i = 0; i < UnlockedSkills.Count; i++)
        {
            Rage skill = UnlockedSkills[i].GetComponent<Rage>();
            if (skill != null)
                return skill;
        }
        return null;
    }

    public bool HasJuggernautSkill()
    {
        for (int i = 0; i < UnlockedSkills.Count; i++)
        {
            Juggernaut skill = UnlockedSkills[i].GetComponent<Juggernaut>();
            if (skill != null)
                return true;
        }
        return false;
    }

    #endregion

}
