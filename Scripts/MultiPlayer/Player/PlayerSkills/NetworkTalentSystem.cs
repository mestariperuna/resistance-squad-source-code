﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

// TalentSystem holds information of all talents in the game.
// TalentSystem has a reference to SkillSystem, which holds information of all skills and talents for each
// game character.
// TalentSystem also holds the information of usable talentpoints for each character.
// Dependency on SkillManager.

public class NetworkTalentSystem : NetworkBehaviour
{
    public int TalentPoints { get; private set; }
    public int marauderPoints { get; private set; }
    public int jaegerPoints { get; private set; }
    public int combatMedicPoints { get; private set; }

    private int maxMarauderPoints;
    private int maxJaegerPoints;
    private int maxCombatMedicPoints;

    // All talents in class based arrays.
    public Talent[] MarauderTalents { get; private set; } 
    public Talent[] JaegerTalents { get; private set; }
    public Talent[] CombatMedicTalents { get; private set; }

    private NetworkSkillManager _skillManager;

    #region Initialization

    public override void OnStartServer()
    {
        _skillManager = GetComponent<NetworkSkillManager>();
        InitializeTalents();
    }

    private void InitializeTalents()
    {     
        MarauderTalents = InitializeTalentArray("NetworkMarauder");
        JaegerTalents = InitializeTalentArray("NetworkJaeger");
        CombatMedicTalents = InitializeTalentArray("NetworkCombatMedic");

        maxMarauderPoints = MarauderTalents[0].maxRank;
        maxJaegerPoints = JaegerTalents[0].maxRank;
        maxCombatMedicPoints = CombatMedicTalents[0].maxRank;
    }

    // Load all talents and add them to an array.
    private Talent[] InitializeTalentArray(string treeString)
    {
        Talent[] talents = Resources.LoadAll<Talent>("Talents/" + treeString);
        Talent[] tree = new Talent[talents.Length];

        for (int i = 0; i < tree.Length; i++)
        {
            tree[talents[i].talentIndex] = Instantiate(talents[i]);
        }

        return tree;
    }

    #endregion

    [Server]
    public void ServerOnLevelUp(int points)
    {
        TalentPoints += points;
    }

    [Command]
    public void CmdInvestPoint(int tree)
    {
        if (TalentPoints > 0)
        {
            switch (tree)
            {
                case 0:
                    if(marauderPoints < maxMarauderPoints)
                        marauderPoints++;                     
                    break;
                case 1:
                    if(jaegerPoints < maxJaegerPoints)
                        jaegerPoints++;                    
                    break;
                case 2:
                    if(combatMedicPoints < maxCombatMedicPoints)
                        combatMedicPoints++;
                    break;
                default:
                    break;
            }
        }
        else
            Debug.Log("No points remaining");
    }

    [Command]
    public void CmdActivateTalent(TalentDataToSend pressedTalent)
    {
        bool requirementsMet = false;
        Talent talent = null;

        if (pressedTalent.tree.Equals("Marauder"))
        {
            talent = MarauderTalents[pressedTalent.index];
            if (marauderPoints >= talent.pointsToActivate)
                requirementsMet = ServerCheckRequirements(MarauderTalents, talent);
        }
        else if(pressedTalent.tree.Equals("Jaeger"))
        {
            talent = JaegerTalents[pressedTalent.index];
            if (jaegerPoints >= talent.pointsToActivate)
                requirementsMet = ServerCheckRequirements(JaegerTalents, talent);
        }
        else if(pressedTalent.tree.Equals("CombatMedic"))
        {
            talent = CombatMedicTalents[pressedTalent.index];
            if (combatMedicPoints >= talent.pointsToActivate)
                requirementsMet = ServerCheckRequirements(CombatMedicTalents, talent);
        }

        if (TalentPoints > 0 && requirementsMet)
        {
            _skillManager.ServerUnlockTalent(talent);
            TalentPoints--;
        }
        else
            Debug.Log("Not enough points or skill already at max rank");
    }

    [Server]
    private bool ServerCheckRequirements(Talent[] tree, Talent talent)
    {
        for (int i = 0; i < talent.requiredIndexes.Length; i++)
        {
            if (!tree[talent.requiredIndexes[i]].Active)
                return false;
        }

        if (talent.CurrentRank == talent.maxRank)
            return false;

        return true;
    }

    [Server]
    public bool ServerCheckAvailability(Talent[] tree, Talent talent, int requiredPoints)
    {
        for (int i = 0; i < talent.requiredIndexes.Length; i++)
        {
            if (!tree[talent.requiredIndexes[i]].Active)
                return false;
        }

        if (talent.pointsToActivate > requiredPoints)
            return false;

        return true;
    }

}
