﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkExplosive : Skill
{

    public int damage;
    public float radius;
    public bool throwable;
    public string projectileName;

    private GameObject _projectile;
    private NetworkProjectileTrigger _trigger;

    private void Start()
    {
        _trigger = GetComponentInParent<NetworkProjectileTrigger>();
        _projectile = Resources.Load<GameObject>("GFX/Projectiles/" + projectileName);
    }

    public override void UseSkill()
    {
        if(!onCooldown)
        {
            if (_trigger != null)
                _trigger.ServerInitializeTrigger(_projectile, radius, damage, skillRange, throwable, this);
            else
                Debug.Log("Missing ProjectileTrigger component!");
        }
    }

}
