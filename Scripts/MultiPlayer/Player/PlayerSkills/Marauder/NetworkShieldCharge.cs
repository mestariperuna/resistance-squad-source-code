﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class NetworkShieldCharge : Skill
{
    [Header("ShieldCharge attributes")]
    public int damage = 30;
    [Tooltip("How fast do we move when charging")]
    public float chargeSpeed = 5f;
    [Header("Raycast Masks")]
    public LayerMask obstacleMask;
    public LayerMask groundMask;
    public LayerMask targetMask;

    private NetworkTargetedTrigger _trigger;

    private void Start()
    {
        _trigger = GetComponentInParent<NetworkTargetedTrigger>();
    }

    public override void UseSkill()
    {
        if (!onCooldown)
            _trigger.ServerInitializeTrigger(false,this);
    }

    public override void TriggerCastedSkill(NetworkHumanoid caster, Vector3 target)
    {
        CastSkill(caster, target);
    }

    private void CastSkill(NetworkHumanoid caster, Vector3 target)
    {
        Vector3 dirToTarget = (target - caster.transform.position).normalized;
        Ray ray = new Ray(caster.transform.position, dirToTarget);

        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, skillRange, obstacleMask))
        {
            target = hit.point - dirToTarget * 0.5f;
        }

        StartCoroutine(Charge(target, dirToTarget, caster));
        Cooldown(cooldown);
    }

    private IEnumerator Charge(Vector3 chargePos, Vector3 dir, NetworkHumanoid caster)
    {
        yield return null;

        caster.transform.rotation = Quaternion.LookRotation(dir);
        caster.RpcMoveUnit(chargePos, chargeSpeed);
        caster.Casting = true;

        float dist = 0f;
        float distToCheckCollisions = 0.4f;
        
        List<NetworkSelectable> damagedTargets = new List<NetworkSelectable>();
        
        while (Vector3.Distance(caster.transform.position, chargePos) > 0f)
        {
            float moveDistance = chargeSpeed * Time.deltaTime;
            
            dist += moveDistance;
            if (dist >= distToCheckCollisions)
            {
                Collider[] targets = Physics.OverlapSphere(caster.transform.position, dist, targetMask);

                for (int i = 0; i < targets.Length; i++)
                {
                    NetworkSelectable selectable = targets[i].GetComponent<NetworkSelectable>();
                    if (selectable != null)
                    {
                        // check that we don't damage same object more than once
                        if (!damagedTargets.Contains(selectable))
                        {
                            selectable.TakeDamage(damage, selectable.defence * selectable.damageReductionPercent);
                            damagedTargets.Add(selectable);
                        }
                    }
                }
                dist = 0;               
            }
            yield return null;
        }
        caster.Casting = false;
    }

}
