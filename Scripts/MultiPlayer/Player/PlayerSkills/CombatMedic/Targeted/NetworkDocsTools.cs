﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkDocsTools : Skill
{
    [Header("DocsTools attributes")]
    public int[] healingAmount = { 50, 75, 100, 125, 150 };
    public float regenTimer = 5.0f;
    public float buffDuration = 10f;
    private UnitBonusStats stats;
    private UnitBuff buff;
    private int maxRank = 4;
    private NetworkTargetedTrigger _trigger;

    private void Start()
    {        
        stats = new UnitBonusStats()
        {
            HealthRegenTimerBonus = regenTimer
        };

        buff = new UnitBuff()
        {
            BuffName = "Antibiotics",
            MaxStacks = 1,
            Duration = buffDuration,
            Stats = stats
        };
        _trigger = GetComponentInParent<NetworkTargetedTrigger>();
    }

    public override void UpgradeSkill()
    {
        currentRank++;
        if (currentRank >= maxRank)
            currentRank = maxRank;
    }

    public override void UseSkill()
    {
        if (!onCooldown)
            _trigger.ServerInitializeTrigger(true, this);
    }

    public override void TriggerTargetedSkill(NetworkSelectable selectable)
    {
        CastSkill(selectable);
    }

    private void CastSkill(NetworkSelectable selectable)
    {
        if (selectable != null)
        {
            if (selectable is NetworkHumanoid)
            {
                NetworkHumanoid humanoid = selectable as NetworkHumanoid;
                humanoid.Heal(healingAmount[currentRank]);
                if (currentRank == maxRank)
                    humanoid.ServerAddBuff(buff);
                Cooldown(cooldown);
            }
        }
    }

}
