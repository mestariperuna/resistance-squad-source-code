﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkMedicalSyringe : Skill
{
    [Header("MedicalSyringe attributes")]
    public float[] regenTimerBonus = { 0f, 3f };
    public int[] regenBonus = { 0, 5 };
    public float buffDuration = 20f;
    private UnitBuff buff;
    private UnitBonusStats stats;
    private NetworkTargetedTrigger _trigger;

    private void Start()
    {
        stats = new UnitBonusStats()
        {
            HealthRegenTimerBonus = regenTimerBonus[currentRank],
            HealthRegenBonus = regenBonus[currentRank]
        };

        buff = new UnitBuff()
        {
            BuffName = name,
            MaxStacks = 1,
            Duration = buffDuration,
            Stats = stats
        };

        _trigger = GetComponentInParent<NetworkTargetedTrigger>();
    }

    public override void UpgradeSkill()
    {
        currentRank++;
        stats.HealthRegenTimerBonus = regenTimerBonus[currentRank];
        stats.HealthRegenBonus = regenBonus[currentRank];
    }

    public override void UseSkill()
    {
        if(!onCooldown)
            _trigger.ServerInitializeTrigger(true, this);
    }

    public override void TriggerTargetedSkill(NetworkSelectable selectable)
    {
        CastSkill(selectable);
    }

    private void CastSkill(NetworkSelectable selectable)
    {
        if (selectable != null)
        {
            if (selectable is NetworkHumanoid)
            {
                NetworkHumanoid humanoid = selectable as NetworkHumanoid;
                humanoid.ServerAddBuff(buff);
                Cooldown(cooldown);
            }
        }

    }
}
