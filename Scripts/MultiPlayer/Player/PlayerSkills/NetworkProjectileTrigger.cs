﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;

public class NetworkProjectileTrigger : NetworkBehaviour
{

    public Transform projectileSpawn;
    public float gravity = -10f;
    public float maxHeight = 3f;

    private GameObject _projectile;
    private float _radius;
    private int _damage;
    private float _projectileRange;

    private bool _activated;
    private bool _throwable;
    private Vector3 _targetPos;
    private Skill _currentSkill;

    public override void OnStartAuthority()
    {
        _activated = false;
        _throwable = false;
    }

    protected void Update()
    {
        if (hasAuthority)
        {
            if (_activated)
                ListenInput(CanBeUsed());
        }
    }

    [Server]
    public void ServerInitializeTrigger(GameObject projectile, float radius, int damage, float range, bool throwable, Skill skill)
    {
        _projectile = projectile;
        _radius = radius;
        _damage = damage;
        NetworkConnection conn = GetComponent<NetworkIdentity>().clientAuthorityOwner;
        _currentSkill = skill;
        TargetActivateTrigger(conn, throwable, range);
    }

    [TargetRpc]
    protected void TargetActivateTrigger(NetworkConnection target, bool throwable, float range)
    {
        _activated = true;
        _throwable = throwable;
        _projectileRange = range;
    }

    [Client]
    private void ListenInput(bool canBeUsed)
    {
        if (canBeUsed)
        {
            if (InputFunctions.GetKeyUp("Select"))
            {
                if(_throwable)
                {
                    CmdTriggerThrowable(CalculateThrowableDirection().initialVelocity);
                    _activated = false;
                }
                else
                {
                    CmdTriggerProjectile(CalculateProjectileDirection());
                    _activated = false;
                }
            }
                
            if (InputFunctions.GetKeyUp("Command"))
                _activated = false;

            if (_throwable)
                DrawThrowableTrajectoryPath();
            else
                DrawProjectilePath();
        }
    }

    [Command]
    protected void CmdTriggerProjectile(Vector3 direction)
    {
        GameObject projectileObj = Instantiate(_projectile, projectileSpawn.position, Quaternion.identity);
        NetworkProjectile projectile = projectileObj.GetComponent<NetworkProjectile>();
        projectile.SetProjectile(direction.normalized, _damage, 0, _radius);
        NetworkServer.Spawn(projectileObj);
        _currentSkill.Cooldown(_currentSkill.cooldown);
    }

    [Command]
    private void CmdTriggerThrowable(Vector3 velocity)
    {
        GameObject projectileObj = Instantiate(_projectile, projectileSpawn.position, Quaternion.identity);
        NetworkProjectile projectile = projectileObj.GetComponent<NetworkProjectile>();
        projectile.velocity = velocity.magnitude;
        projectile.SetProjectile(velocity.normalized, _damage, 0, _radius);
        NetworkServer.Spawn(projectileObj);
        _currentSkill.Cooldown(_currentSkill.cooldown);
    }

    [Client]
    private bool CanBeUsed()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100))
        {
            CalculateTargetPosition(hit.point);
            if (EventSystem.current != null)
            {
                if (EventSystem.current.IsPointerOverGameObject())
                    return false;
            }
            return true;
        }
        return false;
    }

    [Client]
    private void CalculateTargetPosition(Vector3 hitPoint)
    {
        Vector3 skillVector = hitPoint - projectileSpawn.position;
        if (skillVector.magnitude > _projectileRange)
            _targetPos = projectileSpawn.position + skillVector.normalized * _projectileRange;
        else
            _targetPos = projectileSpawn.position + skillVector;
    }

    [Client]
    private Vector3 CalculateProjectileDirection()
    {
        return new Vector3(_targetPos.x - projectileSpawn.position.x, 0, _targetPos.z - projectileSpawn.position.z);
    }

    [Client]
    private ThrowableData CalculateThrowableDirection()
    {
        float displacementY = _targetPos.y - projectileSpawn.position.y;
        Vector3 displacementXZ = new Vector3(_targetPos.x - projectileSpawn.position.x, 0, _targetPos.z - projectileSpawn.position.z);

        float height = displacementXZ.magnitude / 2;
        if (height < displacementY && displacementY < maxHeight)
            height = displacementY;

        float time = (Mathf.Sqrt(-2 * height / gravity) + Mathf.Sqrt(2 * (displacementY - height) / gravity));
        Vector3 velocityY = Vector3.up * Mathf.Sqrt(-2 * gravity * height);
        Vector3 velocityXZ = displacementXZ / time;
        return new ThrowableData(velocityXZ + velocityY, time);
    }
    
    [Client]
    private void DrawThrowableTrajectoryPath()
    {
        ThrowableData data = CalculateThrowableDirection();
        Vector3 previousDrawPoint = projectileSpawn.position;

        int resolution = 30;
        for (int i = 1; i <= resolution; i++)
        {
            float simulationTime = i / (float)resolution * data.timeToTarget;
            Vector3 displacement = data.initialVelocity * simulationTime + Vector3.up * gravity * simulationTime * simulationTime / 2f;
            Vector3 drawPoint = projectileSpawn.position + displacement;
            Debug.DrawLine(previousDrawPoint, drawPoint, Color.green);
            previousDrawPoint = drawPoint;
        }
    }

    [Client]
    private void DrawProjectilePath()
    {
        Debug.DrawLine(projectileSpawn.position, _targetPos);
    }

    private struct ThrowableData
    {
        public Vector3 initialVelocity;
        public float timeToTarget;

        public ThrowableData(Vector3 initialVelocity, float timeToTarget)
        {
            this.initialVelocity = initialVelocity;
            this.timeToTarget = timeToTarget;
        }
    }

}
