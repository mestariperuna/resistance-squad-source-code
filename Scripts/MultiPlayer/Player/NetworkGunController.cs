﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkGunController : NetworkBehaviour
{
    #region Variables

    public Weapon EquippedWeapon { get; private set; }
    public float Accuracy { get; set; }

    public GameObject Projectile { get; private set; }
    public AudioClip FireAudio { get; private set; }

    public Transform weaponHold;
    public Transform projectileSpawn;

    public float critChance;
    public float critDamage;

    private float timeBetweenAttacks;
    private float nextAttack;

    private GameObject _muzzleEffect;

    private List<GunBonusStats> _gunPassiveSkillModifiers;
    private List<GunBuff> _currentBuffs;
    private NetworkSkillManager _skillManager;

    #endregion

    public override void OnStartServer()
    {
        _gunPassiveSkillModifiers = new List<GunBonusStats>();
        _currentBuffs = new List<GunBuff>();
        _skillManager = GetComponent<NetworkSkillManager>();
    }

    [Server]
    public void EquipWeapon(Weapon weaponToEquip)
    {
        if(weaponToEquip != null)
        {
            EquippedWeapon = weaponToEquip;
            timeBetweenAttacks = 1 / EquippedWeapon.attacksPerSecond;

            Projectile = Resources.Load<GameObject>("GFX/Projectiles/" + EquippedWeapon.projectileName);
            RpcSetEffects(EquippedWeapon.fireAudio);
        }
    }

    [ClientRpc]
    private void RpcSetEffects(string fireAudio)
    {
        FireAudio = Resources.Load<AudioClip>("SFX/Weapons/" + fireAudio);
        _muzzleEffect = Resources.Load<GameObject>("GFX/Effects/MuzzleFlashEffect");
    }

    [Server]
    public void RemoveWeapon()
    {
        if (EquippedWeapon != null)
            EquippedWeapon = null;
    }

    #region Shooting

    [Server]
    public void Shoot(Transform target)
    {
        if (EquippedWeapon != null && target != null)
        {
            if (Time.time > nextAttack)
            {
                nextAttack = Time.time + timeBetweenAttacks;
                LaunchProjectile(target);
            }
        }
    }

    [Server]
    public void OnPassiveSkillTrigger(Transform target)
    {
        if (EquippedWeapon != null)
            LaunchProjectile(target);
    }

    [Server]
    private void LaunchProjectile(Transform target)
    {
        GameObject go = Instantiate(Projectile, projectileSpawn.position, Quaternion.identity);
        NetworkProjectile projectile = go.GetComponent<NetworkProjectile>();

        // set the damage, the armour penetration and the direction for projectile
        projectile.SetProjectile(CalculateDirection(target), CalculateDamage(target), 0, 1);
        RpcShowEffects(projectileSpawn.position);
        NetworkServer.Spawn(go);
    }

    [ClientRpc]
    private void RpcShowEffects(Vector3 effectPos)
    {
        AudioManager.Instance.PlaySound(FireAudio, effectPos);
        GameObject go = Instantiate(_muzzleEffect, effectPos, Quaternion.identity);
        Destroy(go, 0.2f);
    }

    // calculates the damage for projectiles
    [Server]
    private int CalculateDamage(Transform target)
    {
        int rand = Random.Range(0, 100);

        int damage = EquippedWeapon.baseDamage + BonusDamage(EquippedWeapon.isTwoHander);
        float effectiveRange = EquippedWeapon.effectiveRange + GetBonusGunStat(x => x.EffectiveRange);
        float criticalChance = (critChance + GetBonusGunStat(x => x.CriticalChanceBonus)) * 100;
        float criticalDamage = (critDamage + GetBonusGunStat(x => x.CriticalDamageBonus)) / 100;

        if (target != null)
        {
            if (Vector3.Distance(transform.position, target.position) < effectiveRange)
                damage += GetBonusGunStat(x => x.UpCloseDamageBonus);
        }

        if (rand <= criticalChance)
            damage = Mathf.RoundToInt(damage * criticalDamage);

        return damage;
    }

    // calculates the direction for projectiles
    [Server]
    private Vector3 CalculateDirection(Transform target)
    {
        Vector2 randomPos;
        float accuracy = Accuracy + BonusAccuracy(EquippedWeapon.isTwoHander);
        float effectiveRange = EquippedWeapon.effectiveRange + GetBonusGunStat(x => x.EffectiveRange);

        randomPos.x = Random.Range(accuracy - 1, 1 - accuracy);
        randomPos.y = Random.Range(accuracy - 1, 1 - accuracy);
        
        Vector3 dir = (target.position - transform.position).normalized * effectiveRange + transform.right * randomPos.x + transform.up * randomPos.y;
        return dir.normalized;
    }

    #endregion

    #region BonusStats

[Server]
    public void OnPassiveSkillUnlocked(GunBonusStats gunModifiers)
    {
        if (gunModifiers != null)
            _gunPassiveSkillModifiers.Add(gunModifiers);
    }

    [Server]
    public void OnPassiveSkillRemoved(GunBonusStats gunModifiers)
    {
        if (gunModifiers != null)
        {
            if (_gunPassiveSkillModifiers.Contains(gunModifiers))
                _gunPassiveSkillModifiers.Remove(gunModifiers);
        }
    }

    private int BonusDamage(bool isTwoHander)
    {
        int damage = GetBonusGunStat(x => x.DamageBonus);
        if (_skillManager.HasJuggernautSkill())
        {
            damage += GetBonusGunStat(x => x.TwoHandDamageBonus);
            damage += GetBonusGunStat(x => x.OneHandDamageBonus);
        }
        else if (isTwoHander)
            damage += GetBonusGunStat(x => x.TwoHandDamageBonus);
        else
            damage += GetBonusGunStat(x => x.OneHandDamageBonus);

        return damage;
    }

    private float BonusAccuracy(bool isTwoHander)
    {
        float accuracy = GetBonusGunStat(x => x.AccuracyBonus);
        if (_skillManager.HasJuggernautSkill())
        {
            accuracy += GetBonusGunStat(x => x.TwoHandAccuracyBonus);
            accuracy += GetBonusGunStat(x => x.OneHandAccuracyBonus);
        }
        else if (isTwoHander)
            accuracy += GetBonusGunStat(x => x.TwoHandAccuracyBonus);
        else
            accuracy += GetBonusGunStat(x => x.OneHandAccuracyBonus);

        return accuracy;
    }

    public float GetBonusGunStat(System.Func<GunBonusStats, float> selector)
    {
        float stat = 0;
        for (int i = 0; i < _gunPassiveSkillModifiers.Count; i++)
            stat += selector(_gunPassiveSkillModifiers[i]);

        return stat;
    }

    public int GetBonusGunStat(System.Func<GunBonusStats, int> selector)
    {
        int stat = 0;
        for (int i = 0; i < _gunPassiveSkillModifiers.Count; i++)
            stat += selector(_gunPassiveSkillModifiers[i]);

        return stat;
    }

    #endregion

    #region Buffs

    [Server]
    public void ServerAddBuff(GunBuff buff)
    {
        if (buff != null)
        {
            if (!CurrentBuffsHasMaxStacks(buff))
            {
                _gunPassiveSkillModifiers.Add(buff.Stats);
                _currentBuffs.Add(buff);
                StartCoroutine(RemoveBuffAfterDuration(buff));
            }
        }
    }

    // check if the current buffs list has max amount of the buffs max stacks
    private bool CurrentBuffsHasMaxStacks(GunBuff buff)
    {
        int stackCount = 0;
        for (int i = 0; i < _currentBuffs.Count; i++)
        {
            if (_currentBuffs[i].BuffName.Equals(buff.BuffName))
                stackCount++;

            if (stackCount >= buff.MaxStacks)
                return true;
        }
        return false;
    }

    private IEnumerator RemoveBuffAfterDuration(GunBuff buff)
    {
        yield return new WaitForSeconds(buff.Duration);
        _gunPassiveSkillModifiers.Remove(buff.Stats);
        _currentBuffs.Remove(buff);
    }

    #endregion

}
