﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkSetUp : NetworkBehaviour
{
    public GameObject setUpPrefab;
    public GameObject unitPrefab;
    [SerializeField]
    private int amountUnitsToSpawn = 1;

    public override void OnStartLocalPlayer()
    {
        Debug.Log("This is" + (isServer ? " Server" : " Client"));
        Debug.Log("Client Connection is " + (connectionToServer.isReady ? "ready" : "not ready"));
        CmdSetup();
    }

    [Command]
    private void CmdSetup()
    {
        Debug.Log("Server Connection is " + (connectionToClient.isReady ? "ready" : "not ready"));
        StartCoroutine(SetupWhenConnected());
    }

    private IEnumerator SetupWhenConnected()
    {
        while(!connectionToClient.isReady)
        {
            yield return null;
        }
        Debug.Log("Server Connection is " + (connectionToClient.isReady ? "ready" : "not ready"));
        ServerSetUpPlayer();
    }

    [Server]
    private void ServerSetUpPlayer()
    {
        GameObject go = Instantiate(setUpPrefab, transform.position, Quaternion.identity) as GameObject;        
        NetworkServer.SpawnWithClientAuthority(go, connectionToClient);
        NetworkGameManager.Instance.ServerRegisterPlayer(go);

        // spawn units for players and register them
        for (int i = 0; i < amountUnitsToSpawn; i++)
        {
            Vector3 pos = transform.position;
            Vector3 randomPos = new Vector3(Random.Range(pos.x - 2, pos.x + 2), pos.y, Random.Range(pos.z - 2, pos.z + 2));
            GameObject newUnit = Instantiate(unitPrefab, randomPos, Quaternion.identity) as GameObject;
            NetworkHumanoid humanoid = newUnit.GetComponent<NetworkHumanoid>();
            humanoid.InitializeNavMeshAgent();
            humanoid.navMeshAgent.Warp(randomPos);

            NetworkServer.SpawnWithClientAuthority(newUnit, connectionToClient);
            NetworkGameManager.Instance.ServerRegisterPlayerUnit(go, newUnit);
        }      
    }

}
