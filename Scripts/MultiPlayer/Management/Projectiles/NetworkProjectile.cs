﻿using UnityEngine;
using UnityEngine.Networking;

public class NetworkProjectile : NetworkBehaviour
{
    protected LayerMask collisionMask;
    public int Damage { get; set; }
    public float ArmourPenetration { get; set; }
    public float Radius { get; set; }
    public float velocity = 20.0f;
    public float lifeTime = 5.0f;

    protected Rigidbody rb;

    public override void OnStartServer()
    {
        collisionMask = LayerMask.GetMask("Obstacle", "Ground");
        Invoke("DestroyObject", lifeTime);
        rb = GetComponent<Rigidbody>();
    }

    protected virtual void Update ()
    {
        if(isServer)
        {
            CheckCollisions(rb.velocity);
        }
    }

    [Server]
    protected void CheckCollisions(Vector3 velocity)
    {
        Ray ray = new Ray(transform.position, velocity.normalized);
        float distance = velocity.magnitude * Time.deltaTime;
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, distance + 0.25f, collisionMask, QueryTriggerInteraction.Collide))
            OnObjectHit(hit);
    }

    [Server]
    protected virtual void OnObjectHit(RaycastHit hit)
    {
        NetworkSelectable unit = hit.collider.gameObject.GetComponent<NetworkSelectable>();
        if (unit != null)
            unit.TakeDamage(Damage, ArmourPenetration);

        DestroyObject();
    }

    [Server]
    public void SetProjectile(Vector3 dir, int damage, float armourPenetration, float radius)
    {
        if(rb == null)
            rb = GetComponent<Rigidbody>();

        rb.velocity = dir * velocity;
        Damage = damage;
        ArmourPenetration = armourPenetration;
        Radius = radius;
    }

    [Server]
    protected void DestroyObject()
    {
        NetworkServer.UnSpawn(gameObject);
        Destroy(gameObject);
    }

}
