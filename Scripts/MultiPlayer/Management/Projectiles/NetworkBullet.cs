﻿using UnityEngine;

public class NetworkBullet : NetworkProjectile
{
    public override void OnStartServer()
    {
        collisionMask = LayerMask.GetMask("Target", "Obstacle", "Ground");
        Invoke("DestroyObject", lifeTime);
        rb = GetComponent<Rigidbody>();
    }

}
