﻿using UnityEngine;
using UnityEngine.Networking;

public class NetworkEnemyBullet : NetworkProjectile
{

    public override void OnStartServer()
    {
        collisionMask = LayerMask.GetMask("Silhouette", "Obstacle", "Ground");
        Invoke("DestroyObject", lifeTime);
        rb = GetComponent<Rigidbody>();
    }

}
