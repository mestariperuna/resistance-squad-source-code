﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkExplosion : NetworkProjectile
{
    public LayerMask targetMask;
    private GameObject _explosion;
    private AudioClip _explosionAudio;

    public override void OnStartServer()
    {
        collisionMask = LayerMask.GetMask("Obstacle", "Ground", "Target");
        Invoke("DestroyObject", lifeTime);
        rb = GetComponent<Rigidbody>();
    }

    public override void OnStartClient()
    {
        _explosion = Resources.Load<GameObject>("GFX/Effects/BigExplosionEffect");
        _explosionAudio = Resources.Load<AudioClip>("SFX/GrenadeExplosion");
    }

    protected override void OnObjectHit(RaycastHit hit)
    {
        Collider[] collisions = Physics.OverlapSphere(hit.point, Radius, targetMask);

        for (int i = 0; i < collisions.Length; i++)
        {
            NetworkSelectable ns = collisions[i].GetComponent<NetworkSelectable>();
            if (ns != null)
                ns.TakeDamage(Damage, ArmourPenetration);
        }

        RpcShowEffects();
        DestroyObject();
    }

    [ClientRpc]
    private void RpcShowEffects()
    {
        // Instantiate _explosion visual effect.
        GameObject explosionEffect = Instantiate(_explosion, transform.position, transform.rotation);
        AudioManager.Instance.PlaySound(_explosionAudio, explosionEffect.transform.position);
        Destroy(explosionEffect, 2);
    }
}
