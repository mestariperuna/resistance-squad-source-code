﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkGenerateLoot : NetworkBehaviour
{

    #region Singleton

    static NetworkGenerateLoot _instance;

    public static NetworkGenerateLoot Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<NetworkGenerateLoot>();
            return _instance;
        }
    }

    #endregion

    public int zoneLevel = 1;
    private Object[] _loot;

    public override void OnStartServer()
    {
        if (_loot == null)
            _loot = (Resources.LoadAll("GameItems/NetworkLoot", typeof(Item)));
    }

    [Server]
    public Item GetRandomLoot()
    {
        if(_loot == null)
            _loot = (Resources.LoadAll("GameItems/NetworkLoot", typeof(Item)));

        Item item = Instantiate((Item)_loot[Random.Range(0, _loot.Length)]);

        item.RollStats(zoneLevel);
        return item;
    }

    [Server]
    public Item GetAssaultRifleLoot()
    {
        Item item = Instantiate((Item)Resources.Load<Item>("GameItems/Loot/Assault_Rifle"));
        item.RollStats(zoneLevel);
        return item;
    }

    [Server]
    public ItemDataToSend ServerRequestItemData(Item item)
    {
        ItemDataToSend newData;

        if (item is Weapon)
        {
            Weapon weapon = item as Weapon;
            newData = new ItemDataToSend()
            {
                itemLevel = weapon.ItemLevel,
                itemName = weapon.name,
                description = weapon.description,
                itemType = "Weapon",
                weaponType = weapon.isTwoHander ? "Two-Handed" : "One-Handed",
                equipmentSlot = weapon.equipSlot.ToString(),
                damage = weapon.baseDamage,
                effectiveRange = weapon.effectiveRange,
                attacksPerSecond = weapon.attacksPerSecond
            };
        }
        else if (item is Gear)
        {
            Gear equipment = item as Gear;
            newData = new ItemDataToSend()
            {
                itemLevel = equipment.ItemLevel,
                itemName = equipment.name,
                description = equipment.description,
                itemType = "Equipment",
                equipmentSlot = equipment.equipSlot.ToString(),
                armour = equipment.baseArmor
            };
        }
        else if (item is Resource)
        {
            Resource resource = item as Resource;
            newData = new ItemDataToSend()
            {
                itemLevel = resource.ItemLevel,
                itemName = resource.name,
                description = resource.description,
                itemType = "Resource",
                quantity = resource.quantity
            };
        }
        else
        {
            newData = new ItemDataToSend()
            {
                itemLevel = item.ItemLevel,
                itemName = item.name,
                description = item.description,
                itemType = "Item",
            };
        }
        return newData;
    }

}

public struct ItemDataToSend
{
    public int itemLevel;
    public string itemName;
    public string iconName;
    public string description;
    public string itemType;
    public string weaponType;
    public string equipmentSlot;
    public int armour;
    public int damage;
    public int magSize;
    public float effectiveRange;
    public float attacksPerSecond;
    public float reloadSpeed;
    public int quantity;
}
