﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkLoot : NetworkInteractable
{
    public Item[] Inventory { get; set; }
    public bool generateLoot = false;
    public int amountToGenerate = 1;

    private NetworkIdentity _owner;
    private GameObject _lootBagObj;
    private NetworkLootBag _lootBag;

    #region Initialization

    public override void OnStartAuthority()
    {
        Initialization();
    }

    public override void OnStartServer()
    {
        Initialization();
    }

    private void Initialization()
    {
        if (string.IsNullOrEmpty(objectName))
            objectName = name;
    }

    #endregion

    private void Update()
    {
        if (!isServer)
            return;

        if (!focused && interacted)
        {
            RpcCloseLootBag();
            interacted = false;
        }

        if (focused && !interacted)
        {
            float distance = Vector3.Distance(focuser.position, transform.position);
            if (distance <= radius)
            {
                Interact();
                interacted = true;
            }
        }
    }

    [ClientRpc]
    private void RpcCloseLootBag()
    {
        if(hasAuthority)
            _lootBagObj.SetActive(false);
    }

    public override void Interact()
    {
        if (!isServer)
            return;

        NetworkSelectable selectable = focuser.GetComponent<NetworkSelectable>();

        if(selectable != null)
        {
            selectable.RpcStopUnit();

            NetworkIdentity identity = GetComponent<NetworkIdentity>();
            NetworkConnection conn = selectable.GetComponent<NetworkIdentity>().clientAuthorityOwner;
            identity.AssignClientAuthority(conn);

            if (generateLoot)
            {
                Inventory = new Item[amountToGenerate];

                for (int i = 0; i < Inventory.Length; i++)
                    Inventory[i] = NetworkGenerateLoot.Instance.GetRandomLoot();

                generateLoot = false;
            }
            ServerFindOwnerObject(conn);
        }
    }

    [Server]
    private void ServerFindOwnerObject(NetworkConnection conn)
    {
        _owner = NetworkGameManager.Instance.ServerFindOwnerObject(conn);
        TargetSetDependencies(conn, _owner);
    }

    [TargetRpc]
    private void TargetSetDependencies(NetworkConnection target, NetworkIdentity ownerObject)
    {
        GameObject go = ClientScene.FindLocalObject(ownerObject.netId);
        if (go != null)
        {
            _lootBagObj = go.GetComponent<NetworkInventoryUI>().LootBag;
            _lootBag = go.GetComponent<NetworkLootBag>();
            Tooltip = go.GetComponentInChildren<UITooltip>();
            CmdShowCurrentLoot();
        }
    }

    [Command]
    private void CmdShowCurrentLoot()
    {
        NetworkLootBag bag = NetworkServer.FindLocalObject(_owner.netId).GetComponent<NetworkLootBag>();
        if(bag != null)
        {
            bag.CurrentLoot = this;
            TargetOpenLootBag(_owner.clientAuthorityOwner);
        }
    }

    [TargetRpc]
    private void TargetOpenLootBag(NetworkConnection target)
    {
        Debug.Log("Interacting with " + objectName);
        _lootBagObj.SetActive(true);
        _lootBag.UpdateLootBag();
    }

}
