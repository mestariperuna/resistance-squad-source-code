﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkInteractable : NetworkBehaviour
{

    protected bool interacted;
    protected bool focused;
    protected Transform focuser;

    public string objectName;
    public UITooltip Tooltip { get; set; }

    public float radius = 1.0f;

    public override void OnStartServer()
    {
        Initialization();
    }

    public override void OnStartClient()
    {
        Initialization();
    }

    private void Initialization()
    {
        if (string.IsNullOrEmpty(objectName))
            objectName = name;
    }

    private void Update()
    {
        if (!isServer)
            return;

        if(focused && !interacted)
        {
            float distance = Vector3.Distance(focuser.position, transform.position);
            if(distance <= radius)
            {
                Interact();
                interacted = true;
            }
        }
    }

    public virtual void Interact()
    {
        if (!isServer)
            return;

        Debug.Log("Interacting with " + objectName);

        Animation animation = GetComponent<Animation>();
        if (animation != null)
            animation.Play();
    }

    [Server]
    public void ServerOnFocus(Transform focuserTransform)
    {
        focused = true;
        focuser = focuserTransform;
        interacted = false;
    }

    [Server]
    public void ServerOnDefocus()
    {
        focused = false;
        focuser = null;
    }

    public virtual void OnMouseEnter()
    {
        if(isClient)
        {
            if(Tooltip == null)
            {
                GameObject go = GameObject.Find("NetworkUICanvas");
                if(go != null)
                    Tooltip = go.GetComponent<UITooltip>();
            }
                
            if (Tooltip != null)
                Tooltip.Activate(objectName);
        }
    }

    public virtual void OnMouseExit()
    {
        if(isClient)
        {
            if (Tooltip != null)
                Tooltip.Deactivate();
        }
    }

}
