﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkCollectable : NetworkInteractable
{

    public event System.Action ItemFound;

    public Item CurrentItem { get; set; }
    public MeshFilter MeshFilter { get; set; }
    public MeshRenderer MeshRenderer { get; set; }
    private NetworkInventoryUI inventoryUI;
    public bool isTutorialLoot = false;

    #region Initialization

    public override void OnStartServer()
    {
        Initialization();

        if (isTutorialLoot)
        {
            CurrentItem = NetworkGenerateLoot.Instance.GetAssaultRifleLoot();
        }

        ServerOnCurrentItemChanged();
    }

    public override void OnStartClient()
    {
        StartCoroutine(FindDependencies(0.25f));
        Initialization();
    }

    private void Initialization()
    {
        MeshFilter = GetComponent<MeshFilter>();
        MeshRenderer = GetComponent<MeshRenderer>();

        if (string.IsNullOrEmpty(objectName))
            objectName = name;
    }

    private IEnumerator FindDependencies(float time)
    {
        do
        {
            inventoryUI = FindObjectOfType<NetworkInventoryUI>();
            if (inventoryUI != null)
                Tooltip = inventoryUI.GetComponentInChildren<UITooltip>();

            yield return new WaitForSeconds(time);

        } while (Tooltip == null);
    }

    #endregion

    private void Update()
    {
        if (!isServer)
            return;

        if (focused && !interacted)
        {
            float distance = Vector3.Distance(focuser.position, transform.position);
            if (distance <= radius)
            {
                Interact();
                interacted = true;
            }
        }
    }

    public override void Interact()
    {
        if (!isServer)
            return;
     
        NetworkSelectable selectable = focuser.GetComponent<NetworkSelectable>();

        if(selectable != null)
        {
            selectable.RpcStopUnit();

            if (selectable is NetworkHumanoid)
            {
                NetworkHumanoid humanoid = selectable as NetworkHumanoid;

                if (humanoid.InventoryManager.ServerAddItem(CurrentItem))
                {
                    TargetUpdateInventory(humanoid.GetComponent<NetworkIdentity>().clientAuthorityOwner);

                    if (ItemFound != null)
                        ItemFound();

                    NetworkServer.UnSpawn(gameObject);
                    gameObject.SetActive(false);
                }
            }
        }
    }

    [TargetRpc]
    private void TargetUpdateInventory(NetworkConnection target)
    {
        Debug.Log("Interacting with " + objectName);
        inventoryUI.UpdateInventory();
    }

    public override void OnMouseEnter()
    {
        if(isClient)
        {
            Tooltip.Activate(objectName);
        }
    }

    [Server]
    public void ServerOnCurrentItemChanged()
    {
        if (CurrentItem != null)
            RpcOnCurrentItemChanged(CurrentItem.name);
    }

    [ClientRpc]
    private void RpcOnCurrentItemChanged(string name)
    {
        objectName = name;
    }

}
