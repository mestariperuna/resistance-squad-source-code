﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[Serializable]
public struct NetworkId
{
    public string name;
    public NetworkIdentity netId;

    public NetworkId(string newName, NetworkIdentity newId)
    {
        name = newName;
        netId = newId;
    }
}

public class NetworkGameManager : NetworkBehaviour
{

    #region Singleton

    private static NetworkGameManager _instance;
    public static NetworkGameManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<NetworkGameManager>();
            return _instance;
        }
    }

    #endregion

    private int playerIndex = 1;
    private int unitIndex = 1;
    public List<NetworkId> registeredPlayers;
    public List<NetworkId> registeredUnits;

    public GameObject enemyPrefab;
    public GameObject collectablePrefab;
    [SerializeField]
    private int enemySpawnCount;
    [SerializeField]
    private int itemSpawnCount;
    private GameObject[] _enemySpawnPoints;
    private GameObject[] _itemSpawnPoints;

    public override void OnStartServer()
    {
        registeredPlayers = new List<NetworkId>();
        registeredUnits = new List<NetworkId>();

        _enemySpawnPoints = GameObject.FindGameObjectsWithTag("EnemySpawnPoint");
        _itemSpawnPoints = GameObject.FindGameObjectsWithTag("Collectable");
        ServerSpawnEnemies();
        ServerSpawnItems();
    }

    #region Registration

    [Server]
    public void ServerRegisterPlayer(GameObject newPlayer)
    {
        newPlayer.name = "Player " + playerIndex;
        NetworkId newId = new NetworkId(newPlayer.name, newPlayer.GetComponent<NetworkIdentity>());
        registeredPlayers.Add(newId);

        newPlayer.GetComponent<NetworkPlayerManager>().OnDestroyed += ServerRemoveRegisteredPlayer;
        NetworkMouseSelection selection = newPlayer.GetComponent<NetworkMouseSelection>();

        if (newId.name.Equals("Player 1"))
            selection.currentPlayer = Team.blue;
        else
            selection.currentPlayer = Team.green;
         
        playerIndex++;
    }

    [Server]
    public void ServerRemoveRegisteredPlayer(NetworkIdentity id)
    {
        for (int i = registeredPlayers.Count - 1; i >= 0; i--)
        {
            if (registeredPlayers[i].netId == id)
                registeredPlayers.RemoveAt(i);
        }
    }

    [Server]
    public void ServerRegisterPlayerUnit(GameObject owner, GameObject newUnit)
    {
        newUnit.name = "Player Unit " + unitIndex;
        NetworkId newId = new NetworkId(newUnit.name, newUnit.GetComponent<NetworkIdentity>());
        registeredUnits.Add(newId);

        NetworkMouseSelection selection = owner.GetComponent<NetworkMouseSelection>();
        NetworkSelectable selectable = newUnit.GetComponent<NetworkSelectable>();
        
        if(selection != null && selectable != null)
        {
            selectable.currentTeam = selection.currentPlayer;
            selection.TargetOnUnitFound(newId.netId.clientAuthorityOwner, newId.netId);
            selectable.OnDeath += ServerRemoveRegisteredPlayerUnit;
        }
            
        unitIndex++;
    }

    [Server]
    public void ServerRemoveRegisteredPlayerUnit(NetworkIdentity id)
    {
        for (int i = registeredUnits.Count - 1; i >= 0; i--)
        {
            if (registeredUnits[i].netId == id)
            {
                registeredUnits.RemoveAt(i);
                ServerUpdateClientsAvailableUnits(id);
            }
        }
    }

    [Server]
    private void ServerUpdateClientsAvailableUnits(NetworkIdentity id)
    {
        for (int i = 0; i < registeredPlayers.Count; i++)
        {
            NetworkMouseSelection selection = NetworkServer.FindLocalObject(registeredPlayers[i].netId.netId).GetComponent<NetworkMouseSelection>();
            if(selection != null)
                selection.TargetOnUnitDeath(id.clientAuthorityOwner, id);
        }
    }

    #endregion

    [Server]
    public NetworkIdentity ServerFindOwnerObject(NetworkConnection conn)
    {
        for (int i = 0; i < registeredPlayers.Count; ++i)
        {
            if(registeredPlayers[i].netId.clientAuthorityOwner == conn)
                return registeredPlayers[i].netId;
        }
        return null;
    }

    [Server]
    private void ServerSpawnEnemies()
    {
        for (int i = 0; i < enemySpawnCount; i++)
        {
            GameObject go = Instantiate(enemyPrefab, _enemySpawnPoints[i].transform.position, _enemySpawnPoints[i].transform.rotation);
            NetworkSelectable enemy = go.GetComponent<NetworkSelectable>();
            enemy.currentTeam = Team.red;

            enemy.InitializeNavMeshAgent();
            enemy.navMeshAgent.Warp(_enemySpawnPoints[i].transform.position);
            NetworkServer.Spawn(go);
        }
    }

    [Server]
    private void ServerSpawnItems()
    {
        if(itemSpawnCount - 1 < _itemSpawnPoints.Length)
        {
            for (int i = 0; i < itemSpawnCount; i++)
            {
                GameObject go = Instantiate(collectablePrefab, _itemSpawnPoints[i].transform.position, _itemSpawnPoints[i].transform.rotation);
                go.GetComponent<NetworkCollectable>().CurrentItem = NetworkGenerateLoot.Instance.GetRandomLoot();
                NetworkServer.Spawn(go);
            }
        }
    }

}


