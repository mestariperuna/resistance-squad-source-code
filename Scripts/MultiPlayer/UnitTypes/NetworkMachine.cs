﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;

public class NetworkMachine : NetworkSelectable
{
    public int damage;
    public int damagePerLevel;
    public float attacksPerSecond = 4.0f;

    public float experienceOnDeath = 10;

    [Range(0, 1)]
    public float armourPenetration;

    protected float timeBetweenAttacks;
    protected float nextAttack;

    [SerializeField]
    protected float reloadTime;
    [SerializeField]
    protected int magazineSize;
    [SerializeField]
    protected int currentAmmo;
    protected bool reloading = false;

    public override void OnStartServer()
    {
        _currentHealth = maxHealth;
        timeBetweenAttacks = 1 / attacksPerSecond;
        dead = false;

        viewRange = attackRange + 3f;
        originalPos = transform.position;

        visibleTargets = new List<Transform>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        StartCoroutine(FindTargetsWithDelay(0.25f));

        _dyingPrefab = Resources.Load<GameObject>("Units/" + dyingPrefabName);
    }

    protected override void ApplyLevelMultiplier()
    {
        if (level > maxLevel)
            level = maxLevel;

        defence += (level - 1) * defencePerLevel;
        maxHealth += (level - 1) * healthPerLevel;
        damage += (level - 1) * damagePerLevel;
    }

    [Server]
    protected void ApplyExperience(float xp)
    {
        Collider[] experienceRange = Physics.OverlapSphere(transform.position, 8.0f, targetMask);

        for (int i = 0; i < experienceRange.Length; i++)
        {
            NetworkHumanoid humanoid = experienceRange[i].GetComponent<NetworkHumanoid>();
            if (humanoid != null)
                humanoid.GainExperience(xp);
        }
    }

    protected override void Die()
    {
        GameObject dyingUnit = Instantiate(_dyingPrefab, transform.position, transform.rotation);

        ApplyExperience(experienceOnDeath);

        NetworkServer.Spawn(dyingUnit);
        NetworkServer.UnSpawn(gameObject);
        base.Die();
    }
}
