﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;

[RequireComponent(typeof(NavMeshAgent))]
public class NetworkSelectable : NetworkBehaviour
{

    #region Variables

    public event Action<NetworkIdentity> OnDeath;
    public event Action<float> OnHealthChanged;

    public enum State { idling, moving, combat, attacking, patrolling };

    protected State _currentState;
    public State CurrentState { get { return _currentState; } }

    [SyncVar]
    public Team currentTeam;
    public Sprite icon;
    public Transform eyesTransform;

    [Header("Masks")]
    public LayerMask targetMask;
    public LayerMask obstacleMask;

    [Header("Chase")]
    public float chaseDistance = 5f;
    public float chaseDuration = 5f;
    protected float chaseTimer;

    [Header("Health")]
    public int maxHealth;
    [SyncVar]
    protected int _currentHealth;
    public int CurrentHealth { get { return _currentHealth; } }

    [Header("Defence")]
    public int defence;
    [Range(0, 1)]
    public float damageReductionPercent;

    public float attackRange = 6.0f;
    public float viewRange;
    public float movementSpeed = 1;
    
    [Header("Level")]
    public int level = 1;
    public int maxLevel = 10;
    public int healthPerLevel;
    public int defencePerLevel;
    
    public string dyingPrefabName;
    protected GameObject _dyingPrefab;
    protected bool dead;
    public bool Casting { get; set; }

    public Animator animator { get; set; }
    public NavMeshAgent navMeshAgent { get; set; }
    public Transform target { get; set; }
    
    public Vector3 originalPos { get; set; }

    protected NetworkInteractable focus;
    protected List<Transform> visibleTargets;

    #endregion

    #region Initialization

    public override void OnStartServer()
    {
        _currentHealth = maxHealth;
        dead = false;

        viewRange = attackRange + viewRange;
        originalPos = transform.position;

        visibleTargets = new List<Transform>();
        StartCoroutine(FindTargetsWithDelay(0.25f));

        _dyingPrefab = Resources.Load<GameObject>("Units/" + dyingPrefabName);
        InitializeNavMeshAgent();

        ApplyLevelMultiplier();
    }

    public override void OnStartClient()
    {
        InitializeNavMeshAgent();
    }

    #endregion

    public void InitializeNavMeshAgent()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    #region FindVisibleTargets

    protected IEnumerator FindTargetsWithDelay(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            FindVisibleTargets();
        }
    }

    protected void FindVisibleTargets()
    {
        visibleTargets.Clear();
        Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, viewRange, targetMask);

        for (int i = 0; i < targetsInViewRadius.Length; i++)
        {
            Transform target = targetsInViewRadius[i].transform;
            Vector3 dirToTarget = (target.position - transform.position).normalized;
            float distToTarget = Vector3.Distance(transform.position, target.position);

            if (!Physics.Raycast(eyesTransform.position, dirToTarget, distToTarget, obstacleMask))
                visibleTargets.Add(target);
        }
    }

    #endregion

    private void Update()
    {
        if (!isServer)
            return;

        switch(_currentState)
        {
            case State.idling:
                CheckSurroundings();
                break;
            case State.combat:
                ChaseTarget();
                break;
            case State.patrolling:
                CheckSurroundings();
                Patrol();
                break;
            case State.moving:
                AtDestination();
                break;
            default:
                break;
        }
    }

    #region StateFunctions

    [Server]
    protected virtual void CheckSurroundings()
    {
        if (visibleTargets.Count > 0)
        {
            target = visibleTargets[0];
            for (int i = visibleTargets.Count - 1; i >= 0; i--)
            {
                // clean the target list
                if (!visibleTargets[i].gameObject.activeInHierarchy)
                    visibleTargets.Remove(visibleTargets[i]);
                else
                {
                    // calculate distance between current target and list target
                    float dist1 = Vector3.Distance(transform.position, target.position);
                    float dist2 = Vector3.Distance(transform.position, visibleTargets[i].position);

                    if (dist1 > dist2) // choose the closest target
                        target = visibleTargets[i];
                }
            }
            originalPos = transform.position;
            _currentState = State.combat;
        }
    }

    [Server]
    protected virtual void ChaseTarget()
    {
        if (navMeshAgent.enabled && !Casting)
        {
            // check that the target is alive
            if (!target.gameObject.activeInHierarchy)
                _currentState = State.idling;
            else
            {
                Vector3 dirToTarget = (target.position - transform.position).normalized;
                float distToTarget = Vector3.Distance(transform.position, target.position);

                // a raycast to check that the target isn't behind an obstacle
                if (!Physics.Raycast(eyesTransform.position, dirToTarget, distToTarget, obstacleMask) && distToTarget <= attackRange)
                {
                    // stop the unit and look at target                    
                    navMeshAgent.isStopped = true;
                    RpcStopUnit();

                    Vector3 lookVector = new Vector3(target.position.x, transform.position.y, target.position.z);
                    transform.rotation = Quaternion.LookRotation(lookVector - transform.position);

                    Attack();
                    chaseTimer = 0;
                }
                else   // if target is out of range chase it
                {
                    navMeshAgent.isStopped = false;
                    chaseTimer += Time.deltaTime;

                    // check the distance and timer, so we don't chase forever
                    if (chaseTimer >= chaseDuration && distToTarget >= chaseDistance)
                    {
                        // return to position where we started chasing
                        _currentState = State.idling;
                        RpcMoveUnit(originalPos, movementSpeed);
                    }
                    else
                        RpcMoveUnit(target.position, movementSpeed);
                }
            }
        }
    }

    [Server]
    protected virtual void AtDestination()
    {
        if (navMeshAgent.enabled)
        {
            navMeshAgent.isStopped = false;
            if (!navMeshAgent.pathPending)
            {
                if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
                {
                    if (!navMeshAgent.hasPath || navMeshAgent.velocity.sqrMagnitude == 0f)
                        _currentState = State.idling;
                }
            }
        }
    }

    [Server]
    protected virtual void Patrol()
    {

    }

    [Server]
    protected virtual void Attack()
    {

    }

    #endregion

    #region Commands

    [Command]
    public void CmdMoveUnit(Vector3 point)
    {
        if(navMeshAgent.enabled && !Casting)
        {
            _currentState = State.moving;
            navMeshAgent.isStopped = false;
            RpcMoveUnit(point, movementSpeed);
        }
    }
    
    [ClientRpc]
    public void RpcMoveUnit(Vector3 point, float speed)
    {
        navMeshAgent.speed = speed;
        navMeshAgent.SetDestination(point);
    }

    [Command]
    public void CmdStopUnit()
    {
        if (navMeshAgent.enabled && !Casting)
        {
            _currentState = State.idling;
            RpcStopUnit();
        }
    }

    [ClientRpc]
    public void RpcStopUnit()
    {
        navMeshAgent.SetDestination(transform.position);
    }

    [Command]
    public void CmdSetTarget(NetworkIdentity newTarget)
    {
        GameObject targetObj = NetworkServer.FindLocalObject(newTarget.netId);
        if(targetObj != null)
        {
            target = targetObj.transform;
            _currentState = State.combat;
        }
    }
    
    [Command]
    public void CmdSetFocus(NetworkIdentity newFocus)
    {
        if (navMeshAgent.enabled && !Casting)
        {
            GameObject targetObj = NetworkServer.FindLocalObject(newFocus.netId);
            NetworkInteractable target = targetObj.GetComponent<NetworkInteractable>();
            if(target != focus)
            {
                if (focus != null)
                    focus.ServerOnDefocus();
                focus = target;
            }
            focus.ServerOnFocus(transform);
     
            _currentState = State.moving;
            RpcMoveUnit(target.transform.position, movementSpeed);
        }
    }

    [Command]
    public void CmdRemoveFocus()
    {
        if (focus != null)
            focus.ServerOnDefocus();
        focus = null;
    }

    #endregion

    #region DamagingFunctions

    [Server]
    public virtual void TakeDamage(float damageTaken, float armourPenetration)
    {
        if (!dead)
        {
            float damageReduction = defence * damageReductionPercent - armourPenetration;
            int damage = (damageReduction < 1) ? Mathf.RoundToInt(damageTaken * (1 - damageReduction)) : 0;
            _currentHealth -= damage;

            float percentage = Mathf.Clamp01((float)_currentHealth / maxHealth);
            RpcSetHealthBarScale(percentage);

            if (_currentHealth <= 0)
                Die();
        }
    }

    [ClientRpc]
    private void RpcSetHealthBarScale(float percentage)
    {
        GetComponentInChildren<ChangeColorByScale>().SetHealthVisual(percentage);

        if (OnHealthChanged != null)
            OnHealthChanged(percentage);
    }

    [Server]
    protected virtual void Die()
    {
        dead = true;
        _currentHealth = 0;
        Debug.Log(name + " Dieded");

        if (OnDeath != null)
            OnDeath(GetComponent<NetworkIdentity>());

        gameObject.SetActive(false);
    }

    #endregion

    [Server]
    protected virtual void ApplyLevelMultiplier()
    {
        if (level > maxLevel)
            level = maxLevel;

        defence += (level - 1) * defencePerLevel;
        maxHealth += (level - 1) * healthPerLevel;
    }

    [Server]
    public SelectableDataToSend ServerRequestSelectableData()
    {
        SelectableDataToSend data;

        if(this is NetworkHumanoid)
        {
            NetworkHumanoid humanoid = this as NetworkHumanoid;

            data = new SelectableDataToSend()
            {
                name = humanoid.name,
                selectableType = "Humanoid",
                level = humanoid.level,
                regenRate = (humanoid.regenAmount + humanoid.GetBonusUnitStat(x => x.HealthRegenBonus)) / (humanoid.regenTimer - humanoid.GetBonusUnitStat(x => x.HealthRegenTimerBonus)),
                health = humanoid.maxHealth,
                defence = humanoid.defence,
                accuracy = humanoid.accuracy + humanoid.GunController.GetBonusGunStat(x => x.AccuracyBonus),
                experience = humanoid.experience,
                experienceCap = humanoid.experienceCap
            };

            Weapon weapon = humanoid.GunController.EquippedWeapon;
            if (weapon != null)
            {
                data.hasWeapon = true;
                int weaponBonus = weapon.isTwoHander ? humanoid.GunController.GetBonusGunStat(x => x.TwoHandDamageBonus) : humanoid.GunController.GetBonusGunStat(x => x.OneHandDamageBonus);
                data.damage = weapon.baseDamage + humanoid.GunController.GetBonusGunStat(x => x.DamageBonus) + weaponBonus;
                data.fireRate = weapon.attacksPerSecond;
                data.critChance = humanoid.GunController.critChance + humanoid.GunController.GetBonusGunStat(x => x.CriticalChanceBonus);
                data.critDamage = humanoid.GunController.critDamage + humanoid.GunController.GetBonusGunStat(x => x.CriticalDamageBonus);
                data.effectiveRange = weapon.effectiveRange + humanoid.GunController.GetBonusGunStat(x => x.EffectiveRange);
            }
        }
        else if(this is NetworkMachine)
        {
            NetworkMachine machine = this as NetworkMachine;

            data = new SelectableDataToSend()
            {
                name = machine.name,
                selectableType = "Machine",
                level = machine.level,
                health = machine.maxHealth,
                defence = machine.defence,
                damage = machine.damage,
                fireRate = machine.attacksPerSecond,
                effectiveRange = machine.attackRange
            };
        }
        else
        {
            data = new SelectableDataToSend()
            {
                name = name,
                selectableType = "Selectable",
                level = level,
                health = maxHealth,
                defence = defence
            };
        }
        return data;
    }

}

public struct SelectableDataToSend
{
    public string name;
    public string selectableType;
    public int level;
    public float regenRate;
    public int health;
    public int defence;
    public float accuracy;
    public float experience;
    public float experienceCap;
    public bool hasWeapon;
    public int damage;
    public float fireRate;
    public float critChance;
    public float critDamage;
    public float effectiveRange;
}