﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;

[RequireComponent(typeof(NetworkGunController))]
[RequireComponent(typeof(NetworkInventoryManager))]
[RequireComponent(typeof(NetworkEquipmentManager))]
[RequireComponent(typeof(NetworkSkillManager))]
[RequireComponent(typeof(NetworkTalentSystem))]
public class NetworkHumanoid : NetworkSelectable
{   
    public NetworkGunController GunController { get; private set; }
    public NetworkInventoryManager InventoryManager { get; private set; }
    public NetworkEquipmentManager EquipmentManager { get; private set; }
    public NetworkSkillManager SkillManager { get; private set; }
    public NetworkTalentSystem TalentManager { get; private set; }

    public float accuracy;
    [Range(0, 0.1f)]
    public float accuracyPerLevel;

    [Header("Regeneration")]
    public int regenAmount;
    public float regenTimer;

    public float experience { get; private set; }
    public float experienceCap = 100f;
    [Range(1, 2)]
    public float experienceCapMultiplier = 1;
    public int talentPointsPerLevel = 4;

    private List<UnitBonusStats> _unitPassiveSkillModifiers;
    private List<UnitBuff> _currentBuffs;

    #region Initalization

    public override void OnStartServer()
    {
        InitializeComponents();

        _currentHealth = maxHealth;
        experience = 0;
        dead = false;

        viewRange = attackRange + viewRange;
        originalPos = transform.position;

        visibleTargets = new List<Transform>();

        _unitPassiveSkillModifiers = new List<UnitBonusStats>();
        _currentBuffs = new List<UnitBuff>();

        StartCoroutine(FindTargetsWithDelay(0.25f));
        StartCoroutine(RegenerateOverTime(regenTimer));

        EquipmentManager.OnEquipmentChanged += OnEquip;
        SkillManager.OnSkillUnlocked += OnPassiveSkillUnlocked;
        SkillManager.OnSkillRemoved += OnPassiveSkillRemoved;
        SkillManager.OnSkillUpgraded += OnSkillUpgraded;

        GunController.Accuracy = accuracy;

        ApplyLevelMultiplier();
    }

    public override void OnStartClient()
    {
        InitializeComponents();
    }

    private void InitializeComponents()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        GunController = GetComponent<NetworkGunController>();
        InventoryManager = GetComponent<NetworkInventoryManager>();
        EquipmentManager = GetComponent<NetworkEquipmentManager>();
        SkillManager = GetComponent<NetworkSkillManager>();
        TalentManager = GetComponent<NetworkTalentSystem>();
    }

    #endregion

    private void Update()
    {
        if(hasAuthority)
            HandleAnimations();

        if (!isServer)
            return;

        switch (CurrentState)   // act accordingly to currentState
        {
            case State.idling:
                CheckSurroundings();
                break;
            case State.combat:
                ChaseTarget();
                break;
            case State.patrolling:
                CheckSurroundings();
                Patrol();
                break;
            case State.moving:
                RpcSetFiringAnimation(false);
                AtDestination();
                break;
        }
    }

    protected override void Attack()
    {
        RpcSetFiringAnimation(true);
        GunController.Shoot(target);
    }

    public void OnPassiveSkillTrigger()
    {
        RpcSetFiringAnimation(true);
        GunController.OnPassiveSkillTrigger(target);
    }

    #region Regeneration

    IEnumerator RegenerateOverTime(float time)
    {
        while (true)
        {
            yield return new WaitForSeconds(time - GetBonusUnitStat(x => x.HealthRegenTimerBonus));
            Regenerate();
        }
    }

    private void Regenerate()
    {
        if (isServer)
        {
            int regenbonus = regenAmount;
            if (CurrentHealth < maxHealth)
                _currentHealth = (_currentHealth + regenbonus >= maxHealth) ? maxHealth : _currentHealth + regenbonus;
        }
    }

    public void Heal(int amount)
    {
        if (isServer)
            _currentHealth = (_currentHealth + amount >= maxHealth) ? maxHealth : _currentHealth + amount;
    }

    #endregion

    [Server]
    public void GainExperience(float amount)
    {
        experience += amount;
        if (experience >= experienceCap)
            OnLevelUp();
    }

    [Server]
    private void OnLevelUp()
    {
        if (level < maxLevel)
        {
            accuracy += accuracyPerLevel;
            GunController.Accuracy = accuracy;
            TalentManager.ServerOnLevelUp(talentPointsPerLevel);

            defence += defencePerLevel;
            maxHealth += healthPerLevel;
            _currentHealth = maxHealth;

            experience = experience % experienceCap;
            experienceCap *= experienceCapMultiplier;
            level++;
        }
    }

    #region event callbacks

    [Server]
    private void OnEquip(Equipment newItem, Equipment oldItem)
    {
        if (oldItem != null)
        {
            if (oldItem is Gear)
            {
                Gear gear = oldItem as Gear;
                defence -= gear.baseArmor;
                if (gear.equipSlot == GearSlot.OffHand) // Remove shield armor bonus.
                    defence -= GetBonusUnitStat(x => x.ShieldArmorBonus);
            }

            if (oldItem is Weapon)
            {
                GunController.RemoveWeapon();
                RpcSetGunAnimation(false);
            }
                
        }

        if (newItem != null)
        {
            if (newItem is Gear)
            {
                Gear gear = newItem as Gear;
                defence += gear.baseArmor;
                if (gear.equipSlot == GearSlot.OffHand) // Add shield armor bonus.
                    defence += GetBonusUnitStat(x => x.ShieldArmorBonus);
            }

            if (newItem is Weapon)
            {
                Weapon weapon = newItem as Weapon;
                GunController.EquipWeapon(weapon);
                RpcSetGunAnimation(true);
            }
        }
    }

    [ClientRpc]
    private void RpcSetGunAnimation(bool hasGun)
    {
        animator.SetBool("HasGun", hasGun);
    }

    [ClientRpc]
    private void RpcSetFiringAnimation(bool firing)
    {
        animator.SetBool("Firing", firing);
        if(firing)
            animator.SetTrigger("Fire");
    }

    [Server]
    private void OnPassiveSkillUnlocked(PassiveSkill skill)
    {
        if (skill != null)
        {
            // Add passive skill modifiers to a list.
            if (skill.UnitBonusStatModifier != null)
            {
                // Remove old health, armour and range bonuses
                ServerRemoveBonusStats();
                _unitPassiveSkillModifiers.Add(skill.UnitBonusStatModifier);
                // Add new health, armour and range bonuses from that list.
                ServerAddBonusStats();
            }

            if (skill.GunBonusStatModifier != null)
                GunController.OnPassiveSkillUnlocked(skill.GunBonusStatModifier);
        }
    }

    [Server]
    private void OnPassiveSkillRemoved(PassiveSkill skill)
    {
        if (skill != null)
        {
            if (skill.UnitBonusStatModifier != null)
            {
                if (_unitPassiveSkillModifiers.Contains(skill.UnitBonusStatModifier))
                {
                    ServerRemoveBonusStats();
                    _unitPassiveSkillModifiers.Remove(skill.UnitBonusStatModifier);
                    ServerAddBonusStats();
                }
            }

            if (skill.GunBonusStatModifier != null)
                GunController.OnPassiveSkillRemoved(skill.GunBonusStatModifier);
        }
    }

    [Server]
    private void OnSkillUpgraded(PassiveSkill skill)
    {
        if (skill != null)
        {
            ServerRemoveBonusStats();
            skill.UpgradeSkill();
            ServerAddBonusStats();
        }
    }

    #endregion

    #region BonusStats

    [Server]
    public void ServerRemoveBonusStats()
    {
        maxHealth -= GetBonusUnitStat(x => x.HealthBonus);
        defence -= GetBonusUnitStat(x => x.ArmorBonus);
        regenAmount -= GetBonusUnitStat(x => x.HealthRegenBonus);
        RemoveBonusRanges();
    }

    [Server]
    public void ServerAddBonusStats()
    {        
        maxHealth += GetBonusUnitStat(x => x.HealthBonus);
        defence += GetBonusUnitStat(x => x.ArmorBonus);
        regenAmount += GetBonusUnitStat(x => x.HealthRegenBonus);
        AddBonusRanges();
    }

    private void RemoveBonusRanges()
    {
        float rangeBonus = GetBonusUnitStat(x => x.AttackRangeBonus);
        attackRange -= rangeBonus;
        viewRange -= rangeBonus;
    }

    private void AddBonusRanges()
    {
        float rangeBonus = GetBonusUnitStat(x => x.AttackRangeBonus);
        attackRange += rangeBonus;
        viewRange += rangeBonus;
    }

    // Get bonus stats based on selector.
    public float GetBonusUnitStat(Func<UnitBonusStats, float> selector)
    {
        float stat = 0;
        for (int i = 0; i < _unitPassiveSkillModifiers.Count; i++)
        {
            stat += selector(_unitPassiveSkillModifiers[i]);
        }
        return stat;
    }

    public int GetBonusUnitStat(Func<UnitBonusStats, int> selector)
    {
        int stat = 0;
        for (int i = 0; i < _unitPassiveSkillModifiers.Count; i++)
        {
            stat += selector(_unitPassiveSkillModifiers[i]);
        }
        return stat;
    }

    #endregion

    #region buffs

    [Server]
    public void ServerAddBuff(UnitBuff buff)
    {
        if (buff != null)
        {
            if (!CurrentBuffsHasMaxStacks(buff))
            {
                _unitPassiveSkillModifiers.Add(buff.Stats);
                _currentBuffs.Add(buff);
                StartCoroutine(RemoveBuffAfterDuration(buff));
            }
        }
    }

    // check if the current buffs list has max amount of the buffs max stacks
    private bool CurrentBuffsHasMaxStacks(UnitBuff buff)
    {
        int stackCount = 0;
        for (int i = 0; i < _currentBuffs.Count; i++)
        {
            if (_currentBuffs[i].BuffName.Equals(buff.BuffName))
                stackCount++;

            if (stackCount >= buff.MaxStacks)
                return true;
        }
        return false;
    }

    private IEnumerator RemoveBuffAfterDuration(UnitBuff buff)
    {
        yield return new WaitForSeconds(buff.Duration);
        _unitPassiveSkillModifiers.Remove(buff.Stats);
        _currentBuffs.Remove(buff);
    }

    #endregion

    protected override void ApplyLevelMultiplier()
    {
        if (level > maxLevel)
            level = maxLevel;

        defence += (level - 1) * defencePerLevel;
        maxHealth += (level - 1) * healthPerLevel;

        experienceCap *= Mathf.Pow(experienceCapMultiplier, (level - 1));
        accuracy += (level - 1) * accuracyPerLevel;

        TalentManager.ServerOnLevelUp((level - 1) * talentPointsPerLevel);
    }

    public override void TakeDamage(float damageTaken, float armourPenetration)
    {
        if (!dead)
        {
            // See if this unit has the "Rage" skill.
            Rage rage = SkillManager.HasRageSkill();
            if (rage != null)
                GunController.ServerAddBuff(rage.Buff);
        }

        base.TakeDamage(damageTaken, armourPenetration);
    }

    protected override void Die()
    {
        if (SkillManager.HasMadDocSkill())
            return;

        RpcInstantiateDyingPrefab(EquipmentManager.female);
        NetworkServer.Destroy(gameObject);

        base.Die();
    }

    [ClientRpc]
    private void RpcInstantiateDyingPrefab(bool female)
    {
        if(female)
            Instantiate(Resources.Load<GameObject>("Units/DyingUnitFemaleNetworked"), transform.position, transform.rotation);
        else
            Instantiate(Resources.Load<GameObject>("Units/DyingUnitMaleNetworked"), transform.position, transform.rotation);
    }

    private void HandleAnimations()
    {
        float animationSpeedPercent = navMeshAgent.velocity.magnitude / navMeshAgent.speed;
        animator.SetFloat("speedPercent", animationSpeedPercent, 0.1f, Time.deltaTime);
    }

}
