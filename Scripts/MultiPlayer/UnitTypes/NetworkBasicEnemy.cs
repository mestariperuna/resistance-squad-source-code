﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;

public class NetworkBasicEnemy : NetworkMachine
{
    #region Variables

    [HideInInspector]
    public List<Transform> wayPoints = new List<Transform>();
    public Transform[] bulletSpawnPoints;
    private int curWaypoint = 0;

    private GameObject _projectileObject;
    private GameObject _muzzleEffect;
    private AudioClip _firingSound;
    private GameObject _explosion;
    #endregion

    #region Initialization

    public override void OnStartClient()
    {
        _firingSound = Resources.Load<AudioClip>("SFX/gatlingfire");
        _muzzleEffect = Resources.Load<GameObject>("GFX/Effects/MuzzleFlashEffect");
        _explosion = Resources.Load<GameObject>("GFX/Effects/PlasmaExplosionEffect");
    }

    public override void OnStartServer()
    {
        _currentHealth = maxHealth;
        timeBetweenAttacks = 1 / attacksPerSecond;
        dead = false;

        viewRange = attackRange + 3f;
        originalPos = transform.position;

        visibleTargets = new List<Transform>();
        StartCoroutine(FindTargetsWithDelay(0.25f));

        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();

        _projectileObject = Resources.Load<GameObject>("GFX/Projectiles/NetworkEnemyBullet");       
        _dyingPrefab = Resources.Load<GameObject>("Units/" + dyingPrefabName);

        InitializeWayPoints();
    }

    private void InitializeWayPoints()
    {
        wayPoints.Clear();
        GameObject[] waypointClusters = GameObject.FindGameObjectsWithTag("Waypoint");
        float patrolRange = 3.0f;

        foreach (GameObject waypointCluster in waypointClusters)
        {
            if (Vector3.Distance(transform.position, waypointCluster.transform.position) <= patrolRange)
            {
                for (int i = 0; i < waypointCluster.transform.childCount; i++)
                    wayPoints.Add(waypointCluster.transform.GetChild(i));

                break;
            }
        }

        if (wayPoints.Count > 0)
            _currentState = State.patrolling;
        else
            _currentState = State.idling;
    }

    #endregion

    private void Update()
    {
        if (!isServer)
            return;

        switch (_currentState)   // act accordingly to currentState
        {
            case State.idling:
                CheckSurroundings();
                HandleAnimations(0);
                break;
            case State.combat:
                ChaseTarget();
                HandleAnimations(2);
                break;
            case State.patrolling:
                CheckSurroundings();
                Patrol();
                HandleAnimations(0);
                break;
            case State.moving:
                AtDestination();
                HandleAnimations(0);
                break;
        }


        if (!reloading && currentAmmo <= 0)
            Reload();
    }

    #region StateFunctions

    [Server]
    protected override void Attack()
    {
        if (Time.time > nextAttack && !reloading)
        {
            nextAttack = Time.time + timeBetweenAttacks;
            foreach (Transform bulletSpawn in bulletSpawnPoints)
            {
                animator.SetTrigger("Fire");
                GameObject projectileObj = Instantiate(_projectileObject, bulletSpawn.position, Quaternion.identity);
                NetworkProjectile projectile = projectileObj.GetComponent<NetworkProjectile>();
                if (projectile != null)
                {
                    Vector3 dir = (target.GetComponent<Collider>().bounds.center - bulletSpawn.position).normalized;
                    projectile.SetProjectile(dir, damage, armourPenetration, 1);
                    RpcShowEffects(bulletSpawn.position);
                    --currentAmmo;
                }
                NetworkServer.Spawn(projectileObj);
            }
        }
    }

    [ClientRpc]
    private void RpcShowEffects(Vector3 effectPos)
    {
        AudioManager.Instance.PlaySound(_firingSound, effectPos);
        GameObject go = Instantiate(_muzzleEffect, effectPos, Quaternion.identity);
        Destroy(go, 0.2f);
    }

    private void Reload()
    {
        StartCoroutine(AnimateReload(reloadTime));
    }

    IEnumerator AnimateReload(float reloadtime)
    {
        reloading = true;
        yield return new WaitForSeconds(reloadtime);
        reloading = false;
        currentAmmo = magazineSize;
    }

    protected override void Patrol()
    {
        if (wayPoints.Count > 0)
        {
            // to avoid calculating new path every frame
            if ((!navMeshAgent.hasPath || navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance) && !navMeshAgent.pathPending)
            {
                navMeshAgent.velocity = Vector3.zero;
                navMeshAgent.isStopped = true;
                NextWayPoint();
            }
        }
        else
            _currentState = State.idling;
    }

    private void NextWayPoint()
    {
        navMeshAgent.SetDestination(wayPoints[curWaypoint].position);
        navMeshAgent.isStopped = false;
        curWaypoint = (curWaypoint + 1) % wayPoints.Count;
    }

    #endregion

    private void HandleAnimations(int animstate)
    {
        animator.SetInteger("animationState", animstate);
    }
    protected override void Die()
    {
        GameObject explosionEffect = Instantiate(_explosion, transform.position, transform.rotation) as GameObject;
        AudioManager.Instance.PlaySound(Resources.Load<AudioClip>("SFX/spark"), transform.position);
        base.Die();
        Destroy(explosionEffect, 2);
    }

}
