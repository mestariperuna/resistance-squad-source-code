﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkActivateInventory : MonoBehaviour
{
    private Button _button;

    private void Start()
    {
        NetworkPlayerManager manager = GetComponentInParent<NetworkPlayerManager>();
        if (manager != null)
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(() => manager.ActivateInventory());
        }
        else
            Debug.Log("Missing PlayerManager component!");
    }
}
