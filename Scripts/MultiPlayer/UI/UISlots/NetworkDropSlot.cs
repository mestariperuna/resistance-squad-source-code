﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class NetworkDropSlot : MonoBehaviour, IDropHandler
{
    public event Action<NetworkItemSlot, NetworkEquipmentSlot> OnGroundDrop;

    public void OnDrop(PointerEventData eventData)
    {
        NetworkItemData droppedItem = eventData.pointerDrag.GetComponent<NetworkItemData>();
        if(droppedItem != null)
        {
            NetworkEquipmentSlot equipmentSlot = droppedItem.OriginalParent.GetComponent<NetworkEquipmentSlot>();
            NetworkItemSlot itemSlot = droppedItem.OriginalParent.GetComponent<NetworkItemSlot>();
            OnGroundDrop(itemSlot, equipmentSlot);
        }            
    }

}
