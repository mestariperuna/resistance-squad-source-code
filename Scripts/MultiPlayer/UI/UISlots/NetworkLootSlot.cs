﻿using UnityEngine;
using UnityEngine.EventSystems;

public class NetworkLootSlot : MonoBehaviour, IDropHandler
{
    public event System.Action<NetworkLootSlot, NetworkLootSlot> OnItemSwap;
    public int SlotId { get; set; }

    public void OnDrop(PointerEventData eventData)
    {
        NetworkItemData droppedItem = eventData.pointerDrag.GetComponent<NetworkItemData>();
        if(droppedItem != null)
        {
            NetworkLootSlot fromSlot = droppedItem.OriginalParent.GetComponent<NetworkLootSlot>();
            if(fromSlot != null)
            {
                if (OnItemSwap != null)
                    OnItemSwap(this, fromSlot);
            }
        }
    }
}
