﻿using UnityEngine;
using UnityEngine.EventSystems;

public class NetworkEquipmentSlot : MonoBehaviour, IDropHandler
{
    public GearSlot slot;
    public event System.Action<NetworkLootSlot> OnItemEquipFromLoot;
    public event System.Action<NetworkItemSlot> OnItemEquipFromInventory;

    public void OnDrop(PointerEventData eventData)
    {
        NetworkItemData droppedItem = eventData.pointerDrag.GetComponent<NetworkItemData>();

        if (droppedItem != null)
        {
            NetworkItemSlot itemSlot = droppedItem.OriginalParent.GetComponent<NetworkItemSlot>();
            NetworkLootSlot lootSlot = droppedItem.OriginalParent.GetComponent<NetworkLootSlot>();

            if(droppedItem.Item.Value.equipmentSlot.Equals(slot.ToString()))
            {
                if(itemSlot != null)
                {
                    if(OnItemEquipFromInventory != null)
                        OnItemEquipFromInventory(itemSlot);
                }

                else if(lootSlot != null)
                {
                    if (OnItemEquipFromLoot != null)
                        OnItemEquipFromLoot(lootSlot);
                }
            }
        }

    }
}
