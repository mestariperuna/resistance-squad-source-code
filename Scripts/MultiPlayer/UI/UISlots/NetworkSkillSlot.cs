﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkSkillSlot : MonoBehaviour
{
    public System.Action<int> OnSkillClicked;
    public SkillDataToSend? CurrentSkill { get; private set; }
    public int SlotIndex { get; set; }

    private Button _button;

    private void Awake()
    {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(() => OnSkillClick());
    }

    public void UpdateSkillSlot(SkillDataToSend skill)
    {
        CurrentSkill = skill;
        _button.image.enabled = true;
        _button.image.sprite = Resources.Load<Sprite>("Sprites/SkillIcons/" + skill.iconName);
    }

    public void ClearSkillSlot()
    {
        CurrentSkill = null;
        _button.image.enabled = false;
    }

    private void OnSkillClick()
    {
        if(CurrentSkill.HasValue)
        {
            if (OnSkillClicked != null)
                OnSkillClicked(SlotIndex);
        }

    }
}
