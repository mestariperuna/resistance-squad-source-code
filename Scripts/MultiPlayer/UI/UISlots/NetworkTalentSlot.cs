﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

// TalentSlot knows which talent it has and all information that talent holds. 

public class NetworkTalentSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public System.Action<TalentDataToSend> OnTalentClicked;
    public TalentDataToSend? CurrentTalent { get; private set; }
    private Text _pointsText;
    private Image _lockImage;

    private Button _button;
    private UITooltip _tooltip;
    private Material _greyscaleMat;

    private void Awake()
    {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(() => OnTalentClick());
        _pointsText = GetComponentInChildren<Text>();

        _tooltip = FindObjectOfType<UITooltip>();
        _greyscaleMat = _button.image.material;

        Transform locktransform = transform.Find("Locked");
        if (locktransform != null)
            _lockImage = locktransform.GetComponent<Image>();
    }

    public void UpdateTalentSlot(TalentDataToSend talent, bool available)
    {
        CurrentTalent = talent;
        _button.image.enabled = true;
        _button.image.sprite = Resources.Load<Sprite>("Sprites/SkillIcons/" + talent.iconName);

        // Draw another image for Active talents.
        if (talent.active)
        {
            if (talent.tree.Equals("Marauder"))
                _button.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/MarauderActive");
            if (talent.tree.Equals("Jaeger"))
                _button.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/JaegerActive");
            if (talent.tree.Equals("CombatMedic"))
                _button.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/MedicActive");
        }
        else
        {
            _button.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/SkillNA");
        }

        if (available)
            TalentAvailable();
        else
            EnableLockImage();

        _pointsText.text = talent.rank.ToString() + "/" + talent.maxRank.ToString();

    }
    
    public void ClearTalentSlot()
    {
        CurrentTalent = null;
        _button.image.enabled = false;

        if (_lockImage != null)
            _lockImage.enabled = false;
    }

    private void OnTalentClick()
    {
        if(CurrentTalent.HasValue)
        {
            if (OnTalentClicked != null)
                OnTalentClicked(CurrentTalent.Value);
        }
    }

    private void TalentAvailable()
    {
        _button.image.material = null;
        if (_lockImage != null)
            _lockImage.enabled = false;
    }

    private void EnableLockImage()
    {
        _button.image.material = _greyscaleMat;
        if (_lockImage != null)
            _lockImage.enabled = true;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (_tooltip != null && CurrentTalent != null)
            _tooltip.Activate(CurrentTalent.Value);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (_tooltip != null)
            _tooltip.Deactivate();
    }

}
