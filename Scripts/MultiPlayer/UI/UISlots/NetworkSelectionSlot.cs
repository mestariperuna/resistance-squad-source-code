﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NetworkSelectionSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    public event System.Action<NetworkSelectionSlot> SelectOnClick;

    private UITooltip _tooltip;
    private Sprite _selectionHighlight;
    private Sprite _portrait;
    private string _data;

    public int SlotId { get; set; }
    public NetworkSelectable CurrentSelectable { get; private set; }

    private void Start()
    {
        _tooltip = FindObjectOfType<UITooltip>();
        _portrait = GetComponent<Image>().sprite;
        _selectionHighlight = Resources.Load<Sprite>("Sprites/UI/selectedBorder");
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (SelectOnClick != null)
            SelectOnClick(this);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        _tooltip.Activate(_data);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _tooltip.Deactivate();
    }

    public void UpdateSelectionSlot(NetworkSelectable selectable)
    {
        Transform childTransform = transform.GetChild(0);
        childTransform.localScale = Vector3.one;
        childTransform.GetComponent<Image>().sprite = selectable.icon;

        if(CurrentSelectable != null)
            CurrentSelectable.OnHealthChanged -= UpdateHealthBar;

        CurrentSelectable = selectable;
        selectable.OnHealthChanged += UpdateHealthBar;
        _data = "Level " + selectable.level + " " + selectable.name;
    }

    public void HighlightSelectionSlot()
    {
        GetComponent<Image>().sprite = _selectionHighlight;
    }

    public void ClearHighlight()
    {
        GetComponent<Image>().sprite = _portrait;
    }

    private void UpdateHealthBar(float percentage)
    {
        GetComponentInChildren<ChangeColorByScale>().SetHealthVisual(percentage);

        if (percentage <= 0.02f)
            CurrentSelectable.OnHealthChanged -= UpdateHealthBar;
    }

}
