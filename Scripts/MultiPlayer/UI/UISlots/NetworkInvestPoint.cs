﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkInvestPoint : MonoBehaviour
{
    private Button _button;
    public int tree;

    private void Start()
    {
        NetworkTalentUI talentUI = GetComponentInParent<NetworkTalentUI>();
        if (talentUI != null)
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(() => talentUI.InvestPoints(tree));
        }
        else
            Debug.Log("Missing TalentUI component!");
    }
}
