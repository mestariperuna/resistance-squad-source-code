﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NetworkItemData : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{

    public ItemDataToSend? Item { get; set; }
    public Transform OriginalParent { get; set; }

    private Vector2 offset;
    private UITooltip tooltip;
    private GameObject characterDetailsPanel;
    private GameObject dropZonePanel;

    public void Initialize()
    {
        NetworkInventoryUI inventoryUI = FindObjectOfType<NetworkInventoryUI>();
        tooltip = inventoryUI.GetComponentInChildren<UITooltip>();
        characterDetailsPanel = inventoryUI.transform.Find("NetworkUICanvas/CharacterDetailsPanel").gameObject;
        dropZonePanel = inventoryUI.transform.Find("NetworkUICanvas/DropZone").gameObject;
        Item = null;
        OriginalParent = transform.parent;
    }

    #region EventSystemInterface

    public void OnPointerDown(PointerEventData eventData)
    {
        if(Item != null)
        {
            offset = eventData.position - new Vector2(transform.position.x, transform.position.y);
            transform.position = eventData.position - offset;
        }
       
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if(Item != null)
        {
            OriginalParent = transform.parent;
            transform.SetParent(characterDetailsPanel.transform);
            dropZonePanel.SetActive(true);
            GetComponent<CanvasGroup>().blocksRaycasts = false;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if(Item != null)
            transform.position = eventData.position - offset;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.SetParent(OriginalParent);
        transform.position = OriginalParent.position;
        dropZonePanel.SetActive(false);
        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(tooltip != null)
        {
            if(Item.HasValue)
                tooltip.Activate(Item.Value);
        } 
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if(tooltip != null)
            tooltip.Deactivate();
    }

    public void UpdateItemData(ItemDataToSend newItem)
    {

        Image image = GetComponent<Image>();
        image.enabled = true;
        image.sprite = Resources.Load<Sprite>("Sprites/Items/" + newItem.iconName);
        Item = newItem;

        if (newItem.itemType.Equals("Resource"))
            GetComponentInChildren<Text>().text = newItem.quantity.ToString();
    }

    public void ClearItemData()
    {
        GetComponent<Image>().enabled = false;
        GetComponentInChildren<Text>().text = " ";
        Item = null;
    }

    #endregion

}
