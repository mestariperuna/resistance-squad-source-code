﻿using UnityEngine;
using UnityEngine.EventSystems;

public class NetworkItemSlot : MonoBehaviour, IDropHandler
{

    public event System.Action<NetworkItemSlot, NetworkItemSlot> OnItemSwap;
    public event System.Action<NetworkItemSlot, NetworkLootSlot> OnItemPickUp;
    public event System.Action<NetworkItemSlot, NetworkEquipmentSlot> OnItemUnequip;
    public int SlotId { get; set; }

    public void OnDrop(PointerEventData eventData)
    {
        NetworkItemData droppedItem = eventData.pointerDrag.GetComponent<NetworkItemData>();
        if(droppedItem != null)
        {
            NetworkItemSlot fromSlot = droppedItem.OriginalParent.GetComponent<NetworkItemSlot>();
            NetworkLootSlot lootSlot = droppedItem.OriginalParent.GetComponent<NetworkLootSlot>();
            NetworkEquipmentSlot equipSlot = droppedItem.OriginalParent.GetComponent<NetworkEquipmentSlot>();

            if (fromSlot != null)
            {
                if (OnItemSwap != null)
                    OnItemSwap(this, fromSlot);
            }
            else if(lootSlot != null)
            {
                if (OnItemPickUp != null)
                    OnItemPickUp(this, lootSlot);
            }
            else if(equipSlot != null)
            {
                if (OnItemUnequip != null)
                    OnItemUnequip(this, equipSlot);
            }
        }
    }
}
