﻿using System.Collections.Generic;
using UnityEngine;

public static class Formations
{
    public static List<Vector3> CalculateFormation(int quantity, float unitDistance, Vector3 direction)
    {
        switch(quantity)
        {
            case 2:
                return CalculatePairFormation(unitDistance, direction);
            case 3:
                return CalculatePatrolFormation(unitDistance, direction);
            case 4:
                return CalculateDiamondFormation(unitDistance, direction);
            default:
                return CalculateFormationGroup(quantity, 3, unitDistance);

        }
    }

    private static List<Vector3> CalculatePairFormation(float unitDistance, Vector3 direction)
    {
        List<Vector3> formationPoints = new List<Vector3>();

        Vector3 pos1 = new Vector3(direction.z, 0, -direction.x);
        Vector3 pos2 = pos1 * -1;

        formationPoints.Add(pos1 * unitDistance);
        formationPoints.Add(pos2 * unitDistance);

        return formationPoints;
    }

    private static List<Vector3> CalculatePatrolFormation(float unitDistance, Vector3 direction)
    {
        List<Vector3> formationPoints = new List<Vector3>();

        Vector3 pos0 = Vector3.zero;
        Vector3 pos1 = new Vector3(direction.z, 0, -direction.x);
        Vector3 pos2 = pos1 * -1;

        formationPoints.Add(pos0);
        formationPoints.Add((direction + pos1) * unitDistance);
        formationPoints.Add((direction + pos2) * unitDistance);

        return formationPoints;
    }

    private static List<Vector3> CalculateDiamondFormation(float unitDistance, Vector3 direction)
    {
        List<Vector3> formationPoints = new List<Vector3>();

        Vector3 pos0 = direction * unitDistance;
        Vector3 pos1 = new Vector3(direction.z, 0, -direction.x);
        Vector3 pos2 = pos1 * -1;
        Vector3 pos3 = pos0 * -1;

        formationPoints.Add(pos0 * unitDistance);
        formationPoints.Add(pos1 * unitDistance);
        formationPoints.Add(pos2 * unitDistance);
        formationPoints.Add(pos3 * unitDistance);

        return formationPoints;
    }

    private static List<Vector3> CalculateFormationGroup(int quantity, int maxPerRow, float unitDistance)
    {
        List<Vector3> formationPoints = new List<Vector3>();
        float rowCount = Mathf.Ceil(quantity / maxPerRow);

        for (int i = 0; i < quantity; i++)
        {
            float rowId = Mathf.Ceil(i / maxPerRow);
            float positionInRow = i % maxPerRow;
            float rowLength = Mathf.Clamp(quantity - rowId * maxPerRow, 1, maxPerRow);

            float positionX = (positionInRow * unitDistance) - (maxPerRow / 2 * unitDistance) + ((maxPerRow - rowLength) / 2 * unitDistance) + (rowLength % 2 == 0 ? 0 : unitDistance / 2);
            float positionY = (rowCount / 2 - rowId) * unitDistance;
            formationPoints.Add(new Vector3(positionX, 0, positionY));
        }
        return formationPoints;
    }
}
