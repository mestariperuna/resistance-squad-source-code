﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{
    #region Variables

    public event System.Action OnBuffsChanged;

    public Weapon EquippedWeapon { get; private set; }
    public float Accuracy { get; set; }

    public GameObject Projectile { get; private set; }
    public AudioClip FireAudio { get; private set; }
    private GameObject _muzzleEffect;

    public Transform weaponHold;
    public Transform projectileSpawn;

    private SkillManager _skillManager;
    private List<GunBonusStats> _gunPassiveSkillModifiers = new List<GunBonusStats>();
    private List<GunBuff> _currentBuffs = new List<GunBuff>();

    [Range(0, 1)]
    public float critChance;
    [Range(1, 10)]
    public float critDamage;

    private float timeBetweenAttacks;
    private float nextAttack;

    #endregion

    void Start()
    {
        if (PoolManager.Instance != null)
            _muzzleEffect = PoolManager.Instance.MuzzleEffect;
        else
            Debug.LogWarning("No PoolManager in scene!");

        _skillManager = GetComponent<SkillManager>();
    }

    public void EquipWeapon(Weapon weaponToEquip)
    {
        if(weaponToEquip != null)
        {
            EquippedWeapon = weaponToEquip;
            timeBetweenAttacks = 1 / EquippedWeapon.attacksPerSecond;
            Projectile = Resources.Load<GameObject>("GFX/Projectiles/" + EquippedWeapon.projectileName);
            FireAudio = Resources.Load<AudioClip>("SFX/Weapons/" + EquippedWeapon.fireAudio);
        }
    }

    public void RemoveWeapon()
    {
        if (EquippedWeapon != null)
            EquippedWeapon = null;
    }

    #region Shooting

    public void Shoot(Transform target)
    {
        if (EquippedWeapon != null && target != null)
        {
            if (Time.time > nextAttack)
            {
                nextAttack = Time.time + timeBetweenAttacks;
                LaunchProjectile(target);
            }
        }
    }

    public void OnPassiveSkillTrigger(Transform target)
    {
        if (EquippedWeapon != null)
            LaunchProjectile(target);
    }

    private void LaunchProjectile(Transform target)
    {
        Projectile projectile = SpawnProjectile();
        // set the damage, the armour penetration and the direction for projectile
        projectile.Damage = CalculateDamage(target);
        projectile.Direction = CalculateDirection(target);
        projectile.ArmourPenetration = GetBonusGunStat(x => x.ArmourPenetrationBonus);
    }

    private Projectile SpawnProjectile()
    {
        PoolManager.Instance.ReuseObject(_muzzleEffect, projectileSpawn.position, projectileSpawn.rotation);
        AudioManager.Instance.PlaySound(FireAudio, transform.position);
        Projectile projectile = Instantiate(Projectile, projectileSpawn.position, Quaternion.identity).GetComponent<Projectile>();
        return projectile;
    }

    // calculates the damage for projectiles
    private int CalculateDamage(Transform target)
    {
        int rand = Random.Range(0, 100);
        int damage = EquippedWeapon.baseDamage + BonusDamage(EquippedWeapon.isTwoHander);
        float effectiveRange = EquippedWeapon.effectiveRange + GetBonusGunStat(x => x.EffectiveRange);
        float criticalChance = (critChance + GetBonusGunStat(x => x.CriticalChanceBonus)) * 100;
        float criticalDamage = (critDamage + GetBonusGunStat(x => x.CriticalDamageBonus));

        if (target != null)
        {
            if (Vector3.Distance(transform.position, target.position) < effectiveRange)
                damage += GetBonusGunStat(x => x.UpCloseDamageBonus);
        }

        if (rand <= criticalChance)
            damage = Mathf.RoundToInt(damage * criticalDamage);

        return damage;
    }

    // calculates the direction for projectiles
    private Vector3 CalculateDirection(Transform target)
    {
        Vector2 randomPos;
        float accuracy = Accuracy + BonusAccuracy(EquippedWeapon.isTwoHander);
        float effectiveRange = EquippedWeapon.effectiveRange + GetBonusGunStat(x => x.EffectiveRange);

        randomPos.x = Random.Range(accuracy - 1, 1 - accuracy);
        randomPos.y = Random.Range(accuracy - 1, 1 - accuracy);
        
        Vector3 dir = (target.position - transform.position).normalized * effectiveRange + transform.right * randomPos.x + transform.up * randomPos.y;
        return dir.normalized;
    }

    #endregion

    #region BonusStats

    public void OnPassiveSkillUnlocked(GunBonusStats gunModifiers)
    {
        if (gunModifiers != null)
            _gunPassiveSkillModifiers.Add(gunModifiers);
    }

    public void OnPassiveSkillRemoved(GunBonusStats gunModifiers)
    {
        if (gunModifiers != null)
        {
            if (_gunPassiveSkillModifiers.Contains(gunModifiers))
                _gunPassiveSkillModifiers.Remove(gunModifiers);
        }
    }

    private int BonusDamage(bool isTwoHander)
    {
        int damage = GetBonusGunStat(x => x.DamageBonus);
        if(_skillManager.HasJuggernautSkill())
        {
            damage += GetBonusGunStat(x => x.TwoHandDamageBonus);
            damage += GetBonusGunStat(x => x.OneHandDamageBonus);
        }
        else if(isTwoHander)
            damage += GetBonusGunStat(x => x.TwoHandDamageBonus);
        else
            damage += GetBonusGunStat(x => x.OneHandDamageBonus);

        return damage;
    }

    private float BonusAccuracy(bool isTwoHander)
    {
        float accuracy = GetBonusGunStat(x => x.AccuracyBonus);
        if (_skillManager.HasJuggernautSkill())
        {
            accuracy += GetBonusGunStat(x => x.TwoHandAccuracyBonus);
            accuracy += GetBonusGunStat(x => x.OneHandAccuracyBonus);
        }
        else if (isTwoHander)
            accuracy += GetBonusGunStat(x => x.TwoHandAccuracyBonus);
        else
            accuracy += GetBonusGunStat(x => x.OneHandAccuracyBonus);

        return accuracy;
    }

    public float GetBonusGunStat(System.Func<GunBonusStats, float> selector)
    {
        float stat = 0;
        for (int i = 0; i < _gunPassiveSkillModifiers.Count; i++)
            stat += selector(_gunPassiveSkillModifiers[i]);

        return stat;
    }

    public int GetBonusGunStat(System.Func<GunBonusStats, int> selector)
    {
        int stat = 0;
        for (int i = 0; i < _gunPassiveSkillModifiers.Count; i++)
            stat += selector(_gunPassiveSkillModifiers[i]);

        return stat;
    }

    public void AddBuff(GunBuff buff)
    {
        if (buff != null)
        {
            if (!CurrentBuffsHasMaxStacks(buff))
            {
                _gunPassiveSkillModifiers.Add(buff.Stats);
                _currentBuffs.Add(buff);
                StartCoroutine(RemoveBuffAfterDuration(buff));

                if (OnBuffsChanged != null)
                    OnBuffsChanged();
            }
        }
    }

    // check if the current buffs list has max amount of the buffs max stacks
    private bool CurrentBuffsHasMaxStacks(GunBuff buff)
    {
        int stackCount = 0;
        for (int i = 0; i < _currentBuffs.Count; i++)
        {
            if (_currentBuffs[i].BuffName.Equals(buff.BuffName))
                stackCount++;
                
            if(stackCount >= buff.MaxStacks)
                return true;
        }
        return false;
    }

    private IEnumerator RemoveBuffAfterDuration(GunBuff buff)
    {
        yield return new WaitForSeconds(buff.Duration);
        _gunPassiveSkillModifiers.Remove(buff.Stats);
        _currentBuffs.Remove(buff);

        if (OnBuffsChanged != null)
            OnBuffsChanged();
    }

    #endregion

}
