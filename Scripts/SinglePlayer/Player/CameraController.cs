﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraController : MonoBehaviour
{

    public int cameraSpeed = 15;
    public int rotateSpeed = 75;
    public float smoothTime = 15;

    [Range(10, 20)]
    public int maxHeight = 15;
    [Range(0,90)]
    public float verticalAngle = 60;
    [Range(0, 360)]
    public float horizontalAngle = 45;

    public Terrain terrain;

    [Tooltip("boundary from x-axis 0 + x, boundary from terrain width - y")]
    public Vector2 boundaryWidth = Vector2.zero;
    [Tooltip("boundary from z-axis 0 + x, boundary from terrain length - y")]
    public Vector2 boundaryLength = Vector2.zero;

    private int boundary = 10;
    private float _maxWidth;
    private float _maxLength;

    private float minHeight;
    private float targetHeight;

    public bool enableCameraPanning = true;
    public bool enableKeyboard = true;
    public bool enableRotation = true;
    public bool enableZoom = true;

    private RaycastTooltip _tooltip;
    private Texture2D[] _cursorTextures;

    void Start()
    {
        //lock cursor
        Cursor.lockState = CursorLockMode.Confined;

        if (terrain == null)
            terrain = FindObjectOfType<Terrain>();

        _maxWidth = terrain.terrainData.size.x - boundaryWidth.y;
        _maxLength = terrain.terrainData.size.z - boundaryLength.y;

        float z = Camera.main.transform.localPosition.y / Mathf.Tan(Mathf.Deg2Rad * verticalAngle);
        Camera.main.transform.localPosition = new Vector3(Camera.main.transform.localPosition.x, Camera.main.transform.localPosition.y, -z);
        Camera.main.transform.LookAt(transform.position);

        minHeight = transform.position.y;
        maxHeight += Mathf.RoundToInt(minHeight);

        transform.position = new Vector3(transform.position.x, Mathf.RoundToInt(maxHeight / 2) + minHeight, transform.position.z);
        transform.rotation = Quaternion.Euler(0, horizontalAngle, 0);

        targetHeight = transform.position.y;

        enableCameraPanning = (PlayerPrefs.GetInt("camerapanning") == 1) ? true : false;
        enableRotation = (PlayerPrefs.GetInt("camerarotate") == 1) ? true : false;

        _tooltip = FindObjectOfType<RaycastTooltip>();
        _cursorTextures = Resources.LoadAll<Texture2D>("Cursors");
    }

    void Update()
    {

        Vector2 move = Vector2.zero;
        Vector3 pos = transform.position;

        //moving camera with default arrow keys
        if (enableKeyboard)
        {
            move.x += InputFunctions.GetAxis("Right", "Left", 3);
            move.y += InputFunctions.GetAxis("Forward", "Backward", 3);
        }

        //rotating with middlemouse and mouse movement x-axis
        if (enableRotation)
        {
            if(Input.GetKey(KeyCode.Mouse2))
            {
                transform.Rotate(Vector3.up, Input.GetAxisRaw("Mouse X") * rotateSpeed * Time.deltaTime, Space.World);
            }
        }

        // camera movement with mouse on the edge of the screen
        if (enableCameraPanning)
        {
            if (Input.mousePosition.x > Screen.width - boundary)
                move.x++;
            if (Input.mousePosition.x < 0 + boundary)
                move.x--;
            if (Input.mousePosition.y > Screen.height - boundary)
                move.y++;
            if (Input.mousePosition.y < 0 + boundary)
                move.y--;
        }

        //zooming with mouse scrollwheel
        if (enableZoom)
        {
            float zoom = Input.GetAxisRaw("Mouse ScrollWheel");

            if (zoom < 0 && targetHeight < maxHeight)
                targetHeight += 3;
            else if (zoom > 0 && targetHeight > minHeight)
                targetHeight -= 3;
        
            pos.y = Mathf.MoveTowards(transform.position.y, targetHeight, smoothTime * Time.deltaTime);
            pos.y = Mathf.Clamp(pos.y, minHeight, minHeight + maxHeight);
        }

        Vector3 moveVector = (transform.right * move.x + transform.forward * move.y) * cameraSpeed * Time.deltaTime;

        pos.x = Mathf.Clamp(transform.position.x + moveVector.x, boundaryWidth.x, _maxWidth);
        pos.z = Mathf.Clamp(transform.position.z + moveVector.z, boundaryLength.x, _maxLength);
        transform.position = pos;


        // raycast for changing the cursor.
        Vector2 mousePos = Input.mousePosition;
        Ray ray = Camera.main.ScreenPointToRay(mousePos);
        RaycastHit hit;
        
        CursorManager.SetCursorImage(_cursorTextures[2]);
        _tooltip.Deactivate();

        if (Physics.Raycast(ray, out hit, 100.0f) && !EventSystem.current.IsPointerOverGameObject())
        {
            Interactable interactable = hit.collider.GetComponent<Interactable>();
            Destructable destructable = hit.collider.GetComponent<Destructable>();
            if (hit.collider.GetComponent<Machine>() || (destructable != null && !destructable.indestructable))
            {
                CursorManager.SetCursorImage(_cursorTextures[1]);
            }          
            else if (interactable != null)
            {
                CursorManager.SetCursorImage(_cursorTextures[3]);
                _tooltip.Activate(interactable.objectName);
            }
            else if (hit.collider.GetComponent<BarrierArmorBonus>())
            {
                CursorManager.SetCursorImage(_cursorTextures[0]);
            }
        }

    }
}
