﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerManager : MonoBehaviour
{
    public float unitDistance = 1f;

    private InventoryUI _inventory;
    private TalentUI _talentTree;
    private SkillsUI _skills;
    private SelectionUI _availableUnits;
    private MouseSelection _selection;
    private GameObject _clickPos;

    private int _selectionIndex = 0;
    private int _previouslyCommanded = 0;
    private bool _attackCommand = false;

    private LayerMask groundMask;

    private void Start()
    {
        groundMask = LayerMask.GetMask("Ground", "Target");

        _inventory = GetComponent<InventoryUI>();
        _talentTree = GetComponent<TalentUI>();
        _skills = GetComponent<SkillsUI>();
        _availableUnits = GetComponent<SelectionUI>();
        _availableUnits.OnSelectionSlotClicked += SelectOnClick;

        _selection = FindObjectOfType<MouseSelection>();
        _selection.OnAvailableUnitsChanged += UpdateAvailableUnits;
        _selection.OnSelectedUnitsChanged += UpdateUI;

        _clickPos = Resources.Load<GameObject>("GFX/Arrows");

        UpdateAvailableUnits();
    }

    private void Update()
    {
        if (InputFunctions.GetKeyDown("Cycle"))
            SelectNextOrPreviousFromAvailableUnits(true);

        if (InputFunctions.GetKeyDown("SelectAll"))
            SelectAllAvailableUnits();

        if (InputFunctions.GetKeyUp("Inventory"))
            ActivateInventory();

        if (InputFunctions.GetKeyUp("Talents"))
            ActivateTalentTree();

        UpdatePreviouslyCommandedIndex();
        Command();
    }

    private void UpdatePreviouslyCommandedIndex()
    {
        int? tempValue = _selection.UpdateCurrentAvailableUnitIndex(_selectionIndex);
        if (tempValue.HasValue)
            _previouslyCommanded = tempValue.Value;
    }

    public void SelectNextOrPreviousFromAvailableUnits(bool next)
    {
        if (_selection.SelectFromAvailableUnits(next))
            _selectionIndex = 0;
        else
            _selectionIndex++;

        HandleSelectionIndex();
        UpdateUI();
    }

    public void SelectAllAvailableUnits()
    {
        _selection.SelectAll();
        _selectionIndex = 0;
    }

    private void HandleSelectionIndex()
    {
        if (_selectionIndex >= _selection.SelectedUnits.Count)
            _selectionIndex = 0;
    }

    #region UI

    public void UpdateUI()
    {
        Selectable selectable = null;
        if (_selection.SelectedUnits.Count > 0)
        {
            HandleSelectionIndex();
            selectable = _selection.SelectedUnits[_selectionIndex];
        }
            
        if(selectable is Humanoid)
        {
            Humanoid humanoid = selectable as Humanoid;

            _inventory.UpdateCharacterPanel(humanoid);
            _skills.UpdateSkills(humanoid);
            _talentTree.UpdateTalentTree(humanoid);
            _availableUnits.HighlightSelection(humanoid);
        }

    }

    private void UpdateAvailableUnits()
    {
        _availableUnits.UpdateAvailableUnits(_selection.AvailableUnits);
    }

    public void ActivateInventory()
    {
        Selectable selectable = null;
        if (_selection.SelectedUnits.Count > 0)
        {
            HandleSelectionIndex();
            selectable = _selection.SelectedUnits[_selectionIndex];
        }

        _inventory.ActiveSelfInventory(selectable);
        
    }

    public void ActivateTalentTree()
    {
        Selectable selectable = null;
        if (_selection.SelectedUnits.Count > 0)
        {
            HandleSelectionIndex();
            selectable = _selection.SelectedUnits[_selectionIndex];
        }
        _talentTree.ActivateTalentTree(selectable);
    }

    #endregion

    #region Commands

    public void Command()
    {
        if (InputFunctions.GetKeyDown("Command"))
            GiveCommands(Input.mousePosition);

        if (InputFunctions.GetKey("Centralize"))
            MoveCameraToSelectedUnits();

        if (InputFunctions.GetKeyUp("Ability1"))
            UseSkill(0);

        if (InputFunctions.GetKeyUp("Ability2"))
            UseSkill(1);

        if (InputFunctions.GetKeyUp("Ability3"))
            UseSkill(2);

        if (InputFunctions.GetKeyUp("Ability4"))
            UseSkill(3);

        if (InputFunctions.GetKeyUp("Ability5"))
            UseSkill(4);

        if (InputFunctions.GetKeyUp("Attack"))
            AttackMousePosition();

        if (InputFunctions.GetKeyUp("Stop"))
            Stop();
    }

    private void GiveCommands(Vector2 mousePos)
    {
        if(!EventSystem.current.IsPointerOverGameObject())
        {
            Ray ray = Camera.main.ScreenPointToRay(mousePos);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100f, groundMask))
            {
                Selectable selectable = hit.transform.GetComponent<Selectable>();
                Interactable interactable = hit.transform.GetComponent<Interactable>();
                Destructable destructable = hit.transform.GetComponent<Destructable>();

                _selection.DeselectEnemyUnits();
                if (_selection.CheckSelection(_previouslyCommanded))
                    HandleSelectionIndex();

                RemoveFocus();

                if (selectable != null)
                    SetTarget(selectable);
                else if (interactable != null)
                    SetFocus(interactable);
                else if (destructable != null)
                    SetTargetDestructable(destructable);
                else
                    MoveToFormation(hit.point);

                Instantiate(_clickPos, hit.point, Quaternion.identity);
            }
        }
    }

    private void SetTarget(Selectable newTarget)
    {
        List<Selectable> currentUnits = _selection.SelectedUnits;
        for (int i = 0; i < currentUnits.Count; i++)
        {
            if (!_selection.IsAlly(newTarget))
                currentUnits[i].SetTarget(newTarget.transform);
        }
    }

    private void SetTargetDestructable(Destructable newTarget)
    {
        List<Selectable> currentUnits = _selection.SelectedUnits;
        for (int i = 0; i < currentUnits.Count; i++)
        {
            currentUnits[i].SetTarget(newTarget.transform);
        }
    }

    private void SetFocus(Interactable newFocus)
    {
        if(_selection.SelectedUnits.Count > 0)
            _selection.SelectedUnits[_selectionIndex].SetFocus(newFocus);
    }

    private void RemoveFocus()
    {
        List<Selectable> currentUnits = _selection.SelectedUnits;
        for (int i = 0; i < currentUnits.Count; i++)
            currentUnits[i].RemoveFocus();
    }

    public void MoveToFormation(Vector3 clickPoint)
    {
        List<Selectable> currentUnits = _selection.SelectedUnits;
        if (currentUnits.Count > 0)
        {
            if (currentUnits.Count == 1)
                currentUnits[0].MoveUnit(clickPoint);
            else
            {
                HandleSelectionIndex();
                Vector3 firstPos = currentUnits[_selectionIndex].transform.position;
                Vector3 direction = new Vector3(clickPoint.x - firstPos.x,0, clickPoint.z - firstPos.z).normalized;
                List<Vector3> formationPoints = Formations.CalculateFormation(currentUnits.Count, unitDistance, direction);

                for (int i = 0; i < formationPoints.Count; i++)
                {
                    if (currentUnits[i].gameObject.activeInHierarchy)
                    {
                        Vector3 destination = clickPoint + formationPoints[i];
                        currentUnits[i].MoveUnit(destination);
                    }
                }
            }
        }
    }

    public void MoveCameraToSelectedUnits()
    {
        List<Selectable> currentUnits = _selection.SelectedUnits;
        if (currentUnits.Count > 0)
        {
            Vector3 minPoint = currentUnits[0].transform.position;
            Vector3 maxPoint = currentUnits[0].transform.position;

            for (int i = 0; i < currentUnits.Count; i++)
            {
                Vector3 pos = currentUnits[i].transform.position;
                if (pos.x < minPoint.x)
                    minPoint.x = pos.x;
                if (pos.x > maxPoint.x)
                    maxPoint.x = pos.x;
                if (pos.y < minPoint.y)
                    minPoint.y = pos.y;
                if (pos.y > maxPoint.y)
                    maxPoint.y = pos.y;
                if (pos.z < minPoint.z)
                    minPoint.z = pos.z;
                if (pos.z > maxPoint.z)
                    maxPoint.z = pos.z;
            }
            _selection.transform.position = minPoint + 0.5f * (maxPoint - minPoint);
        }
    }

    public void UseSkill(int index)
    {
        if(_selection.SelectedUnits.Count > 0)
        {
            Humanoid currentUnit = _selection.SelectedUnits[_selectionIndex].GetComponent<Humanoid>();
            if (currentUnit != null)
            {
                currentUnit.SkillManager.UseSkillAtIndex(index);
                _selection.SetCommand(true);
                StartCoroutine(WaitForClick());
            }
        }
    }
    
    public void AttackMousePosition()
    {
        _selection.SetCommand(true);
        StartCoroutine(WaitForClick());
    }

    private IEnumerator WaitForClick()
    {
        yield return new WaitForEndOfFrame();

        while (!Input.anyKeyDown)
        {
            yield return null;
        }

        if (InputFunctions.GetKeyDown("Select") && _attackCommand)
            GiveCommands(Input.mousePosition);

        _attackCommand = false;
        _selection.SetCommand(false);

    }

    public void Stop()
    {
        List<Selectable> currentUnits = _selection.SelectedUnits;
        for (int i = 0; i < currentUnits.Count; i++)
        {
            currentUnits[i].StopUnit();
        }
    }

    private void SelectOnClick(int index)
    {
        _selection.DeselectUnits();
        _selection.SelectUnit(_selection.AvailableUnits[index]);
    }

    #endregion

}
