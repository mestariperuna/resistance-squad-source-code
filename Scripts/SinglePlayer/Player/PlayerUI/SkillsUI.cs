﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillsUI : MonoBehaviour
{
    private GameObject _skillPanel;
    private List<GameObject> _skillSlots = new List<GameObject>();
    private Humanoid currentHumanoid;

    #region Initialization

    private void Start()
    {
        _skillPanel = transform.Find("ActivePanel/UI_MidPanel/SkillPanel").gameObject;
        InitializeSkillSlots();

        UpdateSkills(null);
    }

    private void InitializeSkillSlots()
    {
        for (int i = 0; i < _skillPanel.transform.childCount; i++)
        {
            _skillSlots.Add(_skillPanel.transform.GetChild(i).gameObject);
            SkillSlot skillSlot = _skillSlots[i].GetComponent<SkillSlot>();
            skillSlot.OnSkillClicked += OnSkillClicked;
            skillSlot.SlotIndex = i;
        }
    }

    #endregion

    public void UpdateSkills(Selectable selectable)
    {
        UpdateCurrentHumanoid(selectable);
        ClearSkillSlots();
        UpdateSkillSlots();
    }

    private void UpdateCurrentHumanoid(Selectable selectable)
    {
        if (selectable is Humanoid)
            currentHumanoid = selectable as Humanoid;
        else
            currentHumanoid = null;
    }

    private void ClearSkillSlots()
    {
        for (int i = 0; i < _skillSlots.Count; i++)
            _skillSlots[i].GetComponent<SkillSlot>().ClearSkillSlot();
    }

    private void UpdateSkillSlots()
    {
        if (currentHumanoid != null)
        {
            GameObject[] skills = currentHumanoid.SkillManager.ActiveSkills;
            for (int i = 0; i < skills.Length; i++)
            {
                if (skills[i] != null)
                    _skillSlots[i].GetComponent<SkillSlot>().UpdateSkillSlot(skills[i].GetComponent<Skill>());
            }
        }
    }

    private void OnSkillClicked(int index)
    {
        if(currentHumanoid != null)
        {
            currentHumanoid.SkillManager.UseSkillAtIndex(index);
        }
    }

}
