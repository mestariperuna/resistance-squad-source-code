﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LootBag : MonoBehaviour
{
    public int inventorySize = 16;
    public GameObject uiLootSlot;
    public GameObject uiItem;
    private Loot _currentLoot;
    private Humanoid _currentLooter;
    private InventoryUI _inventory;

    private List<GameObject> _lootSlots = new List<GameObject>();
    private Transform _slotPanel;

    private void Awake()
    {
        _slotPanel = transform.GetChild(0);
        _inventory = GetComponentInParent<InventoryUI>();

        // initialize loot image slots
        for (int i = 0; i < inventorySize; i++)
        {
            GameObject slotItem = Instantiate(uiLootSlot, _slotPanel.transform.position, Quaternion.identity, _slotPanel.transform);
            slotItem.transform.localScale = Vector3.one;

            LootSlot itemSlot = slotItem.GetComponent<LootSlot>();
            itemSlot.SlotId = i;
            itemSlot.OnItemSwap += SwapItems;

            _lootSlots.Add(Instantiate(uiItem, slotItem.transform.position, slotItem.transform.rotation, slotItem.transform));
            _lootSlots[i].transform.localScale = Vector3.one;
            ItemData data = _lootSlots[i].GetComponent<ItemData>();
            data.Initialize();
            data.OnItemClicked += OnItemSlotClicked;
        }
    }

    public void SetLootBag(Loot newLoot, Humanoid newLooter)
    {
        _currentLoot = newLoot;
        _currentLooter = newLooter;

        UpdateLootBag();
    }

    private void UpdateLootBag()
    {
        ClearLootBag();
        ShowLootBag();
    }

    private void ClearLootBag()
    {
        for (int i = 0; i < _lootSlots.Count; i++)
        {
            _lootSlots[i].GetComponent<ItemData>().Item = null;
            _lootSlots[i].GetComponentInChildren<Text>().text = "";
            _lootSlots[i].GetComponent<Image>().enabled = false;
        }
    }

    private void ShowLootBag()
    {
        if(_currentLoot != null)
        {
            Item[] loot = _currentLoot.Inventory;
            for (int i = 0; i < loot.Length; i++)
            {
                if(loot[i] != null)
                {
                    _lootSlots[i].GetComponent<Image>().enabled = true;
                    _lootSlots[i].GetComponent<Image>().sprite = loot[i].icon;
                    _lootSlots[i].GetComponent<ItemData>().Item = loot[i];
                    if (loot[i] is Resource)
                    {
                        Resource resource = loot[i] as Resource;
                        _lootSlots[i].GetComponentInChildren<Text>().text = resource.quantity.ToString();
                    }
                }
            }
        }
    }

    public Item RemoveFromIndex(int index)
    {
        Item item = null;
        if (_currentLoot.Inventory.Length > index)
        {
            if (_currentLoot.Inventory[index] != null)
            {
                item = _currentLoot.Inventory[index];
                _currentLoot.Inventory[index] = null;
                UpdateLootBag();
            }
        }
        return item;
    }

    private void SwapItems(LootSlot toSlot, LootSlot fromSlot)
    {
        if(toSlot != null && fromSlot != null)
        {
            Item tempItem = _currentLoot.Inventory[toSlot.SlotId];
            _currentLoot.Inventory[toSlot.SlotId] = _currentLoot.Inventory[fromSlot.SlotId];
            _currentLoot.Inventory[fromSlot.SlotId] = tempItem;
        }
    }

    private void OnItemSlotClicked(ItemData itemData)
    {
        if(_currentLooter != null)
        {
            LootSlot lootSlot = itemData.OriginalParent.GetComponent<LootSlot>();
            if (lootSlot != null)
            {
                if (_currentLooter.InventoryManager.AddItem(itemData.Item))
                {
                    RemoveFromIndex(lootSlot.SlotId);
                    _inventory.UpdateInventory();
                }
            }         
        }
    }

    public void LootAllButton()
    {
        if(_currentLoot != null && _currentLooter != null)
        {
            for (int i = 0; i < _currentLoot.Inventory.Length; ++i)
            {
                if (_currentLoot.Inventory[i] != null)
                {
                    if(_currentLooter.InventoryManager.AddItem(_currentLoot.Inventory[i]))
                        _currentLoot.Inventory[i] = null;
                }
            }
            UpdateLootBag();
            _inventory.UpdateInventory();
        }
    }

}
