﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUI : MonoBehaviour
{
    public int inventorySize = 18;
    public GameObject uiSlot;
    public GameObject uiItem;
    public GameObject LootBag { get; private set; }

    private GameObject _collectable;
    private GameObject _characterDetailsPanel;
    private GameObject _namePanel;
    private GameObject _unitStatsPanel;
    private GameObject _weaponStatsPanel;
    private GameObject _equipmentPanel;
    private GameObject _inventoryPanel;
    private GameObject _dropZonePanel;

    private TooltipData[] _unitStatsData;
    private TooltipData[] _weaponStatsData;

    private List<GameObject> _inventorySlots = new List<GameObject>();
    private List<GameObject> _equipmentSlots = new List<GameObject>();

    private Humanoid currentHumanoid;

    private Image tutorialImage;

    #region Initialization

    private void Awake ()
    {
        _collectable = Resources.Load<GameObject>("GameItems/Collectable");

        FindCharacterPanel();
        FindStatisticsPanels();

        InitializeInventorySlots();
        InitializeEquipmentSlots();

        _characterDetailsPanel.SetActive(false);
        FindDropZonePanel();
        FindLootBag();

        UpdateCharacterPanel(null);
    }

    private void FindCharacterPanel()
    {
        _characterDetailsPanel = transform.Find("ActivePanel/CharacterDetailsPanel").gameObject;
        _namePanel = _characterDetailsPanel.transform.Find("NamePanel").gameObject;
    }

    private void FindStatisticsPanels()
    {
        _unitStatsPanel = _characterDetailsPanel.transform.Find("UnitStatisticsPanel").gameObject;
        _weaponStatsPanel = _characterDetailsPanel.transform.Find("WeaponStatisticsPanel").gameObject;

        _unitStatsData = _unitStatsPanel.GetComponentsInChildren<TooltipData>();
        _weaponStatsData = _weaponStatsPanel.GetComponentsInChildren<TooltipData>();
    }

    private void InitializeInventorySlots()
    {
        _inventoryPanel = _characterDetailsPanel.transform.Find("InventoryPanel").gameObject;

        for (int i = 0; i < inventorySize; i++)
        {
            GameObject slotItem = Instantiate(uiSlot, _inventoryPanel.transform.position, Quaternion.identity, _inventoryPanel.transform);
            slotItem.transform.localScale = Vector3.one;

            ItemSlot itemSlot = slotItem.GetComponent<ItemSlot>();
            itemSlot.SlotId = i;
            itemSlot.OnItemSwap += SwapItem;
            itemSlot.OnItemPickUp += AddItem;
            itemSlot.OnItemUnequip += UnequipItem;

            _inventorySlots.Add(Instantiate(uiItem, slotItem.transform.position, slotItem.transform.rotation, slotItem.transform));
            _inventorySlots[i].transform.localScale = Vector3.one;
            ItemData data = _inventorySlots[i].GetComponent<ItemData>();
            data.Initialize();
            data.OnItemClicked += OnItemSlotClicked;
        }
    }

    private void InitializeEquipmentSlots()
    {
        _equipmentPanel = _characterDetailsPanel.transform.Find("EquipmentHolderPanel/EquipmentPanel").gameObject;

        for (int k = 0; k < _equipmentPanel.transform.childCount; k++)
        {
            _equipmentSlots.Add(_equipmentPanel.transform.GetChild(k).gameObject);
            EquipmentSlot equipmentSlot = _equipmentSlots[k].GetComponent<EquipmentSlot>();
            equipmentSlot.OnItemEquip += EquipItem;

            GameObject slotItem = Instantiate(uiItem, _equipmentSlots[k].transform);
            slotItem.transform.localScale = Vector3.one;
            ItemData data = slotItem.GetComponent<ItemData>();
            data.Initialize();
            data.OnItemClicked += OnEquipmentSlotClicked;
        }
    }

    private void FindDropZonePanel()
    {
        _dropZonePanel = transform.Find("ActivePanel/DropZone").gameObject;
        _dropZonePanel.GetComponent<DropSlot>().OnGroundDrop += DropItem;
        _dropZonePanel.SetActive(false);
    }

    private void FindLootBag()
    {
        LootBag = transform.Find("ActivePanel/LootBag").gameObject;
        LootBag.SetActive(false);
    }
    
    #endregion

    #region ItemFunctions
    
    private void AddItem(ItemSlot toSlot, LootSlot fromSlot)
    {
        // add item to inventory when looting from other UI element
        if (toSlot != null && fromSlot != null)
        {
            LootBag lootBag = fromSlot.transform.parent.parent.GetComponent<LootBag>();
            if (currentHumanoid != null && lootBag != null)
            {
                Item item = lootBag.RemoveFromIndex(fromSlot.SlotId);
                if (item != null)
                {
                    currentHumanoid.InventoryManager.AddItemByIndex(item, toSlot.SlotId);
                    UpdateInventory();
                }
            }
        }
    }

    private void SwapItem(ItemSlot toSlot, ItemSlot fromSlot)
    {
        // move item in inventory from original index to destination index
        if (toSlot != null && fromSlot != null && currentHumanoid != null)
        {
            currentHumanoid.InventoryManager.SwapItems(toSlot.SlotId, fromSlot.SlotId);
            UpdateInventory();
        }
    }

    private void OnItemSlotClicked(ItemData itemData)
    {
        if(currentHumanoid != null)
        {
            ItemSlot slot = itemData.OriginalParent.GetComponent<ItemSlot>();
            if(slot != null && itemData.Item is Equipment)
            {
                EquipItem(itemData.Item as Equipment, null);
            }
        }
    }

    private void EquipItem(Equipment newEquipment, LootSlot fromSlot)
    {
        if (newEquipment != null && currentHumanoid != null)
        {

            currentHumanoid.EquipmentManager.EquipItem(newEquipment);
            UpdateCharacterPanel(currentHumanoid);

            if (fromSlot != null)
            {
                LootBag lootBag = fromSlot.transform.parent.parent.GetComponent<LootBag>();
                lootBag.RemoveFromIndex(fromSlot.SlotId);
            }
        }
    }

    private void OnEquipmentSlotClicked(ItemData itemData)
    {
        if(currentHumanoid != null)
        {
            EquipmentSlot slot = itemData.OriginalParent.GetComponent<EquipmentSlot>();
            if (slot != null)
            {
                Equipment currentEquipment = itemData.Item as Equipment;
                currentHumanoid.EquipmentManager.UnequipItem((int)currentEquipment.equipSlot, null);
                UpdateCharacterPanel(currentHumanoid);
            }
        }
    }

    private void UnequipItem(ItemSlot toSlot, EquipmentSlot fromSlot)
    {
        if (toSlot != null && fromSlot != null && currentHumanoid != null)
        {
            currentHumanoid.EquipmentManager.UnequipItem((int)fromSlot.slot, toSlot.SlotId);
            StartCoroutine(LateUpdateCharacterPanel());
        }
    }

    private IEnumerator LateUpdateCharacterPanel()
    {
        yield return new WaitForEndOfFrame();
        UpdateCharacterPanel(currentHumanoid);
    }

    private void DropItem(Item item)
    {
        if (item != null && currentHumanoid != null)
        {
            // try removing item from inventory
            bool successful = currentHumanoid.InventoryManager.RemoveItem(item);
            // if it's not in inventory, it's an equipment
            if (!successful)
            {
                // remove from equipped items, this adds unequipped item to inventory
                Equipment equipment = item as Equipment;
                currentHumanoid.EquipmentManager.UnequipItem((int)equipment.equipSlot, null);
                // try removing from inventory again
                successful = currentHumanoid.InventoryManager.RemoveItem(item);
            }

            if (successful)
            {
                GameObject reusedObject = Instantiate(_collectable, DropNearby(currentHumanoid.transform), Quaternion.identity);
                reusedObject.GetComponent<Collectable>().CurrentItem = item;
                UpdateCharacterPanel(currentHumanoid);
            }
        }
    }

    private Vector3 DropNearby(Transform transform)
    {
        return new Vector3(transform.position.x + Random.Range(0.5f, 1), transform.position.y, transform.position.z + Random.Range(-0.5f, 0.5f));
    }

    #endregion

    public void ActiveSelfInventory(Selectable selectable)
    {
        _characterDetailsPanel.SetActive(!_characterDetailsPanel.activeSelf);

        if(selectable is Humanoid)
            UpdateCharacterPanel(selectable as Humanoid);
    }

    public void ActivateInventory(Humanoid humanoid)
    {
        _characterDetailsPanel.SetActive(true);
        UpdateCharacterPanel(humanoid);
    }

    public void DeactivateInventory()
    {
        _characterDetailsPanel.SetActive(false);
    }

    public void UpdateCharacterPanel(Humanoid humanoid)
    {
        UpdateHumanoid(humanoid);
        
        UpdateInventory();
        UpdateEquippedItems();
        UpdateStats();
    }

    private void UpdateHumanoid(Humanoid humanoid)
    {
        if(humanoid != null)
        {
            if (currentHumanoid != null)
            {
                currentHumanoid.OnBuffsChanged -= UpdateStats;
                currentHumanoid.GunController.OnBuffsChanged -= UpdateStats;
            }

            currentHumanoid = humanoid;

            currentHumanoid.OnBuffsChanged += UpdateStats;
            currentHumanoid.GunController.OnBuffsChanged += UpdateStats;
        }
    }

    #region Inventory

    public void UpdateInventory()
    {
        ClearInventory();
        UpdateInventorySlots();
    }

    private void UpdateInventorySlots()
    {   
        if(currentHumanoid != null)
        {
            Item[] inventory = currentHumanoid.InventoryManager.Inventory;
            for (int i = 0; i < inventory.Length; i++)
            {
                if (inventory[i] != null)
                    _inventorySlots[i].GetComponent<ItemData>().UpdateItemData(inventory[i]);
            }
        }
    }

    private void ClearInventory()
    {
        for (int i = 0; i < _inventorySlots.Count; i++)
            _inventorySlots[i].GetComponent<ItemData>().ClearItemData();
    }

    #endregion

    #region Stats

    public void UpdateStats()
    {
        ClearStats();
        UpdateStatTexts();
    }

    private void UpdateStatTexts()
    {
        if (currentHumanoid != null)
        {
            _namePanel.GetComponentInChildren<Text>().text = currentHumanoid.name + " lvl " + currentHumanoid.level;
            _unitStatsData[0].UpdateData("Health: ", Mathf.Round(currentHumanoid.CurrentHealth) + " / " + currentHumanoid.maxHealth.GetValue());
            _unitStatsData[3].UpdateData("Defence: ", currentHumanoid.defence.GetValue().ToString());

            SetHumanoidStats(currentHumanoid);
        }
    }

    private void SetHumanoidStats(Humanoid humanoid)
    {
        Weapon weapon = humanoid.GunController.EquippedWeapon;
        float weaponAccuracy = 0;
        if (weapon != null)
        {
            int weaponBonus = (weapon.isTwoHander) ? humanoid.GunController.GetBonusGunStat(x => x.TwoHandDamageBonus) : humanoid.GunController.GetBonusGunStat(x => x.OneHandDamageBonus);
            _weaponStatsData[0].UpdateData("Damage: ", weapon.baseDamage + humanoid.GunController.GetBonusGunStat(x => x.DamageBonus) + weaponBonus + "");
            _weaponStatsData[1].UpdateData("Fire Rate: ", weapon.attacksPerSecond.ToString());
            _weaponStatsData[2].UpdateData("Effective Range: ", weapon.effectiveRange + humanoid.GunController.GetBonusGunStat(x => x.EffectiveRange) + "m");

            weaponAccuracy = (weapon.isTwoHander) ? humanoid.GunController.GetBonusGunStat(x => x.TwoHandAccuracyBonus) : humanoid.GunController.GetBonusGunStat(x => x.OneHandAccuracyBonus);
        }

        float regeneration = (humanoid.regenAmount + humanoid.GetBonusUnitStat(x => x.HealthRegenBonus)) / (humanoid.regenTimer - humanoid.GetBonusUnitStat(x => x.HealthRegenTimerBonus));
        _unitStatsData[1].UpdateData("Regeneration: ", string.Format("{0:0.0}hp/s", regeneration));
      
        _unitStatsData[2].UpdateData("Accuracy: ", Mathf.RoundToInt((humanoid.accuracy + humanoid.GunController.GetBonusGunStat(x => x.AccuracyBonus) + weaponAccuracy) * 100) + "%");
        _unitStatsData[4].UpdateData("Experience: ", Mathf.RoundToInt(humanoid.Experience) + " / " + Mathf.RoundToInt(humanoid.ExperienceCap));

        _weaponStatsData[3].UpdateData("Critical Chance: ", Mathf.RoundToInt((humanoid.GunController.critChance + humanoid.GunController.GetBonusGunStat(x => x.CriticalChanceBonus)) * 100) + "%");
        _weaponStatsData[4].UpdateData("Critical Damage: ", Mathf.RoundToInt((humanoid.GunController.critDamage + humanoid.GunController.GetBonusGunStat(x => x.CriticalDamageBonus)) * 100) + "%");
    }
    
    private void ClearStats()
    {
        _namePanel.GetComponentInChildren<Text>().text = "";
        ClearStatsPanel(_unitStatsData);
        ClearStatsPanel(_weaponStatsData);
    }

    private void ClearStatsPanel(TooltipData[] array)
    {
        for (int i = 0; i < array.Length; i++)
            array[i].ClearData();
    }

    #endregion

    #region Equipment

    public void UpdateEquippedItems()
    {
        ClearEquippedItems();
        UpdateEquipmentSlots();
    }

    private void UpdateEquipmentSlots()
    {
        if(currentHumanoid != null)
        {
            Equipment[] equipment = currentHumanoid.EquipmentManager.CurrentEquipment;
            for (int i = 0; i < equipment.Length; i++)
            {
                if (equipment[i] != null)
                {
                    if (_equipmentSlots[i].transform.childCount > 0)
                        _equipmentSlots[i].transform.GetChild(0).GetComponent<ItemData>().UpdateItemData(equipment[i]);
                }
            }
        } 
    }

    private void ClearEquippedItems()
    {
        for (int i = 0; i < _equipmentSlots.Count; i++)
        {
            if(_equipmentSlots[i].transform.childCount > 0)
                _equipmentSlots[i].transform.GetChild(0).GetComponent<ItemData>().ClearItemData();
        }
    }

    #endregion


    public void InitiateLootTutorial(Humanoid humanoid)
    {
        if(!TutorialManager.Instance.FirstWeaponPickup)
        {
            tutorialImage = Instantiate<Image>(Resources.Load<Image>("Sprites/UI/TutorialImage"));
            tutorialImage.transform.SetParent(_characterDetailsPanel.transform, false);
            StartCoroutine(CloseTutorial(20f));
        }
    }

    IEnumerator CloseTutorial(float timer)
    {
        while(timer >= 0)
        {
            if (!_characterDetailsPanel.gameObject.activeInHierarchy)
            {
                Destroy(tutorialImage);
                yield break;
            }
                
            timer -= Time.deltaTime;
            yield return null;
        }

        if (tutorialImage.gameObject != null && tutorialImage.gameObject.activeInHierarchy)
            Destroy(tutorialImage);
    }

}
