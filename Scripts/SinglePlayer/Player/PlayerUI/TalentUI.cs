﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TalentUI : MonoBehaviour
{
    private GameObject _characterTalentPanel;
    private Text _talentPointsText;
    private Humanoid currentHumanoid;
    private SkillsUI _skills;
    private InventoryUI _inventory;

    private List<GameObject> _marauderSlots = new List<GameObject>();
    private List<GameObject> _jaegerSlots = new List<GameObject>();
    private List<GameObject> _combatMedicSlots = new List<GameObject>();

    #region Initialization

    private void Start()
    {
        _characterTalentPanel = transform.Find("ActivePanel/CharacterTalentPanel").gameObject;
        _talentPointsText = _characterTalentPanel.transform.Find("TalentPointsPanel").GetComponentInChildren<Text>();
        _skills = GetComponent<SkillsUI>();
        _inventory = GetComponent<InventoryUI>();

        InitializeTalentSlots();
        _characterTalentPanel.SetActive(false);

        UpdateTalentTree(null);
    }

    private void InitializeTalentSlots()
    {
        TalentSlot[] marauderSlots = _characterTalentPanel.transform.GetChild(1).GetComponentsInChildren<TalentSlot>();
        TalentSlot[] jaegerSlots = _characterTalentPanel.transform.GetChild(2).GetComponentsInChildren<TalentSlot>();
        TalentSlot[] combatMedicSlots = _characterTalentPanel.transform.GetChild(3).GetComponentsInChildren<TalentSlot>();

        AddTalentsToListFromArray(_marauderSlots, marauderSlots);
        AddTalentsToListFromArray(_jaegerSlots, jaegerSlots);
        AddTalentsToListFromArray(_combatMedicSlots, combatMedicSlots);
    }

    private void AddTalentsToListFromArray(List<GameObject> list, TalentSlot[] array)
    {
        for (int i = 0; i < array.Length; i++)
        {
            list.Add(array[i].gameObject);
            array[i].OnTalentClicked += OnTalentClicked;
        }
    }

    #endregion

    public void ActivateTalentTree(Selectable selectable)
    {
        if (!TutorialManager.Instance.FirstTalentPanelOpened)
            TutorialManager.Instance.TriggerTalentPanelTutorial();

        _characterTalentPanel.SetActive(!_characterTalentPanel.activeSelf);
        if(selectable is Humanoid)
            UpdateTalentTree(selectable as Humanoid);
    }

    public void UpdateTalentTree(Humanoid humanoid)
    {
        UpdateCurrentHumanoid(humanoid);
        ClearTalentTrees();
        UpdateTalentTrees();
    }

    private void UpdateCurrentHumanoid(Humanoid humanoid)
    {
        currentHumanoid = humanoid;
    }

    private void ClearTalentTrees()
    {
        ClearTree(_marauderSlots);
        ClearTree(_jaegerSlots);
        ClearTree(_combatMedicSlots);
    }

    private void ClearTree(List<GameObject> list)
    {
        for (int i = 0; i < list.Count; i++)
            list[i].GetComponent<TalentSlot>().ClearTalentSlot();
    }

    private void UpdateTalentTrees()
    {
        if (currentHumanoid != null)
        {
            UpdateTree(_marauderSlots, currentHumanoid.TalentSystem.MarauderTalents, currentHumanoid.TalentSystem.marauderPoints);
            UpdateTree(_jaegerSlots, currentHumanoid.TalentSystem.JaegerTalents, currentHumanoid.TalentSystem.jaegerPoints);
            UpdateTree(_combatMedicSlots, currentHumanoid.TalentSystem.CombatMedicTalents, currentHumanoid.TalentSystem.combatMedicPoints);
            _talentPointsText.text = "Available talent points: " + currentHumanoid.TalentSystem.TalentPoints;
        }
        else
            _talentPointsText.text = " ";
    }

    private void UpdateTree(List<GameObject> list, Talent[] array, int points)
    {
        for (int i = 0; i < array.Length; i++)
        {
            list[array[i].talentIndex].GetComponent<TalentSlot>().UpdateTalentSlot(array[i], currentHumanoid.TalentSystem.CheckAvailability(array, array[i], points));
        }          
    }

    public void InvestPoints(int tree)
    {
        if (currentHumanoid != null)
        {
            currentHumanoid.TalentSystem.InvestPoint(tree);
            UpdateClickedTree(tree);
        }
    }

    private void OnTalentClicked(Talent talent)
    {
        if (currentHumanoid != null)
        {
            currentHumanoid.TalentSystem.ActivateTalent(talent);
            UpdateClickedTree((int)talent.currentTree);
            _skills.UpdateSkills(currentHumanoid);
            _inventory.UpdateCharacterPanel(currentHumanoid);
        }
    }

    private void UpdateClickedTree(int tree)
    {
        switch(tree)
        {
            case 0:
                UpdateTree(_marauderSlots, currentHumanoid.TalentSystem.MarauderTalents, currentHumanoid.TalentSystem.marauderPoints);
                break;
            case 1:
                UpdateTree(_jaegerSlots, currentHumanoid.TalentSystem.JaegerTalents, currentHumanoid.TalentSystem.jaegerPoints);
                break;
            case 2:
                UpdateTree(_combatMedicSlots, currentHumanoid.TalentSystem.CombatMedicTalents, currentHumanoid.TalentSystem.combatMedicPoints);
                break;
        }
        _talentPointsText.text = "Available talent points: " + currentHumanoid.TalentSystem.TalentPoints;
    }

}
