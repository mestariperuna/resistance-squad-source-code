﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionUI : MonoBehaviour
{
    public event System.Action<int> OnSelectionSlotClicked;
    private GameObject _selectionPanel;
    private List<GameObject> _selectionSlots = new List<GameObject>();

    #region Initialization

    private void Start()
    {
        _selectionPanel = transform.Find("ActivePanel/CharacterPanel").gameObject;
        InitializeSelectionSlots();
    }

    private void InitializeSelectionSlots()
    {
        for (int j = 0; j < _selectionPanel.transform.childCount; j++)
        {
            _selectionSlots.Add(_selectionPanel.transform.GetChild(j).gameObject);
            SelectionSlot selectSlot = _selectionSlots[j].GetComponent<SelectionSlot>();

            selectSlot.SlotId = j;
            selectSlot.SelectOnClick += SelectOnClick;
        }
    }
    
    #endregion

    public void UpdateAvailableUnits(List<Selectable> availableUnits)
    {
        ClearAvailableUnits();
        ShowAvailableUnits(availableUnits);
    }

    public void HighlightSelection(Selectable toHighlight)
    {
        for (int i = 0; i < _selectionSlots.Count; ++i)
        {
            SelectionSlot selectionSlot = _selectionSlots[i].GetComponent<SelectionSlot>();
            Selectable selectable = selectionSlot.CurrentHumanoid;
            if (selectable != null)
            {
                if (toHighlight == selectable)
                    selectionSlot.HighlightSelectionSlot();
                else
                    selectionSlot.ClearHighlight();
            }
        }
    }

    private void ClearAvailableUnits()
    {
        for (int i = 0; i < _selectionSlots.Count; i++)
            _selectionSlots[i].SetActive(false);
    }

    private void ShowAvailableUnits(List<Selectable> availableUnits)
    {
        for (int i = 0; i < availableUnits.Count; i++)
        {
            _selectionSlots[i].SetActive(true);
            _selectionSlots[i].GetComponent<SelectionSlot>().UpdateSelectionSlot(availableUnits[i]);
        }
    }

    private void SelectOnClick(SelectionSlot pressedObject)
    {
        if (OnSelectionSlotClicked != null)
            OnSelectionSlotClicked(pressedObject.SlotId);
    }

}
