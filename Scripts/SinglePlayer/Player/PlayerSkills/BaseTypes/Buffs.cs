﻿using UnityEngine;
using System;
[Serializable]
public class UnitBonusStats
{
    public int HealthBonus { get; set; }
    public int HealthRegenBonus { get; set; }
    public float HealthRegenTimerBonus { get; set; }

    public int ArmorBonus { get; set; }
    public int ShieldArmorBonus { get; set; }

    public float MovementSpeedBonus { get; set; }
    public float AttackRangeBonus { get; set; }
}

[Serializable]
public class GunBonusStats
{
    public int DamageBonus { get; set; }
    public int OneHandDamageBonus { get; set; }
    public int TwoHandDamageBonus { get; set; }
    public int UpCloseDamageBonus { get; set; }

    public float AccuracyBonus { get; set; }
    public float OneHandAccuracyBonus { get; set; }
    public float TwoHandAccuracyBonus { get; set; }

    public float ArmourPenetrationBonus { get; set; }

    public float CriticalChanceBonus { get; set; }
    public float CriticalDamageBonus { get; set; }
    public float EffectiveRange { get; set; }

}

[Serializable]
public class UnitBuff
{
    public string BuffName { get; set; }
    public float Duration { get; set; }
    public int MaxStacks { get; set; }
    public UnitBonusStats Stats { get; set; }
}

[Serializable]
public class GunBuff
{
    public string BuffName { get; set; }
    public float Duration { get; set; }
    public int MaxStacks { get; set; }
    public GunBonusStats Stats { get; set; }
}