﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierArmorBonus : MonoBehaviour {

    [Header("Barrier bonus attributes")]
    [Tooltip("Increase armor in radius")]
    public int armorbonus = 100;
    [Tooltip("Armor bonus range")]
    public float range = 2;
    public LayerMask alliesMask;
    private UnitBonusStats _UnitBonusStats;
    private float timer = 0.5f;
    private UnitBuff buff;


    // Use this for initialization
    private void Start() {

        alliesMask = LayerMask.GetMask("Silhouette");

        // Define bonus stats that this skill applies.
        _UnitBonusStats = new UnitBonusStats()
        {
            ArmorBonus = armorbonus
        };

        // Create new buff.
        buff = new UnitBuff
        {
            BuffName = "BarrierCover",
            Duration = 1,
            MaxStacks = 1,
            Stats = _UnitBonusStats
        };

        StartCoroutine(ArmorAura(timer));
	}
	
    IEnumerator ArmorAura(float timer)
    {
        while (true)
        {
            Collider[] allies = Physics.OverlapSphere(transform.position, range, alliesMask);

            for (int i = 0; i < allies.Length; i++)
            {
                Humanoid humanoid = allies[i].GetComponent<Humanoid>();
                if (humanoid != null)
                    humanoid.AddBuff(buff);
                NetworkHumanoid networkhumanoid = allies[i].GetComponent<NetworkHumanoid>();
                if (networkhumanoid != null)
                    networkhumanoid.ServerAddBuff(buff);
            }

            yield return new WaitForSeconds(timer);
        }
    }

}
