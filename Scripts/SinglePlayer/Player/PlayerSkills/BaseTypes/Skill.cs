﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill : MonoBehaviour, ISkill
{
    #region Variables

    public enum Effect { AOE, Skillshot, Enemies, Allies, Self };
    public Effect effect; // has no real meaning, used to define skill type

    [Header("Skill properties")]
    public new string name;
    public Sprite icon;
    public string iconName;
    public string description;
    [Range(0, 30)]
    public float skillRange;
    protected int currentRank = 0;

    [Header("Cooldowns")]
    [Range(0, 300)]
    public float cooldown;
    //public int energyCost;
    [HideInInspector]
    public bool onCooldown { get; set; }

    public float Percentage { get; private set; }


    #endregion

    public virtual void UseSkill()
    {
        if(!onCooldown)
        {
            ActivateSkill();
            Cooldown(cooldown);
        }
    }

    protected virtual void ActivateSkill()
    {
        // override skill implementation
    }

    public virtual void UpgradeSkill()
    {
        currentRank++;
    }

    public virtual void Cooldown(float cd)
    {
        StartCoroutine(AnimateCooldown(cd));
    }

    IEnumerator AnimateCooldown(float cd)
    {
        onCooldown = true;

        while (cd > 0)
        {
            cd -= Time.deltaTime;
            Percentage = cd / cooldown;
            yield return null;
        }

        onCooldown = false;
    }

    // used for networking
    public virtual void TriggerTargetedSkill(NetworkSelectable selectable)
    {
        // override implementation
    }

    // used for networking
    public virtual void TriggerCastedSkill(NetworkHumanoid caster, Vector3 target)
    {
        // override impelementation
    }

}
