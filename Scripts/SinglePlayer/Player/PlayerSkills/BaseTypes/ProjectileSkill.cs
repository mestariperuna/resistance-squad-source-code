﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ProjectileSkill : Skill
{
    [Header("Projectile")]
    public Transform projectileSpawn;
    protected GameObject projectile;

    [Header("Projectile properties")]
    public float effectRadius;
    public float effectDamage;

    protected bool activated;
    protected Vector3 targetPos;

	protected virtual void Update ()
    {
		if(activated)
        {
            ListenInput(CanBeUsed());
        }
	}

    public override void UseSkill()
    {
        if (!onCooldown)
            activated = true;
    }

    protected void ListenInput(bool canBeUsed)
    {
        if (canBeUsed)
        {
            DrawPath();

            if (InputFunctions.GetKeyUp("Select"))
                ActivateSkill();

            if (InputFunctions.GetKeyUp("Command"))
                DeactiveSkill();           
        }
    }

    protected virtual bool CanBeUsed()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100) && !onCooldown)
        {
            CalculateTargetPosition(hit.point);
            if (EventSystem.current != null)
            {
                if (EventSystem.current.IsPointerOverGameObject())
                    return false;
            }
            return true;
        }
        return false;
    }

    protected void CalculateTargetPosition(Vector3 hitPoint)
    {     
        Vector3 skillVector = hitPoint - projectileSpawn.position;
        if (skillVector.magnitude > skillRange)
            targetPos = projectileSpawn.position + skillVector.normalized * skillRange;
        else
            targetPos = projectileSpawn.position + skillVector;        
    }

    protected virtual void DrawPath()
    {

    }

    public virtual void DeactiveSkill()
    {

    }
}
