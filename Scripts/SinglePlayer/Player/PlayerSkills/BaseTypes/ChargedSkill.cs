﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargedSkill : Skill
{
    [Header("Charges")]
    public int charges;
    protected int currentCharges;

    public override void UseSkill()
    {
        if (currentCharges > 0)
        {
            currentCharges--;
            ActivateSkill();
            Cooldown(cooldown);
        }
    }

    public override void Cooldown(float cd)
    {
        StartCoroutine(AnimateCooldown(cd));
    }

    IEnumerator AnimateCooldown(float cd)
    {
        onCooldown = true;

        while (cd > 0)
        {
            cd -= Time.deltaTime;
            yield return null;
        }

        currentCharges = Mathf.Clamp(currentCharges++,0, charges);

        onCooldown = false;
    }
}
