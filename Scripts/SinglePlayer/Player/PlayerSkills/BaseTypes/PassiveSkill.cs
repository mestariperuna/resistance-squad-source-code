﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Baseclass for passive skills.

public class PassiveSkill : Skill
{
    [Header("Passive Skill Properties")]
    public bool isTrigger;
    [Range(0, 1)]
    public float triggerChance;

    public GunBonusStats GunBonusStatModifier { get; protected set; }
    public UnitBonusStats UnitBonusStatModifier { get; protected set; }

    protected virtual void Update()
    {
        if(!onCooldown && isTrigger)
        {
            int rand = Random.Range(0, 100);
            if (rand < triggerChance * 100)
                TriggerSkill();
        }
    }

    public virtual void InitializeStats()
    {
        // override skill initializing
    }

    protected virtual void TriggerSkill()
    {
        // override skill implementation
        Cooldown(cooldown);
    }

}
