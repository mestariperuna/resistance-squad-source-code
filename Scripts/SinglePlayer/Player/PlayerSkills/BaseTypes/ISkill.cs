﻿
public interface ISkill
{
    void UseSkill();
    void Cooldown(float cd);
}
