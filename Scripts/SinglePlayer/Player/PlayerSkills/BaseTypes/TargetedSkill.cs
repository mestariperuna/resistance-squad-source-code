﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetedSkill : Skill
{
    protected Selectable target;
    protected Humanoid skillHolder;
    protected bool activated;

    protected virtual void Start()
    {
        skillHolder = transform.parent.GetComponent<Humanoid>();
    }

    public override void UseSkill()
    {
        if (!onCooldown)
            activated = true;
    }

    protected virtual void Update()
    {
        if (activated)
        {
            if (InputFunctions.GetKeyUp("Select"))
                ActivateSkill();

            if (InputFunctions.GetKeyUp("Command"))
                activated = false;

            if (skillHolder != null && target != null)
                MoveToTarget();
        }
    }

    protected override void ActivateSkill()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100))
        {
            Selectable selectable = hit.transform.GetComponent<Selectable>();
            if (selectable != null)
                target = selectable;
        }
    }

    protected void MoveToTarget()
    {
        if (Vector3.Distance(transform.position, target.transform.position) > skillRange)
            skillHolder.MoveUnit(target.transform.position);
        else
            CastSkill();
    }

    protected virtual void CastSkill()
    {
        // override skill implementation
        target = null;
        activated = false;
        Cooldown(cooldown);
    }

    public virtual void DeactivateSkill()
    {
        if(activated)
        {
            activated = false;
        }
    }

}
