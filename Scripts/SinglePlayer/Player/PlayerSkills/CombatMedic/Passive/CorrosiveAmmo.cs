﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorrosiveAmmo : PassiveSkill
{
    [Header("CorrosiveAmmo attributes")]
    [Range(0, 1)]
    public float[] armourPenetration = { 0.1f, 0.2f, 0.3f };

    public override void InitializeStats()
    {
        GunBonusStatModifier = new GunBonusStats()
        {
            ArmourPenetrationBonus = armourPenetration[currentRank]
        };
    }

    public override void UpgradeSkill()
    {
        currentRank++;
        GunBonusStatModifier.ArmourPenetrationBonus = armourPenetration[currentRank];
    }
}
