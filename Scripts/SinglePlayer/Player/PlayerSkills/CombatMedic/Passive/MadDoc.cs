﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MadDoc : PassiveSkill
{
    private Humanoid skillHolder;

    private void Start()
    {
        skillHolder = transform.parent.GetComponent<Humanoid>();
    }

    public bool CastSkill()
    {
        if(!onCooldown)
        {
            if (skillHolder != null)
                skillHolder.Heal((skillHolder.maxHealth.GetValue() / 2));

            Cooldown(cooldown);
            return true;
        }
        return false;
    }

}
