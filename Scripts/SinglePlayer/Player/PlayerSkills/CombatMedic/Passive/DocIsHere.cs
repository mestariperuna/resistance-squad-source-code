﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DocIsHere : PassiveSkill
{
    [Header("DocIsHere skill attributes")]
    [Tooltip("Increase health regeneration within radius")]
    public int[] regen = { 1, 2, 3 };
    public float timer = 0.5f;
    public float buffTimer = 10f;
    public LayerMask alliesMask;
    private UnitBuff buff;

    private void Start()
    {
        UnitBonusStatModifier = new UnitBonusStats()
        {
            HealthRegenBonus = regen[currentRank]
        };

        buff = new UnitBuff()
        {
            BuffName = "Regeneration",
            Duration = buffTimer,
            MaxStacks = 1,
            Stats = UnitBonusStatModifier
        };

        StartCoroutine(RegenerationAura(timer));
    }

    public override void UpgradeSkill()
    {
        currentRank++;
        UnitBonusStatModifier.HealthRegenBonus = regen[currentRank];
    }

    IEnumerator RegenerationAura(float timer)
    {
        while(true)
        {
            Collider[] allies = Physics.OverlapSphere(transform.position, skillRange, alliesMask);

            for (int i = 0; i < allies.Length; i++)
            {
                Humanoid humanoid = allies[i].GetComponent<Humanoid>();
                if(humanoid != null)
                    humanoid.AddBuff(buff);
                NetworkHumanoid networkhumanoid = allies[i].GetComponent<NetworkHumanoid>();
                if (networkhumanoid != null)
                    networkhumanoid.ServerAddBuff(buff);
            }

            yield return new WaitForSeconds(timer);
        }
    }

}
