﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Adrenaline : PassiveSkill
{
    [Header("Adrenaline skill attributes")]   
    [Tooltip("Decrease health regeneration timer")]
    public float[] hpRegen = { 1.0f, 2.0f, 3.0f };
    [Tooltip("Increase movement speed")]
    public float[] movementSpeed = { 0.2f, 0.3f, 0.4f };

    public override void InitializeStats()
    {
        UnitBonusStatModifier = new UnitBonusStats()
        {
            HealthRegenTimerBonus = hpRegen[currentRank],
            MovementSpeedBonus = movementSpeed[currentRank]
        };
    }

    public override void UpgradeSkill()
    {
        currentRank++;
        UnitBonusStatModifier.HealthRegenTimerBonus = hpRegen[currentRank];
        UnitBonusStatModifier.MovementSpeedBonus = movementSpeed[currentRank];
    }
}
