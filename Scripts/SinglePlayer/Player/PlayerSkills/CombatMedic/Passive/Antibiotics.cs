﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Antibiotics : PassiveSkill
{
    private SkillManager skillHolder;
    private NetworkSkillManager altSkillHolder;

    private void Start()
    {
        skillHolder = transform.parent.GetComponent<SkillManager>();
        altSkillHolder = transform.parent.GetComponent<NetworkSkillManager>();
        if (skillHolder != null)
            UpgradeDocTools();

        if (altSkillHolder != null)
            AltUpgradeDocTools();
    }

    public override void UpgradeSkill()
    {
        if (skillHolder != null)
            UpgradeDocTools();

        if (altSkillHolder != null)
            AltUpgradeDocTools();
    }

    private void UpgradeDocTools()
    {
        for (int i = 0; i < skillHolder.UnlockedSkills.Count; i++)
        {
            DocsTools docsTools = skillHolder.UnlockedSkills[i].GetComponent<DocsTools>();
            if (docsTools != null)
            {
                docsTools.UpgradeSkill();
            }
        }
    }

    private void AltUpgradeDocTools()
    {
        for (int i = 0; i < altSkillHolder.UnlockedSkills.Count; i++)
        {
            DocsTools docsTools = altSkillHolder.UnlockedSkills[i].GetComponent<DocsTools>();
            if (docsTools != null)
            {
                docsTools.UpgradeSkill();
            }
        }
    }

}
