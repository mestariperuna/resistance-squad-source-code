﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TacticalShield : PassiveSkill
{
    [Header("Tactical shield attributes")]
    public int shieldArmourBonus = 5;

    public override void InitializeStats()
    {
        UnitBonusStatModifier = new UnitBonusStats()
        {
            ShieldArmorBonus = shieldArmourBonus
        };
    }

}
