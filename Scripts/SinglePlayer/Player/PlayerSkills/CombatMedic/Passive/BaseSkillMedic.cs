﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseSkillMedic : PassiveSkill
{
    [Header("Medic base skill attributes")]
    public int[] armourBonus = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 16, 18, 20, 22, 24 };
    public int[] healthBonus = { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 28, 32, 36, 40, 44, 48 };

    public override void InitializeStats()
    {
        UnitBonusStatModifier = new UnitBonusStats()
        {
            ArmorBonus = armourBonus[currentRank],
            HealthBonus = healthBonus[currentRank]
        };
    }

    public override void UpgradeSkill()
    {
        currentRank++;
        UnitBonusStatModifier.ArmorBonus = armourBonus[currentRank];
        UnitBonusStatModifier.HealthBonus = healthBonus[currentRank];
    }
}
