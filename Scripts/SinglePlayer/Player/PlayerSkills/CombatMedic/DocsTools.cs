﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DocsTools : TargetedSkill
{
    [Header("DocsTools attributes")]
    public int[] healingAmount = { 50, 75, 100, 125, 150 };
    public float regenTimer = 5.0f;
    public float buffDuration = 10f;
    private UnitBonusStats stats;
    private UnitBuff buff;
    private int maxRank = 4;

    protected override void Start()
    {
        base.Start();
        stats = new UnitBonusStats()
        {
            HealthRegenTimerBonus = regenTimer
        };

        buff = new UnitBuff()
        {
            BuffName = "Antibiotics",
            MaxStacks = 1,
            Duration = buffDuration,
            Stats = stats
        };
    }

    public override void UpgradeSkill()
    {
        currentRank++;
        if (currentRank >= maxRank)
            currentRank = maxRank;
    }

    protected override void CastSkill()
    {
        if(target != null)
        {
            if(target is Humanoid)
            {
                Humanoid humanoid = target as Humanoid;
                humanoid.Heal(healingAmount[currentRank]);
                if (currentRank == maxRank)
                    humanoid.AddBuff(buff);

                base.CastSkill();
            }
        }
    }

}
