﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MedicalSyringe : TargetedSkill
{
    [Header("MedicalSyringe attributes")]
    public float[] regenTimerBonus = { 0f, 3f };
    public int[] regenBonus = { 0, 5 };
    public float buffDuration = 20f;
    private UnitBuff buff;
    private UnitBonusStats stats;

    protected override void Start()
    {
        base.Start();
        stats = new UnitBonusStats()
        {
            HealthRegenTimerBonus = regenTimerBonus[currentRank],
            HealthRegenBonus = regenBonus[currentRank]
        };

        buff = new UnitBuff()
        {
            BuffName = name,
            MaxStacks = 1,
            Duration = buffDuration,
            Stats = stats
        };
    }

    public override void UpgradeSkill()
    {
        currentRank++;
        stats.HealthRegenTimerBonus = regenTimerBonus[currentRank];
        stats.HealthRegenBonus = regenBonus[currentRank];
    }

    protected override void CastSkill()
    {
        if (target != null)
        {
            if (target is Humanoid)
            {
                Humanoid humanoid = target as Humanoid;
                humanoid.AddBuff(buff);

                base.CastSkill();
            }
        }
    }
}
