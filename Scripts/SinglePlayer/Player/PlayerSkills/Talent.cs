﻿using UnityEngine;

// Talent holds information for talentsystem.
// It knows which skill it refers to and loads it from the resources.
// It knows which talent tree it belongs to, its tier etc.

public enum TalentTree { Marauder, Jaeger, CombatMedic};
public enum TalentTier { Tier_0, Tier_1, Tier_2, Tier_3, Tier_4 };
[CreateAssetMenu(fileName = "New Talent", menuName = "Talent")]

public class Talent : ScriptableObject
{   
    public string Name { get; private set; }
    public string Description { get; private set; }
    public Sprite Icon { get; private set; }
    public string IconName { get; private set; }
    [Header("Talent attributes")]
    [Tooltip("Position in talent tree")]
    public int talentIndex;

    [Tooltip("In which tree does this talent belong to?")]
    public TalentTree currentTree;

    [Tooltip("Which tier does this talent belong to?")]
    public TalentTier talentTier;

    [Tooltip("Does this talent require other talents to be activated?")]
    public int[] requiredIndexes;

    [Tooltip("How many times can the talent be ranked up")]
    public int maxRank = 1;
    public int CurrentRank { get; private set; }

    [Tooltip("The name of the skill that this talent refers to")]
    public string skillString;

    public bool Active { get; private set; }
    public GameObject TalentSkill { get; private set; }
    public int pointsToActivate { get; private set; }

    private void Awake()
    {
        TalentSkill = Resources.Load<GameObject>("Skills/" + currentTree.ToString() + "/" + skillString);
        CurrentRank = 0;

        switch (talentTier)
        {
            case TalentTier.Tier_0:
                pointsToActivate = 0;
                break;
            case TalentTier.Tier_1:
                pointsToActivate = 3;
                break;
            case TalentTier.Tier_2:
                pointsToActivate = 7;
                break;
            case TalentTier.Tier_3:
                pointsToActivate = 12;
                break;
            case TalentTier.Tier_4:
                pointsToActivate = 18;
                break;
        }

        if(TalentSkill != null)
        {
            Skill skill = TalentSkill.GetComponent<Skill>();
            Name = skill.name;
            Description = skill.description;
            Icon = skill.icon;
            IconName = skill.iconName;
        }
    }

    public void Activate()
    {
        if (!Active)
            Active = true;

        if (CurrentRank < maxRank)
            CurrentRank++;
    }

    public int GetTalentTree()
    {
        return (int)currentTree;
    }
}