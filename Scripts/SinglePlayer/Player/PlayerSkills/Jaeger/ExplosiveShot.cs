﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveShot : ProjectileSkill
{
    private Humanoid skillHolder;
    private LineRenderer _lineRenderer;

    private void Start()
    {
        projectile = Resources.Load<GameObject>("GFX/Projectiles/ExplosiveRound");
        skillHolder = transform.parent.GetComponent<Humanoid>();
        _lineRenderer = GetComponent<LineRenderer>();
        _lineRenderer.enabled = false;
    }

    protected override void ActivateSkill()
    {
        InstantiateExplosiveRound();
        DeactiveSkill();

        Cooldown(cooldown);
    }

    private void InstantiateExplosiveRound()
    {
        ExplosiveRound explosiveRound = Instantiate(projectile, projectileSpawn.position, Quaternion.identity).GetComponent<ExplosiveRound>();
        explosiveRound.Damage = (int)effectDamage;
        explosiveRound.ExplosionRadius = effectRadius;
        Vector3 dir = CalculateDirection(targetPos);
        explosiveRound.Direction = dir;
        StopAndFaceSkillHolder(dir);
    }

    private void StopAndFaceSkillHolder(Vector3 dir)
    {
        if (skillHolder != null)
        {
            skillHolder.StopUnit();
            skillHolder.transform.rotation = Quaternion.LookRotation(dir);
        }
    }

    private Vector3 CalculateDirection(Vector3 targetPos)
    {
        Vector3 newTargetPos = new Vector3(targetPos.x, projectileSpawn.position.y, targetPos.z);
        return (newTargetPos - projectileSpawn.position).normalized;
    }

    protected override void DrawPath()
    {
        _lineRenderer.enabled = true;
        _lineRenderer.positionCount = 2;

        Vector3 startPos = new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z);
        Vector3 endPos = new Vector3(targetPos.x, transform.position.y + 0.5f, targetPos.z);


        _lineRenderer.SetPosition(0, startPos);
        _lineRenderer.SetPosition(1, endPos);
    }

    public override void DeactiveSkill()
    {
        if(activated)
        {
            activated = false;
            _lineRenderer.positionCount = 0;
            _lineRenderer.enabled = false;
        }
    }

}
