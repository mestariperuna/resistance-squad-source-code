﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeAim : PassiveSkill
{
    [Header("Take Aim attributes")]
    public float effectiveRangeBonus = 5f;
    public float attackRangeBonus = 3f;

    public override void InitializeStats()
    {
        UnitBonusStatModifier = new UnitBonusStats()
        {
            AttackRangeBonus = attackRangeBonus
        };

        GunBonusStatModifier = new GunBonusStats()
        {
            EffectiveRange = effectiveRangeBonus
        };
    }
}
