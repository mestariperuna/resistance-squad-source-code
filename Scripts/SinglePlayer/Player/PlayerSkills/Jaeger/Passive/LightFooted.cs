﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFooted : PassiveSkill
{
    [Header("LightFooted attributes")]
    public float movementSpeedBonus = 0.4f;

    public override void InitializeStats()
    {
        UnitBonusStatModifier = new UnitBonusStats()
        {
            MovementSpeedBonus = movementSpeedBonus
        };
    }

}
