﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseSkillJaeger : PassiveSkill
{
    [Header("Jaeger base skill attributes")]
    [Range(0, 1)]
    public float[] accuracyBonus = { 0.01f, 0.02f, 0.03f, 0.04f, 0.05f, 0.06f, 0.07f, 0.08f, 0.09f, 0.1f, 0.11f, 0.12f, 0.14f, 0.16f, 0.18f, 0.2f, 0.22f, 0.24f };
    [Range(0, 1)]
    public float[] criticalChanceBonus = { 0.01f, 0.02f, 0.03f, 0.04f, 0.05f, 0.06f, 0.07f, 0.08f, 0.09f, 0.1f, 0.11f, 0.12f, 0.14f, 0.16f, 0.18f, 0.2f, 0.22f, 0.24f };

    public override void InitializeStats()
    {
        GunBonusStatModifier = new GunBonusStats()
        {
            AccuracyBonus = accuracyBonus[currentRank],
            CriticalChanceBonus = criticalChanceBonus[currentRank]
        };
    }

    public override void UpgradeSkill()
    {
        currentRank++;
        GunBonusStatModifier.AccuracyBonus = accuracyBonus[currentRank];
        GunBonusStatModifier.CriticalChanceBonus = criticalChanceBonus[currentRank];
    }
}
