﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Marksman : PassiveSkill
{
    [Header("Marksman attributes")]
    [Range(0, 1)]
    public float[] twohandAccuracyBonus = { 0.1f, 0.2f };

    public override void InitializeStats()
    {
        GunBonusStatModifier = new GunBonusStats()
        {
            TwoHandAccuracyBonus = twohandAccuracyBonus[currentRank]
        };
    }

    public override void UpgradeSkill()
    {
        currentRank++;
        GunBonusStatModifier.TwoHandAccuracyBonus = twohandAccuracyBonus[currentRank];
    }



}
