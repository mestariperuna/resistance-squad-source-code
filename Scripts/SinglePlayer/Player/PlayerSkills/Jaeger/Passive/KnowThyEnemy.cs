﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnowThyEnemy : PassiveSkill
{
    [Header("KnowThyEnemy attributes")]
    [Range(0,1)]
    public float[] criticalChance = { 0.05f, 0.10f, 0.15f };

    public override void InitializeStats()
    {
        GunBonusStatModifier = new GunBonusStats()
        {
            CriticalChanceBonus = criticalChance[currentRank]
        };
    }

    public override void UpgradeSkill()
    {
        currentRank++;
        GunBonusStatModifier.CriticalChanceBonus = criticalChance[currentRank];
    }
}
