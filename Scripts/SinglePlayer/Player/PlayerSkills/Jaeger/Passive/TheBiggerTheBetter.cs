﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheBiggerTheBetter : PassiveSkill
{
    [Header("The Bigger The Better attributes")]
    public int[] twohandDamageBonus = { 4, 8 };
    public int[] healthBonus = { 30, 60 };

    public override void InitializeStats()
    {
        UnitBonusStatModifier = new UnitBonusStats()
        {
            HealthBonus = healthBonus[currentRank]
        };

        GunBonusStatModifier = new GunBonusStats()
        {
            TwoHandDamageBonus = twohandDamageBonus[currentRank]
        };
    }

    public override void UpgradeSkill()
    {
        currentRank++;
        GunBonusStatModifier.TwoHandDamageBonus = twohandDamageBonus[currentRank];
        UnitBonusStatModifier.HealthBonus = healthBonus[currentRank];
    }

}
