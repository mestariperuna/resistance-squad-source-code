﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteadyHand : PassiveSkill
{
    [Header("SteadyHand attributes")]
    public float effectiveRangeBonus = 5.0f;
    public float twohandAccuracyBonus = 0.15f;

    public override void InitializeStats()
    {
        GunBonusStatModifier = new GunBonusStats()
        {
            EffectiveRange = effectiveRangeBonus,
            TwoHandAccuracyBonus = twohandAccuracyBonus
        };
    }

}
