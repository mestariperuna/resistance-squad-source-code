﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HigherCaliber : PassiveSkill
{
    [Header("Higher Caliber attributes")]
    public float armourPenetrationBonus = 0.5f;
    public float criticalDamageBonus = 1.0f;

    public override void InitializeStats()
    {
        GunBonusStatModifier = new GunBonusStats()
        {
            ArmourPenetrationBonus = armourPenetrationBonus,
            CriticalDamageBonus = criticalDamageBonus
        };
    }

}
