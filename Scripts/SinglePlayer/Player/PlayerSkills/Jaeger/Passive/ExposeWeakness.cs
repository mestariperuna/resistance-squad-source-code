﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExposeWeakness : PassiveSkill
{
    [Header("ExposeWeakness attributes")]
    public float[] criticalDamageBonus = { 0.25f, 0.5f };
    public float[] armourPenetrationBonus = { 0.15f, 0.3f};

    public override void InitializeStats()
    {
        GunBonusStatModifier = new GunBonusStats()
        {
            CriticalDamageBonus = criticalDamageBonus[currentRank],
            ArmourPenetrationBonus = armourPenetrationBonus[currentRank]
        };
    }

    public override void UpgradeSkill()
    {
        currentRank++;
        GunBonusStatModifier.CriticalDamageBonus = criticalDamageBonus[currentRank];
        GunBonusStatModifier.ArmourPenetrationBonus = armourPenetrationBonus[currentRank];
    }
}
