﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

// TalentSystem holds information of all talents in the game.
// TalentSystem has a reference to SkillSystem, which holds information of all skills and talents for each
// game character.
// TalentSystem also holds the information of usable talentpoints for each character.
// Dependency on SkillManager.

public class TalentSystem : MonoBehaviour
{

    public int TalentPoints { get; private set; }
    public int marauderPoints { get; private set; }
    public int jaegerPoints { get; private set; }
    public int combatMedicPoints { get; private set; }

    private int maxMarauderPoints;
    private int maxJaegerPoints;
    private int maxCombatMedicPoints;

    // All talents in class based arrays.
    public Talent[] MarauderTalents { get; private set; } 
    public Talent[] JaegerTalents { get; private set; }
    public Talent[] CombatMedicTalents { get; private set; }

    public event Action OnZeroTalentPoints; 

    private SkillManager _skillManager;


    private void Awake()
    {
        _skillManager = GetComponent<SkillManager>();
    }

    private void Start ()
    {     
        MarauderTalents = InitializeTalentArray("Marauder");
        JaegerTalents = InitializeTalentArray("Jaeger");
        CombatMedicTalents = InitializeTalentArray("CombatMedic");

        maxMarauderPoints = MarauderTalents[0].maxRank;
        maxJaegerPoints = JaegerTalents[0].maxRank;
        maxCombatMedicPoints = CombatMedicTalents[0].maxRank;
    }

    // Load all talents and add them to an array.
    private Talent[] InitializeTalentArray(string treeString)
    {
        Talent[] talents = Resources.LoadAll<Talent>("Talents/" + treeString);
        Talent[] tree = new Talent[talents.Length];

        for (int i = 0; i < tree.Length; i++)
        {
            tree[talents[i].talentIndex] = Instantiate(talents[i]);
        }

        return tree;
    }

    public void OnLevelUp(int points)
    {
        TalentPoints += points;
    }

    public void InvestPoint(int tree)
    {
        if (TalentPoints > 0)
        {
            switch (tree)
            {
                case 0:
                    if(marauderPoints < maxMarauderPoints)
                        marauderPoints++;                     
                    break;
                case 1:
                    if(jaegerPoints < maxJaegerPoints)
                        jaegerPoints++;                    
                    break;
                case 2:
                    if(combatMedicPoints < maxCombatMedicPoints)
                        combatMedicPoints++;
                    break;
                default:
                    break;
            }
        }
        else
            Debug.Log("No points remaining");
    }

    public void ActivateTalent(Talent pressedTalent)
    {
        bool requirementsMet = false;
        TalentTree tree = pressedTalent.currentTree;
        switch (tree)
        {
            case TalentTree.Marauder:
                if (marauderPoints >= pressedTalent.pointsToActivate)
                    requirementsMet = CheckRequirements(MarauderTalents, pressedTalent);
                break;
            case TalentTree.Jaeger:
                if (jaegerPoints >= pressedTalent.pointsToActivate)
                    requirementsMet = CheckRequirements(JaegerTalents, pressedTalent);
                break;
            case TalentTree.CombatMedic:
                if (combatMedicPoints >= pressedTalent.pointsToActivate)
                    requirementsMet = CheckRequirements(CombatMedicTalents, pressedTalent);
                break;
            default:
                break;
        }

        if (TalentPoints > 0 && requirementsMet)
        {
            _skillManager.UnlockTalent(pressedTalent);
            TalentPoints--;
        }
        else
            Debug.Log("Not enough points or skill already at max rank");

        if (TalentPoints <= 0)
        {
            if (OnZeroTalentPoints != null)
                OnZeroTalentPoints();
        }
    }

    private bool CheckRequirements(Talent[] tree,Talent talent)
    {
        for (int i = 0; i < talent.requiredIndexes.Length; i++)
        {
            if (!tree[talent.requiredIndexes[i]].Active)
                return false;
        }

        if (talent.CurrentRank == talent.maxRank)
            return false;
       
        return true;
    }

    public bool CheckAvailability(Talent[] tree, Talent talent, int requiredPoints)
    {
        for (int i = 0; i < talent.requiredIndexes.Length; i++)
        {
            if (!tree[talent.requiredIndexes[i]].Active)
                return false;
        }

        if (talent.pointsToActivate > requiredPoints)
            return false;

        return true;
    }
}
