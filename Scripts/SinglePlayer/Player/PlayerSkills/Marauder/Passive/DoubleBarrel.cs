﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleBarrel : PassiveSkill
{
    private Humanoid skillHolder;

    private void Start()
    {
        skillHolder = transform.parent.GetComponent<Humanoid>();
    }

    protected override void TriggerSkill()
    {
        if(skillHolder != null)
        {
            if(skillHolder.currentState == Selectable.State.combat)
            {
                if(skillHolder.GunController.EquippedWeapon != null)
                {
                    if (skillHolder.GunController.EquippedWeapon.isTwoHander || skillHolder.SkillManager.HasJuggernautSkill())
                    {
                        skillHolder.OnPassiveSkillTrigger();
                        base.TriggerSkill();
                    }
                }
            }
        }
    }

}
