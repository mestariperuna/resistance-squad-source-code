﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slinger : PassiveSkill
{
    [Header("Slinger attributes")]
    [Tooltip("Accuracy modifier to one handed weapons")]
    [Range(0,1)]
    public float[] bonusOneHandAccuracy = { 0.05f, 0.1f, 0.15f };

    
    private void Start()
    {
        GunBonusStatModifier = new GunBonusStats()
        {
            OneHandAccuracyBonus = bonusOneHandAccuracy[currentRank]
        };
    }

    public override void UpgradeSkill()
    {
        currentRank++;
        GunBonusStatModifier.OneHandAccuracyBonus = bonusOneHandAccuracy[currentRank];
    }

}
