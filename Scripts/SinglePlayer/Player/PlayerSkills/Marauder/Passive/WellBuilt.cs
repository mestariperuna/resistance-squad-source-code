﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WellBuilt : PassiveSkill
{
    [Tooltip("Passive health bonus")]
    public int[] healthBonus = { 50, 100 };

	private void Start ()
    {
        UnitBonusStatModifier = new UnitBonusStats()
        {
            HealthBonus = healthBonus[currentRank]
        };
	}

    public override void UpgradeSkill()
    {
        currentRank++;
        UnitBonusStatModifier.HealthBonus = healthBonus[currentRank];
    }

}

