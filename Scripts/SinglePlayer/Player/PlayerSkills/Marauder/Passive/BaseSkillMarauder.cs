﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseSkillMarauder : PassiveSkill
{
    [Header("Marauder base skill attributes")]
    public int[] healthBonus = { 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 42, 48, 54, 60, 66, 72 };
    public float[] regenTimerBonus = { 0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f, 0.9f, 1.0f, 1.1f, 1.2f, 1.35f, 1.5f, 1.65f, 1.8f, 1.95f, 2.1f };

    public override void InitializeStats()
    {
        UnitBonusStatModifier = new UnitBonusStats()
        {
            HealthBonus = healthBonus[currentRank],
            HealthRegenTimerBonus = regenTimerBonus[currentRank]
        };
    }

    public override void UpgradeSkill()
    {
        currentRank++;
        UnitBonusStatModifier.HealthBonus = healthBonus[currentRank];
        UnitBonusStatModifier.HealthRegenTimerBonus = regenTimerBonus[currentRank];
    }
}
