﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rage : PassiveSkill
{

    [Header("Rage attributes")]
    [Tooltip("Maximum stacks")]
    public int maxStacks = 5;
    [Tooltip("Damage bonus per buff")]
    public int damageBonus = 1;
    [Tooltip("How long does the buff last?")]
    public int buffLast = 30;

    public GunBuff Buff { get; set; }

	// Use this for initialization
	void Start ()
    {

        GunBonusStatModifier = new GunBonusStats()
        {
            DamageBonus = damageBonus
        };

        Buff = new GunBuff()
        {
            BuffName = "Rage",
            Duration = buffLast,
            MaxStacks = maxStacks,
            Stats = GunBonusStatModifier
        };
    }

}
