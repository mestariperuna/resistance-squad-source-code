﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpCloseAndPersonal : PassiveSkill
{

    [Header("Up Close And Personal attributes")]
    [Tooltip("Extra damage when close to the enemy")]
    public int[] upCloseBonusDamage = { 3, 6 };

	private void Start ()
    {
        GunBonusStatModifier = new GunBonusStats()
        {
            UpCloseDamageBonus = upCloseBonusDamage[currentRank]
        };
    }

    public override void UpgradeSkill()
    {
        currentRank++;
        GunBonusStatModifier.UpCloseDamageBonus = upCloseBonusDamage[currentRank];
    }
}
