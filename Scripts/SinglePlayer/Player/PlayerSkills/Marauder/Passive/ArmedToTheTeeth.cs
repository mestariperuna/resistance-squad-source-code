﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmedToTheTeeth : PassiveSkill
{
    [Header("ArmedToTheTeeth attributes")]
    [Range(0,1)]
    public float[] accuracyBonus = { 0.05f, 0.1f, 0.15f };

	private void Start ()
    {
        GunBonusStatModifier = new GunBonusStats()
        {
            TwoHandAccuracyBonus = accuracyBonus[currentRank]
        };
	}

    public override void UpgradeSkill()
    {
        currentRank++;
        GunBonusStatModifier.TwoHandAccuracyBonus = accuracyBonus[currentRank];
    }

}
