﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ShieldCharge : TargetedSkill
{
    [Header("ShieldCharge attributes")]
    public int damage = 30;
    [Tooltip("How fast do we move when charging")]
    public float chargeSpeed = 5f;
    [Header("Raycast Masks")]
    public LayerMask obstacleMask;
    public LayerMask groundMask;
    public LayerMask targetMask;
    private Vector3 chargePos;
    private LineRenderer _lineRenderer;

    protected override void Start()
    {
        _lineRenderer = GetComponent<LineRenderer>();
        _lineRenderer.enabled = false;
        base.Start();
    }

    protected override void Update()
    {
        if (activated)
        {
            if(SkillCanBeUsed())
            {
                DrawPath();

                if (InputFunctions.GetKeyUp("Select"))
                    CastSkill();
            }

            if (InputFunctions.GetKeyUp("Command"))
            {
                activated = false;
                DeactivateLineRenderer();
            }
        }
            
    }

    private bool SkillCanBeUsed()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100, groundMask) && !onCooldown)
        {
            if(skillHolder != null)
                CalculateChargeEndPosition(hit.point, skillHolder.transform.position);

            if (EventSystem.current != null)
            {
                if (EventSystem.current.IsPointerOverGameObject())
                    return false;
            }
            return true;
        }

        return false;
    }

    private void CalculateChargeEndPosition(Vector3 hitPoint, Vector3 pos)
    {
        Vector3 skillVector = hitPoint - pos;
        if (skillVector.magnitude > skillRange)
            chargePos = pos + skillVector.normalized * skillRange;
        else
            chargePos = pos + skillVector;
    }

    protected override void CastSkill()
    {
        Vector3 dirToTarget = (chargePos - skillHolder.transform.position).normalized;
        Ray ray = new Ray(skillHolder.transform.position, dirToTarget);

        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, skillRange, obstacleMask))
        {
            chargePos = hit.point - dirToTarget * 0.5f;
        }
        DeactivateLineRenderer();
        StartCoroutine(Charge(chargePos, dirToTarget));

        base.CastSkill();
    }


    private IEnumerator Charge(Vector3 chargePos, Vector3 dir)
    {
        yield return null;

        skillHolder.navMeshAgent.enabled = false;
        skillHolder.transform.rotation = Quaternion.LookRotation(dir);

        float dist = 0f;
        float distToCheckCollisions = 0.4f;
        
        List<Selectable> damagedTargets = new List<Selectable>();

        while (Vector3.Distance(skillHolder.transform.position, chargePos) > 0f)
        {
            float moveDistance = chargeSpeed * Time.deltaTime;
            skillHolder.transform.position = Vector3.MoveTowards(skillHolder.transform.position, chargePos, moveDistance);

            dist += moveDistance;
            if (dist >= distToCheckCollisions)
            {
                Collider[] targets = Physics.OverlapSphere(skillHolder.transform.position, dist, targetMask);

                for (int i = 0; i < targets.Length; i++)
                {
                    Selectable selectable = targets[i].GetComponent<Selectable>();
                    if (selectable != null)
                    {
                        // check that we don't damage same object more than once
                        if (!damagedTargets.Contains(selectable))
                        {
                            selectable.TakeDamage(damage, selectable.defence.GetValue() * selectable.damageReductionPercent);
                            damagedTargets.Add(selectable);
                        }
                    }
                }
                dist = 0;
            }
            yield return null;
        }
        skillHolder.navMeshAgent.enabled = true;
    }

    private void DrawPath()
    {
        if (skillHolder != null)
        {
            _lineRenderer.enabled = true;
            _lineRenderer.positionCount = 2;

            Vector3 startPos = new Vector3(skillHolder.transform.position.x, skillHolder.transform.position.y + 0.5f, skillHolder.transform.position.z);
            Vector3 endPos = new Vector3(chargePos.x, skillHolder.transform.position.y + 0.5f, chargePos.z);

            _lineRenderer.SetPosition(0, startPos);
            _lineRenderer.SetPosition(1, endPos);
        }
    }

    public override void DeactivateSkill()
    {
        if(activated)
        {
            DeactivateLineRenderer();
            activated = false;
        }
    }

    private void DeactivateLineRenderer()
    {
        _lineRenderer.positionCount = 0;
        _lineRenderer.enabled = false;
    }

}
