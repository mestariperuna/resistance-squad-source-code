﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : ProjectileSkill
{
    [Header("Grenade Properties")]
    public float maxHeight = 2;
    public float gravity = -10;

    private Rigidbody grenade;
    private LineRenderer _lineRenderer;

    void Start()
    {
        projectile = Resources.Load<GameObject>("GFX/Projectiles/Grenade");
        _lineRenderer = GetComponent<LineRenderer>();
        _lineRenderer.enabled = false;
    }

    protected override void ActivateSkill()
    {
        InstantiateGrenade();
        grenade.useGravity = true;
        grenade.velocity = CalculateGrenadeData().initialVelocity;

        DeactiveSkill();
        Cooldown(cooldown);
    }

    #region GrenadeData

    // Instantiate new grenade and set its damage and damaging radius
    void InstantiateGrenade()
    {
        GameObject newGrenade = Instantiate(projectile, projectileSpawn.position, projectileSpawn.rotation) as GameObject;
        grenade = newGrenade.GetComponent<Rigidbody>();
        GrenadeExplosion explosion = grenade.GetComponent<GrenadeExplosion>();
        explosion.ExplosionRadius = effectRadius;
        explosion.Damage = (int)effectDamage;
    }

    // Calculate velocity and time for grenade
    GrenadeData CalculateGrenadeData()
    {
        float displacementY = targetPos.y - projectileSpawn.position.y;
        Vector3 displacementXZ = new Vector3(targetPos.x - projectileSpawn.position.x, 0, targetPos.z - projectileSpawn.position.z);

        float height = displacementXZ.magnitude / 2;
        if (height < displacementY && displacementY < maxHeight)
            height = displacementY;

        float time = (Mathf.Sqrt(-2 * height / gravity) + Mathf.Sqrt(2 * (displacementY - height) / gravity));
        Vector3 velocityY = Vector3.up * Mathf.Sqrt(-2 * gravity * height);
        Vector3 velocityXZ = displacementXZ / time;

        return new GrenadeData(velocityXZ + velocityY, time);
    }

    // Draw the trajectory for grenade
    protected override void DrawPath()
    {
        GrenadeData grenadeData = CalculateGrenadeData();    
        Vector3 previousDrawPoint = projectileSpawn.position;
        
        int resolution = 30;
        _lineRenderer.enabled = true;
        _lineRenderer.positionCount = resolution;
        for (int i = 1; i <= resolution; i++)
        {
            float simulationTime = i / (float)resolution * grenadeData.timeToTarget;
            Vector3 displacement = grenadeData.initialVelocity * simulationTime + Vector3.up * gravity * simulationTime * simulationTime / 2f;
            Vector3 drawPoint = projectileSpawn.position + displacement;
            _lineRenderer.SetPosition(i - 1, previousDrawPoint);
            previousDrawPoint = drawPoint;
        }
    }

    public override void DeactiveSkill()
    {
        if(activated)
        {
            activated = false;

            _lineRenderer.positionCount = 0;
            _lineRenderer.enabled = false;
        }
    }

    // struct to store velocity and time for grenade, making trajectory calculations easier
    private struct GrenadeData
    {
        public readonly Vector3 initialVelocity;
        public readonly float timeToTarget;

        public GrenadeData(Vector3 initialVelocity, float timeToTarget)
        {
            this.initialVelocity = initialVelocity;
            this.timeToTarget = timeToTarget;
        }
    }

    #endregion

}

