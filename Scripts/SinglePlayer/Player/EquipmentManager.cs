﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentManager : MonoBehaviour
{
    public event System.Action<Equipment, Equipment> OnEquipmentChanged;

    public bool female;
    public string defaultOutfitNum;
    public Weapon defaultWeapon;
    public SkinnedMeshRenderer targetMesh;
    public Transform weaponHold;
    public Transform shieldHold;
    public Equipment[] CurrentEquipment { get; private set; }
    private SkinnedMeshRenderer[] _currentMeshes;
    private InventoryManager _inventory;
    private SkillManager _skillManager;

    private void Start()
    {
        int numSlots = System.Enum.GetNames(typeof(GearSlot)).Length;
        CurrentEquipment = new Equipment[numSlots];
        _currentMeshes = new SkinnedMeshRenderer[numSlots];
        _inventory = GetComponent<InventoryManager>();
        _skillManager = GetComponent<SkillManager>();

        EquipDefaultItems();

        if (defaultWeapon != null)
        {
            EquipItem(defaultWeapon);
        }
    }

    public void EquipItem(Equipment newItem)
    {
        int slotIndex = (int)newItem.equipSlot;
        _inventory.RemoveItem(newItem);

        Equipment oldItem = null;

        // unequip item if there is an item in a slot
        if(CurrentEquipment[slotIndex] != null)
        {
            oldItem = CurrentEquipment[slotIndex];
            // assume that inventory cannot be full when equipping and unequipping items
            _inventory.AddItem(oldItem);
        }

        // check if the item is twohanded weapon, if it is, try equipping it
        if (UnequippingOffhandFailed(newItem))
        {
            // revert item swap if we failed to unequip
            // this is the only case where we unequip old weapon and offhand item and the inventory is full
            _inventory.RemoveItem(oldItem);
            _inventory.AddItem(newItem);
        }
        else
        {
            // remove twohanded weapon if new item is offhand item
            RemoveTwoHander(newItem);

            if (OnEquipmentChanged != null)
                OnEquipmentChanged(newItem, oldItem);

            if(female)
                UpdateMeshes(newItem.femaleMesh, newItem is Weapon, newItem.equipSlot == GearSlot.OffHand, slotIndex);
            else
                UpdateMeshes(newItem.maleMesh, newItem is Weapon, newItem.equipSlot == GearSlot.OffHand, slotIndex);

            CurrentEquipment[slotIndex] = newItem;
        }
    }

    private void UpdateMeshes(SkinnedMeshRenderer newMesh, bool isWeapon, bool isOffhand, int index)
    {
        SkinnedMeshRenderer mesh = Instantiate(newMesh);

        if (_currentMeshes[index] != null)
            Destroy(_currentMeshes[index].gameObject);

        if (isWeapon)
        {
            mesh.transform.parent = weaponHold.parent;
            mesh.transform.position = weaponHold.position;
            mesh.transform.rotation = weaponHold.rotation;
        }
        else if(isOffhand)
        {
            mesh.transform.parent = shieldHold.parent;
            mesh.transform.position = shieldHold.position;
            mesh.transform.rotation = shieldHold.rotation;
        }
        else
        {
            mesh.transform.parent = targetMesh.transform;
            mesh.bones = targetMesh.bones;
            mesh.rootBone = targetMesh.rootBone;
        }
        _currentMeshes[index] = mesh;
    }

    // this method is called only in EquipItem
    private bool UnequippingOffhandFailed(Equipment item)
    {
        if(item is Weapon)
        {
            Weapon weapon = item as Weapon;
            if(!_skillManager.HasJuggernautSkill())
            {
                if (weapon.isTwoHander)
                    return TryUnequippingOffhand();
            }
        }
        return false;
    }

    // this method is called only if the item is twohanded weapon
    private bool TryUnequippingOffhand()
    {
        int offhandIndex = (int)GearSlot.OffHand;
        Equipment offhandItem = null;

        // when swapping a twohanded weapon remove offhand item aswell
        if (CurrentEquipment[offhandIndex] != null)
        {
            offhandItem = CurrentEquipment[offhandIndex];
            // if inventory is full then revert item swap
            if(_inventory.AddItem(offhandItem))
            {
                CurrentEquipment[offhandIndex] = null;

                if (_currentMeshes[offhandIndex] != null)
                    Destroy(_currentMeshes[offhandIndex].gameObject);

                if (OnEquipmentChanged != null)
                    OnEquipmentChanged(null, offhandItem);
            }
            else
                return true;
        }
        return false;
    }

    private void RemoveTwoHander(Item item)
    {
        if(item is Gear)
        {
            Gear gear = item as Gear;
            if(!_skillManager.HasJuggernautSkill())
            {
                int mainHandIndex = (int)GearSlot.MainHand;
                if (gear.equipSlot == GearSlot.OffHand && CurrentEquipment[mainHandIndex] != null)
                {
                    Weapon weapon = CurrentEquipment[mainHandIndex] as Weapon;
                    if (weapon.isTwoHander)
                        UnequipItem(mainHandIndex, null);
                }
            }
        }
    }

    public void UnequipItem(int slotIndex, int? inventoryIndex)
    {
        if(CurrentEquipment[slotIndex] != null)
        {
            Equipment oldItem = CurrentEquipment[slotIndex];
            bool successful = false;

            if (inventoryIndex != null)
                successful = _inventory.AddItemByIndex(oldItem, inventoryIndex.Value);

            if(!successful)
                successful = _inventory.AddItem(oldItem);

            if(successful)
            {
                
                if (_currentMeshes[slotIndex] != null)
                    Destroy(_currentMeshes[slotIndex].gameObject);

                CurrentEquipment[slotIndex] = null;

                if (OnEquipmentChanged != null)
                    OnEquipmentChanged(null, oldItem);

            }
        }
    }

    public void UnequipAll()
    {
        for (int i = 0; i < CurrentEquipment.Length; ++i)
        {
            UnequipItem(i, null);
        }
    }

    private void EquipDefaultItems()
    {
        SkinnedMeshRenderer[] defaultOutfitMeshes = Resources.LoadAll<SkinnedMeshRenderer>("GameItems/DefaultOutfit" + defaultOutfitNum);

        for (int i = 0; i < defaultOutfitMeshes.Length; ++i)
        {
            EquipDefaultItem(defaultOutfitMeshes[i]);
        }
    }

    private void EquipDefaultItem(SkinnedMeshRenderer mesh)
    {
        SkinnedMeshRenderer newMesh = Instantiate<SkinnedMeshRenderer>(mesh);
        newMesh.transform.parent = targetMesh.transform;

        newMesh.bones = targetMesh.bones;
        newMesh.rootBone = targetMesh.rootBone;
    }

}
