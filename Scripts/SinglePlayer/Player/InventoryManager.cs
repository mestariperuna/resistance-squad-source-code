﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    public event System.Action OnItemChanged;

    public int backBagSize = 10;
    public Item[] Inventory { get; private set; }

    private void Start()
    {
        Inventory = new Item[backBagSize];
    }

    public bool AddItem(Item item)
    {
        for (int i = 0; i < Inventory.Length; i++)
        {
            if (Inventory[i] == null)
            {
                Inventory[i] = item;
                if (OnItemChanged != null)
                    OnItemChanged();
                return true;
            }
        }
        return false;
    }

    public bool AddItemByIndex(Item item, int index)
    {
        if(Inventory[index] == null)
        {
            Inventory[index] = item;
            if (OnItemChanged != null)
                OnItemChanged();
            return true;
        }
        return false;
    }

    public bool RemoveItem(Item item)
    {
        for (int i = 0; i < Inventory.Length; i++)
        {
            if(Inventory[i] == item)
            {
                Inventory[i] = null;
                if (OnItemChanged != null)
                    OnItemChanged();
                return true;
            }
        }
        return false;
    }

    public void SwapItems(int toIndex, int fromIndex)
    {
        Item tempItem = Inventory[toIndex];
        Inventory[toIndex] = Inventory[fromIndex];
        Inventory[fromIndex] = tempItem;

        if (OnItemChanged != null)
            OnItemChanged();
    }

}
