﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseSelection : MonoBehaviour
{

    #region variables

    public event System.Action OnSelectedUnitsChanged;
    public event System.Action OnAvailableUnitsChanged;
    public event System.Action OnAvailableUnitsLost;

    public Team currentPlayer;
    public Team[] allies;
    public int maxUnits = 4;
    public float unitDistance = 1f;
    public GUIStyle mouseDragSkin;

    private float rayCastLength = 100f;
    private bool usingCommand = false;
    private LayerMask _mask;

    public List<Selectable> SelectedUnits { get; private set; }
    public List<Selectable> AvailableUnits { get; private set; }

    // drag start and end points and the box between the points
    private Vector2? dragStart;
    private Vector2 dragEnd;
    private Rect dragRect
    {
        get
        {
            return Rect.MinMaxRect(
                Mathf.Min(dragStart.Value.x, dragEnd.x),
                Mathf.Min(dragStart.Value.y, dragEnd.y),
                Mathf.Max(dragStart.Value.x, dragEnd.x),
                Mathf.Max(dragStart.Value.y, dragEnd.y)
            );
        }
    }
    // to determine if user is dragging
    private bool isDragging
    {
        get { return dragStart != null && dragRect.width >= 4 && dragRect.height >= 4; }
    }

    #endregion

    void Awake()
    {
        SelectedUnits = new List<Selectable>();
        AvailableUnits = new List<Selectable>(maxUnits);
        _mask = LayerMask.GetMask("Silhouette", "Target");
    }

    void Update ()
    {
        if (dragStart == null && InputFunctions.GetKeyDown("Select") && !usingCommand)
            dragStart = Input.mousePosition;

        if(dragStart != null)
        {
            if (InputFunctions.GetKey("Select"))
                dragEnd = Input.mousePosition;

            UpdateSelection(Input.mousePosition);

            if (InputFunctions.GetKeyUp("Select"))
                EndSelection(Input.mousePosition);
        }
    }

    #region GUI

    void OnGUI()
    {
        if (isDragging)
            if (!EventSystem.current.IsPointerOverGameObject())
                GUI.Box(Transform(dragRect), "", mouseDragSkin);
    }

    private Rect Transform(Rect rect)
    {
        return new Rect(rect.x, Screen.height - rect.yMax - 1, rect.width, rect.height);
    }

    #endregion

    #region SelectionFunctions

    public void AddGroupKeyDown()
    {
        if (!InputFunctions.GetKey("Group"))
            DeselectUnits();
    }

    public void UpdateSelection(Vector2 position)
    {
        if (dragStart != null)
            dragEnd = position;

        if (isDragging)
        {
            if(!EventSystem.current.IsPointerOverGameObject())
            {
                AddGroupKeyDown();
                SelectInRange(dragRect);
            }
        }
    }

    public void EndSelection(Vector2 position)
    {
        if(!isDragging)
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                AddGroupKeyDown();
                SelectAtRay(Input.mousePosition);
            }
        }
        dragStart = null;
    }

    public void SelectInRange(Rect rect)
    {
        foreach(Selectable unit in FindObjectsOfType(typeof(Selectable)))
        {
            if(unit.currentTeam == currentPlayer)
            {
                Vector2 screenPos = Camera.main.WorldToScreenPoint(unit.transform.position);
                if (rect.Contains(screenPos))
                    AddSelected(unit);
            }
        }
    }

    public void SelectAtRay(Vector2 mousePos)
    {
        Selectable unit = GetAtRay(mousePos);
        if (unit != null)
            AddSelected(unit);
    }

    public Selectable GetAtRay(Vector2 mousePos)
    {
        Ray ray = Camera.main.ScreenPointToRay(mousePos);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, rayCastLength, _mask))
            return hit.collider.GetComponent<Selectable>();
        return null;
    }

    public void AddSelected(Selectable unit)
    {
        if (!SelectedUnits.Contains(unit))
            SelectUnit(unit);
        else if (InputFunctions.GetKey("AddGroup") && !isDragging)
            DeselectUnit(unit);
    }

    public void SelectUnit(Selectable unit)
    {
        unit.transform.Find("Selected").gameObject.SetActive(true);
        SelectedUnits.Add(unit);

        if (SelectedUnits.Count == 1)
        {
            if (OnSelectedUnitsChanged != null)
                OnSelectedUnitsChanged();
        }
    }

    public bool CheckSelection(int index)
    {
        if(SelectedUnits.Count == 0)
        {
            if(!SelectAvailableUnit(index))
            {
                return SelectFromAvailableUnits(true);
            }
            return true;
        }
        return false;
    }

    public bool SelectAvailableUnit(int index)
    {
        if(AvailableUnits.Count > index)
        {
            if(AvailableUnits[index] != null)
            {
                SelectUnit(AvailableUnits[index]);
                return true;
            }
        }
        return false;
    }

    public bool SelectFromAvailableUnits(bool next)
    {
        int? index = null;
        if (SelectedUnits.Count == 1 && AvailableUnits.Count > 1)
        {
            for (int i = 0; i < AvailableUnits.Count; i++)
            {

                if (SelectedUnits[0] == AvailableUnits[i])
                {
                    index = SelectNextOrPrevious(i, next);
                    break;
                }
            }           
        }
        else if(SelectedUnits.Count == 0)
        {
            index = SelectNextOrPrevious(-1, next);
        }

        if(index != null)
        {
            DeselectUnits();
            SelectUnit(AvailableUnits[index.Value]);
            return true;
        }

        return false;
    }

    private int SelectNextOrPrevious(int index, bool next)
    {
        if(next)
        {
            index++;
            if (index >= AvailableUnits.Count)
                index = 0;
        }
        else
        {
            index--;
            if (index < 0)
                index = AvailableUnits.Count - 1;
        }
        return index;
    }
    
    public void SelectAll()
    {
        DeselectUnits();

        for (int i = 0; i < AvailableUnits.Count; i++)
        {
            SelectUnit(AvailableUnits[i]);   
        }
    }

    public void DeselectUnit(Selectable unit)
    {
        unit.transform.Find("Selected").gameObject.SetActive(false);
        SelectedUnits.Remove(unit);

        if(SelectedUnits.Count == 0)
        {
            if (OnSelectedUnitsChanged != null)
                OnSelectedUnitsChanged();
        }
    }

    public void DeselectUnits()
    {
        if(SelectedUnits.Count > 0)
        {
            for(int i = SelectedUnits.Count - 1; i >= 0; i--)
                DeselectUnit(SelectedUnits[i]);

            SelectedUnits.Clear();
        }
    }

    public void DeselectEnemyUnits()
    {
        for (int i = SelectedUnits.Count - 1; i >= 0; i--)
        {
            if (SelectedUnits[i].currentTeam != currentPlayer)
                DeselectUnit(SelectedUnits[i]);
        }
    }

    public int? UpdateCurrentAvailableUnitIndex(int index)
    {
        if(SelectedUnits.Count > index)
        {
            for (int i = 0; i < AvailableUnits.Count; ++i)
            {
                if (AvailableUnits[i] == SelectedUnits[index])
                    return i;
            }
        }
        return null;
    }

    #endregion

    public bool OnNewCharacter(Selectable character)
    {
        if (character != null)
        {
            if (AvailableUnits.Count < maxUnits)
            {
                if (!AvailableUnits.Contains(character))
                {
                    AvailableUnits.Add(character);
                    character.OnDeath += OnCharacterDied;

                    if (OnAvailableUnitsChanged != null)
                        OnAvailableUnitsChanged();

                    return true;
                }
            }
        }
        return false;
    }

    private void OnCharacterDied(Selectable character)
    {
        if (character != null)
        {
            if (AvailableUnits.Contains(character))
                AvailableUnits.Remove(character);

            if (SelectedUnits.Contains(character))
                SelectedUnits.Remove(character);

            if (OnAvailableUnitsChanged != null)
                OnAvailableUnitsChanged();

            if (AvailableUnits.Count == 0)
            {
                if (OnAvailableUnitsLost != null)
                    OnAvailableUnitsLost();
            }

            character.OnDeath -= OnCharacterDied;
        }
    }

    public bool IsAlly(Selectable target)
    {
        for (int i = 0; i < allies.Length; i++)
        {
            if (target.currentTeam == allies[i])
                return true;
        }
        return false;
    }

    public void SetCommand(bool isUsingCommand)
    {
        usingCommand = isUsingCommand;
    }

}
