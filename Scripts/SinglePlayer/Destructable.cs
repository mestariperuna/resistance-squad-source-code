﻿using UnityEngine;
using UnityEngine.AI;

public class Destructable : MonoBehaviour
{
    public int maxHealth;
    private int _currentHealth;
    private bool _destroyed = false;
    private GameObject _explosion;
    private ChangeColorByScale _healthBar;
    private GameObject _crateSplinterSmall;
    private GameObject _crateSplinterBig;
    private AudioClip _explosionAudio;
    public bool isCrate;
    public bool isExplosive;
    public bool indestructable = false;

    void Start ()
    {
        _explosion = Resources.Load<GameObject>("GFX/Effects/BigExplosionEffect");
        _explosionAudio = Resources.Load<AudioClip>("SFX/GrenadeExplosion");
        _currentHealth = maxHealth;
        if (isCrate)
        {
            _crateSplinterSmall = Resources.Load<GameObject>("Splinters/CrateSplinterSmall");
            _crateSplinterBig = Resources.Load<GameObject>("Splinters/CrateSplinterBig");
        }
        _healthBar = GetComponentInChildren<ChangeColorByScale>();
        UpdateHealthBar(Mathf.Clamp01((float)_currentHealth / maxHealth));

        if (indestructable)
        {
            _healthBar.transform.parent.gameObject.SetActive(false);
            gameObject.layer = LayerMask.NameToLayer("Obstacle");
            TutorialManager.Instance.OnSurvivorSaved += BecomeDestructable;
        }
    }

    public virtual void TakeExplosionDamage(float damageTaken)
    {
        if (!_destroyed && !indestructable)
        {
            int damage = Mathf.RoundToInt(damageTaken);
            _currentHealth -= damage;

            UpdateHealthBar(Mathf.Clamp01((float)_currentHealth / maxHealth));

            if (_currentHealth <= 0)
                Destroy();
        }
    }

    public virtual void TakeDamage(float damageTaken)
    {
        if (!_destroyed && !indestructable)
        {
            int damage = Mathf.RoundToInt(damageTaken);
            _currentHealth -= damage;

            UpdateHealthBar(Mathf.Clamp01((float)_currentHealth / maxHealth));

            if (_currentHealth <= 0)
                Destroy();
        }
    }

    private void BecomeDestructable()
    {
        _healthBar.transform.parent.gameObject.SetActive(true);
        gameObject.layer = LayerMask.NameToLayer("Ground");
        indestructable = false;
    }

    protected virtual void Destroy()
    {
        _destroyed = true;
        _currentHealth = 0;

        NavMeshObstacle obstacle = GetComponent<NavMeshObstacle>();
        obstacle.carving = false;
        obstacle.enabled = false;
        
        // Explosion effect.
        if (isExplosive)
        {
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, 3f);

            GameObject explosionEffect = Instantiate(_explosion, transform.position, transform.rotation) as GameObject;
            AudioManager.Instance.PlaySound(_explosionAudio, explosionEffect.transform.position);

            for (int i = 0; i < hitColliders.Length; ++i)
            {
                hitColliders[i].SendMessage("TakeExplosionDamage", 30f, SendMessageOptions.DontRequireReceiver);
            }
            Destroy(explosionEffect, 2);           
        }
        if (isCrate)
        {
            for (int i = 0; i < 4; ++i)
            {
                Vector3 randomPos = new Vector3(Random.Range(transform.position.x - 0.5f, transform.position.x + 0.5f), transform.position.y, Random.Range(transform.position.z - 0.5f, transform.position.z + 0.5f));

                Rigidbody smallSplinters = Instantiate(_crateSplinterSmall, randomPos, transform.rotation).GetComponent<Rigidbody>();
                smallSplinters.AddExplosionForce(5, transform.position, 3.0f, 1.0f, ForceMode.Impulse);

                Rigidbody bigSplinters = Instantiate(_crateSplinterBig, randomPos, transform.rotation).GetComponent<Rigidbody>();
                bigSplinters.AddExplosionForce(5, transform.position, 3.0f, 1.0f, ForceMode.Impulse);
            }
        }
        gameObject.SetActive(false);
    }   

    private void UpdateHealthBar(float percentage)
    {
        _healthBar.SetHealthVisual(percentage);
    }
}
