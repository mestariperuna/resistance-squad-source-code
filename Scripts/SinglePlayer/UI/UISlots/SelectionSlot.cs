﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectionSlot : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{
    public event System.Action<SelectionSlot> SelectOnClick;
    private UITooltip _tooltip;
    private Sprite _selectionHighlight;
    private Sprite _portrait;
    private string _data;
    private ChangeColorByScale _healthBar;
    private Transform _notificationImage;

    public Humanoid CurrentHumanoid { get; private set; }
    public int SlotId { get; set; }

    void Start()
    {
        _tooltip = FindObjectOfType<UITooltip>();
        _portrait = GetComponent<Image>().sprite;
        _selectionHighlight = Resources.Load<Sprite>("Sprites/UI/selectedBorder");
        _healthBar = GetComponentInChildren<ChangeColorByScale>();
        _notificationImage = transform.Find("NotificationImage");
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (SelectOnClick != null)
            SelectOnClick(this);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        _tooltip.Activate(_data);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _tooltip.Deactivate();
    }

    public void UpdateSelectionSlot(Selectable selectable)
    {
        if (selectable is Humanoid)
        {
            Humanoid humanoid = selectable as Humanoid;

            Transform childTransform = transform.GetChild(0);
            childTransform.localScale = Vector3.one;
            childTransform.GetComponent<Image>().sprite = humanoid.Icon;

            if(CurrentHumanoid != null)
            {
                CurrentHumanoid.OnDamageTaken -= SetHealthBar;
                CurrentHumanoid.OnLevelGain -= OnCurrentHumanoidLevelUp;
                CurrentHumanoid.TalentSystem.OnZeroTalentPoints -= DisableNotificationImage;
            }

            CurrentHumanoid = humanoid;

            CurrentHumanoid.OnDamageTaken += SetHealthBar;
            CurrentHumanoid.OnLevelGain += OnCurrentHumanoidLevelUp;
            CurrentHumanoid.TalentSystem.OnZeroTalentPoints += DisableNotificationImage;

            _data = "Level " + selectable.level + " " + selectable.name;
        }
    }

    public void HighlightSelectionSlot()
    {
        GetComponent<Image>().sprite = _selectionHighlight;
    }

    public void ClearHighlight()
    {
        GetComponent<Image>().sprite = _portrait;
    }

    private void OnCurrentHumanoidLevelUp(float scale)
    {
        if(CurrentHumanoid != null)
        {
            _data = "Level " + CurrentHumanoid.level + " " + CurrentHumanoid.name;
            _healthBar.SetHealthVisual(scale);
            _notificationImage.GetComponent<Image>().enabled = true;
            _data += "\n" + CurrentHumanoid.TalentSystem.TalentPoints + " Unspent talentpoints";
        }
    }

    private void SetHealthBar(float scale)
    {
        _healthBar.SetHealthVisual(scale);
    }

    private void DisableNotificationImage()
    {
        _notificationImage.GetComponent<Image>().enabled = false;
        _data = "Level " + CurrentHumanoid.level + " " + CurrentHumanoid.name;
    }

}
