﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

// TalentSlot knows which talent it has and all information that talent holds. 

public class TalentSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public System.Action<Talent> OnTalentClicked;
    public Talent CurrentTalent { get; set; }
    private Text pointsText;
    private Image _lockImage;

    private Button _button;
    private UITooltip _tooltip;
    private Material _greyscaleMat;

    private void Awake()
    {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(() => OnTalentClick());
        pointsText = GetComponentInChildren<Text>();
        _tooltip = FindObjectOfType<UITooltip>();

        _greyscaleMat = _button.image.material;

        Transform locktransform = transform.Find("Locked");
        if (locktransform != null)
            _lockImage = locktransform.GetComponent<Image>();
    }

    public void UpdateTalentSlot(Talent talent, bool available)
    {
        CurrentTalent = talent;
        _button.image.enabled = true;
        _button.image.sprite = talent.Icon;

        // Draw another image for Active talents.
        if (talent.Active)
        {
            if(talent.GetTalentTree() == 0)
                _button.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/MarauderActive");
            if (talent.GetTalentTree() == 1)
                _button.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/JaegerActive");
            if (talent.GetTalentTree() == 2)
                _button.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/MedicActive");           
        }       
        else
        {
            _button.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI/SkillNA");
        }

        if (available)
            TalentAvailable();
        else
            EnableLockImage();

        pointsText.text = CurrentTalent.CurrentRank.ToString() + "/" + CurrentTalent.maxRank.ToString();

    }

    public void ClearTalentSlot()
    {
        CurrentTalent = null;
        _button.image.enabled = false;

        if (_lockImage != null)
            _lockImage.enabled = false;
    }

    private void OnTalentClick()
    {
        if (OnTalentClicked != null)
            OnTalentClicked(CurrentTalent);
    }

    private void EnableLockImage()
    {
        _button.image.material = _greyscaleMat;
        if (_lockImage != null)
            _lockImage.enabled = true;
    }

    private void TalentAvailable()
    {
        _button.image.material = null;
        if (_lockImage != null)
            _lockImage.enabled = false;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (_tooltip != null && CurrentTalent != null)
            _tooltip.Activate(CurrentTalent);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if(_tooltip != null)
            _tooltip.Deactivate();
    }
}
