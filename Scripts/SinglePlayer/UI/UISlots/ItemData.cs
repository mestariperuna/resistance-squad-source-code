﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemData : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public event Action<ItemData> OnItemClicked;

    public Item Item { get; set; }
    public Transform OriginalParent { get; set; }

    private Vector2 offset;
    private UITooltip tooltip;
    private GameObject characterDetailsPanel;
    private GameObject dropZonePanel;

    public void Initialize()
    {
        InventoryUI inventoryUI = FindObjectOfType<InventoryUI>();
        tooltip = inventoryUI.GetComponentInChildren<UITooltip>();
        characterDetailsPanel = inventoryUI.transform.Find("ActivePanel/CharacterDetailsPanel").gameObject;
        dropZonePanel = inventoryUI.transform.Find("ActivePanel/DropZone").gameObject;
        Item = null;
        OriginalParent = transform.parent;
    }

    #region EventSystemInterface

    public void OnPointerDown(PointerEventData eventData)
    {
        if(Item != null)
        {
            offset = eventData.position - new Vector2(transform.position.x, transform.position.y);
            transform.position = eventData.position - offset;
        }
       
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if(Item != null)
        {
            OriginalParent = transform.parent;
            transform.SetParent(characterDetailsPanel.transform);
            dropZonePanel.SetActive(true);
            GetComponent<CanvasGroup>().blocksRaycasts = false;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if(Item != null)
            transform.position = eventData.position - offset;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.SetParent(OriginalParent);
        transform.position = OriginalParent.position;
        dropZonePanel.SetActive(false);
        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(tooltip != null)
            tooltip.Activate(Item);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if(tooltip != null)
            tooltip.Deactivate();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (Item != null && OnItemClicked != null)
            OnItemClicked(this);
    }

    public void UpdateItemData(Item newItem)
    {
        if(newItem != null)
        {
            Image image = GetComponent<Image>();
            image.enabled = true;
            image.sprite = newItem.icon;
            Item = newItem;

            if (Item is Resource)
            {
                Resource resource = Item as Resource;
                GetComponentInChildren<Text>().text = resource.quantity.ToString();
            }
        }
    }

    public void ClearItemData()
    {
        GetComponent<Image>().enabled = false;
        GetComponentInChildren<Text>().text = " ";
        Item = null;
    }

    #endregion

}
