﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropSlot : MonoBehaviour, IDropHandler
{
    public event Action<Item> OnGroundDrop;

    public void OnDrop(PointerEventData eventData)
    {
        ItemData droppedItem = eventData.pointerDrag.GetComponent<ItemData>();
        if(droppedItem != null)
            OnGroundDrop(droppedItem.Item);
    }

}
