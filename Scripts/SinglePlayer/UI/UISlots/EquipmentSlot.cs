﻿using UnityEngine;
using UnityEngine.EventSystems;

public class EquipmentSlot : MonoBehaviour, IDropHandler
{
    public GearSlot slot;
    public event System.Action<Equipment,LootSlot> OnItemEquip;

    public void OnDrop(PointerEventData eventData)
    {
        ItemData droppedItem = eventData.pointerDrag.GetComponent<ItemData>();

        if (droppedItem.Item != null && droppedItem.Item is Equipment)
        {
            LootSlot fromSlot = droppedItem.OriginalParent.GetComponent<LootSlot>();
            Equipment equipment = droppedItem.Item as Equipment;

            if (equipment.equipSlot == slot)
                if (OnItemEquip != null)
                    OnItemEquip(equipment, fromSlot);
        }
    }

}
