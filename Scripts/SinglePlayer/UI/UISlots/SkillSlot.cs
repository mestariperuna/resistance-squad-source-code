﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillSlot : MonoBehaviour
{
    public System.Action<int> OnSkillClicked;
    public Skill CurrentSkill { get; private set; }
    public int SlotIndex { get; set; }

    private Button _button;
    private Image _cooldownImage;

    private void Awake()
    {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(() => OnSkillClick());
        _cooldownImage = gameObject.transform.GetChild(0).GetChild(0).GetComponent<Image>();
    }

    public void UpdateSkillSlot(Skill skill)
    {
        CurrentSkill = skill;
        _button.image.enabled = true;
        _button.image.sprite = skill.icon;
    }

    public void ClearSkillSlot()
    {
        CurrentSkill = null;
        _button.image.enabled = false;
    }

    private void OnSkillClick()
    {
        if (CurrentSkill != null)
        {
            if (OnSkillClicked != null)
                OnSkillClicked(SlotIndex);
        }
    }

    private void FixedUpdate()
    {
        if (CurrentSkill == null)
            _cooldownImage.fillAmount = 0;

        else
            _cooldownImage.fillAmount = CurrentSkill.Percentage;
    }
}
