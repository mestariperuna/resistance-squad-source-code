﻿using UnityEngine;
using UnityEngine.EventSystems;

public class LootSlot : MonoBehaviour, IDropHandler
{
    public event System.Action<LootSlot, LootSlot> OnItemSwap;
    public int SlotId { get; set; }

    public void OnDrop(PointerEventData eventData)
    {
        ItemData droppedItem = eventData.pointerDrag.GetComponent<ItemData>();
        if(droppedItem != null)
        {
            LootSlot fromSlot = droppedItem.OriginalParent.GetComponent<LootSlot>();
            if(fromSlot != null)
            {
                if (OnItemSwap != null)
                    OnItemSwap(this, fromSlot);
            }
        }
    }
}
