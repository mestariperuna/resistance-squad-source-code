﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ItemSlot : MonoBehaviour, IDropHandler
{

    public event System.Action<ItemSlot, ItemSlot> OnItemSwap;
    public event System.Action<ItemSlot, LootSlot> OnItemPickUp;
    public event System.Action<ItemSlot, EquipmentSlot> OnItemUnequip;
    public int SlotId { get; set; }

    public void OnDrop(PointerEventData eventData)
    {
        ItemData droppedItem = eventData.pointerDrag.GetComponent<ItemData>();
        if(droppedItem != null)
        {
            ItemSlot fromSlot = droppedItem.OriginalParent.GetComponent<ItemSlot>();
            LootSlot lootSlot = droppedItem.OriginalParent.GetComponent<LootSlot>();
            EquipmentSlot equipSlot = droppedItem.OriginalParent.GetComponent<EquipmentSlot>();

            if (fromSlot != null)
            {
                if (OnItemSwap != null)
                    OnItemSwap(this, fromSlot);
            }
            else if(lootSlot != null)
            {
                if (OnItemPickUp != null)
                    OnItemPickUp(this, lootSlot);
            }
            else if(equipSlot != null)
            {
                if (OnItemUnequip != null)
                    OnItemUnequip(this, equipSlot);
            }
        }
    }
}
