﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TooltipData : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private UITooltip _tooltip;
    private Text _text;
    private string _data;

    private void Awake()
    {
        _tooltip = GetComponentInParent<UITooltip>();
        _text = GetComponentInChildren<Text>();
    }

    public void UpdateData(string newData, string textData)
    {
        _data = newData + textData;
        _text.text = textData;
    }

    public void ClearData()
    {
        _data = "";
        _text.text = " ";
    }
    
    public void OnPointerEnter(PointerEventData eventData)
    {
        if(!string.IsNullOrEmpty(_data))
            _tooltip.Activate(_data);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _tooltip.Deactivate();
    }
    
}
