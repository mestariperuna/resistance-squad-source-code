﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickActionObjectives : MonoBehaviour
{
    public GameObject objectives;
    private Button _button;
    public bool triggered = false;

    void Awake()
    {
        objectives.SetActive(false);
    }
    void Start()
    {
        objectives.SetActive(false);
        _button = GetComponent<Button>();
        _button.onClick.AddListener(() => OnPointerClick());
    }

    public void OnPointerClick()
    {
        objectives.SetActive(!objectives.activeSelf);
        if (objectives.activeSelf == false)
        {
            triggered = false;
        }
        else if (objectives.activeSelf == true)
        {
            triggered = true;
        }


    }
}
