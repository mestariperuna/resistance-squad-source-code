﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// Script that Lerp the color of a image depending of the scale of the transform
public class ChangeColorByScale : MonoBehaviour
{
    public enum SelectedAxis
    {
        xAxis,
        yAxis,
        zAxis
    }
    public SelectedAxis selectedAxis = SelectedAxis.xAxis;

    // Target
    public Image healthBar;

    // Parameters
    public float minValue = 0.0f;
    public float maxValue = 1.0f;

    public Color minColor = Color.red;
    public Color maxColor = Color.green;

    private void Start()
    {
        SetHealthVisual(1.0f);
    }

    //Health between [0.0f,1.0f] == (currentHealth / totalHealth)
    public void SetHealthVisual(float healthNormalized)
    {
       healthBar.transform.localScale = new Vector3(healthNormalized, healthBar.transform.localScale.y, healthBar.transform.localScale.z);
        LerpColor();
    }

    private void LerpColor()
    {
        switch (selectedAxis)
        {
            case SelectedAxis.xAxis:
                healthBar.color = Color.Lerp(minColor, maxColor, Mathf.Lerp(minValue, maxValue, transform.localScale.x));
                break;
            case SelectedAxis.yAxis:
                healthBar.color = Color.Lerp(minColor, maxColor, Mathf.Lerp(minValue, maxValue, transform.localScale.y));
                break;
            case SelectedAxis.zAxis:
                healthBar.color = Color.Lerp(minColor, maxColor, Mathf.Lerp(minValue, maxValue, transform.localScale.z));
                break;
        }
    }

}
