﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Tutorial", menuName = "TutorialInfo")]
public class Info : ScriptableObject
{
    public enum InfoNumber { num0, num1, num2, num3, num4, num5, num6, num7, num8, num9, num10,
        num11, num12, num13, num14, num15, num16 };
    public InfoNumber num;
    public string imageName;

    public string GetDescription()
    {
        string desc;
        switch (num)
        {
            case InfoNumber.num0:
                desc = Num0Description();
                break;
            case InfoNumber.num1:
                desc = Num1Description();
                break;
            case InfoNumber.num2:
                desc = Num2Description();
                break;
            case InfoNumber.num3:
                desc = Num3Description();
                break;
            case InfoNumber.num4:
                desc = Num4Description();
                break;
            case InfoNumber.num5:
                desc = Num5Description();
                break;
            case InfoNumber.num6:
                desc = Num6Description();
                break;
            case InfoNumber.num7:
                desc = Num7Description();
                break;
            case InfoNumber.num8:
                desc = Num8Description();
                break;
            case InfoNumber.num9:
                desc = Num9Description();
                break;
            case InfoNumber.num10:
                desc = Num10Description();
                break;
            case InfoNumber.num11:
                desc = Num11Description();
                break;
            case InfoNumber.num12:
                desc = Num12Description();
                break;
            case InfoNumber.num13:
                desc = Num13Description();
                break;
            case InfoNumber.num14:
                desc = Num14Description();
                break;
            case InfoNumber.num15:
                desc = Num15Description();
                break;
            default:
                desc = "";
                break;
        }
        return desc;
    }

    public Sprite GetInfoSprite()
    {
        if (!string.IsNullOrEmpty(imageName))
            return Resources.Load<Sprite>("Sprites/Tutorials/" + imageName);
        return null;
    }

    private string Num0Description()
    {
        return "Press <color=#feaf09>" + InputFunctions.GetKeyName("Forward", false) + ", " + InputFunctions.GetKeyName("Left", false) + ", " + 
            InputFunctions.GetKeyName("Backward", false) + ", " + InputFunctions.GetKeyName("Right", false) +
            "</color> or touch screen edges with cursor to <color=#feaf09>pan camera</color>. \n\n<color=#feaf09>Rotate camera</color> by pressing the middle mouse button while moving the mouse." +
            "\n\nUse <color=#feaf09>Mouse wheel</color> to <color=#feaf09>zoom</color> camera in and out.";
    }
    
    private string Num1Description()
    {
        return "Press <color=#feaf09>'" + InputFunctions.GetKeyName("Select", false) +
            "'</color> to <color=#feaf09>select</color> characters.\n\n<color=#feaf09>Hold and drag</color> to create a selection box to select multiple characters.";
    }

    private string Num2Description()
    {
        return "Press <color=#feaf09>'" + InputFunctions.GetKeyName("Command", false) +
                      "'</color> to <color=#feaf09>move, loot, attack</color> and give commands to selected characters.";
    }

    private string Num3Description()
    {
        return "<color=#feaf09>Pick up</color> an item by pressing <color=#feaf09>'" + InputFunctions.GetKeyName("Command", false) + "'</color> over the item.";
    }

    private string Num4Description()
    {
        return "Drag or click items with <color=#feaf09>'" + InputFunctions.GetKeyName("Select", false) + "'</color> to equip them. \nPress <color=#feaf09>'" + 
            InputFunctions.GetKeyName("Inventory", false) + "'</color> to open and close <color=#feaf09>inventory</color>.";
    }

    private string Num5Description()
    {
        return "Press <color=#feaf09>'" + InputFunctions.GetKeyName("Command", false) +
            "'</color> to <color=#feaf09>attack</color> obstacles or enemies with selected characters.";
    }

    private string Num6Description()
    {
        return "You need a <color=#feaf09>weapon</color> to destroy obstacles and enemies. Find one!";
    }

    private string Num7Description()
    {
        return "Congratulations, You earned your first level! Open the talent panel by pressing <color=#feaf09>'" + InputFunctions.GetKeyName("Talents", false) + "'</color>"; 
    }

    private string Num8Description()
    {
        return "Each <color=#feaf09>level</color> awards your character with 4 <color=#feaf09>talent points</color>! Assing them in a class tree to <color=#feaf09>unlock</color> further talents.";
    }

    private string Num9Description()
    {
        return "Unlocked <color=#feaf09>active skills</color> appear on the bottom panel. <color=#feaf09>Activate</color>" +
            " a skill by clicking or pressing a hotkey and use it with '" + InputFunctions.GetKeyName("Select", false) + "'";
    }

    private string Num10Description()
    {
        return "Your character is dead! You can pickup their items with another character.";
    }

    private string Num11Description()
    {
        return "There is a <color=#feaf09>survivor</color> nearby. Press <color=#feaf09>'" + InputFunctions.GetKeyName("Command", false) + "'</color> to take them with you.";
    }
  
    private string Num12Description()
    {
        return "On the left side of the screen, you can see your <color=#feaf09>characters' avatars</color>.  <color=#feaf09>Press</color> any of the <color=#feaf09>avatars</color> to select that character." +
            "\n\n<color=#feaf09>The blue highlight</color> tells you which character's inventory, talents and skills are currently shown.";
    }

    private string Num13Description()
    {
        return "Press <color=#feaf09>" + InputFunctions.GetKeyName("Select", false) + "</color> and drag to select multiple characters.\n\nPress <color=#feaf09>" +
            InputFunctions.GetKeyName("Cycle", false) + "</color> to cycle characters within selection.\n\n Press <color=#feaf09>" +
            InputFunctions.GetKeyName("SelectAll", false) + "</color> to select all characters.";
    }

    private string Num14Description()
    {
        return "Command your characters to attack enemies using <color=#feaf09>" + InputFunctions.GetKeyName("Command", false) + 
            "</color>.\n\nDestroyed enemies drop loot and yield experience to your characters";
    }

    private string Num15Description()
    {
        return "Use obstacles and barricades to your advantage!\n\nThe <color=#feaf09>blue barricades</color> provides an additional armor bonus when standing nearby!";
    }

}