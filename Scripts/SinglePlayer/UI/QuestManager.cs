﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[ExecuteInEditMode]
public class QuestManager : MonoBehaviour
{

    private bool survivorTriggered;
    private bool enemyTriggered;
    private bool surv1st;
    private bool enem1st;

    public float speed = 1.0f;

    public Color firstColor;
    public Color endColor;
    public bool repeatable = false;
    float startTime;

    public ClickActionObjectives objectivePanelTriggered;
    public GameObject objectives;
    private Image but;
    public GameManager gManager;
    public GameObject primary1Obj;
    public GameObject secondary1Obj;
    public GameObject secondary2Obj;

    //objective 1 find base of operations descriptions
    private string obj1_name = "Find a military compound";
    private string obj1_d_1 = "We need to find a location \n to establish a base.";
    private int obj1_state = 0;

    //objective 2 find survivors descriptions
    private string obj2_name = "Find survivors";
    private string obj2_d_1 = "There might be survivors. \nSo lets find out. They can \nhelp us to fight back";
    private string obj2_d_2 = "Yes at last, a survivor!. \nHe told us that east from \nhere, there is a big gasstation \nwhich holds more survivors. ";
    private string obj2_d_3 = "We found this gasstation. \nLets look around";

    //objective 3 Kill enemies descriptions
    private string obj3_name = "Kill enemies";
    private string obj3_d_1 = "This area was overrun by machines \n past week.\nWe must hunt them down!";
    private string obj3_d_2 = "We encountered an enemy. \nAnd destroyed it! \nWOHAA!!";

    void Start()
    {
        survivorTriggered = false;
        enemyTriggered = false;
        but = objectives.GetComponent<Image>();
        InitObjectives();

        startTime = Time.time;
        surv1st = false;
        enem1st = false;


    }
    void Update()
    {
        int enemiesCount = gManager.TurbineSpawnPointCount + gManager.MachineSpawnPointCount;
        secondary1Obj.transform.GetChild(1).GetComponent<Text>().text = gManager.SurvivorCount + "/" + gManager.NPCSpawnPointCount;
        secondary2Obj.transform.GetChild(1).GetComponent<Text>().text = gManager.EnemiesCount + "/" + enemiesCount;
        if (gManager.SurvivorCount == 1)
        {

            survivorTriggered = true;
            if (survivorTriggered == true && objectivePanelTriggered.triggered == false && surv1st == false)
            {
                //lerp();
            }
            else if (survivorTriggered == true && objectivePanelTriggered == true)
            {
                surv1st = true;
                but.color = Color.white;
            }

            secondary1Obj.transform.GetChild(2).GetComponent<Text>().text = obj2_d_2;
        }
        else survivorTriggered = false;
        if (gManager.EnemiesCount == 1)
        {
            enemyTriggered = true;
            if (enemyTriggered == true && objectivePanelTriggered.triggered == false && enem1st == false)
            {
                //lerp();
            }
            else if (enemyTriggered == true && objectivePanelTriggered == true)
            {
                enem1st = true;
                but.color = Color.white;
            }
            secondary2Obj.transform.GetChild(2).GetComponent<Text>().text = obj3_d_2;
        }
        else enemyTriggered = false;
    }
    void InitObjectives()
    {
        int enemiesCount = gManager.TurbineSpawnPointCount + gManager.MachineSpawnPointCount;
        primary1Obj.transform.GetChild(0).GetComponent<Text>().text = obj1_name;
        primary1Obj.transform.GetChild(1).GetComponent<Text>().text = obj1_state + "/1";
        primary1Obj.transform.GetChild(2).GetComponent<Text>().text = obj1_d_1;

        secondary1Obj.transform.GetChild(0).GetComponent<Text>().text = obj2_name;
        secondary1Obj.transform.GetChild(1).GetComponent<Text>().text = gManager.SurvivorCount + "/" + gManager.NPCSpawnPointCount;
        secondary1Obj.transform.GetChild(2).GetComponent<Text>().text = obj2_d_1;

        secondary2Obj.transform.GetChild(0).GetComponent<Text>().text = obj3_name;
        secondary2Obj.transform.GetChild(1).GetComponent<Text>().text = gManager.EnemiesCount + "/" + enemiesCount;
        secondary2Obj.transform.GetChild(2).GetComponent<Text>().text = obj3_d_1;

    }
    /*
    void lerp()
    {
        if (!repeatable)
        {
            float t = (Time.time - startTime) * speed;
            but.color = Color.Lerp(firstColor, endColor, t);
        }
        else
        {
            float t = (Mathf.Sin(Time.time - startTime) * speed);
            but.color = Color.Lerp(firstColor, endColor, t);

        }
    }*/
}
