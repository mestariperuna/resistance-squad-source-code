﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Networking;

public class UIManager : MonoBehaviour
{
    #region Variables

    [SerializeField]
    private Slider[] volumeSliders;
    [SerializeField]
    private Dropdown dropDown;
    [SerializeField]
    private Toggle fullScreenToggle;
    [SerializeField]
    private Toggle cameraPanningToggle;
    [SerializeField]
    private Toggle cameraRotateToggle;
    [SerializeField]
    private Toggle enableTutorialsToggle;

    [SerializeField]
    private GameObject activePanel;
    [SerializeField]
    private GameObject pausePanel;
    [SerializeField]
    private GameObject tutorialPanel;

    private Resolution[] allResolutions;
    private List<string> resolutions = new List<string>();
    private int activeScreenResIndex;

    private CameraController camController;
    private InfoPanel infoPanel;

    #endregion

    void Start()
    {
        InitializeResolutionOptions();
        InitializeVolumeOptions();
        InitializeCameraOptions();
        InitializeTutorialOptions();

        Time.timeScale = 1;

        infoPanel = tutorialPanel.GetComponent<InfoPanel>();
        infoPanel.OnDisable += CloseTutorialPanel;

        HidePaused();
    }

    void Update()
    {
        //pause the game when Pause is pressed
        if (InputFunctions.GetKeyDown("Pause"))
            TogglePause();

        if (InputFunctions.GetKeyDown("Menu"))
            PauseControl();
    }

    #region Pause

    private void TogglePause()
    {
        Time.timeScale = (Time.timeScale == 1) ? 0 : 1;
    }

    //Pause the game and show paused objects (menu etc.)
    public void PauseControl()  
    {
        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
            ShowPaused();
        }
        else if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
            HidePaused();
        }
    }

    // hide all pause-objects
    public void HidePaused()
    {
        activePanel.SetActive(true);
        pausePanel.SetActive(false);
        tutorialPanel.SetActive(false);
    }

    public void ShowPaused()
    {
        activePanel.SetActive(false);
        pausePanel.SetActive(true);
    }

#endregion

#region SceneLoading

    // reload scene
    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    // load a wanted scene
    public void LoadByIndex(int sceneIndex)
    {    
        SceneManager.LoadScene(sceneIndex);
    }

    // quit the game
    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

#endregion

#region Resolutions

    private void InitializeResolutionOptions()
    {
        activeScreenResIndex = PlayerPrefs.GetInt("screenResIndex");
        bool isFullScreen = (PlayerPrefs.GetInt("fullscreen") == 1) ? true : false;

        dropDown.ClearOptions();
        allResolutions = Screen.resolutions;
        for (int i = 0; i < allResolutions.Length; i++)
        {
            resolutions.Add(allResolutions[i].ToString());
        }
        dropDown.AddOptions(resolutions);
        dropDown.value = activeScreenResIndex;

        fullScreenToggle.isOn = isFullScreen;
    }

    // set screen resolution to the index of available resolutions from the dropdown menu in options and save it to playerprefs
    public void SetScreenResolution(int i)
    {
        activeScreenResIndex = i;
        Screen.SetResolution(allResolutions[i].width, allResolutions[i].height, false);
        PlayerPrefs.SetInt("screenResIndex", activeScreenResIndex);
        PlayerPrefs.Save();
    }

    // set fullscreen resolution and save it to playerprefs
    public void SetFullScreen(bool isFullScreen)
    {
        dropDown.interactable = !isFullScreen;

        if(isFullScreen)
        {
            Resolution maxResolution = allResolutions[allResolutions.Length - 1];
            Screen.SetResolution(maxResolution.width, maxResolution.height, true);
        }
        else
        {
            SetScreenResolution(activeScreenResIndex);
        }
        PlayerPrefs.SetInt("fullscreen", ((isFullScreen) ? 1 : 0));
        PlayerPrefs.Save();
    }

#endregion

#region Volumes

    private void InitializeVolumeOptions()
    {
        if(AudioManager.Instance != null)
        {
            volumeSliders[0].value = AudioManager.Instance.masterVolumePercent;
            volumeSliders[1].value = AudioManager.Instance.musicVolumePercent;
            volumeSliders[2].value = AudioManager.Instance.sfxVolumePercent;
        }
        else
        {
            Debug.LogWarning("Missing AudioManager instance!");
        }
    }

    // set master volume in options
    public void SetMasterVolume(float value)
    {
        AudioManager.Instance.SetVolume(value, AudioManager.AudioChannel.Master);
    }

    // set music volume in options
    public void SetMusicVolume(float value)
    {
        AudioManager.Instance.SetVolume(value, AudioManager.AudioChannel.Music);
    }

    // set sound effects volume in options
    public void SetSfxVolume(float value)
    {
        AudioManager.Instance.SetVolume(value, AudioManager.AudioChannel.Sfx);
    }

#endregion

#region Camera

    private void InitializeCameraOptions()
    {
        bool enablePanning = (PlayerPrefs.GetInt("camerapanning", 1) == 1) ? true : false;
        bool enableRotating = (PlayerPrefs.GetInt("camerarotate", 1) == 1) ? true : false;
        cameraPanningToggle.isOn = enablePanning;
        cameraRotateToggle.isOn = enableRotating;

        camController = FindObjectOfType<CameraController>();
    }
    
    public void EnableCameraPanning(bool enable)
    {
        if (camController != null)
            camController.enableCameraPanning = enable;

        PlayerPrefs.SetInt("camerapanning", ((enable) ? 1 : 0));
        PlayerPrefs.Save();
    }

    public void EnableCameraRotation(bool enable)
    {
        if (camController != null)
            camController.enableRotation = enable;

        PlayerPrefs.SetInt("camerarotate", ((enable) ? 1 : 0));
        PlayerPrefs.Save();
    }

    #endregion

    #region tutorials

    private void InitializeTutorialOptions()
    {
        bool enableTutorials = (PlayerPrefs.GetInt("enabletutorials", 1) == 1) ? true : false;
        enableTutorialsToggle.isOn = enableTutorials;
    }

    public void EnableTutorials(bool enable)
    {
        PlayerPrefs.SetInt("enabletutorials", ((enable) ? 1 : 0));
        PlayerPrefs.Save();
    }

    public void ShowTutorialPanel(Info[] currentInfos)
    {
        if(enableTutorialsToggle.isOn)
        {
            TogglePause();
            tutorialPanel.SetActive(true);
            infoPanel.SetCurrentTutorials(currentInfos);
        }
    }

    public void CloseTutorialPanel()
    {
        tutorialPanel.SetActive(false);
        TogglePause();
    }

    #endregion

}
