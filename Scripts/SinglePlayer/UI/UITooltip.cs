﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UITooltip : MonoBehaviour
{
    private string _data;
    private GameObject _tooltip;
    private RectTransform _rectTransform;

    private void Start()
    {
        _tooltip = GameObject.Find("UITooltip");
        _rectTransform = _tooltip.GetComponent<RectTransform>();
        _tooltip.SetActive(false);
    }

    private void Update()
    {
        if(_tooltip.activeInHierarchy)
        {
            _rectTransform.position = new Vector3(Input.mousePosition.x + 10, Input.mousePosition.y, 0);
            ClampToScreen();

            if(!EventSystem.current.IsPointerOverGameObject())
                Deactivate();
        }
    }
    
    private void ClampToScreen()
    {
        Vector3 pos = _rectTransform.position;

        float maxPosX = Screen.width - _rectTransform.rect.width;
        float minPosY = _rectTransform.rect.height;

        pos.x = Mathf.Clamp(_rectTransform.position.x, 0, maxPosX);
        pos.y = Mathf.Clamp(_rectTransform.position.y, minPosY, Screen.height);

        _rectTransform.position = pos;
    }

    public void Activate(string data)
    {
        if (!string.IsNullOrEmpty(data))
        {
            SetText(data);
            _tooltip.SetActive(true);
        }         
    }
    
	public void Activate(Item item)
    {
        if(item != null)
        {
            ConstructDataString(item);
            _tooltip.SetActive(true);
        }
    }

    public void Activate(ItemDataToSend data)
    {
        ConstructDataString(data);
        _tooltip.SetActive(true);
    }

    public void Activate(Talent talent)
    {
        if(talent != null)
        {
            ConstructDataString(talent);
            _tooltip.SetActive(true);
        }
    }

    public void Activate(TalentDataToSend talent)
    {
        ConstructDataString(talent);
        _tooltip.SetActive(true);
    }
    
    public void Deactivate()
    {
        _tooltip.SetActive(false);
    }

    #region ConstructText

    public void ConstructDataString(Item item)
    {
        if (item is Resource)
        {
            Resource resource = item as Resource;
            _data = "<color=#8E6700><b>" + resource.name + "</b> ilvl " + resource.ItemLevel + "</color>\n\nQuantity: " + resource.quantity + "\n\n<i>" + resource.description + "</i>";
        }
        else if (item is Gear)
        {
            Gear gear = item as Gear;
            _data = "<color=#8E6700><b>" + gear.name + "</b> ilvl " + gear.ItemLevel + "</color>\n\n" + 
                gear.equipSlot + " Equipment" + "\nArmour: " + gear.baseArmor + "\n\n<i>" + gear.description + "</i>";
        }
        else if (item is Weapon)
        {
            Weapon weapon = item as Weapon;
            _data = "<color=#8E6700><b>" + weapon.name + "</b> ilvl " + weapon.ItemLevel + "</color>\n\n" + 
                (weapon.isTwoHander ? "Two-Handed " : "One-Handed ") + "Weapon\nDamage: " + weapon.baseDamage + "\nFire rate: " + weapon.attacksPerSecond +
                "\nEffective range: " + weapon.effectiveRange + "\n\n<i>" + weapon.description + "</i>";
        }
        else
            _data = "<color=#8E6700><b>" + item.name + "</b> ilvl " + item.ItemLevel + "</color>\n\n<i>" + item.description + "</i>";

        SetText(_data);
    }

    public void ConstructDataString(ItemDataToSend data)
    {
        if(data.itemType.Equals("Weapon"))
        {
            _data = "<color=#8E6700><b>" + data.itemName + "</b> ilvl " + data.itemLevel + "</color>\n\n" + data.weaponType + 
                " " + data.itemType + "\nDamage: " + data.damage + "\nFire rate: " + data.attacksPerSecond +
                "\nEffective range: " + data.effectiveRange + "\nReload time: " + data.reloadSpeed + "\nMagazine size: " + data.magSize + "\n\n<i>" + data.description + "</i>";
        }
        else if(data.itemType.Equals("Equipment"))
        {
            _data = "<color=#8E6700><b>" + data.itemName + "</b> ilvl " + data.itemLevel + "</color>\n\n" + data.equipmentSlot + 
                " " + data.itemType + "\nArmour: " + data.armour + "\n\n<i>" + data.description + "</i>";
        }
        else if(data.itemType.Equals("Resource"))
        {
            _data = "<color=#8E6700><b>" + data.itemName + "</b> ilvl " + data.itemLevel + "</color>\n\nQuantity: "
                + data.quantity + "\n\n<i>" + data.description + "</i>";
        }
        else
        {
            _data = "<color=#8E6700><b>" + data.itemName + "</b> ilvl " + data.itemLevel + "</color>\n\n<i>" + data.description + "</i>";
        }
        SetText(_data);
    }

    public void ConstructDataString(Talent data)
    {
        _data = "<color=#8E6700><b>" + data.Name + "</b></color>\n\nTier " + (int)data.talentTier + " " + data.currentTree.ToString() +
            "\nRequired points " + data.pointsToActivate + "\n\n" + data.Description + "\n\nRank (" + data.CurrentRank + "/" + data.maxRank + ")";
        SetText(_data);
    }

    public void ConstructDataString(TalentDataToSend data)
    {
        _data = "<color=#8E6700><b>" + data.name + "</b></color>\n\nTier " + data.tier + " " + data.tree +
            "\nRequired points " + data.pointsToActivate + "\n\n" + data.description + "\n\nRank (" + data.rank + "/" + data.maxRank + ")";
        SetText(_data);
    }

    private void SetText(string data)
    {
        _tooltip.transform.GetChild(0).GetComponent<Text>().text = data;
    }

    #endregion

}
