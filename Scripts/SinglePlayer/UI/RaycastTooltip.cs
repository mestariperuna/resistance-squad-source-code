﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RaycastTooltip : MonoBehaviour
{
    private GameObject _tooltip;
    private RectTransform _rectTransform;

    private void Start()
    {
        _tooltip = GameObject.Find("RaycastTooltip");
        _rectTransform = _tooltip.GetComponent<RectTransform>();
        _tooltip.SetActive(false);
    }

    private void Update()
    {
        if(_tooltip.activeInHierarchy)
        {
            _rectTransform.position = new Vector3(Input.mousePosition.x + 10, Input.mousePosition.y, 0);
            ClampToScreen();

            if (EventSystem.current.IsPointerOverGameObject())
                Deactivate();
        }
    }

    private void ClampToScreen()
    {
        Vector3 pos = _rectTransform.position;

        float maxPosX = Screen.width - _rectTransform.rect.width;
        float minPosY = _rectTransform.rect.height;

        pos.x = Mathf.Clamp(_rectTransform.position.x, 0, maxPosX);
        pos.y = Mathf.Clamp(_rectTransform.position.y, minPosY, Screen.height);

        _rectTransform.position = pos;
    }

    public void Activate(string data)
    {
        if (!string.IsNullOrEmpty(data))
        {
            SetText(data);
            _tooltip.SetActive(true);
        }
    }

    public void Deactivate()
    {
        _tooltip.SetActive(false);
    }

    private void SetText(string data)
    {
        _tooltip.transform.GetChild(0).GetComponent<Text>().text = data;
    }

}
