﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InfoBox : MonoBehaviour
{

    #region Singleton

    private static InfoBox _instance;

    public static InfoBox Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<InfoBox>();
            return _instance;
        }
    }

    #endregion

    private GameObject _holderPanel;
    private Image _infoImage;
    private Text _infoText;
    private Toggle _infoToggle;
    private Button _closeButton;
    private Image _backGround;
    public float timeToDisable;
    public float fadingTime;

    private void Start()
    {
        _holderPanel = transform.Find("HolderPanel").gameObject;

        SetBackGroundImage();
        SetInfoImage();
        SetInfoText();
        SetCloseButton();
        SetInfoToggle();

        CloseInfoBox();
    }

    private void SetBackGroundImage()
    {
        _backGround = GetComponent<Image>();
        Color c = _backGround.color;
        c.a = 0f;
        _backGround.color = c;
    }

    private void SetInfoImage()
    {
        _infoImage = _holderPanel.transform.Find("InfoImage").GetComponent<Image>();
        _infoImage.enabled = false;
    }

    private void SetInfoText()
    {
        _infoText = _holderPanel.transform.Find("InfoText").GetComponent<Text>();
        _infoText.text = "";
    }

    private void SetCloseButton()
    {
        _closeButton = _holderPanel.transform.Find("CloseButton").GetComponent<Button>();
        _closeButton.onClick.AddListener(() => CloseInfoBox());
    }

    private void SetInfoToggle()
    {
        _infoToggle = _holderPanel.transform.Find("InfoToggle").GetComponent<Toggle>();
        UpdateEnableTutorials();
        _infoToggle.onValueChanged.AddListener((x) => SetTutorials(x));
    }

    private void SetTutorials(bool enable)
    {
        PlayerPrefs.SetInt("enabletutorials", ((enable) ? 1 : 0));
        PlayerPrefs.Save();
    }

    private void UpdateEnableTutorials()
    {
        bool enableTutorials = (PlayerPrefs.GetInt("enabletutorials", 1) == 1) ? true : false;
        _infoToggle.isOn = enableTutorials;
    }

    public void ActivateInfoBox(Info info, Vector3 screenPosition)
    {
        UpdateEnableTutorials();

        if (_infoToggle.isOn)
        {
            CloseInfoBox();

            _infoText.text = info.GetDescription();
            Sprite infoSprite = info.GetInfoSprite();
            if (infoSprite != null)
            {
                _infoImage.enabled = true;
                _infoImage.sprite = infoSprite;
            }

            _backGround.enabled = true;
            _holderPanel.SetActive(true);

            transform.position = screenPosition;          
            StartCoroutine(FadeInInfoBox());
        }
    }

    private IEnumerator FadeInInfoBox()
    {
        yield return StartCoroutine(AlphaFading(0f, 1.0f));     
        StartCoroutine(FadeOutInfoBoxAfterTime());
    }

    private IEnumerator FadeOutInfoBoxAfterTime()
    {
        yield return new WaitForSeconds(timeToDisable);
        yield return StartCoroutine(AlphaFading(1.0f, 0f));

        CloseInfoBox();
    }

    private IEnumerator AlphaFading(float min, float max)
    {
        Text toggleText = _infoToggle.GetComponentInChildren<Text>();

        ColorBlock[] componentCB = { _closeButton.colors, _infoToggle.colors };
        Color[] colorsToFade = { _backGround.color, _infoText.color, _infoImage.color, toggleText.color, componentCB[0].normalColor, componentCB[1].normalColor };

        float fadeAmount = 0;
        float time = fadingTime;

        while (time >= 0)
        {
            float a = Mathf.Lerp(min, max, fadeAmount);
            int cbIndex = colorsToFade.Length - componentCB.Length;

            for (int i = 0; i < colorsToFade.Length; ++i)
            {
                colorsToFade[i].a = a;
                if(i >= cbIndex)
                    componentCB[i - cbIndex].normalColor = colorsToFade[i];

            }

            _backGround.color = colorsToFade[0];
            _infoText.color = colorsToFade[1];
            _infoImage.color = colorsToFade[2];
            toggleText.color = colorsToFade[3];
            
            _closeButton.colors = componentCB[0];
            _infoToggle.colors = componentCB[1];

            time -= Time.deltaTime;
            fadeAmount += (1 / fadingTime) * Time.deltaTime;

            yield return null;
        }
    }
    
    private void CloseInfoBox()
    {
        StopAllCoroutines();

        _infoText.text = "";
        _infoImage.enabled = false;
        _holderPanel.SetActive(false);
        _backGround.enabled = false;
    }

}
