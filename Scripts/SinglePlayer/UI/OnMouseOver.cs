﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class OnMouseOver : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{ 
    public GameObject misc;
    public GameObject des;

    void awake ()
    {
        des.SetActive(false);
        misc.SetActive(true);
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        reveal();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        close();
    }

    public void reveal()
    {
        misc.SetActive(false);
        des.SetActive(true);
    }

    public void close()
    {
        des.SetActive(false);
        misc.SetActive(true);
    }
    
}