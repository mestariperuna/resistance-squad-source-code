﻿using UnityEngine;

public class Bullet : Projectile
{

    protected override void Start()
    {
        collisionMask = LayerMask.GetMask("Target", "Obstacle", "Ground", "Cover");
        Destroy(gameObject, lifeTime);
    }
	
	protected override void Update()
    {
        base.Update();
	}

}
