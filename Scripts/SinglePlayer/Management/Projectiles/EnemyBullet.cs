﻿using UnityEngine;

public class EnemyBullet : Projectile
{

    protected override void Start()
    {
        collisionMask = LayerMask.GetMask("Silhouette", "Obstacle", "Ground", "Cover");
        Destroy(lifeTime);
    }

}
