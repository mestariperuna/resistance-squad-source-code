﻿using UnityEngine;

public class Projectile : PoolObject
{
    protected LayerMask collisionMask;
    public int Damage { get; set; }
    public float ArmourPenetration { get; set; }
    public Vector3 Direction { get; set; }
    public float velocity = 20.0f;
    public float lifeTime = 5.0f;

    protected virtual void Start ()
    {
        collisionMask = LayerMask.GetMask("Obstacle", "Ground", "Cover");
        Destroy(lifeTime);
    }

	protected virtual void Update ()
    {
        float moveDistance = velocity * Time.deltaTime;
        CheckCollisions(moveDistance,Direction);
        transform.Translate(Direction * moveDistance);
    }

    protected void CheckCollisions(float moveDistance, Vector3 dir)
    {
        Ray ray = new Ray(transform.position, dir);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, moveDistance + 0.5f, collisionMask, QueryTriggerInteraction.Collide))
            OnObjectHit(hit);
    }

    protected virtual void OnObjectHit(RaycastHit hit)
    {
        Selectable unit = hit.collider.gameObject.GetComponent<Selectable>();
        Destructable destructable = hit.collider.gameObject.GetComponent<Destructable>();
        if (unit != null)
            unit.TakeDamage(Damage, ArmourPenetration);
        if (destructable != null)
            destructable.TakeDamage(Damage);

        Destroy();
    }

    public override void OnObjectReuse()
    {
        Destroy(lifeTime);
    }

}
