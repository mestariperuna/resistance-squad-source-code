﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveRound : Projectile
{
    public float ExplosionRadius { get; set; }
    public LayerMask targetMask;
    private GameObject explosion;
    private AudioClip explosionAudio;

    protected override void Start()
    {
        collisionMask = LayerMask.GetMask("Ground", "Obstacle", "Target");
        Destroy(gameObject, lifeTime);
        explosion = Resources.Load<GameObject>("GFX/Effects/SmallExplosionEffect");
        explosionAudio = Resources.Load<AudioClip>("SFX/GrenadeExplosion");
    }

    protected override void OnObjectHit(RaycastHit hit)
    {
        Collider[] collisions = Physics.OverlapSphere(hit.point, ExplosionRadius, targetMask);

        for (int i = 0; i < collisions.Length; i++)
        {
            Selectable s = collisions[i].GetComponent<Selectable>();
            NetworkSelectable ns = collisions[i].GetComponent<NetworkSelectable>();

            if (s != null)
            {
                s.TakeDamage(Damage, ArmourPenetration);
            }

            if(ns != null)
            {
                ns.TakeDamage(Damage, ArmourPenetration);
            }
        }

        // Instantiate explosion visual effect.
        GameObject explosionEffect = Instantiate(explosion, transform.position, transform.rotation) as GameObject;
        AudioManager.Instance.PlaySound(explosionAudio, explosionEffect.transform.position);
        Destroy(explosionEffect, 2);
        Destroy(gameObject);
    }

}
