﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeExplosion : Projectile
{

    public float ExplosionRadius { get; set; }
    public LayerMask targetMask;
    private Rigidbody rb;
    private GameObject _explosion;
    private AudioClip _explosionAudio;

    protected override void Start()
    {
        collisionMask = LayerMask.GetMask("Ground", "Obstacle", "Target");
        rb = GetComponent<Rigidbody>();
        Destroy(gameObject, lifeTime);
        _explosion = Resources.Load<GameObject>("GFX/Effects/BigExplosionEffect");
        _explosionAudio = Resources.Load<AudioClip>("SFX/GrenadeExplosion");
    }

    protected override void Update()
    {
        float moveDistance = rb.velocity.magnitude * Time.deltaTime;
        CheckCollisions(moveDistance, rb.velocity.normalized);
    }

    protected override void OnObjectHit(RaycastHit hit)
    {
        Collider[] collisions = Physics.OverlapSphere(hit.point, ExplosionRadius, targetMask);

        for (int i = 0; i < collisions.Length; i++)
        {
            Selectable s = collisions[i].GetComponent<Selectable>();
            if (s != null)
                s.TakeDamage(Damage, ArmourPenetration);
        }

        // Instantiate explosion visual effect.
        GameObject explosionEffect = Instantiate(_explosion, transform.position, transform.rotation) as GameObject;
        AudioManager.Instance.PlaySound(_explosionAudio, explosionEffect.transform.position);
        Destroy(explosionEffect, 2);
        Destroy(gameObject);
    }
}
