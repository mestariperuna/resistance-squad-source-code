﻿using UnityEngine;

public class DisablePoolObject : PoolObject
{

    public float lifeTime;

	void Start ()
    {
        Destroy(lifeTime);
	}


    public override void OnObjectReuse()
    {
        Destroy(lifeTime);
    }
}
