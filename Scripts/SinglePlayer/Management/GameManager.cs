﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public enum Team { red, blue, green, yellow };

public class GameManager : MonoBehaviour
{

    #region Variables

    // A list of all entities in the scene
    private List<Selectable> selectables = new List<Selectable>();

    private List<GameObject> _availableLootBoxPoints = new List<GameObject>();
    private List<GameObject> _availableSurvivorSpawnPoints = new List<GameObject>();
    private List<GameObject> _availableEnemySpawnPoints = new List<GameObject>();
    private List<GameObject> _availableTurbineSpawnPoints = new List<GameObject>();
    private GameObject[] _eventSpawnPoints;

    [SerializeField]
    private Transform[] _playerStartingPoints;

    private MouseSelection _selection;
    private TutorialManager _tutorials;
    public Team CurrentPlayerTeam { get; private set; }

    private GameObject _lootBoxPrefab;
    private GameObject _maleCharacter;
    private GameObject _femaleCharacter;
    private GameObject _maleSurvivor;
    private GameObject _femaleSurvivor;
    private GameObject _enemyPrefab;
    private GameObject _turbinePrefab;

    private GameObject gameWonPanel;
    private GameObject gameOverPanel;
    private UIManager _UIManager;

    private string[] _maleNames = {"Jack", "Stanton", "Dorian", "Corbec", "Thomas", "Jeffrey", "Bennet", "Cole", "Wes" };
    private string[] _femaleNames = {"Diana", "Sara", "Rene", "Rosette", "Tilly"};
    private string[] _surNames = { "Drayton", "Larkson", "Wynn", "Turner", "Mason", "Mercer" };
    private Sprite[] _MaleIcons;
    private Sprite[] _FemaleIcons;

    public int LootBoxSpawnPointCount { get; private set; }
    public int NPCSpawnPointCount { get; private set; }
    public int MachineSpawnPointCount { get; private set; }
    public int TurbineSpawnPointCount { get; private set; }

    public int eventEnemyCount = 6;

    public int SurvivorCount { get; set; }
    public int EnemiesCount { get; set; }
    public int LootBoxesCount { get; set; }
    public int PlayerUnitsLost { get; set; }

    // A singleton instance of this class
    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GameManager>();
            return _instance;
        }
    }

    #endregion

    #region Awake&Start

    private void Awake()
    {
        Time.timeScale = 1;

        _lootBoxPrefab = Resources.Load<GameObject>("GameItems/LootBox");
        _maleCharacter = Resources.Load<GameObject>("Units/MaleCharacter");
        _femaleCharacter = Resources.Load<GameObject>("Units/FemaleCharacter");
        _maleSurvivor = Resources.Load<GameObject>("Units/MaleSurvivor");
        _femaleSurvivor = Resources.Load<GameObject>("Units/FemaleSurvivor");
        _enemyPrefab = Resources.Load<GameObject>("Units/BasicEnemy");
        _turbinePrefab = Resources.Load<GameObject>("Units/Turbine");

        _MaleIcons = Resources.LoadAll<Sprite>("Sprites/UI/MaleIcons");
        _FemaleIcons = Resources.LoadAll<Sprite>("Sprites/UI/FemaleIcons");

        _selection = FindObjectOfType<MouseSelection>();
        _selection.OnAvailableUnitsLost += DeclareLost;
        CurrentPlayerTeam = _selection.currentPlayer;

        SpawnPointRandomization();
        PoolCreation();
        SpawnLootBoxes();
        SpawnSurvivors();
        SpawnEnemies();
    }

    private void SpawnPointRandomization()
    {
        _availableLootBoxPoints.AddRange(GameObject.FindGameObjectsWithTag("LootBox"));
        _availableSurvivorSpawnPoints.AddRange(GameObject.FindGameObjectsWithTag("NPC"));
        _availableEnemySpawnPoints.AddRange(GameObject.FindGameObjectsWithTag("EnemySpawnPoint"));
        _availableTurbineSpawnPoints.AddRange(GameObject.FindGameObjectsWithTag("TurbineSpawnPoint"));
        _eventSpawnPoints = GameObject.FindGameObjectsWithTag("EventSpawnPoint");

        LootBoxSpawnPointCount = Random.Range(_availableLootBoxPoints.Count / 2, _availableLootBoxPoints.Count);
        NPCSpawnPointCount = _availableSurvivorSpawnPoints.Count;
        MachineSpawnPointCount = _availableEnemySpawnPoints.Count;
        TurbineSpawnPointCount = _availableTurbineSpawnPoints.Count;
    }

    private void PoolCreation()
    {
        if (PoolManager.Instance != null)
        {
            PoolManager.Instance.CreatePool(_lootBoxPrefab, LootBoxSpawnPointCount);
            PoolManager.Instance.CreatePool(_maleSurvivor, NPCSpawnPointCount);
            PoolManager.Instance.CreatePool(_maleCharacter, NPCSpawnPointCount);
            PoolManager.Instance.CreatePool(_femaleSurvivor, NPCSpawnPointCount);
            PoolManager.Instance.CreatePool(_femaleCharacter, NPCSpawnPointCount);
            PoolManager.Instance.CreatePool(_enemyPrefab, MachineSpawnPointCount + eventEnemyCount);
            PoolManager.Instance.CreatePool(_turbinePrefab, TurbineSpawnPointCount);
        }
        else
            Debug.LogWarning("Missing PoolManager!");
    }

    private void SpawnLootBoxes()
    {
        // initialize items
        for (int i = 0; i < LootBoxSpawnPointCount; i++)
        {
            Transform spawnPoint = FetchRandomSpawnPoint(_availableLootBoxPoints);
            GameObject lootObj = PoolManager.Instance.ReuseObject(_lootBoxPrefab, spawnPoint.position, spawnPoint.rotation);
            if (lootObj != null)
            {
                Loot lootBox = lootObj.GetComponent<Loot>();
                lootBox.generateLoot = true;
                lootBox.amountToGenerate = Random.Range(1, 4);
                lootBox.OnLootFound += OnLootBoxFound;
            }
        }
    }

    private void SpawnSurvivors()
    {
        // initialize survivors
        for (int j = 0; j < NPCSpawnPointCount; j++)
        {
            Transform spawnPoint = FetchRandomSpawnPoint(_availableSurvivorSpawnPoints);
            int rand = Random.Range(0, 100);
            GameObject survivor;

            if (rand < 50)
                survivor = PoolManager.Instance.ReuseObject(_femaleSurvivor, spawnPoint.position, spawnPoint.rotation);
            else
                survivor = PoolManager.Instance.ReuseObject(_maleSurvivor, spawnPoint.position, spawnPoint.rotation);

            if (survivor != null)
                survivor.GetComponent<Survivor>().Found += OnSurvivorFound;
        }
    }

    private void SpawnEnemies()
    {
        SpawnEnemy(_enemyPrefab, _availableEnemySpawnPoints, MachineSpawnPointCount, "Machine");
        SpawnEnemy(_turbinePrefab, _availableTurbineSpawnPoints, TurbineSpawnPointCount, "Turbine");
    }

    private void SpawnEnemy(GameObject prefab, List<GameObject> spawnPoints, int amount, string name)
    {
        for (int u = 0; u < amount; ++u)
        {          
            Transform spawnPoint = FetchRandomSpawnPoint(spawnPoints);
            GameObject go = PoolManager.Instance.ReuseObject(prefab, spawnPoint.position, spawnPoint.rotation);
            if (go != null)
            {
                go.name = name;
                Selectable enemy = go.GetComponent<Selectable>();
                enemy.navMeshAgent.Warp(spawnPoint.position);
                RegisterSelectable(enemy);
            }
        }
    }

    public Transform FetchRandomSpawnPoint(List<GameObject> spawnPoints)
    {
        GameObject newPoint = spawnPoints[Random.Range(0, spawnPoints.Count)];
        spawnPoints.Remove(newPoint);
        return newPoint.transform;
    }

    private void Start()
    {
        _tutorials = TutorialManager.Instance;
        InitializeUIPanels();
        StartPlayerCharacters();
    }

    private void InitializeUIPanels()
    {
        _UIManager = FindObjectOfType<UIManager>();
        gameWonPanel = _UIManager.transform.Find("PausePanel/GameWonPanel").gameObject;
        gameOverPanel = _UIManager.transform.Find("PausePanel/GameOverPanel").gameObject;
        gameWonPanel.SetActive(false);
        gameOverPanel.SetActive(false);
    }

    private void StartPlayerCharacters()
    {       
        for (int i = 0; i < _playerStartingPoints.Length; ++i)
        {
            int rand = Random.Range(0, 100);

            GameObject newCharacter;
            if (rand < 50)
            {
                newCharacter = Instantiate(_femaleCharacter, _playerStartingPoints[i].transform.position, _playerStartingPoints[i].transform.rotation);
                newCharacter.name = _femaleNames[Random.Range(0, _femaleNames.Length)] + " " + _surNames[Random.Range(0, _surNames.Length)];
                newCharacter.GetComponent<Humanoid>().Icon = _FemaleIcons[Random.Range(0, _FemaleIcons.Length)];
            }
            else
            {
                newCharacter = Instantiate(_maleCharacter, _playerStartingPoints[i].transform.position, _playerStartingPoints[i].transform.rotation);               
                newCharacter.name = _maleNames[Random.Range(0, _maleNames.Length)] + " " + _surNames[Random.Range(0, _surNames.Length)];
                newCharacter.GetComponent<Humanoid>().Icon = _MaleIcons[Random.Range(0, _MaleIcons.Length)];
            }

            Selectable selectable = newCharacter.GetComponent<Selectable>();
            selectable.navMeshAgent.Warp(_playerStartingPoints[i].transform.position);
            selectable.navMeshAgent.SetDestination(new Vector3(60.0f, 10.2f, 35.0f));

            if (_selection.OnNewCharacter(selectable))
                RegisterSelectable(selectable);
            else // Send to base            
                Destroy(newCharacter);

        }
    }

    #endregion

    #region Events

    private void OnSurvivorFound(Survivor survivor)
    {
        GameObject newCharacter;
        if (survivor.female)
        {
            newCharacter = PoolManager.Instance.ReuseObject(_femaleCharacter, survivor.transform.position, survivor.transform.rotation);
            newCharacter.name = newCharacter.name = _femaleNames[Random.Range(0, _femaleNames.Length)] + " " + _surNames[Random.Range(0, _surNames.Length)];
            newCharacter.GetComponent<Humanoid>().Icon = _FemaleIcons[Random.Range(0, _FemaleIcons.Length)];
        }
        else
        {
            newCharacter = PoolManager.Instance.ReuseObject(_maleCharacter, survivor.transform.position, survivor.transform.rotation);
            newCharacter.name = _maleNames[Random.Range(0, _maleNames.Length)] + " " + _surNames[Random.Range(0, _surNames.Length)];
            newCharacter.GetComponent<Humanoid>().Icon = _MaleIcons[Random.Range(0, _MaleIcons.Length)];
        }
        
        Selectable selectable = newCharacter.GetComponent<Selectable>();
        selectable.navMeshAgent.Warp(survivor.transform.position);

        SurvivorCount++;

        if (_selection.OnNewCharacter(selectable))
            RegisterSelectable(selectable);          
        else  // Send to base      
            Destroy(newCharacter);

        if (!_tutorials.FirstMultiCharacter)
            _tutorials.TriggerMultiCharacterTutorial();

    }

    private void OnUnitDeath(Selectable entity)
    {
        if(entity.currentTeam != _selection.currentPlayer)
            EnemiesCount++;
        else
        {
            PlayerUnitsLost++;
            if (!_tutorials.FirstCharacterDead)
                _tutorials.TriggerDeathTutorial();
        }

        if (selectables.Contains(entity))
            DeregisterSelectable(entity);
    }

    private void OnLootBoxFound()
    {
        LootBoxesCount++;
    }

    private void DeclareLost()
    {
        _UIManager.PauseControl();
        gameOverPanel.SetActive(true);
    }

    public void DeclareWin()
    {
        _UIManager.PauseControl();
        gameWonPanel.SetActive(true);
    }

    public void OnEnemyEvent()
    {
        StartCoroutine(SpawnWave(3.0f));
    }

    private IEnumerator SpawnWave(float delay)
    {
        int enemiesSpawned = 0;
        
        while(enemiesSpawned < eventEnemyCount)
        {
            Transform spawnPoint = _eventSpawnPoints[enemiesSpawned % _eventSpawnPoints.Length].transform;
            GameObject go = PoolManager.Instance.ReuseObject(_enemyPrefab, spawnPoint.position, spawnPoint.rotation);
            if (go != null)
            {
                go.name = "Machine";
                Selectable enemy = go.GetComponent<Selectable>();
                enemy.navMeshAgent.Warp(spawnPoint.position);
                RegisterSelectable(enemy);
            }

            yield return new WaitForSeconds(delay);
            ++enemiesSpawned;
        }
    }

    // Keep track of which selectables exist in the scene
    public void RegisterSelectable(Selectable selectable)
    {
        selectables.Add(selectable);
        selectable.OnDeath += OnUnitDeath;
    }

    public void DeregisterSelectable(Selectable selectable)
    {
        selectables.Remove(selectable);
        selectable.OnDeath -= OnUnitDeath;
    }

    #endregion

}
