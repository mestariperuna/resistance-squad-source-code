﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{

    public static TutorialManager Instance;
    public event System.Action OnSurvivorSaved;

    private bool _firstAttack;
    private bool _firstSurvivor;
    private bool _firstItemToPickup;
    private bool _firstTalentClicked;
    private bool _firstCombat;
    public bool FirstSkill { get; private set; }
    public bool FirstLevelUp { get; private set; }
    public bool FirstWeaponPickup { get; private set; }
    public bool FirstTalentPanelOpened { get; private set; }
    public bool FirstCharacterDead { get; private set; }
    public bool FirstMultiCharacter { get; private set; }

    private InfoBox _infobox;
    private Info[] _tutorialInfos;
    private UIManager _uiManager;

    public RectTransform[] infoPositions;

    private void Awake()
    {
        if (Instance != null)
            Destroy(gameObject);
        else
            Instance = this;

    }

    void Start ()
    {
        _infobox = InfoBox.Instance;
        _tutorialInfos = Resources.LoadAll<Info>("Tutorials");
        _uiManager = FindObjectOfType<UIManager>();

        Invoke("TriggerCameraMovementTutorial", 4.0f);
	}

    // called in the beginning - how to move with camera info
    private void TriggerCameraMovementTutorial()
    {
        Info[] cameraTutorials = { _tutorialInfos[0], _tutorialInfos[1], _tutorialInfos[2] };
        TriggerTutorialPanel(cameraTutorials);
    }

    // called from scene object trigger event - how to loot info
    public void TriggerLootTutorial()
    {
        if(!_firstItemToPickup)
        {
            TriggerTutorialInfo(3, infoPositions[0].position);
            _firstItemToPickup = true;
        }       
    }

    // called from collectable where tutorial loot is true - how to open/close inventory info
    public void TriggerInventoryTutorial()
    {
        if(!FirstWeaponPickup)
        {
            TriggerTutorialInfo(4, infoPositions[0].position);
            FirstWeaponPickup = true;
        }
    }
    
    // called from scene object trigger event - how to attack info
    public void TriggerAttackTutorial()
    {
        if(!_firstAttack && FirstWeaponPickup)
        {
            TriggerTutorialInfo(5, infoPositions[0].position);
            _firstAttack = true;
        }
        else
        {
            TriggerTutorialInfo(6, infoPositions[0].position);
        }
    }

    //  called from humanoid when leveled up - how to put talent points.
    public void TriggerLevelUpTutorial()
    {
        if(!FirstLevelUp)
        {
            TriggerTutorialInfo(7, infoPositions[0].position); // tell the player how to open talent window.
            FirstLevelUp = true;
        }
    }

    public void TriggerTalentPanelTutorial()
    {
        if (FirstLevelUp && !FirstTalentPanelOpened)
        {
            TriggerTutorialInfo(8, infoPositions[1].position); // tell how to use talents
            FirstTalentPanelOpened = true;
        }
        else
            return;
    }

    // called when levelling a first skill.
    public void TriggerSkillTutorial()
    {
        if(!FirstSkill)
        {
            TriggerTutorialInfo(9, infoPositions[1].position);
            FirstSkill = true;
        }
    }

    // called when a character dies.
    public void TriggerDeathTutorial()
    {
        if(!FirstCharacterDead)
        {
            TriggerTutorialInfo(10, infoPositions[1].position);
            FirstCharacterDead = true;
        }
    }

    public void TriggerSurvivorTutorial()
    {
        if(!_firstSurvivor)
        {
            Info[] survivorTutorial = { _tutorialInfos[11] };
            TriggerTutorialPanel(survivorTutorial);
            _firstSurvivor = true;
        }
    }

    public void TriggerMultiCharacterTutorial()
    {
        if(!FirstMultiCharacter)
        {
            Info[] multipleCharacterTutorials = { _tutorialInfos[12], _tutorialInfos[13] };
            TriggerTutorialPanel(multipleCharacterTutorials);
            FirstMultiCharacter = true;

            if (OnSurvivorSaved != null)
                OnSurvivorSaved();
        }
    }
    
    public void TriggerCombatTutorial()
    {
        if(!_firstCombat)
        {
            Info[] combatTutorials = { _tutorialInfos[14], _tutorialInfos[15] };
            TriggerTutorialPanel(combatTutorials);
            _firstCombat = true;
        }
    }

    private void TriggerTutorialInfo(int infoNum, Vector3 screenPosition)
    {
        _infobox.ActivateInfoBox(_tutorialInfos[infoNum], screenPosition);
    }

    private void TriggerTutorialPanel(Info[] infos)
    {
        _uiManager.ShowTutorialPanel(infos);
    }

}
