﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoPanel : MonoBehaviour
{
    public event System.Action OnDisable;
    private Button _button;
    private Text _infoText;
    private GameObject _imagePanel;
    private Toggle _infoToggle;
    private Image _image;
    private Info[] _currentInfos;
    private int _infoIndex;

    private delegate void ShowNext();
    private ShowNext ShowNextCallback;

    private void Awake()
    {
        SetButtonDelegate();
        SetUIElements();
        SetInfoToggle();
    }

    private void SetButtonDelegate()
    {
        _button = transform.Find("ContinueButton").GetComponent<Button>();
        _button.onClick.AddListener(() => ShowNextCallback());
    }

    private void SetUIElements()
    {
        _imagePanel = transform.Find("ImagePanel").gameObject;
        _image = _imagePanel.transform.GetChild(0).GetComponent<Image>();
        _infoText = transform.Find("InfoText").GetComponent<Text>();
    }

    private void SetInfoToggle()
    {
        _infoToggle = transform.Find("InfoToggle").GetComponent<Toggle>();
        UpdateEnableTutorials();
        _infoToggle.onValueChanged.AddListener((x) => SetTutorials(x));
    }

    private void SetTutorials(bool enable)
    {
        PlayerPrefs.SetInt("enabletutorials", ((enable) ? 1 : 0));
        PlayerPrefs.Save();
    }

    public void SetCurrentTutorials(Info[] currentInfos)
    {
        _currentInfos = currentInfos;
        _infoIndex = 0;
        ShowNextCallback = ShowNextInfo;
        ShowNextInfo();
    }

    private void UpdateEnableTutorials()
    {
        bool enableTutorials = (PlayerPrefs.GetInt("enabletutorials", 1) == 1) ? true : false;
        _infoToggle.isOn = enableTutorials;
    }

    private void ShowNextInfo()
    {
        UpdateEnableTutorials();

        if (_infoToggle.isOn)
        {
            ClearPreviousInfo();
            Sprite sprite = _currentInfos[_infoIndex].GetInfoSprite();
            if (sprite != null)
            {
                _imagePanel.SetActive(true);
                _image.sprite = sprite;
            }

            _infoText.text = _currentInfos[_infoIndex].GetDescription();

            _infoIndex++;

            if (_infoIndex == _currentInfos.Length)
                ShowNextCallback = DisableInfoPanel;
        }
        else
            DisableInfoPanel();
    }

    private void DisableInfoPanel()
    {
        ClearPreviousInfo();
        if (OnDisable != null)
            OnDisable();
    }

    private void ClearPreviousInfo()
    {
        _imagePanel.SetActive(false);
        _image.sprite = null;
        _infoText.text = "";
    }
}
