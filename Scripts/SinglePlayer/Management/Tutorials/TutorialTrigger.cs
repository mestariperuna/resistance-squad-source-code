﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialTrigger : MonoBehaviour
{
    public enum TriggerAction { looting, attacking, combat, survivor }
    public TriggerAction action;
    private TutorialManager _tutorials;
    private bool triggered;

    private void Start()
    {
        _tutorials = TutorialManager.Instance;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(!triggered)
        {
            Humanoid humanoid = other.GetComponent<Humanoid>();

            if (humanoid != null)
            {
                if(action == TriggerAction.looting)
                {
                    _tutorials.TriggerLootTutorial();
                    triggered = true;
                }

                if (action == TriggerAction.attacking)
                {
                    _tutorials.TriggerAttackTutorial();
                    if(_tutorials.FirstWeaponPickup)
                        triggered = true;
                }

                if(action == TriggerAction.combat)
                {
                    _tutorials.TriggerCombatTutorial();
                    triggered = true;
                }

                if(action == TriggerAction.survivor)
                {
                    _tutorials.TriggerSurvivorTutorial();
                    triggered = true;
                }
            }
        }
    }

}
