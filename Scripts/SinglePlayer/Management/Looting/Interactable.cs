﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{

    protected bool interacted;
    protected bool focused;
    protected Transform focuser;

    public string objectName;

    public float radius = 1.0f;

    protected virtual void Start()
    {
        if (string.IsNullOrEmpty(objectName))
            objectName = name;
    }

    protected virtual void Update()
    {
        if(focused && !interacted)
        {
            float distance = Vector3.Distance(focuser.position, transform.position);
            if(distance <= radius)
            {
                Interact();
                interacted = true;
            }
        }
    }

    public virtual void Interact()
    {
        // meant to be overwritten
        Debug.Log("Interacting with " + objectName);

        Animator animator = GetComponent<Animator>();
        if (animator != null)
            animator.SetTrigger("Interact");
    }

    public void OnFocus(Transform focuserTransform)
    {
        focused = true;
        focuser = focuserTransform;
        interacted = false;
    }

    public void OnDefocus()
    {
        focused = false;
        focuser = null;
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
