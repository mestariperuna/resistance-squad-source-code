﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loot : Interactable
{
    public event System.Action OnLootFound;

    public Item[] Inventory { get; set; }
    public bool generateLoot = false;
    public int amountToGenerate = 1;

    private InventoryUI _inventoryUI;
    private GameObject _lootBagObj;
    private LootBag _lootBag;

    private GameObject _lootIndicator;
    private GameObject lootIndicator;
    public bool isEnemyDrop;
    protected override void Start()
    {
        base.Start();
        
        _inventoryUI = FindObjectOfType<InventoryUI>();
        _lootBagObj = _inventoryUI.LootBag;
        _lootBag = _lootBagObj.GetComponent<LootBag>();

        if (isEnemyDrop)
        {
            _lootIndicator = Resources.Load<GameObject>("GFX/Effects/LootIndicator_1");
            lootIndicator = Instantiate(_lootIndicator, transform.position + new Vector3(0,0.5f,0), transform.rotation) as GameObject;
        }
    }

    protected override void Update()
    {
        if (!focused && interacted)
        {
            _lootBagObj.SetActive(false);
            _inventoryUI.DeactivateInventory();
            interacted = false;
        }
        
        base.Update();
    }

    public override void Interact()
    {
        base.Interact();
        Humanoid humanoid = focuser.GetComponent<Humanoid>();

        if(humanoid != null)
        {           
            humanoid.StopUnit();

            if(generateLoot)
            {
                Inventory = new Item[amountToGenerate];

                for (int i = 0; i < Inventory.Length; i++)
                    Inventory[i] = GenerateLoot.Instance.GetRandomLoot();

                generateLoot = false;
            }
            
            _lootBagObj.SetActive(true);
            
            _inventoryUI.ActivateInventory(humanoid);
            _lootBag.SetLootBag(this, humanoid);

            if (isEnemyDrop) {Destroy(lootIndicator, 2); }
            
            if (OnLootFound != null)
                OnLootFound();

        }
    }

}
