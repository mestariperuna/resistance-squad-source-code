﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateLoot : MonoBehaviour
{

    #region Singleton

    static GenerateLoot _instance;

    public static GenerateLoot Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GenerateLoot>();
            return _instance;
        }
    }

    #endregion

    public int zoneLevel = 1;
    private List<Item> _loot = new List<Item>();

    private void Awake()
    {
        _loot.AddRange(Resources.LoadAll<Item>("GameItems/Loot"));
    }

    public Item GetRandomLoot()
    {
        Item item = Instantiate((Item)_loot[Random.Range(0, _loot.Count)]);

        item.RollStats(zoneLevel);
        return item;
    }
    public Item GetAssaultRifleLoot()
    {
        Item item = Instantiate((Item)Resources.Load<Item>("GameItems/Loot/Assault_Rifle"));
        item.RollStats(zoneLevel);
        return item;
    }

}
