﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Collectable : Interactable
{

    public event System.Action ItemFound;

    public Item CurrentItem { get; set; }
    public bool isTutorialLoot = false;
    private InventoryUI inventoryUI;

    // for tutorial purposes
    private Image tutorialImage;


    protected override void Start ()
    {
        base.Start();

        if (isTutorialLoot)
            CurrentItem = GenerateLoot.Instance.GetAssaultRifleLoot();

        if(CurrentItem != null)
            objectName = CurrentItem.name;
       
        inventoryUI = FindObjectOfType<InventoryUI>();
    }

    public override void Interact()
    {
        Debug.Log("Interacting with " + objectName);
        Selectable selectable = focuser.GetComponent<Selectable>();

        if(selectable != null)
        {
            selectable.StopUnit();
            if(selectable is Humanoid)
            {
                Humanoid humanoid = selectable as Humanoid;
                
                if(humanoid.InventoryManager.AddItem(CurrentItem))
                {
                    inventoryUI.UpdateInventory();
                    inventoryUI.ActivateInventory(humanoid);

                    if (isTutorialLoot)
                    {
                        inventoryUI.InitiateLootTutorial(humanoid);
                        TutorialManager.Instance.TriggerInventoryTutorial();
                    }

                    if (ItemFound != null)
                        ItemFound();

                    gameObject.SetActive(false);
                }
            }
        }
    }
}
