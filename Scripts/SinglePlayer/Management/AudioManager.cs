﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{

    public enum AudioChannel { Master, Sfx, Music };

    public float masterVolumePercent { get; private set; }
    public float sfxVolumePercent { get; private set; }
    public float musicVolumePercent { get; private set; }

    private AudioSource sfx2DSource;
    private AudioSource[] musicSources;
    private int activeMusicSourceIndex;

    AudioClip menuTheme;
    List<AudioClip> playlist;
    public static AudioManager Instance;

    void Awake()
    {
        if (Instance != null)
            Destroy(gameObject);
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            // set the music volumes either from playerprefs or default values
            masterVolumePercent = PlayerPrefs.GetFloat("masterVol", 0.5f);
            sfxVolumePercent = PlayerPrefs.GetFloat("sfxVol", 0.5f);
            musicVolumePercent = PlayerPrefs.GetFloat("musicVol", 0.5f);

            // create 2 music object to fade from one to another
            musicSources = new AudioSource[2];
            for (int i = 0; i < 2; i++)
            {
                GameObject newMusicSource = new GameObject("Music source " + (i + 1));
                musicSources[i] = newMusicSource.AddComponent<AudioSource>();
                musicSources[i].ignoreListenerPause = true;
                newMusicSource.transform.parent = transform;
            }
            // create gameobject to play a 2D sound
            GameObject newSfx2DSource = new GameObject("Sfx2D source");
            sfx2DSource = newSfx2DSource.AddComponent<AudioSource>();
            newSfx2DSource.transform.parent = transform;

            //Initialize playlist and add music to it
            playlist = new List<AudioClip>();
            playlist.Add(Resources.Load<AudioClip>("SFX/Track1"));
            menuTheme = Resources.Load<AudioClip>("SFX/Roaming");

            SceneManager.sceneLoaded += OnSceneLoad;
        }
    }

    void Update()
    {
        if (!isPlaying())
        {
            //if menuscene or lobbyscene, play menutheme
            if (SceneManager.GetActiveScene() == SceneManager.GetSceneByBuildIndex(0) || SceneManager.GetActiveScene() == SceneManager.GetSceneByBuildIndex(2))
            {
                musicSources[activeMusicSourceIndex].Stop();
                PlayMusic(menuTheme, 2f);
            }             
            //otherwise play music from a playlist
            else
            {
                musicSources[activeMusicSourceIndex].Stop();
                PlayMusic(playlist[Random.Range(0, playlist.Count)], 3f);
            }            
        }
        MoreThanOnePlaying();
    }

    void OnSceneLoad(Scene scene, LoadSceneMode mode)
    {

        if (scene == SceneManager.GetSceneByBuildIndex(0) || scene == SceneManager.GetSceneByBuildIndex(2))
            PlayMusic(menuTheme, 2f);
        else
            PlayMusic(playlist[Random.Range(0, playlist.Count)], 3f);
    }

    // check if any music clip is playing
    private bool isPlaying()
    {
        for (int i = 0; i < musicSources.Length; ++i)
        {
            if (musicSources[i].isPlaying)
                return true;
        }
        return false;
    }

    private void MoreThanOnePlaying()
    {
        List<AudioSource> sourcesPlaying = new List<AudioSource>();
        for (int i = 0; i < musicSources.Length; ++i)
        {
            if (musicSources[i].isPlaying)
                sourcesPlaying.Add(musicSources[i]);
        }

        if(sourcesPlaying.Count > 1)
        {
            sourcesPlaying.RemoveAt(0);
            for (int i = 0; i < sourcesPlaying.Count; ++i)
            {
                sourcesPlaying[i].Stop();
            }
        }
    }

    // play a music clip with a fade
    public void PlayMusic(AudioClip clip, float fadeDuration = 1)
    {
        activeMusicSourceIndex = 1 - activeMusicSourceIndex;
        musicSources[activeMusicSourceIndex].clip = clip;
        musicSources[activeMusicSourceIndex].Play();
        // fades between music clips
        // StartCoroutine(AnimateMusicCrossFade(fadeDuration));
    }

    // play a sound effect at a position
    public void PlaySound(AudioClip clip, Vector3 pos)
    {
        if (clip != null)
            AudioSource.PlayClipAtPoint(clip, pos, sfxVolumePercent * masterVolumePercent);
    }

    // play a 2D sound
    public void PlaySound2D(AudioClip clip)
    {
        if (clip != null)
            sfx2DSource.PlayOneShot(clip, sfxVolumePercent * masterVolumePercent);
    }

    // fade away and fade in music clips
    IEnumerator AnimateMusicCrossFade(float duration)
    {
        float percent = 0;

        while (percent < 1)
        {
            percent += Time.deltaTime * 1 / duration;
            musicSources[activeMusicSourceIndex].volume = Mathf.Lerp(0, musicVolumePercent * masterVolumePercent, percent);
            musicSources[1 - activeMusicSourceIndex].volume = Mathf.Lerp(musicVolumePercent * masterVolumePercent, 0, percent);
            yield return null;
        }
    }

    // set the music volume in the options and save it to playerprefs
    public void SetVolume(float volumePercent, AudioChannel channel)
    {
        switch (channel)
        {
            case AudioChannel.Master:
                masterVolumePercent = volumePercent;
                break;
            case AudioChannel.Sfx:
                sfxVolumePercent = volumePercent;
                break;
            case AudioChannel.Music:
                musicVolumePercent = volumePercent;
                break;
        }

        musicSources[0].volume = musicVolumePercent * masterVolumePercent;
        musicSources[1].volume = musicVolumePercent * masterVolumePercent;

        PlayerPrefs.SetFloat("masterVol", masterVolumePercent);
        PlayerPrefs.SetFloat("sfxVol", sfxVolumePercent);
        PlayerPrefs.SetFloat("musicVol", musicVolumePercent);
        PlayerPrefs.Save();
    }
}
