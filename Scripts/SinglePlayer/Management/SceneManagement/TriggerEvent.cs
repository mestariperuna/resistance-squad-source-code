﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEvent : MonoBehaviour
{
    private bool _triggered = false;

    private void OnTriggerEnter(Collider other)
    {
        Humanoid humanoid = other.GetComponent<Humanoid>();

        if(humanoid != null && !_triggered)
        {
            GameManager.Instance.OnEnemyEvent();
            _triggered = true;
        }
    }

}
