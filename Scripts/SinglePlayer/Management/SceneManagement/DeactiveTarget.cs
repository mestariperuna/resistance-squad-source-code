﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactiveTarget : MonoBehaviour
{

	void DestroyTargetObject()
    {
        Destroy(gameObject);
    }
}
