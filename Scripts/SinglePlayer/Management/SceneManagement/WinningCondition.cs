﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinningCondition : MonoBehaviour
{
    public LayerMask targetMask;
    private float radius = 10f;

    private void OnTriggerStay(Collider other)
    {
        Humanoid humanoid = other.GetComponent<Humanoid>();

        if(humanoid != null)
        {
            Collider[] enemiesInRange = Physics.OverlapSphere(transform.position, radius, targetMask);

            if (enemiesInRange.Length == 0)
                GameManager.Instance.DeclareWin();
        }
    }
}
