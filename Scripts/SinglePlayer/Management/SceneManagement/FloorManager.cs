﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FloorManager : MonoBehaviour {

    public BoxCollider[] floorColliders;

    private void OnTriggerStay(Collider other)
    {
        CameraController cam = other.GetComponent<CameraController>();

        if (cam != null)
        {
            for (int i = 0; i < floorColliders.Length; i++)
            {
                if (!floorColliders[i].bounds.Contains(cam.transform.position) && cam.transform.position.y < floorColliders[i].bounds.center.y)
                    SetInvisible(floorColliders[i].transform);

                else if (!floorColliders[i].bounds.Contains(cam.transform.position))
                    SetVisible(floorColliders[i].transform);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        CameraController cam = other.GetComponent<CameraController>();

        if(cam != null)
        {
            for(int i = 0; i < floorColliders.Length; ++i)
            {
                SetVisible(floorColliders[i].transform);
            }
        }
    }

    private void SetInvisible(Transform collider)
    {
        foreach (Transform t in collider.GetComponentsInChildren<Transform>())
        {
            if(t.GetComponent<Renderer>())
                t.GetComponent<Renderer>().enabled = false;
        }
    }

    private void SetVisible(Transform collider)
    {
        foreach (Transform t in collider.GetComponentsInChildren<Transform>())
        {
            if (t.GetComponent<Renderer>())
                t.GetComponent<Renderer>().enabled = true;
        }
    }

}
