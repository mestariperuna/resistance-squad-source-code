﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Resource")]
public class Resource : Item
{
    [Header("Resource attributes")]
    public int quantity;
}
