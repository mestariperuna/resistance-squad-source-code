﻿using UnityEngine;

// Item baseclass.

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/BaseItem")]

public class Item : ScriptableObject
{
    [Header("Item attributes")]
    new public string name = "New Item";
    public string description = " ";
    public Sprite icon = null;
    public string iconPath = "";

    [Tooltip("How many item levels until we change item tier")]
    [Range(1, 10)]
    public int levelsToChangeTier = 1;
    [Tooltip("How much do we want to modify the items attributes")]
    public int statModifier;
    public int ItemLevel { get; private set; }

    public virtual void RollStats(int zoneLevel)
    {
        ItemLevel = zoneLevel;
    }

}
