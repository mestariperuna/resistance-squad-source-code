﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Inventory/Weapon")]
public class Weapon : Equipment
{
    [Header("Weapon attributes")]
    public bool isTwoHander;
    public int baseDamage;
    [Tooltip("Modify items damage with random value between x to y")]
    public Vector2 randomDamageBonus;
    public float effectiveRange;
    public float attacksPerSecond;

    [Header("Resources path name")]
    public string projectileName;
    public string fireAudio;

    public override void RollStats(int zoneLevel)
    {
        base.RollStats(zoneLevel);

        int sum = statModifier * Mathf.FloorToInt((ItemLevel / levelsToChangeTier));
        randomDamageBonus.x += sum;
        randomDamageBonus.y += sum;
        baseDamage += (int)Random.Range(randomDamageBonus.x, randomDamageBonus.y);
    }
}
