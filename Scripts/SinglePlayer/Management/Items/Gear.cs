﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Equipment", menuName = "Inventory/Gear")]
public class Gear : Equipment
{
    [Header("Armor attributes")]
    public int baseArmor;
    public Vector2 randomArmorBonus;

    public override void RollStats(int zoneLevel)
    {
        base.RollStats(zoneLevel);

        int sum = statModifier * Mathf.FloorToInt((ItemLevel / levelsToChangeTier));
        randomArmorBonus.x += sum;
        randomArmorBonus.y += sum;
        baseArmor += (int)Random.Range(randomArmorBonus.x, randomArmorBonus.y);
    }
}
