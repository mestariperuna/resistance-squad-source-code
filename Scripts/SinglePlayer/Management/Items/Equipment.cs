﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Base class for equipment. Weapons etc. herit Equipment slots.

public enum GearSlot { Head, Chest, Legs, MainHand, OffHand };
public class Equipment : Item
{
    public string meshName;
    public SkinnedMeshRenderer maleMesh;
    public SkinnedMeshRenderer femaleMesh;
    public GearSlot equipSlot;
}