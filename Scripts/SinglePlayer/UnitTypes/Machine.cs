﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Machine : Selectable
{

    protected GameObject _dyingPrefab;
    [Header("Robot attributes")]
    public float experienceOnDeath = 10;
    public Stat damage;
    public int damagePerLevel;
    public float attacksPerSecond = 4.0f;

    [Range(0, 1)]
    public float armourPenetration;

    protected float timeBetweenAttacks;
    protected float nextAttack;

    [SerializeField]
    protected float reloadTime;
    [SerializeField]
    protected int magazineSize;
    [SerializeField]
    protected int currentAmmo;
    protected bool reloading = false;
    private GameObject _explosion;

    protected override void Start()
    {
        base.Start();
        timeBetweenAttacks = 1 / attacksPerSecond;
        _explosion = Resources.Load<GameObject>("GFX/Effects/PlasmaExplosionEffect");
    }

    protected override void Die()
    {
        GameObject explosionEffect = Instantiate(_explosion, transform.position, transform.rotation) as GameObject;
        Instantiate(_dyingPrefab, transform.position, transform.rotation);
        ApplyExperience(experienceOnDeath);

        base.Die();
        Destroy(explosionEffect, 2);
    }

    protected void ApplyExperience(float xp)
    {
        Collider[] experienceRange = Physics.OverlapSphere(transform.position, 25.0f, targetMask);

        for (int i = 0; i < experienceRange.Length; i++)
        {
            Humanoid humanoid = experienceRange[i].GetComponent<Humanoid>();
            if(humanoid != null)
                humanoid.GainExperience(xp);
        }
    }

    protected void OnEnable()
    {
        StartCoroutine(FindTargetsWithDelay(.25f));
    }

    protected override void ApplyLevelMultiplier()
    {
        base.ApplyLevelMultiplier();

        damage.AddModifier((level - 1) * damagePerLevel);
    }

}
