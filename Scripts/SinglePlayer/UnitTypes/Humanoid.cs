﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(SkillManager))]
[RequireComponent(typeof(InventoryManager))]
[RequireComponent(typeof(EquipmentManager))]
[RequireComponent(typeof(GunController))]
[RequireComponent(typeof(TalentSystem))]

public class Humanoid : Selectable
{
    #region Variables

    public event Action<float> OnDamageTaken;
    public event Action<float> OnLevelGain;
    public event Action OnBuffsChanged;

    [Header("Attack")]
    [Range(0, 1)]
    public float accuracy;
    [Range(0,0.1f)]
    public float accuracyPerLevel;

    [Header("Regeneration")]
    public int regenAmount;
    public float regenTimer;

    [Header("Movement")]
    public float movementSpeed = 5f;
    public float runSpeed = 5f;

    [Header("TalentPoints")]
    public int talentPointsPerLevel = 4;
    
    public GunController GunController { get; private set; }
    public SkillManager SkillManager { get; private set; }
    public InventoryManager InventoryManager { get; private set; }
    public EquipmentManager EquipmentManager { get; private set; }
    public TalentSystem TalentSystem { get; private set; }
    public float Experience { get; private set; }
    public float ExperienceCap { get; private set; }

    private bool _aggressive;

    private List<UnitBonusStats> _unitPassiveSkillModifiers = new List<UnitBonusStats>();
    private List<UnitBuff> _currentBuffs = new List<UnitBuff>();

    #endregion

    #region Awake&Start

    protected override void Awake()
    {
        base.Awake();
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
        GunController = GetComponent<GunController>();
        SkillManager = GetComponent<SkillManager>();
        InventoryManager = GetComponent<InventoryManager>();
        EquipmentManager = GetComponent<EquipmentManager>();
        TalentSystem = GetComponent<TalentSystem>();

        EquipmentManager.OnEquipmentChanged += OnEquip;

        SkillManager.OnSkillUnlocked += OnPassiveSkillUnlocked;
        SkillManager.OnSkillRemoved += OnPassiveSkillRemoved;
        SkillManager.OnSkillUpgraded += OnSkillUpgraded;
    }

    protected override void Start()
    {
        base.Start();

        currentState = State.idling;
        GunController.Accuracy = accuracy;

        Experience = 0;
        ExperienceCap = 100;

        StartCoroutine(FindTargetsWithDelay(.25f));
        StartCoroutine(RegenerateOverTime(regenTimer));  
    }

    #endregion

    protected void OnEnable()
    {
        StartCoroutine(FindTargetsWithDelay(.25f));
        StartCoroutine(RegenerateOverTime(regenTimer));
    }

    void Update()
    {
        
        switch (currentState)   // act accordingly to currentState
        {
            case State.idling:
                Aggression();
                break;
            case State.combat:        
                ChaseTarget();   
                break;
            case State.patrolling:
                Aggression();
                Patrol();
                break;
            case State.moving:
                AtDestination();
                break;
            case State.attacking:
                Attack();
                break;
        }

        HandleAnimations();
    }

    private void Aggression()
    {
        if (GunController.EquippedWeapon != null && _aggressive)
        {
            CheckSurroundings();
            _aggressive = false;
        }
    }

    protected override void Attack()
    {
        if(target.gameObject.activeInHierarchy && InLifeOfSight())
        {
            animator.SetBool("Firing", true);
            GunController.Shoot(target);
            animator.SetTrigger("Fire");
        }
        else
        {
            currentState = State.combat;
            animator.SetBool("Firing", false);
        }
    }

    public void OnPassiveSkillTrigger()
    {
        animator.SetBool("Firing", true);
        GunController.OnPassiveSkillTrigger(target);
        animator.SetTrigger("Fire");
    }

    public override void MoveUnit(Vector3 point)
    {
        navMeshAgent.speed = movementSpeed + GetBonusUnitStat(x => x.MovementSpeedBonus);
        animator.SetBool("Firing", false);
        base.MoveUnit(point);
    }

    #region Regeneration

    IEnumerator RegenerateOverTime(float time)
    {
        while (true)
        {
            yield return new WaitForSeconds(time - GetBonusUnitStat(x => x.HealthRegenTimerBonus));
            Regenerate();
            float scale = Mathf.Clamp01((float)CurrentHealth / maxHealth.GetValue());
            healthBar.SetHealthVisual(scale);
            if (OnDamageTaken != null)
                OnDamageTaken(scale);
        }
    }

    private void Regenerate()
    {
        int regenbonus = regenAmount + GetBonusUnitStat(x => x.HealthRegenBonus);
        if (CurrentHealth < maxHealth.GetValue())
            CurrentHealth = (CurrentHealth + regenbonus >= maxHealth.GetValue()) ? maxHealth.GetValue() : CurrentHealth + regenbonus;

    }

    public void Heal(int amount)
    {
        CurrentHealth = (CurrentHealth + amount >= maxHealth.GetValue()) ? maxHealth.GetValue() : CurrentHealth + amount;
        float scale = Mathf.Clamp01((float)CurrentHealth / maxHealth.GetValue());
        healthBar.SetHealthVisual(scale);
        if(OnDamageTaken != null)
            OnDamageTaken(scale);
    }

    #endregion

    #region StatModifiers

    public void GainExperience(float amount)
    {
        Experience += amount;
        if (Experience >= ExperienceCap)
            OnLevelUp();
    }

    private void OnLevelUp()
    {
        if(level < maxLevel)
        {

            if (!TutorialManager.Instance.FirstLevelUp)
                TutorialManager.Instance.TriggerLevelUpTutorial();

            GameObject lvlup = Instantiate(Resources.Load<GameObject>("GFX/Effects/LevelUpParticles"), transform.position, transform.rotation);
            lvlup.transform.parent = this.transform;
            Destroy(lvlup, 2f);

            accuracy += accuracyPerLevel;
            GunController.Accuracy = accuracy;
            TalentSystem.OnLevelUp(talentPointsPerLevel);

            defence.AddModifier(defencePerLevel);
            maxHealth.AddModifier(healthPerLevel);
            CurrentHealth = maxHealth.GetValue();

            Experience = Experience % ExperienceCap;
            ExperienceCap *= 1.2f;
            level++;

            float scale = Mathf.Clamp01((float)CurrentHealth / maxHealth.GetValue());
            healthBar.SetHealthVisual(scale);

            if (OnLevelGain != null)
                OnLevelGain(scale);
        }
    }

    private void OnEquip(Equipment newItem, Equipment oldItem)
    {
        if (oldItem != null)
        {
            if (oldItem is Gear)
            {
                Gear gear = oldItem as Gear;
                defence.RemoveModifier(gear.baseArmor);
                if (gear.equipSlot == GearSlot.OffHand) // Remove shield armor bonus.
                    defence.RemoveModifier(GetBonusUnitStat(x => x.ShieldArmorBonus));
            }

            if (oldItem is Weapon)
            {
                GunController.RemoveWeapon();
                animator.SetBool("HasGun", false);
            }
        }

        if (newItem != null)
        {
            if (newItem is Gear)
            {
                Gear gear = newItem as Gear;
                defence.AddModifier(gear.baseArmor);
                if (gear.equipSlot == GearSlot.OffHand) // Add shield armor bonus.
                    defence.AddModifier(GetBonusUnitStat(x => x.ShieldArmorBonus));
            }

            if (newItem is Weapon)
            {
                Weapon weapon = newItem as Weapon;
                GunController.EquipWeapon(weapon);
                animator.SetBool("HasGun", true);
            }
        }
    }

    private void OnPassiveSkillUnlocked(PassiveSkill skill)
    {
        if(skill != null)
        {
            // Add passive skill modifiers to a list.
            if (skill.UnitBonusStatModifier != null)
            {
                // Remove old health, armour and range bonuses
                RemoveBonusStats();
                _unitPassiveSkillModifiers.Add(skill.UnitBonusStatModifier);
                // Add new health, armour and range bonuses from that list.
                AddBonusStats();
            }

            if (skill.GunBonusStatModifier != null)
                GunController.OnPassiveSkillUnlocked(skill.GunBonusStatModifier);
        }
    }

    private void OnPassiveSkillRemoved(PassiveSkill skill)
    {
        if(skill != null)
        {
            if (skill.UnitBonusStatModifier != null)
            {
                if (_unitPassiveSkillModifiers.Contains(skill.UnitBonusStatModifier))
                {
                    RemoveBonusStats();
                    _unitPassiveSkillModifiers.Remove(skill.UnitBonusStatModifier);
                    AddBonusStats();
                }
            }

            if (skill.GunBonusStatModifier != null)
                GunController.OnPassiveSkillRemoved(skill.GunBonusStatModifier);
        }
    }

    private void OnSkillUpgraded(PassiveSkill skill)
    {
        if(skill != null)
        {
            RemoveBonusStats();
            skill.UpgradeSkill();
            AddBonusStats();
        }
    }

    protected override void ApplyLevelMultiplier()
    {
        base.ApplyLevelMultiplier();

        accuracy += (level - 1) * accuracyPerLevel;
    }

    public void RemoveBonusStats()
    {
        RemoveBonusHealth();
        RemoveBonusRanges();
        RemoveBonusArmour();
    }

    public void AddBonusStats()
    {
        AddBonusHealth();
        AddBonusRanges();
        AddBonusArmour();
    }

    private void RemoveBonusHealth()
    {
        maxHealth.RemoveModifier(GetBonusUnitStat(x => x.HealthBonus));
    }

    private void AddBonusHealth()
    {
        maxHealth.AddModifier(GetBonusUnitStat(x => x.HealthBonus));
    }

    private void RemoveBonusArmour()
    {
        defence.RemoveModifier(GetBonusUnitStat(x => x.ArmorBonus));
    }

    private void AddBonusArmour()
    {
        defence.AddModifier(GetBonusUnitStat(x => x.ArmorBonus));
    }

    private void RemoveBonusRanges()
    {
        float rangeBonus = GetBonusUnitStat(x => x.AttackRangeBonus);
        attackRange -= rangeBonus;
        viewRange -= rangeBonus;
    }

    private void AddBonusRanges()
    {
        float rangeBonus = GetBonusUnitStat(x => x.AttackRangeBonus);
        attackRange += rangeBonus;
        viewRange += rangeBonus;
    }

    // Get bonus stats based on selector.
    public float GetBonusUnitStat(Func<UnitBonusStats, float> selector)
    {
        float stat = 0;
        for (int i = 0; i < _unitPassiveSkillModifiers.Count; i++)
        {
            stat += selector(_unitPassiveSkillModifiers[i]);
        }
        return stat;
    }

    public int GetBonusUnitStat(Func<UnitBonusStats, int> selector)
    {
        int stat = 0;
        for (int i = 0; i < _unitPassiveSkillModifiers.Count; i++)
        {
            stat += selector(_unitPassiveSkillModifiers[i]);
        }
        return stat;
    }

    public void AddBuff(UnitBuff buff)
    {
        if (buff != null)
        {
            if(!CurrentBuffsHasMaxStacks(buff))
            {
                RemoveBonusStats();
                _unitPassiveSkillModifiers.Add(buff.Stats);
                _currentBuffs.Add(buff);
                AddBonusStats();
                StartCoroutine(RemoveBuffAfterDuration(buff));

                if (OnBuffsChanged != null)
                    OnBuffsChanged();
            }
        }
    }

    // check if the current buffs list has max amount of the buffs max stacks
    private bool CurrentBuffsHasMaxStacks(UnitBuff buff)
    {
        int stackCount = 0;
        for (int i = 0; i < _currentBuffs.Count; i++)
        {
            if (_currentBuffs[i].BuffName.Equals(buff.BuffName))
                stackCount++;

            if (stackCount >= buff.MaxStacks)
                return true;
        }
        return false;
    }

    private IEnumerator RemoveBuffAfterDuration(UnitBuff buff)
    {      
        yield return new WaitForSeconds(buff.Duration);
        RemoveBonusStats();
        _unitPassiveSkillModifiers.Remove(buff.Stats);
        _currentBuffs.Remove(buff);
        AddBonusStats();

        if (OnBuffsChanged != null)
            OnBuffsChanged();
    }

    #endregion

    private void HandleAnimations()
    {
        float animationSpeedPercent = navMeshAgent.velocity.magnitude / navMeshAgent.speed;
        animator.SetFloat("speedPercent", animationSpeedPercent, 0.1f, Time.deltaTime);
    }

    protected override void Die()
    {
        if(SkillManager.HasMadDocSkill())
            return;

        GameObject dyingUnit;
        if (EquipmentManager.female)
            dyingUnit = Instantiate(Resources.Load<GameObject>("Units/DyingUnitFemale"), transform.position, transform.rotation);
        else
            dyingUnit = Instantiate(Resources.Load<GameObject>("Units/DyingUnitMale"), transform.position, transform.rotation);

        EquipmentManager.UnequipAll();
        dyingUnit.GetComponent<Loot>().Inventory = InventoryManager.Inventory;
        dyingUnit.GetComponent<Loot>().objectName = name + " (Dead)";

        base.Die();
    }

    public override void TakeDamage(float damageTaken, float armourPenetration)
    {
        if (!dead)
        {
            // See if this unit has the "Rage" skill.
            Rage rage = SkillManager.HasRageSkill();
            if (rage != null)
                GunController.AddBuff(rage.Buff);

            _aggressive = true;
            float damageReduction = defence.GetValue() * damageReductionPercent - armourPenetration;
            int damage = (damageReduction < 1) ? Mathf.RoundToInt(damageTaken * (1 - damageReduction)) : 0;
            CurrentHealth -= damage;

            float scale = Mathf.Clamp01((float)CurrentHealth / maxHealth.GetValue());
            healthBar.SetHealthVisual(scale);
            if (OnDamageTaken != null)
                OnDamageTaken(scale);


            if (CurrentHealth <= 0)
                Die();
        }
    }

    // for testing purposes
    void OnValidate()
    {
        if(GunController != null)
        {
            GunController.Accuracy = accuracy;
        } 
    }

}
