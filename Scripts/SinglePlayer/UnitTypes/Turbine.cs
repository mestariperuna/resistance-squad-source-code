﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Turbine : Machine
{
    #region Variables

    [Header("Laser Components")]
    public GameObject laser;
    public Transform startPoint;
    public Transform endPoint;
    public Collider laserHitBox;
    public GameObject chargingPoint;

    public GameObject smokeEffect;
    
    private LineRenderer laserLine;
    private bool charging;

    [Header("Laser timers")]
    public float chargeTimer = 3f; // time it takes to charge laser.
    public float laserTimer = 0.5f; // time the laser is active.
    public float cooldownTimer = 3f; // time the enemy needs to cooldown before charging the laser.
    public float damagingTimer = 1f; // how ofter units takes damage within hitbox.
    public float laserTurnSpeed = 8.0f; // how fast the laser rotates while charging laser.

    [Header("SFX")]
    private AudioClip weaponFireSound;


    #endregion

    #region Awake&Start

    protected override void Awake()
    {
        base.Awake();

        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
        laserLine = laser.GetComponentInChildren<LineRenderer>();
        _dyingPrefab = Resources.Load<GameObject>("Units/DyingTurbine");
        weaponFireSound = Resources.Load<AudioClip>("SFX/TurbineLaserFire");
    }

    protected override void Start ()
    {
        base.Start();

        float attacks = laserTimer / damagingTimer;
        attacksPerSecond = attacks / (chargeTimer + laserTimer);

        laserLine.startWidth = .2f;
        laserLine.endWidth = .2f;

        laser.SetActive(false);
        chargingPoint.SetActive(false);

        currentState = State.idling;

        StartCoroutine(FindTargetsWithDelay(0.25f));
    }

    #endregion

    private void Update()
    {
        switch (currentState)   // act accordingly to currentState
        {
            case State.idling:
                CheckSurroundings();
                HandleAnimations(0);
                break;
            case State.combat:
                ChaseTarget();
                break;
            case State.attacking:
                TurnTowardsTarget();
                Attack();
                break;
            case State.patrolling:
                CheckSurroundings();
                HandleAnimations(1);
                Patrol();
                break;
            case State.moving:
                HandleAnimations(1);
                AtDestination();
                break;
        }
    }

    #region LaserFunctions

    IEnumerator ChargeLaser()
    {
        laserLine.SetPosition(0, startPoint.position);
        laserLine.SetPosition(1, endPoint.position);

        HandleAnimations(2);
        chargingPoint.SetActive(true);

        yield return new WaitForSeconds(chargeTimer);

        chargingPoint.SetActive(false);
        StartCoroutine(FireLaser());

    }

    IEnumerator FireLaser()
    {
        float timer = 0;
        float dmgTimer = damagingTimer;
        AudioManager.Instance.PlaySound(weaponFireSound, laser.transform.position);
        
        laser.SetActive(true);
        animator.SetTrigger("Fire");

        while (timer <= laserTimer)
        {
            if (dmgTimer >= damagingTimer)
                LaserHit();

            timer += Time.deltaTime;
            dmgTimer -= Time.deltaTime;

            if (dmgTimer <= 0)
                dmgTimer = damagingTimer;

            yield return null;
        }

        laser.SetActive(false);
        currentState = State.combat;

        smokeEffect.SetActive(true);
        yield return new WaitForSeconds(cooldownTimer);
        smokeEffect.SetActive(false);

        charging = false;

    }

    private void LaserHit()
    {
        if (!laserHitBox.gameObject.activeInHierarchy)
            laserHitBox.gameObject.SetActive(true);

        Collider[] hitColliders = Physics.OverlapBox(laserHitBox.bounds.center, laserHitBox.bounds.extents, Quaternion.identity, targetMask);

        for (int i = 0; i < hitColliders.Length; i++)
        {
            hitColliders[i].GetComponent<Selectable>().TakeDamage(damage.GetValue(), armourPenetration);
        }
    }

    #endregion

    protected override void Attack()
    {
        if (!charging)
        {
            charging = true;
            StartCoroutine(ChargeLaser());
        }
    }

    private void TurnTowardsTarget()
    {
        if(!target.gameObject.activeInHierarchy)
        {        
            StopCoroutine(ChargeLaser());
            StopCoroutine(FireLaser());
            currentState = State.idling;
        }
        else
        {
            Quaternion targetRotation = Quaternion.LookRotation(target.position - transform.position);
            transform.eulerAngles = Vector3.up * Mathf.MoveTowardsAngle(transform.eulerAngles.y, targetRotation.eulerAngles.y, laserTurnSpeed * Time.deltaTime);
        }
    }

    protected override void ChaseTarget()
    {
        // check that the target is alive
        if (!target.gameObject.activeInHierarchy)
            currentState = State.idling;
        else
        {
            Vector3 dirToTarget = (target.position - transform.position).normalized;
            float distToTarget = Vector3.Distance(transform.position, target.position);

            // a raycast to check that the target isn't behind an obstacle
            if (!Physics.Raycast(transform.position, dirToTarget, distToTarget, obstacleMask) && distToTarget <= attackRange)
            {
                // stop the unit and look at target
                if (navMeshAgent.enabled)
                {
                    HandleAnimations(1);
                    navMeshAgent.isStopped = true;
                    Vector3 lookVector = new Vector3(target.position.x, transform.position.y, target.position.z);
                    transform.rotation = Quaternion.LookRotation(lookVector - transform.position);
                    Attack();
                }
                chaseTimer = 0;
            }
            else   // if target is out of range chase it
            {
                if (navMeshAgent.enabled)
                {
                    HandleAnimations(1);
                    navMeshAgent.isStopped = false;
                    chaseTimer += Time.deltaTime;

                    // check the distance and timer, so we don't chase forever
                    if (chaseTimer >= chaseDuration && distToTarget >= chaseDistance)
                    {
                        // return to position where we started chasing
                        navMeshAgent.SetDestination(originalPos);
                        currentState = State.idling;
                    }
                    else
                        navMeshAgent.SetDestination(target.position);
                }
            }
        }
    }

    private void HandleAnimations(int animstate)
    {
        animator.SetInteger("animationState", animstate);
    }

}
