﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BasicEnemy : Machine
{
    #region Variables

    [HideInInspector]
    public List<Transform> wayPoints = new List<Transform>();

    public Transform[] bulletSpawnPoints;
    public float maneuverSpeed = 5.0f;
    private float maneuverTime = 1.5f;
    private bool isManeuvering = false;
    private Vector3 perpendicular;
    private int curWaypoint = 0;

    private GameObject _projectileObject;
    private GameObject _muzzleEffect;
    private AudioClip _firingSound;

    #endregion

    #region Awake&Start

    protected override void Awake()
    {
        base.Awake();
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
    }

    protected override void Start ()
    {
        base.Start();

        if (PoolManager.Instance != null)
        {
            _projectileObject = PoolManager.Instance.EnemyProjectile;
            _muzzleEffect = PoolManager.Instance.MuzzleEffect;
        }
        else
            Debug.Log("No PoolManager in scene!");

        _dyingPrefab = Resources.Load<GameObject>("Units/DyingBasicEnemy");
        _firingSound = Resources.Load<AudioClip>("SFX/gatlingfire");

        wayPoints.Clear();
        GameObject[] waypointClusters = GameObject.FindGameObjectsWithTag("Waypoint");
        float patrolRange = 3.0f;

        foreach (GameObject waypointCluster in waypointClusters)
        {
            if (Vector3.Distance(transform.position, waypointCluster.transform.position) <= patrolRange)
            {
                for (int i = 0; i < waypointCluster.transform.childCount; i++)
                {
                    wayPoints.Add(waypointCluster.transform.GetChild(i));
                }
                break;
            }
        }

        if (wayPoints.Count > 0)
            currentState = State.patrolling;
        else
            currentState = State.idling;

        StartCoroutine(FindTargetsWithDelay(0.25f));
    }

    #endregion

    private void Update()
    {
        switch (currentState)   // act accordingly to currentState
        {
            case State.idling:
                CheckSurroundings();
                HandleAnimations(0);
                break;
            case State.combat:
                ChaseTarget();
                HandleAnimations(2);
                break;
            case State.patrolling:
                CheckSurroundings();
                Patrol();
                HandleAnimations(0);
                break;
            case State.moving:
                AtDestination();
                HandleAnimations(0);
                break;
            case State.attacking:
                Attack();
                DoSideManeuver();
                HandleAnimations(2);
                break;
        }


        if (!reloading && currentAmmo <= 0)
            Reload();
    }

    #region StateFunctions

    protected override void Attack()
    {
        if(target.gameObject.activeInHierarchy && InLifeOfSight())
        {
            if (Time.time > nextAttack && !reloading)
            {
                nextAttack = Time.time + timeBetweenAttacks;
                foreach (Transform bulletSpawn in bulletSpawnPoints)
                {
                    animator.SetTrigger("Fire");
                    AudioManager.Instance.PlaySound(_firingSound, bulletSpawn.position);
                    PoolManager.Instance.ReuseObject(_muzzleEffect, bulletSpawn.position, bulletSpawn.rotation);
                    Projectile projectile = PoolManager.Instance.ReuseObject(_projectileObject, bulletSpawn.position, Quaternion.identity).GetComponent<Projectile>();

                    if (projectile != null)
                    {
                        projectile.Direction = (target.GetComponent<Collider>().bounds.center - bulletSpawn.position).normalized;
                        projectile.Damage = damage.GetValue();
                        --currentAmmo;
                    }
                }
            }
        }
        else
        {
            currentState = State.combat;
        }
    }

    private void DoSideManeuver()
    {
        navMeshAgent.isStopped = false;
        if(target.gameObject.activeInHierarchy)
        {
            Vector3 direction = (target.position - transform.position).normalized;
                 
            if(!isManeuvering)
            {
                int random = Random.Range(0, 100);
                if (random < 50)
                    perpendicular = new Vector3(-direction.z, direction.y, direction.x);
                else
                    perpendicular = new Vector3(direction.z, direction.y, -direction.x);

                isManeuvering = true;
                maneuverTime = 1.5f;
            }

            if(isManeuvering)
            {
                navMeshAgent.Move(perpendicular * maneuverSpeed * Time.deltaTime);
                maneuverTime -= Time.deltaTime;

                if (maneuverTime <= 0f)
                    isManeuvering = false;
            }

        }
    }

    private void Reload()
    {
        StartCoroutine(AnimateReload(reloadTime));
    }

    IEnumerator AnimateReload(float reloadtime)
    {
        reloading = true;
        yield return new WaitForSeconds(reloadtime);
        reloading = false;
        currentAmmo = magazineSize;
    }

    protected override void Patrol()
    {
        if (wayPoints.Count > 0)
        {
            // to avoid calculating new path every frame
            if ((!navMeshAgent.hasPath || navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance) && !navMeshAgent.pathPending)
            {
                navMeshAgent.velocity = Vector3.zero;
                navMeshAgent.isStopped = true;
                NextWayPoint();
            }
        }
        else
            currentState = State.idling;
    }

    private void NextWayPoint()
    {
        navMeshAgent.SetDestination(wayPoints[curWaypoint].position);
        navMeshAgent.isStopped = false;
        curWaypoint = (curWaypoint + 1) % wayPoints.Count;
    }

    #endregion

    private void HandleAnimations(int animstate)
    {
        animator.SetInteger("animationState", animstate);
    }

    protected override void Die()
    {
        AudioManager.Instance.PlaySound(Resources.Load<AudioClip>("SFX/spark"), transform.position);
        base.Die();
    }

}
