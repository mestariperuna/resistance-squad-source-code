﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Harvester : Machine
{
    [SerializeField]
    protected int heavyAttackDamage;
    public float zOffset;

    #region Awake&Start

    protected override void Awake()
    {
        base.Awake();
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();

        _dyingPrefab = Resources.Load<GameObject>("Units/DyingHarvester");
    }

    protected override void Start ()
    {
        base.Start();
        currentState = State.idling;
        StartCoroutine(FindTargetsWithDelay(.25f));
    }

    #endregion

    private void Update()
    {
        switch (currentState)   // act accordingly to currentState
        {
            case State.idling:
                CheckSurroundings();
                HandleAnimations(0);
                break;
            case State.combat:
                ChaseTarget();
                HandleAnimations(0);
                break;
            case State.patrolling:
                CheckSurroundings();
                Patrol();
                HandleAnimations(0);
                break;
            case State.moving:
                AtDestination();
                HandleAnimations(0);
                break;
            case State.attacking:
                Attack();
                break;
        }

        CheckRotation();
    }

    private void CheckRotation()
    {

        // BROKEN!.
        Vector3 oldEulerAngles = transform.rotation.eulerAngles;

        if (Mathf.Abs(oldEulerAngles.y - transform.rotation.eulerAngles.y) > 5.0f)
        {
            animator.SetTrigger("Rotate");
            oldEulerAngles = transform.rotation.eulerAngles;
        }
    }

    protected override void Attack()
    {
        if (target.gameObject.activeInHierarchy && InLifeOfSight())
        {
            if (Time.time > nextAttack)
            {
                int attackRandom = Random.Range(1, 10);
                nextAttack = Time.time + timeBetweenAttacks;
                // calculating melee attack damage area
                Vector3 centerPoint = new Vector3(transform.position.x, transform.position.y, transform.position.z + zOffset);
                Collider[] hitColliders = Physics.OverlapSphere(centerPoint, attackRange, targetMask);

                if (hitColliders.Length > 1)
                {
                    attackRandom += 2;
                }

                // Heavy attack with all arms.
                if (attackRandom > 7)
                {
                    animator.SetTrigger("MultipleAttacks");
                    for (int i = 0; i < hitColliders.Length; i++)
                    {
                        hitColliders[i].GetComponent<Selectable>().TakeDamage(heavyAttackDamage, armourPenetration);
                    }
                }

                // Light attack with one arm.
                else
                {
                    animator.SetTrigger("SingleAttack");

                    for (int i = 0; i < hitColliders.Length; i++)
                    {
                        hitColliders[i].GetComponent<Selectable>().TakeDamage(damage.GetValue(), armourPenetration);
                    }
                }
            }
        }
        else
        {
            currentState = State.combat;
        }
    }

    private void HandleAnimations(int animationState)
    {
        animator.SetInteger("animationState", animationState);
    }
}
