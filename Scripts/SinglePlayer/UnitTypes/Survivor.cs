﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Survivor : Interactable
{

    public event System.Action<Survivor> Found;
    public bool female;

    public override void Interact()
    {
        Debug.Log("Interacting with " + objectName);

        Humanoid humanoid = focuser.GetComponent<Humanoid>();
        if (humanoid != null)
        {
            humanoid.StopUnit();

            if (Found != null)
                Found(this);

            gameObject.SetActive(false);
        }
    }

}
