﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Selectable : MonoBehaviour
{

    #region Variables

    public event Action<Selectable> OnDeath;

    public enum State { idling, moving, combat, attacking, patrolling };
    public State currentState { get; set; }

    public Team currentTeam;
    public Sprite Icon { get; set; }
    public Transform eyesTransform;

    [Header("Masks")]
    public LayerMask targetMask;
    public LayerMask obstacleMask;

    [Header("Chase")]
    public float chaseDistance = 5f;
    public float chaseDuration = 5f;
    protected float chaseTimer;

    [Header("Health")]
    public Stat maxHealth;   
    public int CurrentHealth { get; protected set; }
    protected bool dead;

    [Header("Defence")]
    public Stat defence;
    [Range(0, 1)]
    public float damageReductionPercent;

    [Header("Attack range")]
    public float attackRange = 6.0f;
    [Header("ViewRange (Combined with attackRange)")]
    public float viewRange = 3.0f;

    [Header("Level")]
    public int level = 1;
    public int maxLevel = 10;
    public int healthPerLevel;
    public int defencePerLevel;

    public Animator animator { get; set; }
    public NavMeshAgent navMeshAgent { get; set; }
    public Transform target { get; set; }
    
    
    public Vector3 originalPos { get; set; }
    protected ChangeColorByScale healthBar;
    protected Interactable focus;
    protected List<Transform> visibleTargets = new List<Transform>();

    #endregion

    #region Awake&Start

    protected virtual void Awake()
    {
        
    }

    protected virtual void Start()
    {
        dead = false;
        ApplyLevelMultiplier();

        CurrentHealth = maxHealth.GetValue();
        viewRange = attackRange + viewRange;
        originalPos = transform.position;
        healthBar = GetComponentInChildren<ChangeColorByScale>();
    }

    #endregion

    #region FindVisibleTargets

    protected IEnumerator FindTargetsWithDelay(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            FindVisibleTargets();
        }
    }

    protected void FindVisibleTargets()
    {
        visibleTargets.Clear();
        Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, viewRange, targetMask);

        for (int i = 0; i < targetsInViewRadius.Length; i++)
        {
            Transform target = targetsInViewRadius[i].transform;
            Vector3 dirToTarget = (target.position - transform.position).normalized;
            float distToTarget = Vector3.Distance(transform.position, target.position);

            if (!Physics.Raycast(eyesTransform.position, dirToTarget, distToTarget, obstacleMask))
                visibleTargets.Add(target);
        }
    }

    #endregion

    #region StateFunctions

    protected virtual void CheckSurroundings()
    {
        if (visibleTargets.Count > 0)
        {
            target = visibleTargets[0];
            for (int i = visibleTargets.Count - 1; i >= 0; i--)
            {
                // clean the target list
                if (!visibleTargets[i].gameObject.activeInHierarchy)
                    visibleTargets.Remove(visibleTargets[i]);
                else
                {
                    // calculate distance between current target and list target
                    float dist1 = Vector3.Distance(transform.position, target.position);
                    float dist2 = Vector3.Distance(transform.position, visibleTargets[i].position);

                    if (dist1 > dist2) // choose the closest target
                        target = visibleTargets[i];
                }
            }
            originalPos = transform.position;
            currentState = State.combat;
        }
    }

    protected virtual void ChaseTarget()
    {
        // check that the target is alive
        if (!target.gameObject.activeInHierarchy)
            currentState = State.idling;
        else
        {
            if(!InLifeOfSight())  // if target is not in life of sight chase it
            {
                if (navMeshAgent.enabled)
                {
                    navMeshAgent.isStopped = false;
                    chaseTimer += Time.deltaTime;

                    // check the distance and timer, so we don't chase forever
                    if (chaseTimer >= chaseDuration && Vector3.Distance(transform.position, target.position) >= chaseDistance)
                    {
                        // return to position where we started chasing
                        navMeshAgent.SetDestination(originalPos);
                        currentState = State.idling;
                    }
                    else
                        navMeshAgent.SetDestination(target.position);
                }
            }
        }
    }

    protected bool InLifeOfSight()
    {
        Vector3 dirToTarget = (target.position - transform.position).normalized;
        float distToTarget = Vector3.Distance(transform.position, target.position);

        // a raycast to check that the target isn't behind an obstacle
        if (!Physics.Raycast(eyesTransform.position, dirToTarget, distToTarget, obstacleMask) && distToTarget <= attackRange)
        {
            // stop the unit and look at target
            if (navMeshAgent.enabled)
            {
                navMeshAgent.isStopped = true;
                Vector3 lookVector = new Vector3(target.position.x, transform.position.y, target.position.z);
                transform.rotation = Quaternion.LookRotation(lookVector - transform.position);

                currentState = State.attacking;
                chaseTimer = 0;

                return true;
            }
        }
        return false;
    }

    protected virtual void AtDestination()
    {
        if (navMeshAgent.enabled)
        {
            navMeshAgent.isStopped = false;
            if (!navMeshAgent.pathPending)
            {
                if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
                {
                    if (!navMeshAgent.hasPath || navMeshAgent.velocity.sqrMagnitude == 0f)
                        currentState = State.idling;
                }
            }
        }
    }

    protected virtual void Patrol()
    {

    }

    protected virtual void Attack()
    {
        
    }

    public virtual void MoveUnit(Vector3 point)
    {
        if (navMeshAgent.enabled)
            navMeshAgent.SetDestination(point);
        currentState = State.moving;
    }

    public void StopUnit()
    {
        currentState = State.idling;
        if (navMeshAgent.enabled)
            navMeshAgent.isStopped = true;
    }

    public void SetTarget(Transform newTarget)
    {
        target = newTarget;
        currentState = State.combat;
    }

    public void SetFocus(Interactable newFocus)
    {
        if(newFocus != focus)
        {
            if (focus != null)
                focus.OnDefocus();
            focus = newFocus;
        }
        focus.OnFocus(transform);
        MoveUnit(newFocus.transform.position);

    }

    public void RemoveFocus()
    {
        if (focus != null)
            focus.OnDefocus();
        focus = null;
    }

    #endregion

    #region DamagingFunctions
 
    public virtual void TakeDamage(float damageTaken, float armourPenetration)
    {
        if (!dead)
        {
            float damageReduction = defence.GetValue() * damageReductionPercent - armourPenetration;
            int damage = (damageReduction < 1) ? Mathf.RoundToInt(damageTaken * (1 - damageReduction)) : 0;
            CurrentHealth -= damage;

            float scale = Mathf.Clamp01((float)CurrentHealth / maxHealth.GetValue());
            healthBar.SetHealthVisual(scale);

            if (CurrentHealth <= 0)
                Die();
        }
    }

    protected virtual void Die()
    {
        Debug.Log(name + " Dieded");
        dead = true;
        CurrentHealth = 0;

        if (OnDeath != null)
            OnDeath(this);

        gameObject.SetActive(false);
    }

    #endregion


    protected virtual void ApplyLevelMultiplier()
    {
        if (level > maxLevel)
            level = maxLevel;

        defence.AddModifier((level - 1) * defencePerLevel);
        maxHealth.AddModifier((level - 1) * healthPerLevel);
    }

}
