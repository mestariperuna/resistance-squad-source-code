﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

//[ExecuteInEditMode]
public class UIElementSizeCalculator : MonoBehaviour {

    public float width;
    public float height;

    public List<string> values;
    string path = "Assets/Resources/test.txt";

    // Use this for initialization
    void Start () {

        values = new List<string>();
        width = GetComponent<RectTransform>().rect.width;
        height = GetComponent<RectTransform>().rect.height;

        StreamWriter writer = new StreamWriter(path, true);

        foreach (RectTransform g in gameObject.GetComponentsInChildren<RectTransform>())
        {
            if(g != null)
            {
                values.Add(g.name + " width: " + g.GetComponent<RectTransform>().rect.width.ToString() + " height: " +
                    g.GetComponent<RectTransform>().rect.height.ToString());
            }

            
            
        }
        writer.Close();

    }
    // Update is called once per frame
    void Update () {
		
	}
}
