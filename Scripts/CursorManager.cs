﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CursorManager
{
    
    // Set cursor image from resources folder.
    public static void SetCursorImage(Texture2D texture)
    {
        Cursor.SetCursor(texture, Vector2.zero, CursorMode.Auto);
    }
}
