﻿using UnityEngine;
using System.Collections;
//[ExecuteInEditMode]
public class DayNightCycle : MonoBehaviour
{
    public float cycleSpeed = 0.0f;

    void Update()
    {
        transform.RotateAround(Vector3.zero, (Vector3.right + Vector3.forward * 2), cycleSpeed * Time.deltaTime);
        transform.LookAt(Vector3.zero);
    }
}